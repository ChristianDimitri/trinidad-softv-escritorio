<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frmoxxo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBPrefijo_ProveedorLabel As System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Prefijo_ProveedorTextBox = New System.Windows.Forms.TextBox()
        Me.CONGeneralesOxxoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.CONGeneralesOxxoTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONGeneralesOxxoTableAdapter()
        Me.GUARDAGeneralesOxxoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDAGeneralesOxxoTableAdapter = New sofTV.DataSetEDGARTableAdapters.GUARDAGeneralesOxxoTableAdapter()
        CMBPrefijo_ProveedorLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.CONGeneralesOxxoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDAGeneralesOxxoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBPrefijo_ProveedorLabel
        '
        CMBPrefijo_ProveedorLabel.AutoSize = True
        CMBPrefijo_ProveedorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBPrefijo_ProveedorLabel.Location = New System.Drawing.Point(13, 39)
        CMBPrefijo_ProveedorLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        CMBPrefijo_ProveedorLabel.Name = "CMBPrefijo_ProveedorLabel"
        CMBPrefijo_ProveedorLabel.Size = New System.Drawing.Size(146, 15)
        CMBPrefijo_ProveedorLabel.TabIndex = 0
        CMBPrefijo_ProveedorLabel.Text = "Prefijo del Proveedor:"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(200, 265)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(105, 33)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(89, 265)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(105, 33)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&GUARDAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(CMBPrefijo_ProveedorLabel)
        Me.Panel1.Controls.Add(Me.Prefijo_ProveedorTextBox)
        Me.Panel1.Location = New System.Drawing.Point(9, 10)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(296, 239)
        Me.Panel1.TabIndex = 4
        '
        'Prefijo_ProveedorTextBox
        '
        Me.Prefijo_ProveedorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Prefijo_ProveedorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralesOxxoBindingSource, "Prefijo_Proveedor", True))
        Me.Prefijo_ProveedorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prefijo_ProveedorTextBox.Location = New System.Drawing.Point(163, 37)
        Me.Prefijo_ProveedorTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.Prefijo_ProveedorTextBox.MaxLength = 2
        Me.Prefijo_ProveedorTextBox.Name = "Prefijo_ProveedorTextBox"
        Me.Prefijo_ProveedorTextBox.Size = New System.Drawing.Size(37, 21)
        Me.Prefijo_ProveedorTextBox.TabIndex = 0
        '
        'CONGeneralesOxxoBindingSource
        '
        Me.CONGeneralesOxxoBindingSource.DataMember = "CONGeneralesOxxo"
        Me.CONGeneralesOxxoBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONGeneralesOxxoTableAdapter
        '
        Me.CONGeneralesOxxoTableAdapter.ClearBeforeFill = True
        '
        'GUARDAGeneralesOxxoBindingSource
        '
        Me.GUARDAGeneralesOxxoBindingSource.DataMember = "GUARDAGeneralesOxxo"
        Me.GUARDAGeneralesOxxoBindingSource.DataSource = Me.DataSetEDGAR
        '
        'GUARDAGeneralesOxxoTableAdapter
        '
        Me.GUARDAGeneralesOxxoTableAdapter.ClearBeforeFill = True
        '
        'Frmoxxo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(320, 317)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Frmoxxo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales de cobros en el OXXO"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONGeneralesOxxoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDAGeneralesOxxoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents CONGeneralesOxxoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONGeneralesOxxoTableAdapter As sofTV.DataSetEDGARTableAdapters.CONGeneralesOxxoTableAdapter
    Friend WithEvents Prefijo_ProveedorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GUARDAGeneralesOxxoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDAGeneralesOxxoTableAdapter As sofTV.DataSetEDGARTableAdapters.GUARDAGeneralesOxxoTableAdapter
End Class
