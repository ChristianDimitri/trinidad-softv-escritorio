﻿Imports System.Data
Imports System.Data.SqlClient
Imports sofTV.Base

Public Class FrmSeriesFolio

    Public serie As String, folios As Integer
    Dim consulta As New CBase

    Private Sub FrmSeriesFolio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LlenaCombo()

    End Sub

    Private Sub LlenaCombo()
        Dim dt As DataTable = consulta.consultarDT("SP_SerieFolio", Nothing)

        If dt.Rows.Count > 0 Then
            Cmb_serie.DataSource = dt
            Cmb_serie.DisplayMember = "Serie"
            Cmb_serie.ValueMember = "Clave"
        End If

    End Sub

    Private Sub InsertarSerie(ByVal vserie As String, ByVal vfolios As Integer)
        Try
            Using conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim cmm As New SqlCommand()
                cmm.Connection = conexion
                cmm.CommandText = "SP_InsertarSerieFolio"
                cmm.CommandType = CommandType.StoredProcedure

                cmm.Parameters.Add("@serie", SqlDbType.NVarChar, 150)
                cmm.Parameters.Add("@folio", SqlDbType.Int)

                cmm.Parameters("@serie").Value = vserie
                cmm.Parameters("@folio").Value = vfolios

                cmm.ExecuteNonQuery()
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub InsertFolio(ByVal series As String, ByVal folios As Integer)
        Try
            Using conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim cmm As New SqlCommand()
                cmm.Connection = conexion
                cmm.CommandText = "SpInsertNewTbl_Folios"
                cmm.CommandType = CommandType.StoredProcedure

                cmm.Parameters.Add("@serie", SqlDbType.NVarChar, 150)
                cmm.Parameters.Add("@folio", SqlDbType.Int)

                cmm.Parameters("@serie").Value = series
                cmm.Parameters("@folio").Value = folios

                cmm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            MsgBox(ex)
        End Try
    End Sub

    Private Sub Btn_Imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Imprimir.Click
        If Cmb_serie.SelectedValue <> 0 And IsNumeric(Txt_Serie.Text) Then
            serie = Cmb_serie.GetItemText(Cmb_serie.SelectedItem).ToString()
            folios = Convert.ToInt32(Txt_Serie.Text.ToString())

            InsertFolio(serie, folios)

            InsertarSerie(serie, folios)

            'REPORTEPreContrato(0, serie)

            LocGloOpRep = 20
            GloSeries = serie

            FrmImprimirFac.Show()
        Else
            MsgBox("Verifique que la cantidad de folios sea numérico o seleccione una Dosificación", MsgBoxStyle.Information)
        End If

        LocGloOpRep = 0
        Me.Close()
    End Sub

    Private Sub Btn_Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub

    Private Sub REPORTEPreContrato(ByVal OP As Integer, ByVal SERIE As String)
        Dim dTable As New DataTable
        Dim rDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, SERIE, 50)
        dTable = BaseII.ConsultaDT("REPORTEPreContrato")

        rDocument = New CrystalDecisions.CrystalReports.Engine.ReportDocument
        rDocument.Load(RutaReportes + "\REPORTEPreContrato.rpt")
        rDocument.SetDataSource(dTable)

        If dTable.Rows.Count = 0 Then Exit Sub

        MessageBox.Show("Se imprimirá la parte frontal de " + dTable.Rows.Count.ToString + " precontratos.", "Atención", MessageBoxButtons.OK)

        rDocument.PrintOptions.PrinterName = IMPRESORA_CONTRATOS
        rDocument.PrintToPrinter(1, False, 0, 0)

        MessageBox.Show("Se imprimirá la parte posterior de " + dTable.Rows.Count.ToString + " precontratos.", "Atención", MessageBoxButtons.OK)

        rDocument = New CrystalDecisions.CrystalReports.Engine.ReportDocument
        rDocument.Load(RutaReportes + "\REPORTEPreContratoPosterior.rpt")
        rDocument.PrintOptions.PrinterName = IMPRESORA_CONTRATOS

        For Each e As DataRow In dTable.Rows
            rDocument.PrintToPrinter(1, False, 0, 0)
        Next


    End Sub

End Class