Imports System.Data.SqlClient
Imports System.Text
Public Class FrmEncuestas

    Dim ClvSession As Long = 0
    Dim IDPreguntaIzq As Integer = 0
    Dim IDPreguntaDer As Integer = 0

    Private Sub DameClvSession()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            ClvSession = CLng(parametro.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ConEncuestas(ByVal IDEncuesta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConEncuestas ")
        strSQL.Append(CStr(IDEncuesta))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try
            dataAdapter.Fill(dataTable)
            TextBoxNombre.Text = dataTable.Rows(0)(0).ToString()
            TextBoxDescripcion.Text = dataTable.Rows(0)(1).ToString()
            'dataTable.Rows(0)(2).ToString()
            CheckBoxActiva.Checked = Boolean.Parse(dataTable.Rows(0)(3).ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub NueEncuestas(ByVal Nombre As String, ByVal Descripcion As String, ByVal Activa As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueEncuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Nombre", SqlDbType.VarChar, 50)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Nombre
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Descripcion
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Activa", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Activa
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@IDEncuesta", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eIDEncuesta = CInt(parametro4.Value.ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ModEncuestas(ByVal Nombre As String, ByVal Descripcion As String, ByVal Activa As Boolean, ByVal IDEncuesta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModEncuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Nombre", SqlDbType.VarChar, 50)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Nombre
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Descripcion
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Activa", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Activa
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@IDEncuesta", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = IDEncuesta
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorEncuestas(ByVal IDEncuesta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorEncuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDEncuesta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDEncuesta
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConTmpDetEncuestasIzq(ByVal ClvSession As Long, ByVal IDEncuesta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConTmpDetEncuestasIzq ")
        strSQL.Append(CStr(ClvSession) & ", ")
        strSQL.Append(CStr(IDEncuesta) & ", ")
        strSQL.Append(CStr(Op))

        Dim x As Integer = 0
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try
            
            dataAdapter.Fill(DataTable)
            TreeViewIzq.Nodes.Clear()

            For Each Row As DataRow In DataTable.Rows
                TreeViewIzq.Nodes.Add(Row("Pregunta").ToString())
                TreeViewIzq.Nodes(x).Tag = Row("IDPregunta").ToString()
                x += 1
            Next

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConTmpDetEncuestasDer(ByVal ClvSession As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConTmpDetEncuestasDer ")
        strSQL.Append(CStr(ClvSession))

        Dim x As Integer = 0
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try
            dataAdapter.Fill(dataTable)
            TreeViewDer.Nodes.Clear()

            For Each Row As DataRow In dataTable.Rows
                TreeViewDer.Nodes.Add(Row("Pregunta").ToString())
                TreeViewDer.Nodes(x).Tag = Row("IDPregunta").ToString()
                x += 1
            Next
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub InsertaTmpDetEncuestas(ByVal ClvSession As Long, ByVal IDPregunta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaTmpDetEncuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = IDPregunta
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Op", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Op
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub EliminaTmpDetEncuestas(ByVal ClvSession As Long, ByVal IDPregunta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("EliminaTmpDetEncuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = IDPregunta
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Op", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Op
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub GrabaDetEncuestas(ByVal ClvSession As Long, ByVal IDEncuesta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GrabaDetEncuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@IDEncuesta", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = IDEncuesta
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Limpiar()
        TextBoxNombre.Clear()
        TextBoxDescripcion.Clear()
        CheckBoxActiva.Checked = False
        TreeViewIzq.Nodes.Clear()
        TreeViewDer.Nodes.Clear()
    End Sub

    Private Sub TreeViewIzq_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewIzq.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            IDPreguntaIzq = e.Node.Tag
        End If
    End Sub

    Private Sub TreeViewDer_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewDer.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            IDPreguntaDer = e.Node.Tag
        End If
    End Sub

    Private Sub FrmEncuestas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        DameClvSession()

        If eOpcion = "N" Then
            TSBNuevo.Enabled = False
            TSBEliminar.Enabled = False
            CheckBoxActiva.Checked = True
        ElseIf eOpcion = "C" Then
            ConEncuestas(eIDEncuesta)
            BindingNavigatorEncuestas.Enabled = False
            ButtonInsertarUno.Enabled = False
            ButtonInsertarTodos.Enabled = False
            ButtonEliminarUno.Enabled = False
            ButtonEliminarTodos.Enabled = False
        ElseIf eOpcion = "M" Then
            ConEncuestas(eIDEncuesta)
        End If

        ConTmpDetEncuestasIzq(ClvSession, eIDEncuesta, 0)
        ConTmpDetEncuestasDer(ClvSession)
        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub ButtonInsertarUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonInsertarUno.Click
        InsertaTmpDetEncuestas(ClvSession, IDPreguntaIzq, 0)
        ConTmpDetEncuestasIzq(ClvSession, eIDEncuesta, 1)
        ConTmpDetEncuestasDer(ClvSession)
    End Sub

    Private Sub ButtonInsertarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonInsertarTodos.Click
        InsertaTmpDetEncuestas(ClvSession, 0, 1)
        ConTmpDetEncuestasIzq(ClvSession, eIDEncuesta, 1)
        ConTmpDetEncuestasDer(ClvSession)
    End Sub

    Private Sub ButtonEliminarUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminarUno.Click
        EliminaTmpDetEncuestas(ClvSession, IDPreguntaDer, 0)
        ConTmpDetEncuestasIzq(ClvSession, eIDEncuesta, 1)
        ConTmpDetEncuestasDer(ClvSession)
    End Sub

    Private Sub ButtonEliminarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminarTodos.Click
        EliminaTmpDetEncuestas(ClvSession, 0, 1)
        ConTmpDetEncuestasIzq(ClvSession, eIDEncuesta, 1)
        ConTmpDetEncuestasDer(ClvSession)
    End Sub

    Private Sub TSBNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBNuevo.Click
        DameClvSession()
        eOpcion = "N"
        eIDEncuesta = 0
        TSBNuevo.Enabled = False
        TSBEliminar.Enabled = False
        Limpiar()
        ConTmpDetEncuestasIzq(ClvSession, eIDEncuesta, 0)
        ConTmpDetEncuestasDer(ClvSession)
    End Sub

    Private Sub TSBGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBGuardar.Click

        If TextBoxNombre.Text.Length = 0 Then
            MsgBox("Teclea un nombre.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If TreeViewDer.Nodes.Count = 0 Then
            MsgBox("No hay preguntas asignadas.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If eOpcion = "N" Then
            TSBNuevo.Enabled = True
            TSBEliminar.Enabled = True
            NueEncuestas(TextBoxNombre.Text, TextBoxDescripcion.Text, CheckBoxActiva.Checked)
            eOpcion = "M"
        ElseIf eOpcion = "M" Then
            ModEncuestas(TextBoxNombre.Text, TextBoxDescripcion.Text, CheckBoxActiva.Checked, eIDEncuesta)
        End If
        GrabaDetEncuestas(ClvSession, eIDEncuesta)
        MsgBox(mensaje5, MsgBoxStyle.Information)
        eBnd = True
    End Sub

    Private Sub TSBEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBEliminar.Click
        If ValidaEliminarEncuestas(eIDEncuesta).Length <> 0 Then
            MsgBox(ValidaEliminarEncuestas(eIDEncuesta))
            Exit Sub
        End If
        BorEncuestas(eIDEncuesta)
        MsgBox(mensaje6, MsgBoxStyle.Information)
        eBnd = True
        Me.Close()
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Function ValidaEliminarEncuestas(ByVal IDEncuesta As Integer) As String
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaEliminarEncuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDEncuesta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDEncuesta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Return parametro3.Value.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Function

    Private Sub TextBoxNombre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxNombre.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBoxNombre, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub TextBoxDescripcion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxDescripcion.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBoxDescripcion, Asc(LCase(e.KeyChar)), "S")))
    End Sub
End Class