﻿
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmTrabajosTecnico
    Dim FechaTrabajosTecnicos As String


    Private Sub FrmTrabajosTecnico_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.DateTimePicker1.Text = FechaAgenda
    End Sub


    Private Sub FrmTrabajosTecnico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.DateTimePicker1.Text = FechaAgenda

        CMBNombre.Text = GLONOM_TECNICO
        'FechaAgenda = 


        If OpcionQuejaOrden = "Queja" Then

            TrabajosQuejas(FechaAgenda, GloClv_tecnico)
        Else
            TrabajosOrdenes(FechaAgenda, GloClv_tecnico)
        End If

    End Sub
    Private Sub TrabajosOrdenes(ByVal Fecha As DateTime, ByVal GloClv_tecnico As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC TrabajosOrdenesTecnicoDia ")
        StrSQL.Append("'" & Fecha & "',")
        StrSQL.Append(CStr(GloClv_tecnico))

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource

            If DT.Rows.Count > 0 Then
                CMBQ.Text = DT.Rows(0)("TOTALQUEJAS").ToString()
                CMBO.Text = DT.Rows(0)("TOTALORDENES").ToString()
            End If
            'Label2.Text = Me.DataGridView1.SelectedCells(11).Value
            'Label4.Text = Me.DataGridView1.SelectedCells(10).Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub TrabajosQuejas(ByVal Fecha As DateTime, ByVal GloClv_tecnico As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC TrabajosQuejasTecnicoDia ")
        StrSQL.Append("'" & Fecha & "',")
        StrSQL.Append(CStr(GloClv_tecnico))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource

            If DT.Rows.Count > 0 Then
                CMBQ.Text = DT.Rows(0)("TOTALQUEJAS").ToString()
                CMBO.Text = DT.Rows(0)("TOTALORDENES").ToString()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        FechaTrabajosTecnicos = Me.DateTimePicker1.Text
        If FechaTrabajosTecnicos = "" Then
            Me.DateTimePicker1.Text = FechaAgenda
        Else
            FechaAgenda = Me.DateTimePicker1.Text
        End If
        If OpcionQuejaOrden = "Queja" Then

            TrabajosQuejas(FechaAgenda, GloClv_tecnico)
        Else
            TrabajosOrdenes(FechaAgenda, GloClv_tecnico)
        End If
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel5.Click

    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBQ.Click

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class