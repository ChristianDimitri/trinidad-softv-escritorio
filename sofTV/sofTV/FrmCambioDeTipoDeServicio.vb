﻿Imports System.Collections.Generic
Public Class FrmCambioDeTipoDeServicio

    Private Sub LLENA_cajas()

        Dim dTable As New DataTable
        Dim i As Integer = 0
        BaseII.limpiaParametros()
        Me.ComboBox1.DataSource = BaseII.ConsultaDT("SP_MUESTRATipoCaja")
        Me.ComboBox1.ValueMember = "NOARTICULO"
        Me.ComboBox1.DisplayMember = "CAJA"

        'DataGridView1.Columns.Item(7).Visible = False
    End Sub

    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        dTable = BaseII.ConsultaDT("MuestraServiciosEric")
        If Clv_TipSer = 1 Then
            cbServicioTv.DataSource = dTable
        Else
            cbServicioDig.DataSource = dTable
        End If
    End Sub

    Private Sub MUESTRAClientesCambioDeTipoDeServicio(ByVal Op As Integer, ByVal Clv_TipSer As Integer, ByVal Contrato As Integer, ByVal Nombre As String)
        Dim dSet As New DataSet

        Dim l As New List(Of String)
        l.Add("Consulta")
        l.Add("Mensaje")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre, 250)
        dSet = BaseII.ConsultaDS("MUESTRAClientesCambioDeTipoDeServicio", l)


        eClv_Servicio = 0
        eClv_Servicio = dSet.Tables("Consulta").Rows(0)(2).ToString
        eMsj = ""
        eMsj = dSet.Tables("Mensaje").Rows(0)(0).ToString


        gbServicioDig.Enabled = False
        gbServicioTv.Enabled = False

        If eMsj.Length = 0 Then
            If cbDeTvADig.Checked = True Then
                gbServicioDig.Enabled = True
            Else
                gbServicioTv.Enabled = True
            End If
            bnGuardar.Enabled = True
        Else
            MessageBox.Show(eMsj)
            Limpiar()
        End If

    End Sub

    Private Sub NUECambioDeTipoDeServicio(ByVal Contrato As Integer, ByVal Clv_Usuario As String, ByVal DeTvADig As Boolean, ByVal DeDigATv As Boolean, ByVal Clv_ServicioTV As Integer, ByVal TvSinPago As Integer, ByVal TvConPago As Integer, ByVal Clv_ServicioDig As Integer, ByVal NumeroDeCajas As Integer, ByVal ExtensionesAnalogas As Integer, ByVal OCLVSESSION As Long, ByVal oModelo As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, Clv_Usuario, 10)
        BaseII.CreateMyParameter("@DeTvADig", SqlDbType.Bit, DeTvADig)
        BaseII.CreateMyParameter("@DeDigATv", SqlDbType.Bit, DeDigATv)
        BaseII.CreateMyParameter("@Clv_ServicioTV", SqlDbType.Int, Clv_ServicioTV)
        BaseII.CreateMyParameter("@TvSinPago", SqlDbType.Int, TvSinPago)
        BaseII.CreateMyParameter("@TvConPago", SqlDbType.Int, TvConPago)
        BaseII.CreateMyParameter("@Clv_ServicioDig", SqlDbType.Int, Clv_ServicioDig)
        BaseII.CreateMyParameter("@NumeroDeCajas", SqlDbType.Int, NumeroDeCajas)
        BaseII.CreateMyParameter("@ExtensionesAnalogas", SqlDbType.Int, ExtensionesAnalogas)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, OCLVSESSION)
        BaseII.CreateMyParameter("@ModeloCaja", SqlDbType.Int, oModelo)
        BaseII.ProcedimientoOutPut("NUECambioDeTipoDeServicio")
        eMsj = ""
        eMsj = BaseII.dicoPar("@Msj").ToString

        eRefrescar = True
        MessageBox.Show(eMsj)
        Limpiar()

    End Sub

    Private Sub Limpiar()
        eContrato = 0
        eClv_TipSer = 1
        eClv_Servicio = 0
        tbContrato.Clear()
        nudCajas.Value = 1
        nudExtensiones.Value = 0
        nudTvSinPago.Value = 0
        nudTvConPago.Value = 0
        gbServicioDig.Enabled = False
        gbServicioTv.Enabled = False
        bnGuardar.Enabled = False
        cbDeTvADig.Checked = True
    End Sub

    Private Sub FrmCambioDeTipoDeServicio_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmCambioDeTipoDeServicio_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Limpiar()
        MuestraServiciosEric(1, 0, 0)
        MuestraServiciosEric(3, 0, 0)
        LLENA_cajas()
    End Sub

    Private Sub bnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnBuscar.Click
        eContrato = 0
        Dim FrmMuestaCambio As New FrmMUESTRAClientesCambioDeTipoDeServicio
        'FrmMUESTRAClientesCambioDeTipoDeServicio.Show()
        FrmMuestaCambio.ShowDialog()
        If eContrato > 0 Then
            tbContrato.Text = eContrato
            eContrato = 0
            DameClv_Session()
            If cbDeTvADig.Checked = True Then
                MUESTRAClientesCambioDeTipoDeServicio(1, 1, tbContrato.Text, "")
                If IsNumeric(tbContrato.Text) = True Then
                    DameCuantasAnalogas()
                End If
            Else
                MUESTRAClientesCambioDeTipoDeServicio(1, 3, tbContrato.Text, "")
            End If
        End If
        If tbAnalogas.Text.Length > 0 Then
            If nudCajas.Value < CInt(tbAnalogas.Text) Then
                If (CInt(tbAnalogas.Text) - nudCajas.Value) <= 3 Then
                    nudExtensiones.Value = (CInt(tbAnalogas.Text) - nudCajas.Value)
                Else
                    nudExtensiones.Value = 3
                End If
            Else
                nudExtensiones.Value = 0
            End If
        End If
    End Sub

    Private Sub bnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGuardar.Click

        If cbDeTvADig.Checked = True Then
            'If ComboBox1.SelectedIndex >= 1 Then
            '    LocNoArticuloCajas = ComboBox1.SelectedValue

            'Else
            '    LocNoArticuloCajas = 0
            '    MsgBox("Seleccione un modelo de caja por favor ", MsgBoxStyle.Information, "Importante")
            '    Exit Sub
            'End If
            ValidaCajasCambtipserv()
            If Len(eMsj) > 0 Then
                MsgBox(eMsj, MsgBoxStyle.Information)
                Exit Sub
            End If

            If (nudCajas.Value + nudExtensiones.Value) > 4 Then
                MsgBox("El cliente solo puede tener un maximo de 4tvs")
                Exit Sub
            End If

            Dim reSPUESTA As MsgBoxResult = MsgBoxResult.No
            reSPUESTA = MsgBox(" Este proceso realizara varias afectaciones y si se procesa no habrá marcha atrás. ¿ Deseas realizar el proceso ahora ?", MsgBoxStyle.YesNo, "Importante")
            If reSPUESTA = vbYes Then
                NUECambioDeTipoDeServicio(tbContrato.Text, GloUsuario, cbDeTvADig.Checked, cbDeDigATv.Checked, eClv_Servicio, 0, 0, cbServicioDig.SelectedValue, nudCajas.Value, nudExtensiones.Value, eClv_Session, LocNoArticuloCajas)
                Me.Close()
            End If
        Else

            BndCambioServicio = "C"
            eContrato = tbContrato.Text
            BndValRecon = 0
            Dim frmaparatosrecibir As New FormAparatosRecibir
            frmaparatosrecibir.ShowDialog()
            BndCambioServicio = "R"

            If BndValRecon = 1 Then 'Autorizados si no nel
                Dim resul As String = SP_VALIDA_APARATOS_RECON(eClv_Session, BndCambioServicio)
                If resul.Length > 0 Then
                    MsgBox(resul)
                    BndValRecon = 0
                Else
                    BndValRecon = 1 'Autorizados si no nel
                    NUECambioDeTipoDeServicio(tbContrato.Text, GloUsuario, cbDeTvADig.Checked, cbDeDigATv.Checked, cbServicioTv.SelectedValue, nudTvSinPago.Value, nudTvConPago.Value, eClv_Servicio, 0, 0, eClv_Session, 0)
                    Me.Close()
                End If

            End If
        End If


    End Sub

    Private Sub ValidaCajasCambtipserv()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@NumCajas", SqlDbType.Int, nudCajas.Value)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, CInt(tbContrato.Text))
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@msj", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.ProcedimientoOutPut("ValidaCajasCambtipserv")
        eMsj = ""
        eMsj = BaseII.dicoPar("@msj").ToString

    End Sub

    Private Sub DameCuantasAnalogas()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, CInt(tbContrato.Text))
        BaseII.CreateMyParameter("@Tvs", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameCuantasAnalogas")
        tbAnalogas.Text = BaseII.dicoPar("@Tvs").ToString

    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Dim reSPUESTA2 As MsgBoxResult = MsgBoxResult.No
        reSPUESTA2 = MsgBox("Se perderan las configuraciones realizadas hasta este momento, ¿Desea salir?", MsgBoxStyle.YesNo, "Importante")
        If reSPUESTA2 = vbYes Then
            Borra_Tbl_CambtipservPropio()
        End If
        Me.Close()
    End Sub

    Private Sub cbDeTvADig_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDeTvADig.CheckedChanged
        eClv_TipSer = 1
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        If cbDeTvADig.Checked = True Then
            MUESTRAClientesCambioDeTipoDeServicio(1, 1, tbContrato.Text, "")
        Else
            MUESTRAClientesCambioDeTipoDeServicio(1, 3, tbContrato.Text, "")
        End If
    End Sub

    Private Sub cbDeDigATv_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDeDigATv.CheckedChanged
        eClv_TipSer = 3
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        If cbDeTvADig.Checked = True Then
            MUESTRAClientesCambioDeTipoDeServicio(1, 1, tbContrato.Text, "")
        Else
            MUESTRAClientesCambioDeTipoDeServicio(1, 3, tbContrato.Text, "")
        End If
    End Sub

    Private Sub tbContrato_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        DameClv_Session()
        If cbDeTvADig.Checked = True Then
            MUESTRAClientesCambioDeTipoDeServicio(1, 1, tbContrato.Text, "")
            If IsNumeric(tbContrato.Text) = True Then
                DameCuantasAnalogas()
            End If
        Else
            MUESTRAClientesCambioDeTipoDeServicio(1, 3, tbContrato.Text, "")
        End If

    End Sub

    Private Sub tbContrato_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbContrato.TextChanged

    End Sub

    Private Sub nudCajas_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudCajas.ValueChanged
        Llena_Cajas_Camtipserv()
        If tbAnalogas.Text.Length > 0 Then
            If nudCajas.Value < CInt(tbAnalogas.Text) Then
                If (CInt(tbAnalogas.Text) - nudCajas.Value) <= 3 Then
                    nudExtensiones.Value = (CInt(tbAnalogas.Text) - nudCajas.Value)
                Else
                    nudExtensiones.Value = 3
                End If
            Else
                nudExtensiones.Value = 0
            End If

        End If
    End Sub

    Private Sub Llena_Cajas_Camtipserv()

        Dim dTable As New DataTable
        Dim i As Integer = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@NumCajas", SqlDbType.BigInt, nudCajas.Value)
        Me.ComboBox2.DataSource = BaseII.ConsultaDT("Llena_Cajas_Camtipserv")
        Me.ComboBox2.ValueMember = "id"
        Me.ComboBox2.DisplayMember = "caja"

        
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Label59.Visible = True
            CheckBox3.Visible = True
            CheckBox4.Visible = True
            Label57.Visible = True
            Label58.Visible = True
            TextBox31.Visible = True
            TextBox35.Visible = True

            Label1.Visible = False
            ComboBox1.Visible = False
        ElseIf CheckBox1.Checked = False Then
            Label59.Visible = False
            CheckBox3.Visible = False
            CheckBox4.Visible = False
            Label57.Visible = False
            Label58.Visible = False
            TextBox31.Visible = False
            TextBox35.Visible = False

            Label1.Visible = True
            ComboBox1.Visible = True
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            TextBox31.Enabled = True
            Label1.Visible = False
            ComboBox1.Visible = False
        ElseIf CheckBox3.Checked = False Then
            TextBox31.Enabled = False
        End If

        If CheckBox4.Checked = True And CheckBox3.Checked = False Then
            Label1.Visible = True
            ComboBox1.Visible = True
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.Checked = True Then
            TextBox35.Enabled = True
        ElseIf CheckBox4.Checked = False Then
            TextBox35.Enabled = False
            Label1.Visible = False
            ComboBox1.Visible = False
        End If

        If CheckBox4.Checked = True And CheckBox3.Checked = False Then
            Label1.Visible = True
            ComboBox1.Visible = True
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If cbServicioDig.SelectedValue > 0 Then
            If CheckBox1.Checked = False Then 'GloCajaPropia = 0 
                If ComboBox1.SelectedIndex >= 1 Then
                    LocNoArticuloCajas = ComboBox1.SelectedValue
                    GloClvCajaDig = 0
                    GloClvTarjDig = 0
                    Guarda_Tbl_CambtipservPropio()
                    MsgBox("Guardado Correctamente", MsgBoxStyle.Information)
                Else
                    LocNoArticuloCajas = 0
                    MsgBox("Seleccione un modelo de STB por favor ", MsgBoxStyle.Information, "Importante")
                End If
            ElseIf CheckBox1.Checked = True Then 'GloCajaPropia = 1
                If CheckBox3.Checked = True And CheckBox4.Checked = False Then
                    GloCajaOTarjeta = 1
                ElseIf CheckBox3.Checked = False And CheckBox4.Checked = True Then
                    GloCajaOTarjeta = 2
                ElseIf CheckBox3.Checked = True And CheckBox4.Checked = True Then
                    GloCajaOTarjeta = 3
                End If

                If GloCajaOTarjeta = 1 Then
                    'If IsNumeric(TextBox31.Text) Then
                    ValidaSerieCajaBaja()
                    If GloCajaenBaja = 1 Then
                        LocNoArticuloCajas = 0
                        GloClvCajaDig = TextBox31.Text
                        GloClvTarjDig = 0
                        Guarda_Tbl_CambtipservPropio()
                        MsgBox("Guardado Correctamente", MsgBoxStyle.Information)
                    Else
                        MsgBox("La serie que capturo no existe o se encuentra instalada actualmente", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'Else
                    ' MsgBox("El numero de serie de la caja es incorrecto", MsgBoxStyle.Information)
                    ' End If
                ElseIf GloCajaOTarjeta = 2 Then
                    'If IsNumeric(TextBox35.Text) Then
                    If ComboBox1.SelectedValue = 0 Then
                        MsgBox("Debe Seleccionar la Marca del STB antes de continuar", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    ValidaSerieCajaBaja()
                    If GloCajaenBaja = 1 Then
                        LocNoArticuloCajas = ComboBox1.SelectedValue
                        GloClvTarjDig = TextBox35.Text
                        GloClvCajaDig = 0
                        Guarda_Tbl_CambtipservPropio()
                        MsgBox("Guardado Correctamente", MsgBoxStyle.Information)
                    Else
                        MsgBox("La serie que capturo no existe o se encuentra instalada actualmente", MsgBoxStyle.Information)
                        GloGuardarDig = False
                        Exit Sub
                    End If
                    'Else
                    ' MsgBox("El numero de serie de la Tarjeta es incorrecto", MsgBoxStyle.Information)
                    'End If
                ElseIf GloCajaOTarjeta = 3 Then
                    'If IsNumeric(TextBox31.Text) And IsNumeric(TextBox35.Text) Then
                    ValidaSerieCajaBaja()
                    If GloCajaenBaja = 1 Then
                        LocNoArticuloCajas = 0
                        GloClvCajaDig = TextBox31.Text
                        GloClvTarjDig = TextBox35.Text
                        Guarda_Tbl_CambtipservPropio()
                        MsgBox("Guardado Correctamente", MsgBoxStyle.Information)
                    Else
                        MsgBox("La serie que capturo no existe o se encuentra instalada actualmente", MsgBoxStyle.Information)
                        GloGuardarDig = False
                        Exit Sub
                    End If
                    'Else
                    'MsgBox("El numero de serie de la caja o la tarjeta es incorrecto", MsgBoxStyle.Information)
                    'End If
                End If
            End If
        Else
            MsgBox("Seleccione un Servicio por favor!", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub
    Private Sub Consulta_Tbl_CambtipservPropio()
        Dim dtable As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@Caja", SqlDbType.Int, ComboBox2.SelectedValue)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, tbContrato.Text)
        dtable = BaseII.ConsultaDT("Consulta_Tbl_CambtipservPropio")

        CheckBox1.Checked = dtable.Rows(0)(0).ToString
        ComboBox1.SelectedValue = dtable.Rows(0)(1).ToString
        TextBox31.Text = dtable.Rows(0)(2).ToString
        If Len(TextBox31.Text) > 0 Then
            If TextBox31.Text <> "0" Then
                CheckBox3.Checked = True
            End If
        End If
        TextBox35.Text = dtable.Rows(0)(3).ToString
        If Len(TextBox35.Text) > 0 Then
            If TextBox35.Text <> "0" Then
                CheckBox4.Checked = True
            End If
        End If
        cbServicioDig.SelectedValue = dtable.Rows(0)(4).ToString

    End Sub

    Private Sub Guarda_Tbl_CambtipservPropio()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@Caja ", SqlDbType.Int, ComboBox2.SelectedValue)
        BaseII.CreateMyParameter("@CajaPropia ", SqlDbType.Bit, CheckBox1.Checked)
        BaseII.CreateMyParameter("@ModeloCaja ", SqlDbType.Int, LocNoArticuloCajas)
        BaseII.CreateMyParameter("@MacCaja ", SqlDbType.VarChar, GloClvCajaDig, 50)
        BaseII.CreateMyParameter("@MacTarjeta ", SqlDbType.VarChar, GloClvTarjDig, 50)
        BaseII.CreateMyParameter("@Clv_Servicio ", SqlDbType.Int, cbServicioDig.SelectedValue)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, tbContrato.Text)
        BaseII.Inserta("Guarda_Tbl_CambtipservPropio")
    End Sub

    Private Sub Borra_Tbl_CambtipservPropio()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, eClv_Session)
        BaseII.Inserta("Borra_Tbl_CambtipservPropio")
    End Sub

    Private Sub ValidaSerieCajaBaja()
        If TextBox31.Text = "" Then
            TextBox31.Text = 0
        End If
        If TextBox35.Text = "" Then
            TextBox35.Text = 0
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CajaOTarj", SqlDbType.Int, GloCajaOTarjeta)
        BaseII.CreateMyParameter("@SerieCaja", SqlDbType.VarChar, TextBox31.Text, 50)
        BaseII.CreateMyParameter("@SerieTarj", SqlDbType.VarChar, TextBox35.Text, 50)
        BaseII.CreateMyParameter("@Estaenbaja", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ValidaSerieCajaBaja")
        GloCajaenBaja = 0
        GloCajaenBaja = BaseII.dicoPar("@Estaenbaja").ToString
    End Sub

    Private Sub cbServicioDig_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbServicioDig.SelectedIndexChanged

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Consulta_Tbl_CambtipservPropio()
    End Sub
End Class