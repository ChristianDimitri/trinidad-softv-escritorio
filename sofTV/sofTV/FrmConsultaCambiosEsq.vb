﻿Imports System.Text
Imports System.Data.SqlClient

Public Class FrmConsultaCambiosEsq

    Private Sub FrmConsultaCambiosEsq_Invalidated(ByVal sender As Object, ByVal e As System.Windows.Forms.InvalidateEventArgs) Handles Me.Invalidated

    End Sub

    Private Sub FrmConsultaCambiosEsq_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)

        MuestraInfoCliente(GloContrato)
        If EsqoPer = "E" Then
            MuestraInfoCambioEsquema(GloContrato, IDCamesquema)
        ElseIf EsqoPer = "P" Then
            Me.Text = "Consulta Cambios Periodo"
            MuestraInfoCambioPeriodo(GloContrato, IDCamesquema)
        End If
    End Sub

    Private Sub MuestraInfoCliente(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraInfoCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                Me.LabelNombre.Text = reader(0).ToString()
                Me.LabelCalle.Text = reader(1).ToString()
                Me.LabelNumero.Text = reader(2).ToString()
                Me.LabelColonia.Text = reader(3).ToString()
                Me.LabelCiudad.Text = reader(4).ToString()
                Me.LabelTelefono.Text = reader(5).ToString()
                Me.LabelCelular.Text = reader(6).ToString()
                Me.CheckBoxSoloInternet.Checked = reader(7).ToString
            End While

            'UspMuestraTipoEsquema(Contrato)
            'MUESTRATipoContratacionClientes(Contrato, Me.cmbEsquemaPago.SelectedValue)
            'UspMuestraTipoEsquema_Opcion(Contrato)
            'GroupBox2.Enabled = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

   


    Private Sub MuestraInfoCambioEsquema(ByVal Contrato As Long, ByVal id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraInfoCambioEsquema", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@id", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = id
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                Me.LbContrato.Text = reader(0).ToString()
                Me.LbUsuario.Text = reader(1).ToString()
                Me.LbFecha.Text = reader(2).ToString()
                Me.LbEsqant.Text = reader(3).ToString()
                Me.LbDiasPrepant.Text = reader(4).ToString()
                Me.LbEsqNew.Text = reader(5).ToString()
                Me.LbDiasPrepNew.Text = reader(6).ToString()
                Me.Lbstatus.Text = reader(7).ToString
            End While

            If LbEsqant.Text = "Mensual por Periodos" Or LbEsqant.Text = "Mensual Anteriores" Then
                Label17.Visible = False
                LbDiasPrepant.Visible = False
            ElseIf LbEsqNew.Text = "Mensual por Periodos" Or LbEsqant.Text = "Mensual Anteriores" Then
                Label2.Visible = False
                LbDiasPrepNew.Visible = False
            End If
            'UspMuestraTipoEsquema(Contrato)
            'MUESTRATipoContratacionClientes(Contrato, Me.cmbEsquemaPago.SelectedValue)
            'UspMuestraTipoEsquema_Opcion(Contrato)
            'GroupBox2.Enabled = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub MuestraInfoCambioPeriodo(ByVal Contrato As Long, ByVal id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraInfoCambioPeriodo", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@id", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = id
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                Me.LbContrato.Text = reader(0).ToString()
                Me.LbUsuario.Text = reader(1).ToString()
                Me.LbFecha.Text = reader(2).ToString()
                Me.LbEsqant.Text = reader(3).ToString()
                ''Me.LbDiasPrepant.Text = reader(4).ToString()
                Me.LbEsqNew.Text = reader(4).ToString()
                'Me.LbDiasPrepNew.Text = reader(6).ToString()
                Me.Lbstatus.Text = reader(5).ToString
            End While


            Label17.Visible = False
            LbDiasPrepant.Visible = False
            Label2.Visible = False
            LbDiasPrepNew.Visible = False

            Label19.Text = "Periodo Anterior"
            Label11.Text = "Periodo Nuevo"

            'End If
            'UspMuestraTipoEsquema(Contrato)
            'MUESTRATipoContratacionClientes(Contrato, Me.cmbEsquemaPago.SelectedValue)
            'UspMuestraTipoEsquema_Opcion(Contrato)
            'GroupBox2.Enabled = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LbEsqNew.Click

    End Sub

    Private Sub Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Salir.Click
        GloContrato = 0
        IDCamesquema = 0
        Me.Close()
    End Sub
End Class