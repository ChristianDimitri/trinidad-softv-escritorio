﻿Public Class FrmtblSeleccionCalle

    Private Sub MUESTRAtblSeleccionCalle(ByVal Clv_Session As Integer, ByVal Op As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        dTable = BaseII.ConsultaDT("MUESTRAtblSeleccionCalle")
        If Op = 0 Then dgvIzq.DataSource = dTable
        If Op = 1 Then dgvDer.DataSource = dTable
    End Sub

    Private Sub INSERTAtblSeleccionCalle(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Calle As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, Clv_Calle)
        BaseII.Inserta("INSERTAtblSeleccionCalle")
    End Sub

    Private Sub ELIMINAtblSeleccionCalle(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Calle As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, Clv_Calle)
        BaseII.Inserta("ELIMINAtblSeleccionCalle")
    End Sub

    Private Sub DameClv_Session()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        eClv_Session = CLng(BaseII.dicoPar("@Clv_Session").ToString)
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        If dgvIzq.SelectedCells(0).Value = 0 Then
            Exit Sub
        End If
        INSERTAtblSeleccionCalle(eClv_Session, 0, dgvIzq.SelectedCells(0).Value)
        MUESTRAtblSeleccionCalle(eClv_Session, 0)
        MUESTRAtblSeleccionCalle(eClv_Session, 1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        INSERTAtblSeleccionCalle(eClv_Session, 1, 0)
        MUESTRAtblSeleccionCalle(eClv_Session, 0)
        MUESTRAtblSeleccionCalle(eClv_Session, 1)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINAtblSeleccionCalle(eClv_Session, 0, dgvDer.SelectedCells(0).Value)
        MUESTRAtblSeleccionCalle(eClv_Session, 0)
        MUESTRAtblSeleccionCalle(eClv_Session, 1)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINAtblSeleccionCalle(eClv_Session, 1, dgvDer.SelectedCells(0).Value)
        MUESTRAtblSeleccionCalle(eClv_Session, 0)
        MUESTRAtblSeleccionCalle(eClv_Session, 1)
    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona una Calle.")
            Exit Sub
        End If

        GlobndClv_Calle = True

        If dgvDer.RowCount = 1 Then
            GLONOMCalle = dgvDer.SelectedCells(1).Value
        Else
            GLONOMCalle = "SELECCIÓN MÚLTIPLE"
        End If

        Me.Close()
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmtblSeleccionCalle_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'DameClv_Session()
        ELIMINAtblSeleccionCalle(eClv_Session, 1, 0)
        MUESTRAtblSeleccionCalle(eClv_Session, 0)
        MUESTRAtblSeleccionCalle(eClv_Session, 1)
    End Sub
End Class