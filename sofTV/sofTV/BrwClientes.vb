﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.Collections.Generic
Imports System.Text
Imports System.Collections

Public Class BrwClientes
    Private customersByCityReport As ReportDocument
    Private LocNomImpresora_Contratos As String = Nothing
    Private LocNomImpresora_Tarjetas As String = Nothing
    Private errorfactura As Integer = 0
    Private clv_empresa As String = Nothing
    Private LocOpTarjetas As Integer = 0
    Private LocTipoTarjeta As String = Nothing


    Private eClv_Id As Integer = 0
    Private eNombreBD As String = Nothing
    Private eClave_Txt As String = Nothing
    Private eCiudad As String = Nothing
    Dim dsrpt As DataSet
    Public DstReporte As New DataSet
    Public DstReporteRojas As New DataSet
    Public DstReporteRojasTMP As New DataTable
    Dim obj_Impresora As Object



    Private Sub valida_factura(ByVal contrato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(contrato) = True And contrato > 0 Then
            Me.Valida_si_facturoTableAdapter.Connection = CON
            Me.Valida_si_facturoTableAdapter.Fill(Me.DataSetarnoldo.Valida_si_facturo, contrato, errorfactura)
            If IdSistema = "TO" Then
                If errorfactura > 0 Then
                    Me.Button10.Visible = True
                Else
                    Me.Button10.Visible = False
                End If
            End If
        End If
        CON.Close()
    End Sub

    'Private Sub BUSCACLIENTES(ByVal OP As Integer)
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()

    '        If OP = 0 Then
    '            If IsNumeric(Me.bcONTRATO.Text) = True Then
    '                Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
    '                Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(bcONTRATO.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
    '            Else
    '                Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
    '                Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
    '            End If
    '            Me.bcONTRATO.Clear()
    '        ElseIf OP = 1 Then
    '            Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
    '            Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType(Me.BNOMBRE.Text, String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
    '            'Me.BNOMBRE.Clear()
    '        ElseIf OP = 2 Then
    '            Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
    '            Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType(Me.BCALLE.Text, String)), (CType(Me.BNUMERO.Text, String)), (CType(Me.BCIUDAD.Text, String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
    '            'Me.BCALLE.Clear()
    '            'Me.BNUMERO.Clear()
    '            'Me.BCIUDAD.Clear()
    '        Else
    '            Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
    '            Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
    '            'Me.BCALLE.Clear()
    '            'Me.BNUMERO.Clear()
    '            'Me.BCIUDAD.Clear()
    '        End If
    '        CON.Close()
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Try

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If OP = 0 Then
                If IsNumeric(Me.bcONTRATO.Text) = True Then

                    BuscaSeparado(Me.bcONTRATO.Text, "", "", "", "", "", "", "", OP, Me.cmbColonias.SelectedValue)
                Else

                    BuscaSeparado(0, "", "", "", "", "", "", "", OP, Me.cmbColonias.SelectedValue)
                End If
                'Me.bcONTRATO.Clear()
            ElseIf OP = 1 Then
                If Len(Trim(Me.BNOMBRE.Text)) > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then

                    BuscaSeparado(0, Me.BNOMBRE.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", OP, Me.cmbColonias.SelectedValue)
                Else

                    BuscaSeparado(0, "", "", "", "", "", "", "", 0, Me.cmbColonias.SelectedValue)
                End If
            ElseIf OP = 2 Then
                If Len(Trim(Me.BCALLE.Text)) > 0 Or Len(Trim(Me.BNUMERO.Text)) > 0 Or Len(Trim(Me.BCIUDAD.Text)) > 0 Then

                    BuscaSeparado(0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, Me.BCIUDAD.Text, "", OP, Me.cmbColonias.SelectedValue)
                Else
                    BuscaSeparado(0, "", "", "", "", "", "", "", 0, Me.cmbColonias.SelectedValue)
                End If

            Else
                BuscaSeparado(0, "", "", "", " ", "", "", "", 3, Me.cmbColonias.SelectedValue)
            End If
            CON.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub BuscaSeparado(ByVal Contrato As Integer, ByVal NOMBRE As String, ByVal ApePaterno As String, ByVal ApeMaterno As String, ByVal CALLE As String, _
                              ByVal NUMERO As String, ByVal CIUDAD As String, ByVal Telefono As String, ByVal op As Integer, ByVal clvColonia As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlDataAdapter()
        Dim consulta As String = Nothing
        Try
            CON.Open()
            If Len(NOMBRE) = 0 Then
                NOMBRE = "''"
            ElseIf Len(NOMBRE) > 0 Then
                NOMBRE = "'" + NOMBRE + "'"
            End If
            If Len(ApePaterno) = 0 Then
                ApePaterno = "''"
            ElseIf Len(ApePaterno) > 0 Then
                ApePaterno = "'" + ApePaterno + "'"
            End If
            If Len(ApeMaterno) = 0 Then
                ApeMaterno = "''"
            ElseIf Len(ApeMaterno) > 0 Then
                ApeMaterno = "'" + ApeMaterno + "'"
            End If
            If Len(CALLE) = 0 Then
                CALLE = "''"
            ElseIf Len(CALLE) > 0 Then
                CALLE = "'" + CALLE + "'"
            End If
            If Len(NUMERO) = 0 Then
                NUMERO = "''"
            ElseIf Len(NUMERO) > 0 Then
                NUMERO = "'" + NUMERO + "'"
            End If
            If Len(CIUDAD) = 0 Then
                CIUDAD = "''"
            ElseIf Len(CIUDAD) > 0 Then
                CIUDAD = "'" + CIUDAD + "'"
            End If
            If Len(Telefono) = 0 Then
                Telefono = "''"
            ElseIf Len(Telefono) > 0 Then
                Telefono = "'" + Telefono + "'"
            End If

            consulta = "Exec uspBuscaContratoSeparado " + CStr(Contrato) + "," + CStr(NOMBRE) + "," + CStr(ApePaterno) + "," + CStr(ApeMaterno) + "," + CStr(CALLE) + "," + CStr(NUMERO) + _
                        "," + CStr(CIUDAD) + "," + CStr(Telefono) + "," + CStr(op) + "," + CStr(clvColonia)
            'MsgBox(consulta, MsgBoxStyle.Information)
            CMD = New SqlDataAdapter(consulta, CON)
            Dim dt As New DataTable
            Dim BS As New BindingSource

            CMD.Fill(dt)
            BS.DataSource = dt
            Me.DataGridView1.DataSource = BS.DataSource
            Try
                Me.CONTRATOLabel1.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
                Me.NOMBRELabel1.Text = (Me.DataGridView1.SelectedCells(1).Value) + " " + (Me.DataGridView1.SelectedCells(2).Value) + " " + (Me.DataGridView1.SelectedCells(3).Value) + " "
                Me.CALLELabel1.Text = (Me.DataGridView1.SelectedCells(4).Value)
                Me.NUMEROLabel1.Text = (Me.DataGridView1.SelectedCells(5).Value)
                Me.COLONIALabel1.Text = (Me.DataGridView1.SelectedCells(6).Value)
            Catch ex As System.Exception

            End Try
            CON.Close()
            'MessageBox.Show(DataGridView1.Columns.Count)

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Contrato = 0
            OpcionCli = "N"

            FrmClientes.Show()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BUSCACLIENTES(0)
    End Sub

    'Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
    '    Me.BUSCACLIENTES(1)
    'End Sub

    Private Sub BrwClientes_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim valida As Integer = 0
        Dim clave As Long = 0
        Dim comando As New SqlClient.SqlCommand
        Dim comando2 As New SqlClient.SqlCommand
        Dim comando3 As New SqlClient.SqlCommand
        Dim ConLidia As New SqlClient.SqlConnection(MiConexion)
        Dim Contrato As Integer = Nothing
        If GloBnd = True Then
            GloBnd = False
            Me.BUSCACLIENTES(3)
            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                valida_factura(CInt(Me.CONTRATOLabel1.Text))
            End If
        End If
        If bndcontt = True Then
            bndcontt = False
            ConfigureCrystalReportsContratoTomatlan(clv_empresa)
            valida = MsgBox("Voltee la hoja Para Continuar la Impresión", MsgBoxStyle.YesNo, "Pausa")
            If valida = 6 Then
                ConfigureCrystalReportsContratoTomatlan2(clv_empresa)
            ElseIf valida = 7 Then
                MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
            End If
            MsgBox("Se Imprimio con Exito", MsgBoxStyle.Information)
        End If
       

        Me.Button12.Visible = False
    End Sub

    Private Sub BrwClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        Dim CON As New SqlConnection(MiConexion)
        Dim Comando As SqlClient.SqlCommand
        CON.Open()

        'Me.ConGeneralRelBDTableAdapter.Connection = CON
        'Me.ConGeneralRelBDTableAdapter.Fill(Me.DataSetEric.ConGeneralRelBD)

        Try
            Comando = New SqlClient.SqlCommand
            With Comando
                .Connection = CON
                .CommandText = "ConGeneralRelBD"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                '' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Clv_Id", SqlDbType.Int)
                Dim prm1 As New SqlParameter("@NombreBD", SqlDbType.VarChar, 250)
                Dim prm2 As New SqlParameter("@Clave_Txt", SqlDbType.VarChar, 50)
                Dim prm3 As New SqlParameter("@Ciudad", SqlDbType.VarChar, 250)

                prm.Direction = ParameterDirection.Output
                prm1.Direction = ParameterDirection.Output
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Output

                prm.Value = 0
                prm1.Value = " "
                prm2.Value = " "
                prm3.Value = " "
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                Dim i As Integer = Comando.ExecuteNonQuery()
                eClv_Id = prm.Value
                eNombreBD = prm1.Value
                eClave_Txt = prm2.Value
                eCiudad = prm3.Value
            End With
        Catch
        End Try


        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)

        'MsgBox(Me.DataGridView1.SelectedCells.ToString)




        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla según sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
        llenaComboColonias() 'LLENAMOS EL COMBO DE LAS COLONIAS
        Me.BUSCACLIENTES(3)
        Me.Selecciona_Impresora_SucursalTableAdapter.Connection = CON
        Me.Selecciona_Impresora_SucursalTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora_Sucursal, GloClv_Sucursal, LocNomImpresora_Tarjetas, LocNomImpresora_Contratos)
        ImpresoraContatos = LocNomImpresora_Contratos
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            valida_factura(CInt(Me.CONTRATOLabel1.Text))
        End If
        Me.Button11.Visible = False
        If IdSistema = "SA" Or IdSistema = "VA" Then
            Me.Button11.Visible = True
        End If
        If IdSistema = "VA" Then
            Me.Button13.Visible = True
        End If
        CON.Close()
        frmctr = New FrmCtrl_ServiciosCli
        frmTelefonia = New FrmClientesTel
        frmInternet2 = New FrmInternet
        frmctr.MdiParent = FrmClientes
        frmInternet2.MdiParent = FrmClientes
        'frmctr.WindowState = FormWindowState.Normal
        frmInternet2.Show()
        frmInternet2.Hide()
        frmctr.Show()
        frmctr.Hide()


        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub bcONTRATO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bcONTRATO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(0)
        End If
    End Sub

    Private Sub bcONTRATO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bcONTRATO.TextChanged

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Me.BUSCACLIENTES(2)
    End Sub

    Private Sub BNOMBRE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(1)
            'Me.BNOMBRE.Text = ""
        End If
    End Sub

    Private Sub BNOMBRE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub

    Private Sub BCALLE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCALLE.TextChanged

    End Sub

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub

    Private Sub BNUMERO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNUMERO.TextChanged

    End Sub

    Private Sub BCIUDAD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCIUDAD.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub




    Private Sub CONTRATOLabel1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CONTRATOLabel1.TextChanged
        Try
            CREAARBOL()

            If Me.CONTRATOLabel1.Text <> "" Then
                valida_factura(CInt(Me.CONTRATOLabel1.Text))
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SOLOINTERNETCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SOLOINTERNETCheckBox.CheckedChanged
        If Me.SOLOINTERNETCheckBox.Checked = False Then
            Me.SOLOINTERNETCheckBox.Enabled = False
        Else
            Me.SOLOINTERNETCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ESHOTELCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ESHOTELCheckBox.CheckedChanged
        If Me.ESHOTELCheckBox.Checked = False Then
            Me.ESHOTELCheckBox.Enabled = False
        Else
            Me.ESHOTELCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ServicioListBox_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)

    End Sub

    Private Sub ServicioListBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Me.bcONTRATO.Focus()
    End Sub

    Private Sub ServicioListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ESHOTELLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub SOLOINTERNETLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub

    Private Sub consultar()
        Try
            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Contrato = Me.CONTRATOLabel1.Text
                OpcionCli = "C"

                FrmClientes.Show()
            Else
                MsgBox("Seleccione un Cliente para poder Consultar ", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        consultar()
    End Sub

    Private Sub modificar()
        Try
            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Contrato = Me.CONTRATOLabel1.Text
                OpcionCli = "M"
                Locbndvalcliente = True

                FrmClientes.Show()
            Else
                MsgBox("Seleccione un Cliente para poder Modificar ", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        modificar()
    End Sub

    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.CONTRATOLabel1.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False

            Dim PasaJNet As Boolean = False
            Dim jNet As Integer = -1
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Teléfonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then
                    I = I + 1
                    pasa = False
                End If

            Next
            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
            CON.Close()
            'Me.TreeView1.Nodes(0).ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub CREAARBOL2()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.CONTRATOLabel1.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If



            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Servicio").ToString()))
                I += 1
            Next



            CON.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub




    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If LocNomImpresora_Tarjetas = "" Then
            MsgBox("No se ha asignado una Impresora de Tarjetas a esta Sucursal", MsgBoxStyle.Information)
        Else
            LocTarjNo_Contrato = CLng(Me.CONTRATOLabel1.Text)
            'LocGloOpRep = 1
            ConfigureCrystalReports1(LocTarjNo_Contrato)
            MsgBox("Se Imprimio con Exito", MsgBoxStyle.Information)
            'FrmImprimirFac.Show()
        End If
    End Sub

    Private Sub ConfigureCrystalReports1(ByVal No_Contrato As Integer)

        ' Dim impresora As String = nothing

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Contrato_txt As String = Nothing
        Dim reportPath As String = Nothing


        reportPath = RutaReportes + "\Tarjetas.rpt"

        'customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@No_Contrato 
        Contrato_txt = CStr(No_Contrato)
        If Len(Contrato_txt) = 1 Then
            Contrato_txt = "00" & Contrato_txt
        ElseIf Len(Contrato_txt) = 2 Then
            Contrato_txt = "0" & Contrato_txt

        End If
        'customersByCityReport.SetParameterValue(0, No_Contrato)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@no_contrato", SqlDbType.Int, Contrato_txt)

        Dim Lt As New List(Of String)
        Lt.Add("Imprime_Tarjetas")

        Dim DS As New DataSet
        DS.Clear()
        DS = BaseII.ConsultaDS("Imprime_Tarjetas", Lt)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)

        mySelectFormula = "*" & Contrato_txt & "*"
        customersByCityReport.DataDefinition.FormulaFields("codigo").Text = "'" & mySelectFormula & "'"
        ' Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 3, impresora)

        customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Tarjetas
        customersByCityReport.PrintToPrinter(1, True, 1, 1)


        'customersByCityReport.PrintOptions.PrinterName = "Datacard Printer"






    End Sub

    Private Sub ConfigureCrystalReportsBoleta(ByVal No_Contrato As Long)

        ' Dim impresora As String = nothing

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Contrato_txt As String = Nothing
        Dim reportPath As String = Nothing


        reportPath = RutaReportes + "\Boletas_de_Pago.rpt"

        customersByCityReport.Load(reportPath)

        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@No_Contrato 
        customersByCityReport.SetParameterValue(0, No_Contrato)
        Dim MIcIUDAD As String = Nothing
        MIcIUDAD = GloSucursal
        customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

        customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos

        customersByCityReport.PrintToPrinter(1, True, 1, 0)

    End Sub
    Private Sub ConfigureCrystalReportsBoletaTVS(ByVal No_Contrato As Long)
        ' Dim impresora As String = nothing

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Contrato_txt As String = Nothing
        Dim reportPath As String = Nothing


        reportPath = RutaReportes + "\ReportTalonPagosTVS.rpt"

        customersByCityReport.Load(reportPath)

        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@No_Contrato 
        customersByCityReport.SetParameterValue(0, No_Contrato)

        customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos

        customersByCityReport.PrintToPrinter(1, True, 1, 0)
    End Sub



    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim valida As Integer = 0
        Dim clave As Long = 0
        Dim comando As New SqlClient.SqlCommand
        Dim comando2 As New SqlClient.SqlCommand
        Dim comando3 As New SqlClient.SqlCommand
        Dim ConLidia As New SqlClient.SqlConnection(MiConexion)
        Dim contador As Integer = 0

        CON.Open()
        'AQUI DEBE DE IR LO DE LA AGENDA
        
        conlidia.Open()
        With comando
            .CommandText = "Checar Citas"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = CON
            Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.CONTRATOLabel1.Text
            .Parameters.Add(prm)
            Dim prm1 As New SqlParameter("@Cont", SqlDbType.Int)
            prm1.Direction = ParameterDirection.Output
            prm1.Value = 0
            .Parameters.Add(prm1)
            Dim i As Integer = comando.ExecuteNonQuery
            contador = prm1.Value
        End With
        conlidia.Close()
        If contador >= 1 Then
            opcion = "N"
            Dim FrmA As New FrmAgendaRapida
            FrmA.ShowDialog()
            Dim Clv_Cita As Long
            If Len(Trim(FechaAgenda)) > 0 And Len(Trim(HoraAgenda)) Then
                'Me.NUE_CITASTableAdapter.Connection = CON
                'Me.NUE_CITASTableAdapter.Fill(Me.NewSofTvDataSet.NUE_CITAS, Me.Tecnicos.SelectedValue, New System.Nullable(Of Date)(CType(FechaAgenda, Date)), New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), "", "", "Q", Clv_Cita)
                'Me.CONDetCitasTableAdapter.Connection = CON
                'Me.CONDetCitasTableAdapter.Delete(Clv_Cita)
                'Me.CONDetCitasTableAdapter.Connection = CON
                'Me.CONDetCitasTableAdapter.Insert(Clv_HoraAgenda, Clv_Cita, clave)
                'Me.NUEREL_CITAS_QUEJASTableAdapter.Connection = CON
                'Me.NUEREL_CITAS_QUEJASTableAdapter.Fill(Me.NewSofTvDataSet.NUEREL_CITAS_QUEJAS, New System.Nullable(Of Long)(CType(Clv_Cita, Long)), New System.Nullable(Of Long)(CType(Me.Clv_quejaTextBox.Text, Long)))
                '[NUE_CITAS] (@Clv_Tecnico INT, @Fecha DATETIME, @Contrato BIGINT, @Descripcion_corta VARCHAR(50), @Descripcion VARCHAR(300), @Queja_o_Orden_o_Otro VARCHAR(1),@Clv_Cita BIGINT OUTPUT)	AS
                ConLidia.Open()
                With comando2
                    .CommandText = "Nue_Citas"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = ConLidia

                    Dim prm As New SqlParameter("@Clv_tecnico", SqlDbType.Int)
                    Dim prm2 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                    Dim prm3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm4 As New SqlParameter("@Descripcion_corta", SqlDbType.VarChar, 50)
                    Dim prm5 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 300)
                    Dim prm6 As New SqlParameter("@Queja_o_Orden_o_Otro", SqlDbType.VarChar, 1)
                    Dim prm8 As New SqlParameter("@Clv_cita", SqlDbType.BigInt)
                    '@TURNO-TurnoAgenda
                    Dim prm9 As New SqlParameter("@TURNO", SqlDbType.VarChar, 50)


                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm5.Direction = ParameterDirection.Input
                    prm6.Direction = ParameterDirection.Input
                    prm8.Direction = ParameterDirection.Output
                    prm9.Direction = ParameterDirection.Input

                    prm.Value = GloClv_tecnico
                    prm2.Value = FechaAgenda
                    prm3.Value = CInt(Me.CONTRATOLabel1.Text)
                    prm4.Value = "Acometida"
                    prm5.Value = "Instalación Acometida"
                    prm6.Value = "O"
                    prm8.Value = 0
                    prm9.Value = TurnoAgenda
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    Dim i As Integer = .ExecuteNonQuery
                    Clv_Cita = prm8.Value

                End With
                With comando3
                    .CommandText = "NUEDetCitas"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON
                    '(@CLV_HORA INT,@CLV_CITA INT ,@CLAVE INT OUTPUT) 
                    Dim prm As New SqlParameter("@CLV_HORA", SqlDbType.Int)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = Clv_HoraAgenda
                    .Parameters.Add(prm)
                    Dim Prm2 As New SqlParameter("@CLV_CITA", SqlDbType.Int)
                    Prm2.Direction = ParameterDirection.Input
                    Prm2.Value = Clv_Cita
                    .Parameters.Add(Prm2)
                    Dim Prm3 As New SqlParameter("@CLAVE", SqlDbType.Int)
                    Prm3.Direction = ParameterDirection.Output
                    Prm3.Value = 0
                    .Parameters.Add(Prm3)
                    Dim i As Integer = .ExecuteNonQuery
                    clave = Prm3.Value
                End With
                NUEMOVREL_CITAS(Clv_Cita, ComentarioAgenda)
                'With comando3
                '    .CommandText = "NUEREL_CITAS"
                '    .CommandTimeout = 0
                '    .CommandType = CommandType.StoredProcedure
                '    .Connection = ConLidia
                '    Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.Int)
                '    Dim Prm2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
                '    prm.Direction = ParameterDirection.Input
                '    Prm2.Direction = ParameterDirection.Input
                '    prm.Value = Clv_Cita
                '    prm.Value = 0 'clv_orden
                '    .Parameters.Add(prm)
                '    .Parameters.Add(Prm2)

                'End With
                '[NUEREL_CITAS_ORDENES](@CLV_CITA BIGINT,@CLV_ORDEN BIGINT)
                ConLidia.Close()

                FechaAgenda = ""
                HoraAgenda = ""
                ComentarioAgenda = ""
                'If LocBndAgendaClientes = False Then
                '    Impresion_Contrato()
                'ElseIf LocBndAgendaClientes = True Then
                '    LocBndAgendaClientes = False
                'End If

            End If

            'ElseIf contador > 0 Then
            '    'Me.Impresion_Contrato()
            '    REPORTEContrato(DataGridView1.SelectedCells(0).Value)
        End If

        REPORTEContrato(DataGridView1.SelectedCells(0).Value)

    End Sub
    Private Sub Impresion_Contrato()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim valida As Integer = 0
        Dim valida2 As Integer = 0
        Dim yes As Integer = 0
        Dim tv As Integer = 0, Int As Integer = 0, Dig As Integer = 0
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            eContrato = Me.CONTRATOLabel1.Text
            Me.Dame_clv_EmpresaTableAdapter.Connection = CON
            Me.Dame_clv_EmpresaTableAdapter.Fill(Me.DataSetarnoldo.Dame_clv_Empresa, clv_empresa)



            If LocNomImpresora_Contratos = "" Then
                MsgBox("No se ha asignado una Impresora de Contratos a esta Sucursal", MsgBoxStyle.Information)
            Else
                If clv_empresa = "AG" Or clv_empresa = "SA" Or clv_empresa = "VA" Then
                    Me.ChecaServicioContratadoTableAdapter.Connection = CON
                    Me.ChecaServicioContratadoTableAdapter.Fill(Me.DataSetEDGAR.ChecaServicioContratado, CInt(Me.CONTRATOLabel1.Text), eErrorContrato)
                    If eErrorContrato = 0 Then
                        If clv_empresa = "SA" Or clv_empresa = "VA" Then
                            Me.Valida_Factura_contratoTableAdapter.Connection = CON
                            Me.Valida_Factura_contratoTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_Factura_contrato, CInt(Me.CONTRATOLabel1.Text), valida2)
                            If valida2 = 0 Then
                                Me.Checa_servicios_clienteTableAdapter.Connection = CON
                                Me.Checa_servicios_clienteTableAdapter.Fill(Me.ProcedimientosArnoldo2.Checa_servicios_cliente, CInt(Me.CONTRATOLabel1.Text), tv, Int, Dig)
                                If tv > 0 Then
                                    MsgBox("Presione el botón para iniciar la impresión del contrato de Tv por Cable", MsgBoxStyle.Information)
                                    ConfigureCrystalReportsContratoSAComun(1)
                                    yes = MsgBox("Presione Sí para seguir con la Impresion de Contrato de Tv por Cable", MsgBoxStyle.YesNo)
                                    If yes = 6 Then
                                        ConfigureCrystalReportsContratoSAAtras(1)
                                    ElseIf yes = 7 Then
                                        MsgBox("El contrato no se imprimio Completo", MsgBoxStyle.Information)
                                    End If
                                End If
                                If Int > 0 Then
                                    MsgBox("Presione el botón para iniciar la Impresión del Contrato de Servicios de Internet", MsgBoxStyle.Information)
                                    ConfigureCrystalReportsContratoSAComun(2)
                                    yes = MsgBox("Presione Sí para seguir con la Impresion de Contrato de Servicios de Internet", MsgBoxStyle.YesNo)
                                    If yes = 6 Then
                                        ConfigureCrystalReportsContratoSAAtras(2)
                                    ElseIf yes = 7 Then
                                        MsgBox("El contrato no se imprimio Completo", MsgBoxStyle.Information)
                                    End If
                                End If
                                If Dig > 0 Then
                                    MsgBox("Presione el botón para Iniciar la Impresión del Contrato del Servicio de Canales Adicionales", MsgBoxStyle.Information)
                                    ConfigureCrystalReportsContratoSAComun(3)
                                    yes = MsgBox("Presione Sí para seguir con la Impresion de Contrato del Servicio de Canales Adicionales", MsgBoxStyle.YesNo)
                                    If yes = 6 Then
                                        ConfigureCrystalReportsContratoSAAtras(3)
                                    ElseIf yes = 7 Then
                                        MsgBox("El contrato no se imprimio Completo", MsgBoxStyle.Information)
                                    End If
                                End If
                            ElseIf valida2 > 0 Then
                                MsgBox("No se puede imprimir el contrato", MsgBoxStyle.Information)
                            End If
                        ElseIf clv_empresa = "AG" Then
                            ConfigureCrystalReportsContrato(eContrato)
                        End If
                        If valida2 = 0 Then
                            MsgBox("Se Imprimio con Éxito", MsgBoxStyle.Information)
                        End If
                        'FrmImprimirContrato.Show()
                    Else
                        MsgBox("El Cliente no cuenta con Servicios Asignados", , "Advertencia")
                    End If
                ElseIf clv_empresa = "TO" Then
                    Me.Valida_Contrato_ServTableAdapter.Connection = CON
                    Me.Valida_Contrato_ServTableAdapter.Fill(Me.DataSetarnoldo.Valida_Contrato_Serv, eContrato, eErrorContrato)
                    If eErrorContrato = 0 Then
                        Dim Frm1 As New FrmHorasInst
                        Frm1.ShowDialog()
                    Else
                        MsgBox("El cliente no cuenta con servicios Asignados", , "Advertencia")
                    End If
                    'ElseIf clv_empresa = "VA" Then
                    '    Me.Valida_Contrato_ServTableAdapter.Connection = CON
                    '    Me.Valida_Contrato_ServTableAdapter.Fill(Me.DataSetarnoldo.Valida_Contrato_Serv, eContrato, eErrorContrato)
                    '    If eErrorContrato = 0 Then
                    '        'va el formulario correspondiente
                    '        FrmDatosContrato.Show()
                    '    Else
                    '        MsgBox("El cliente no cuenta con servicios Asignados", , "Advertencia")
                    '    End If
                End If
            End If
        Else
            MsgBox("No Se Ha Seleccionado Un Contrato ", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub
    Private Sub ConfigureCrystalReportsContrato(ByVal Clv_Factura As Long)
        'Dim impresora As String = Nothing
        'customersByCityReport = New ReportDocument

        'Dim reportPath As String = Nothing
        'reportPath = RutaReportes + "\ReportConContratoVA.rpt"

        ''@Contrato 
        'Dim varprueba As String = "Hola"
        'Dim Cantidad_Letra As String = ""
        'Cantidad_Letra = DameCantidadALetra(550.25)

        'Dim DS As New DataSet
        'DS.Clear()

        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, eContrato)

        'Dim Lt As New List(Of String)
        'Lt.Add("ConContrato")
        'Lt.Add("DameDatosGenerales_2")
        'Lt.Add("DameDesglose_Contrato")

        'DS = BaseII.ConsultaDS("ConContrato", Lt)

        'customersByCityReport.Load(reportPath)
        'SetDBReport(DS, customersByCityReport)

        'customersByCityReport.SetParameterValue(0, 1)
        'customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\Sp_Contrato.rpt"

            ''@Contrato 
            'Dim varprueba As String = "Hola"
            'Dim Cantidad_Letra As String = ""
            'Cantidad_Letra = DameCantidadALetra(550.25)

            Dim DS As New DataSet
            DS.Clear()

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, CInt(eContrato))
            BaseII.CreateMyParameter("@User", SqlDbType.VarChar, CStr(GloUsuario))

            Dim Lt As New List(Of String)
            Lt.Add("Sp_Contrato")
            'Lt.Add("DameDatosGenerales_2")
            'Lt.Add("DameDesglose_Contrato")

            DS = BaseII.ConsultaDS("Sp_Contrato", Lt)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            'customersByCityReport.SetParameterValue(0, 1)
            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
        Catch ex As Exception
            MsgBox("Seleccione la impresora de contratos", MsgBoxStyle.Information)
            'System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsContratoTomatlan(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            Dim cmd As SqlClient.SqlCommand
            Dim horaini As String = Nothing
            Dim horafin As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ContratoTomatlan.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)


            'horaini = InputBox("Apartir de ", "Captura Hora")
            'horafin = InputBox("Capture la hora de la Instalación Final", "Captura Hora")



            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
            'If horaini = "" Then horaini = "0"
            'If horafin = "" Then horafin = "0"

            customersByCityReport.DataDefinition.FormulaFields("horaini").Text = "'" & horaini & "'"
            customersByCityReport.DataDefinition.FormulaFields("horafin").Text = "'" & horafin & "'"


            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            With cmd
                .CommandText = "Hora_ins"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON
                Dim prm As New SqlParameter("@Hora_ini", SqlDbType.VarChar, 10)
                Dim prm1 As New SqlParameter("@Hora_fin", SqlDbType.VarChar, 10)
                Dim prm2 As New SqlParameter("@contrato", SqlDbType.BigInt)
                Dim prm3 As New SqlParameter("@observaciones", SqlDbType.VarChar)

                '@Hora_ini varchar(10),@Hora_fin varchar(10),@contrato bigint,@observaciones varchar(max)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)

                prm.Value = horaini
                prm1.Value = horafin
                prm2.Value = eContrato
                prm3.Value = ""

                Dim i As Integer = .ExecuteNonQuery

            End With

            'Me.Hora_insTableAdapter.Connection = CON
            'Me.Hora_insTableAdapter.Fill(Me.DataSetarnoldo.Hora_ins, horaini, horafin, eContrato)
            Me.Inserta_Comentario2TableAdapter.Connection = CON
            Me.Inserta_Comentario2TableAdapter.Fill(Me.DataSetarnoldo.Inserta_Comentario2, eContrato)
            CON.Close()
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalReportsContratoTomatlan2(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            Dim horaini As String = Nothing
            Dim horafin As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\TomatlanAtras.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)





            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsContratoSAComun(ByVal Tipo As Integer)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim LocTipo As String = Nothing
            If clv_empresa = "SA" Then
                reportPath = RutaReportes + "\ReportConContratoSA.rpt"
            ElseIf clv_empresa = "VA" Then
                reportPath = RutaReportes + "\ReportConContratoVA.rpt"
            End If
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            '@TIpo
            customersByCityReport.SetParameterValue(1, Tipo)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.ShowPrintButton = True

            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)

            Select Case Tipo
                Case 1
                    LocTipo = "SERVICIOS DE TV POR CABLE"
                Case 2
                    LocTipo = "SERVICIOS DE INTERNET"
                Case 3
                    LocTipo = "SERVICIOS DE CANALES ADICIONALES"
            End Select
            customersByCityReport.DataDefinition.FormulaFields("Titulo2").Text = "'" & LocTipo & "'"

            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing


        Catch ex As Exception
            MsgBox("La Impresora Especificada no es válida (" & LocNomImpresora_Contratos & ")")
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsContratoSAAtras(ByVal op As Integer)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            If clv_empresa = "SA" Then
                Select Case op
                    Case 1
                        reportPath = RutaReportes + "\ParteAtrasSATvcable.rpt"
                    Case 2
                        reportPath = RutaReportes + "\ParteAtrasIntRey.rpt"
                    Case 3
                        reportPath = RutaReportes + "\ParteAtrasTvDigRey.rpt"
                End Select
            ElseIf clv_empresa = "VA" Then
                'Select Case op
                'Case 1
                reportPath = RutaReportes + "\ParteAtrasVA.rpt"
                'Case 2
                'reportPath = RutaReportes + "\ParteAtrasIntVA.rpt"
                'Case 3
                'reportPath = RutaReportes + "\ParteAtrasDigVA.rpt"
                'End Select
            End If


            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.ShowPrintButton = True

            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)


            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing


        Catch ex As Exception
            MsgBox("La Impresora Especificada no es válida (" & LocNomImpresora_Contratos & ")")
        End Try

    End Sub





    Private Sub CONTRATOLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRATOLabel1.Click

    End Sub


    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Dim CON As New SqlConnection(MiConexion)

        CON.Open()
        Dim Status As String = Nothing
        Me.Dime_Si_BoletasTableAdapter.Connection = CON
        Me.Dime_Si_BoletasTableAdapter.Fill(Me.DataSetEdgarRev2.Dime_Si_Boletas, CLng(Me.CONTRATOLabel1.Text), Status)
        If Status = "I" Then
            If eClave_Txt = "TV" Then
                ConfigureCrystalReportsBoletaTVS(CLng(Me.CONTRATOLabel1.Text))
            Else
                ConfigureCrystalReportsBoleta(CLng(Me.CONTRATOLabel1.Text))
            End If

        Else
            MsgBox("El Cliente Tiene que estar con Status de Instalado ", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub


    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim I As Integer = 0
        Dim X As Integer = 0
        Dim Txt As String = Nothing
        Dim GLOBND As Boolean = True
        'Me.DameGeneralesBancosTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos, "PR")

        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            Dim Nom_Archivo As String = Nothing
            Dim Encabezado As String = Nothing
            Dim imp1 As String = Nothing
            Dim Rutatxt As String = Nothing

            Dim Nom_ArchivoBat As String = Nothing
            Nom_ArchivoBat = "C:" + "\" + "ImprimeEtiqueta.bat"
            Dim fileExists2 As Boolean
            fileExists2 = My.Computer.FileSystem.FileExists(Nom_ArchivoBat)
            If fileExists2 = True Then
                File.Delete(Nom_ArchivoBat)
            End If
            Using sw2 As StreamWriter = File.CreateText(Nom_ArchivoBat)
                sw2.WriteLine("cd c:\")
                sw2.WriteLine("Print Etiqueta_1.txt > lpt1")
                sw2.Close()
            End Using
            'If (result = DialogResult.OK) Then
            Nom_Archivo = "C:" + "\" + "Etiqueta_1.txt"

            Dim fileExists As Boolean
            fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
            If fileExists = True Then
                File.Delete(Nom_Archivo)
            End If
            Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                'Dim FilaRow As DataRow
                'Me.CONSULTACNRTableAdapter.Fill(Me.DataSetLidia.CONSULTACNR)
                Dim NumeroAfiliacion As String = Nothing
                Dim ClaveBanco As String = Nothing
                Dim ReferenciaCliente As String = Nothing
                Dim NumeroTarjeta As String = Nothing
                Dim StDetalle As String = Nothing
                Dim StMonto As String = Nothing

                'Me.Reporte_TiposCliente_CiudadTableAdapter.Fill(Me.DataSetEdgarRev2.Reporte_TiposCliente_Ciudad, LocClv_session, GloClv_tipser2, LocBndC, LocBndB, LocBndI, LocBndD, LocBndS, LocBndF, LocBndDT, 1, LocValidaHab, LocPeriodo1, LocPeriodo2)
                'For Each FilaRow In Me.DataSetEdgarRev2.Reporte_TiposCliente_Ciudad.Rows
                'If FilaRow("Contrato".ToString()) Is Nothing Then
                'Exit For
                'End If
                sw.WriteLine("Q400,025")
                sw.WriteLine("q1200")
                sw.WriteLine("rN")
                sw.WriteLine("S4")
                sw.WriteLine("D7")
                sw.WriteLine("ZT")
                sw.WriteLine("JB")
                sw.WriteLine("OD")
                sw.WriteLine("R50,50")
                sw.WriteLine("N")
                sw.WriteLine("A0,0,0,5,1,1,N," & Chr(34) & Trim(Me.CONTRATOLabel1.Text) & Chr(34))
                sw.WriteLine("A500,0,0,5,1,1,N," & Chr(34) & Trim(Me.CONTRATOLabel1.Text) & Chr(34))
                sw.WriteLine("P1")
                sw.WriteLine("^@")

                'Next

                '    Txt = "save"
                '    sw.Write(Txt)
                sw.Close()
            End Using
            Dim myProcess As New Process()
            Dim myProcessStartInfo As New ProcessStartInfo("C:\\ImprimeEtiqueta.bat")
            'Dim myProcessStartInfo As New ProcessStartInfo("C:\\Print Etiqueta2.txt ", "> lpt1")
            myProcessStartInfo.WorkingDirectory = "C:\\"
            'C:\Program Files\Network Registrar\Local\bin
            myProcessStartInfo.UseShellExecute = False
            myProcessStartInfo.RedirectStandardOutput = True
            myProcess.StartInfo = myProcessStartInfo
            myProcess.Start()



        End If

    End Sub
    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        Dim ConLidia As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand
        Dim Status As String = Nothing
        ConLidia.Open()
        With cmd
            .CommandText = "Dime_si_Estado"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConLidia
            Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.CONTRATOLabel1.Text
            .Parameters.Add(prm)

            Dim Prm2 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
            Prm2.Direction = ParameterDirection.Output
            Prm2.Value = ""
            .Parameters.Add(Prm2)

            Dim i As Integer = .ExecuteNonQuery
            Status = Prm2.Value
        End With

        ConLidia.Close()
        If Status = "I" Then
            LiContrato = Me.CONTRATOLabel1.Text
            ImpresoraEstado = LocNomImpresora_Contratos
            Dim frmnumm As New FrmNumMeses
            frmnumm.ShowDialog()
        Else
            MsgBox("El Cliente tiene que Tener Status Instalado", MsgBoxStyle.Information)
        End If

    End Sub



    Private Sub btnBuscarPorNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarPorNombre.Click
        Me.BUSCACLIENTES(1)
    End Sub

    Private Sub DataGridView1_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.CurrentCellChanged

        ObtenerClv()
        If (Contrato > 0) Then
            CONTRATOLabel1.Text = Contrato
            NOMBRELabel1.Text = DataGridView1.SelectedRows(0).Cells("Nombre").Value.ToString() + " " + DataGridView1.SelectedRows(0).Cells("Apellido_Paterno").Value.ToString() + " " + DataGridView1.SelectedRows(0).Cells("Apellido_Materno").Value.ToString()
            CALLELabel1.Text = DataGridView1.SelectedRows(0).Cells("Calle").Value.ToString()
            NUMEROLabel1.Text = DataGridView1.SelectedRows(0).Cells("NUMERO").Value.ToString()
            COLONIALabel1.Text = DataGridView1.SelectedRows(0).Cells("COLONIA").Value.ToString()
            'SOLOINTERNETCheckBox.Checked = Boolean.Parse(DataGridView1.SelectedRows(0).Cells("SOLOINTERNET").Value.ToString())
        End If


    End Sub

    Private Sub ObtenerClv()
        Try
            Contrato = Integer.Parse(DataGridView1.SelectedRows(0).Cells(0).Value.ToString())
        Catch ex As Exception
            Contrato = 0
        End Try
    End Sub

    Private Sub BNOMBRE_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles BNOMBRE.KeyDown, APaternoTextBox.KeyDown, AMaternoTextBox.KeyDown
        If (e.KeyCode = Keys.Enter) Then
            btnBuscarPorNombre_Click(sender, e)
        End If
    End Sub

    Private Sub REPORTEContrato(ByVal CONTRATO As Integer)

        If IMPRESORA_CONTRATOS.Length = 0 Then
            MessageBox.Show("No se ha asignado una impresora de contratos en esta sucursal.")
            Exit Sub
        End If

        Dim rDocument As ReportDocument
        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("DatosCliente")
        tableNameList.Add("DatosFactura")
        'tableNameList.Add("Tvs")
        'tableNameList.Add("Tarifas")
        'tableNameList.Add("Factura")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
        dSet = BaseII.ConsultaDS("REPORTEContrato", tableNameList)

        rDocument = New ReportDocument
        rDocument.Load(RutaReportes + "\REPORTE_Contrato.rpt")
        rDocument.SetDataSource(dSet)

        MessageBox.Show("Se imprimirá la parte frontal del contrato.", "Atención", MessageBoxButtons.OK)

        rDocument.PrintOptions.PrinterName = IMPRESORA_CONTRATOS
        rDocument.PrintToPrinter(1, False, 0, 0)


        rDocument = New ReportDocument
        rDocument.Load(RutaReportes + "\Reporte_ContratoCaratula.rpt")
        rDocument.SetDataSource(dSet)

        'MessageBox.Show("Se imprimirá la parte trasera del contrato.", "Atención", MessageBoxButtons.OK)

        rDocument.PrintOptions.PrinterName = IMPRESORA_CONTRATOS
        rDocument.PrintToPrinter(1, False, 0, 0)

    End Sub

#Region "COMBO COLONIAS"
    Private Sub llenaComboColonias()
        Dim colonias As New BaseIII
        Try
            colonias.limpiaParametros()
            Me.cmbColonias.DataSource = colonias.ConsultaDT("uspConsultaColonias")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        If Me.DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            Try
                Dim Locvalor1 As String
                Dim Locvalor2 As String

                FrmTarjetas.ShowDialog()

                If GloTarjVerde = True Then
                    GloTarjVerde = False
                    Locvalor1 = MsgBox("¿Esta listo para imprimir la Tarjeta Verde?", MsgBoxStyle.YesNo)
                    If Locvalor1 = MsgBoxResult.Yes Then
                        Me.CONTRATOLabel1.Text = Me.DataGridView1.SelectedCells.Item(0).Value()
                        LocOpTarjetas = 1
                        LocTipoTarjeta = "V"
                        Dim CORTESIA As New DataTable

                        CORTESIA = UspValidarClienteConCortesia(Me.CONTRATOLabel1.Text)
                        If CORTESIA.Rows(0)(0).ToString = "1" Then
                            MsgBox("No se pueden imprimir las Tarjetas, los contratos tienen estatus diferente a instalado o es cortesía")
                            Exit Sub
                        End If
                        GenerarEstados(Me.CONTRATOLabel1.Text, 1)

                    End If
                End If
                If GloTarjRoja = True Then
                    GloTarjRoja = False
                    Locvalor2 = MsgBox("¿Esta listo para imprimir la Tarjeta Roja?", MsgBoxStyle.YesNo)
                    If Locvalor2 = MsgBoxResult.Yes Then
                        Me.CONTRATOLabel1.Text = Me.DataGridView1.SelectedCells.Item(0).Value()
                        LocOpTarjetas = 0
                        LocTipoTarjeta = "R"
                        GenerarEstados(Me.CONTRATOLabel1.Text, 0)
                    End If

                End If
            Catch ex As Exception

            End Try


        End If
    End Sub
    Private Sub Button15_Click(sender As System.Object, e As System.EventArgs)

    End Sub
    Private Sub BtnHistorialTarjetas_Click(sender As System.Object, e As System.EventArgs)

    End Sub
    Private Sub Button16_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Public Sub GenerarEstados(ByVal oContrato As Long, ByVal prmOp As Integer)
        Try


            Dim max As Long
            Dim LocError As Integer
            Dim locimporte As Decimal
            Dim DT As New DataTable
            Dim DS As New DataSet

            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
            Dim listatablas As New List(Of String)
            listatablas.Add("PREFacturas_EDO_SOFTV")
            DS = BaseII.ConsultaDS("Genera_Codigo_OXXO_PorContrato1", listatablas)
            Dim rd As ReportDocument
            Dim i As Long = 0
            Dim contador As Integer = 1
            max = DS.Tables(0).Rows.Count
            Dim prefijo As String = ""
            Dim Txt As String = Nothing

            If max = 0 Then
                MsgBox("El Cliente esta dado de baja", MsgBoxStyle.Information)
                Exit Sub
                'Else
                '    If Len(DS.Tables(0).Rows(0)(4).ToString) = 0 Then
                '        MsgBox("El Cliente tiene Cortesía", MsgBoxStyle.Information)
                '        Exit Sub
                '    End If
            End If
            'If Len(DS.Tables(0).Rows(0)(8).ToString) > 0 Then
            '    LocError = 1
            'Else
            '    LocError = 0
            'End If
            'If LocError = 1 Then
            '    MsgBox(DS.Tables(0).Rows(0)(9).ToString, MsgBoxStyle.Information)
            '    Exit Sub
            'End If
            Dim dsrpt As DataSet
            For Each dr As DataRow In DS.Tables(0).Rows
                i = Long.Parse(dr(0).ToString())
                Txt = dr(10).ToString()
                AgregaImgCodeBarEstadoDeCuentaTV(oContrato, Txt, LocOpTarjetas)
                'rd = New ReportDocument()
                'rd.Load(RutaReportes + "\rptEstadoDeCuentaTV.rpt")

                'dsrpt.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
                Dim listatablas2 As New List(Of String)
                listatablas2.Add("PREFacturas_EDO_SOFTV")
                dsrpt = BaseII.ConsultaDS("Consulta_Codigo_OXXO_PorContrato", listatablas2)
                'rd.SetDataSource(dsrpt)
                DT = dsrpt.Tables(0)
                If LocTipoTarjeta = "V" Then
                    locimporte = CDec(DT.Rows(0)(31).ToString)
                Else
                    locimporte = CDec(DT.Rows(0)(4).ToString)
                End If
                UspTarjetasOXXO(CInt(DT.Rows(0)(0).ToString), CInt(DT.Rows(0)(1).ToString), CDate(DT.Rows(0)(2).ToString), locimporte, 0, DT.Rows(0)(10).ToString, DT.Rows(0)(14).ToString, DT.Rows(0)(16).ToString, DT.Rows(0)(17).ToString, DT.Rows(0)(18).ToString, DT.Rows(0)(19).ToString, DT.Rows(0)(20).ToString, DT.Rows(0)(21).ToString, DT.Rows(0)(23).ToString, DT.Rows(0)(27).ToString, LocTipoTarjeta, 1)

                BaseII.llamarReporteCentralizado(RutaReportes + "\rptOxxoSaltillo", dsrpt)
                'If chbGenerarConsecutivo.CheckState = CheckState.Checked Then
                '    rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, txtDirectorio.Text + "\" + contador.ToString() + "_" + i.ToString() + ".pdf")
                'Else
                '    rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, txtDirectorio.Text + "\" + i.ToString() + ".pdf")
                'End If

                contador = contador + 1
                GC.Collect()
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub UspTarjetasOXXO(ByVal PRMCLVRECIBO As Long, ByVal PRMCONTRATO As Long, ByVal PRMFECHAIMPRESION As Date, ByVal PRMIMPORTE As Decimal, ByVal PRMCANCELADA As Integer, _
                          ByVal PRMTXT As String, ByVal PRMNOMBRE As String, ByVal PRMDIRECCION As String, ByVal PRMCOLONIA As String, ByVal PRMUNICIPIO As String, _
                          ByVal PRMESTADO As String, ByVal PRMCP As String, ByVal PRMENTRECALLES As String, ByVal PRMTELEFONO As String, ByVal PRMCANTIDAD As String, _
                          ByVal PRMTIPO As String, ByVal PRMOP As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvRECIBO", SqlDbType.BigInt, PRMCLVRECIBO)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, PRMCONTRATO)
        BaseII.CreateMyParameter("@FechaImpresion", SqlDbType.Date, PRMFECHAIMPRESION)
        BaseII.CreateMyParameter("@importe", SqlDbType.Decimal, PRMIMPORTE)
        BaseII.CreateMyParameter("@cancelada", SqlDbType.Int, PRMCANCELADA)
        BaseII.CreateMyParameter("@TXT", SqlDbType.VarChar, PRMTXT, 150)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, PRMNOMBRE, 250)
        BaseII.CreateMyParameter("@Direccion", SqlDbType.VarChar, PRMDIRECCION, 250)
        BaseII.CreateMyParameter("@Colonia", SqlDbType.VarChar, PRMCOLONIA, 250)
        BaseII.CreateMyParameter("@Municipio", SqlDbType.VarChar, PRMUNICIPIO, 250)
        BaseII.CreateMyParameter("@Estado", SqlDbType.VarChar, PRMESTADO, 250)
        BaseII.CreateMyParameter("@CP", SqlDbType.VarChar, PRMCP, 250)
        BaseII.CreateMyParameter("@EntreCalle", SqlDbType.VarChar, PRMENTRECALLES, 250)
        BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, PRMTELEFONO, 150)
        BaseII.CreateMyParameter("@CantidadLetra", SqlDbType.VarChar, PRMCANTIDAD, 250)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, PRMTIPO, 1)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, PRMOP)
        BaseII.Inserta("UspTarjetasOXXO")
    End Sub

    Private Sub Button15_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        Dim frm2 As New FrmImpresionMasivaTarjetas
        frm2.Show()
    End Sub

    Private Sub Button16_Click_1(sender As System.Object, e As System.EventArgs) Handles Button16.Click
        Dim DT As New DataTable
        Me.OpenFileDialog1.FileName = ""
        Me.OpenFileDialog1.ShowDialog()

        If Me.OpenFileDialog1.FileName = "" Or Me.OpenFileDialog1.FileName = "OpenFileDialog1" Then
            MsgBox("No se Selecciono el Archivo")
        Else
            DT = TarjetasLayout(Me.OpenFileDialog1.FileName)
        End If

        Dim I, X, Y, Z, cont, A, B, C, CONTRATOTARJETAS As Integer
        Dim resp As Integer = 0
        Dim fila As Data.DataRow
        Dim RutaPDF As String
        obj_Impresora = CreateObject("WScript.Network")
        obj_Impresora.setdefaultprinter("Tarjetas")

        FolderBrowserDialog1.ShowDialog()

        RutaPDF = FolderBrowserDialog1.SelectedPath



        DstReporte = New DataSet
        DstReporteRojas = New DataSet
        DstReporteRojasTMP = New DataTable
        For I = 0 To DT.Rows.Count - 1
            LocOpTarjetas = 1
            LocTipoTarjeta = "V"
            GenerarEstadosLayOut(CLng(DT.Rows(I)(0).ToString), 1)
            'LocOpTarjetas = 1
            'LocTipoTarjeta = "R"
            'GenerarEstados(I, 1)
        Next
        If DstReporte.Tables.Count = 0 Then
            MsgBox("No se pueden imprimir las Tarjetas, los contratos tienen estatus diferente a instalado o es cortesía")
            Exit Sub
        End If
        Dim frmic As New FrmImprimirCentralizada()
        Dim rd As New ReportDocument()
        rd.Load(RutaReportes + "\rptOxxoSaltilloMasivoVerdes" + ".rpt")

        rd.SetDataSource(DstReporte)

        rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaPDF + "\Tarjetas_OXXO_Verdes.pdf")
        DT = New DataTable
        DT.Columns.Add("contratos")
        'DT.Merge(DstReporte.Tables(0))


        Dim BND As Boolean = False

        C = 0
        While (BND = False)
            CONTRATOTARJETAS = DstReporte.Tables(0).Rows(C)(1).ToString
            DT.Rows.Add(New Object() {CONTRATOTARJETAS})
            If C = DstReporte.Tables(0).Rows.Count - 1 Then
                A = DstReporte.Tables(0).Rows.Count
                resp = A Mod 3

                If resp <> 0 Then
                    A = A + 1
                    resp = A Mod 3
                    If resp <> 0 Then
                        DT.Rows.Add(New Object() {0})
                    End If
                    DT.Rows.Add(New Object() {0})
                    'DT.Rows.Add(New Object() {DstReporte.Tables(0).Rows(C + 1)(1).ToString})
                    BND = True
                Else
                    'DT.Rows.Add(New Object() {DstReporte.Tables(0).Rows(C + 1)(1).ToString})
                    BND = True
                End If
            End If
            C = C + 1
        End While



        frmic.rd = rd
        frmic.ShowDialog()
        DstReporte = New DataSet

        For I = 0 To DT.Rows.Count - 1
            'LocOpTarjetas = 0
            'LocTipoTarjeta = "V"
            'GenerarEstados(I, 0)
            LocOpTarjetas = 0
            LocTipoTarjeta = "R"
            GenerarEstadosLayOut(CLng(DT.Rows(I)(0).ToString), 0)
        Next
        DstReporteRojas.Merge(DstReporte)
        DstReporteRojas.Clear()
        cont = DstReporte.Tables(0).Rows.Count / 3
        X = 2
        For I = 0 To cont
            Y = X
            For Z = 0 To 2
                If Y < DstReporte.Tables(0).Rows.Count Then
                    DstReporteRojas.Tables(0).Rows.Add(DstReporte.Tables(0).Rows(Y).ItemArray)
                End If
                Y = Y - 1
            Next
            X = X + 3
        Next

        rd = New ReportDocument()
        rd.Load(RutaReportes + "\rptOxxoSaltilloMasivoRojas" + ".rpt")
        rd.SetDataSource(DstReporteRojas)

        rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaPDF + "\Tarjetas_OXXO_Rojas.pdf")

        frmic.rd = rd
        frmic.ShowDialog()
        RutaPDF = ""
    End Sub

    Private Function TarjetasLayout(ByVal prmruta As String) As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@RUTA", SqlDbType.VarChar, prmruta, 1000)
            TarjetasLayout = BaseII.ConsultaDT("UspTarjetasLayout")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub GenerarEstadosLayOut(ByVal oContrato As Long, ByVal prmOp As Integer)
        Try

            Dim max As Long
            Dim LocError As Integer
            Dim locimporte As Decimal
            Dim DT As New DataTable
            Dim DS As New DataSet

            Dim CORTESIA As New DataTable

            CORTESIA = UspValidarClienteConCortesia(oContrato)
            If CORTESIA.Rows(0)(0).ToString = "1" Then
                Exit Sub
            End If

            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
            Dim listatablas As New List(Of String)
            listatablas.Add("PREFacturas_EDO_SOFTV")
            DS = BaseII.ConsultaDS("Genera_Codigo_OXXO_PorContrato", listatablas)
            Dim rd As ReportDocument
            Dim i As Long = 0
            Dim contador As Integer = 1
            max = DS.Tables(0).Rows.Count
            Dim prefijo As String = ""
            Dim Txt As String = Nothing

            If max = 0 Then
                Exit Sub
                'Else
                '    If Len(DS.Tables(0).Rows(0)(4).ToString) = 0 Then
                '        Exit Sub
                '    End If
            End If
            'If Len(DS.Tables(0).Rows(0)(8).ToString) > 0 Then
            '    LocError = 1
            'Else
            '    LocError = 0
            'End If
            'If LocError = 1 Then
            '    Exit Sub
            'End If

            For Each dr As DataRow In DS.Tables(0).Rows
                i = Long.Parse(dr(0).ToString())
                Txt = dr(10).ToString()
                If Len(Txt) > 0 Then
                    AgregaImgCodeBarEstadoDeCuentaTV(oContrato, Txt, LocOpTarjetas)
                End If
                'rd = New ReportDocument()
                'rd.Load(RutaReportes + "\rptEstadoDeCuentaTV.rpt")

                'dsrpt.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
                Dim listatablas2 As New List(Of String)
                listatablas2.Add("PREFacturas_EDO_SOFTV")
                dsrpt = BaseII.ConsultaDS("Consulta_Codigo_OXXO_PorContrato", listatablas2)
                'rd.SetDataSource(dsrpt)
                DT = dsrpt.Tables(0)
                If LocTipoTarjeta = "V" Then
                    locimporte = CDec(DT.Rows(0)(31).ToString)
                Else
                    locimporte = CDec(DT.Rows(0)(4).ToString)
                End If
                UspTarjetasOXXO(CInt(DT.Rows(0)(0).ToString), CInt(DT.Rows(0)(1).ToString), CDate(DT.Rows(0)(2).ToString), locimporte, 0, DT.Rows(0)(10).ToString, DT.Rows(0)(14).ToString, DT.Rows(0)(16).ToString, DT.Rows(0)(17).ToString, DT.Rows(0)(18).ToString, DT.Rows(0)(19).ToString, DT.Rows(0)(20).ToString, DT.Rows(0)(21).ToString, DT.Rows(0)(23).ToString, DT.Rows(0)(27).ToString, LocTipoTarjeta, 1)

                DstReporte.Merge(dsrpt)

                'BaseII.llamarReporteCentralizado(RutaReportes + "\rptOxxoSaltillo", dsrpt)


                'If chbGenerarConsecutivo.CheckState = CheckState.Checked Then
                '    rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, txtDirectorio.Text + "\" + contador.ToString() + "_" + i.ToString() + ".pdf")
                'Else
                '    rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, txtDirectorio.Text + "\" + i.ToString() + ".pdf")
                'End If

                contador = contador + 1
                GC.Collect()
            Next
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub BtnHistorialTarjetas_Click_1(sender As System.Object, e As System.EventArgs) Handles BtnHistorialTarjetas.Click
        GloContrato = Me.CONTRATOLabel1.Text
        Dim FrmHT As New FrmHistorialTarjetas
        FrmHT.ShowDialog()
    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class