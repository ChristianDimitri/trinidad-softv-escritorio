﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCatalogoIP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.StatusLabel1 = New System.Windows.Forms.Label()
        Me.ClaveLabel = New System.Windows.Forms.Label()
        Me.Numero_telLabel = New System.Windows.Forms.Label()
        Me.Ult_Cliente_AsignadoLabel = New System.Windows.Forms.Label()
        Me.Ultima_FechaAsigLabel = New System.Windows.Forms.Label()
        Me.Ultima_Fecha_LiberacionLabel = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.cbStatus = New System.Windows.Forms.ComboBox()
        Me.tbClave = New System.Windows.Forms.TextBox()
        Me.tbIP = New System.Windows.Forms.TextBox()
        Me.tbUltimoClienteAsignado = New System.Windows.Forms.TextBox()
        Me.tbUltimaFechaLiberacion = New System.Windows.Forms.TextBox()
        Me.tbUltimaFechaAsignacion = New System.Windows.Forms.TextBox()
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.SuspendLayout()
        Me.CMBPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusLabel1
        '
        Me.StatusLabel1.AutoSize = True
        Me.StatusLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel1.Location = New System.Drawing.Point(55, 109)
        Me.StatusLabel1.Name = "StatusLabel1"
        Me.StatusLabel1.Size = New System.Drawing.Size(59, 16)
        Me.StatusLabel1.TabIndex = 16
        Me.StatusLabel1.Text = "Status :"
        '
        'ClaveLabel
        '
        Me.ClaveLabel.AutoSize = True
        Me.ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveLabel.Location = New System.Drawing.Point(58, 41)
        Me.ClaveLabel.Name = "ClaveLabel"
        Me.ClaveLabel.Size = New System.Drawing.Size(56, 16)
        Me.ClaveLabel.TabIndex = 3
        Me.ClaveLabel.Text = "Clave :"
        '
        'Numero_telLabel
        '
        Me.Numero_telLabel.AutoSize = True
        Me.Numero_telLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero_telLabel.Location = New System.Drawing.Point(14, 77)
        Me.Numero_telLabel.Name = "Numero_telLabel"
        Me.Numero_telLabel.Size = New System.Drawing.Size(96, 16)
        Me.Numero_telLabel.TabIndex = 5
        Me.Numero_telLabel.Text = "Dirección IP:"
        '
        'Ult_Cliente_AsignadoLabel
        '
        Me.Ult_Cliente_AsignadoLabel.AutoSize = True
        Me.Ult_Cliente_AsignadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ult_Cliente_AsignadoLabel.Location = New System.Drawing.Point(368, 115)
        Me.Ult_Cliente_AsignadoLabel.Name = "Ult_Cliente_AsignadoLabel"
        Me.Ult_Cliente_AsignadoLabel.Size = New System.Drawing.Size(182, 16)
        Me.Ult_Cliente_AsignadoLabel.TabIndex = 13
        Me.Ult_Cliente_AsignadoLabel.Text = "Ultimo Cliente Asignado :"
        '
        'Ultima_FechaAsigLabel
        '
        Me.Ultima_FechaAsigLabel.AutoSize = True
        Me.Ultima_FechaAsigLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ultima_FechaAsigLabel.Location = New System.Drawing.Point(338, 56)
        Me.Ultima_FechaAsigLabel.Name = "Ultima_FechaAsigLabel"
        Me.Ultima_FechaAsigLabel.Size = New System.Drawing.Size(212, 16)
        Me.Ultima_FechaAsigLabel.TabIndex = 9
        Me.Ultima_FechaAsigLabel.Text = "Ultima Fecha De Asignación :"
        '
        'Ultima_Fecha_LiberacionLabel
        '
        Me.Ultima_Fecha_LiberacionLabel.AutoSize = True
        Me.Ultima_Fecha_LiberacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ultima_Fecha_LiberacionLabel.Location = New System.Drawing.Point(342, 87)
        Me.Ultima_Fecha_LiberacionLabel.Name = "Ultima_Fecha_LiberacionLabel"
        Me.Ultima_Fecha_LiberacionLabel.Size = New System.Drawing.Size(208, 16)
        Me.Ultima_Fecha_LiberacionLabel.TabIndex = 11
        Me.Ultima_Fecha_LiberacionLabel.Text = "Ultima Fecha De Liberación :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(571, 187)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(147, 36)
        Me.Button5.TabIndex = 26
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Consulta_Cat_Num_TelefonoBindingNavigator
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.AddNewItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.CountItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem})
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Name = "Consulta_Cat_Num_TelefonoBindingNavigator"
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PositionItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Size = New System.Drawing.Size(727, 25)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.TabIndex = 25
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(61, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Name = "Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem"
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Size = New System.Drawing.Size(105, 22)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'CMBPanel1
        '
        Me.CMBPanel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel1.Controls.Add(Me.CMBTextBox1)
        Me.CMBPanel1.Controls.Add(Me.StatusLabel1)
        Me.CMBPanel1.Controls.Add(Me.cbStatus)
        Me.CMBPanel1.Controls.Add(Me.ClaveLabel)
        Me.CMBPanel1.Controls.Add(Me.tbClave)
        Me.CMBPanel1.Controls.Add(Me.Numero_telLabel)
        Me.CMBPanel1.Controls.Add(Me.tbIP)
        Me.CMBPanel1.Controls.Add(Me.tbUltimoClienteAsignado)
        Me.CMBPanel1.Controls.Add(Me.Ult_Cliente_AsignadoLabel)
        Me.CMBPanel1.Controls.Add(Me.tbUltimaFechaLiberacion)
        Me.CMBPanel1.Controls.Add(Me.Ultima_FechaAsigLabel)
        Me.CMBPanel1.Controls.Add(Me.Ultima_Fecha_LiberacionLabel)
        Me.CMBPanel1.Controls.Add(Me.tbUltimaFechaAsignacion)
        Me.CMBPanel1.Location = New System.Drawing.Point(12, 28)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(706, 153)
        Me.CMBPanel1.TabIndex = 24
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox1.Location = New System.Drawing.Point(347, 25)
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.Size = New System.Drawing.Size(344, 15)
        Me.CMBTextBox1.TabIndex = 18
        Me.CMBTextBox1.Text = "Fechas De"
        Me.CMBTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cbStatus
        '
        Me.cbStatus.DisplayMember = "Status"
        Me.cbStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbStatus.FormattingEnabled = True
        Me.cbStatus.Location = New System.Drawing.Point(116, 108)
        Me.cbStatus.Name = "cbStatus"
        Me.cbStatus.Size = New System.Drawing.Size(173, 24)
        Me.cbStatus.TabIndex = 17
        Me.cbStatus.ValueMember = "clv_status"
        '
        'tbClave
        '
        Me.tbClave.Enabled = False
        Me.tbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClave.Location = New System.Drawing.Point(116, 41)
        Me.tbClave.Name = "tbClave"
        Me.tbClave.Size = New System.Drawing.Size(80, 22)
        Me.tbClave.TabIndex = 4
        '
        'tbIP
        '
        Me.tbIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIP.Location = New System.Drawing.Point(116, 74)
        Me.tbIP.MaxLength = 15
        Me.tbIP.Name = "tbIP"
        Me.tbIP.Size = New System.Drawing.Size(173, 22)
        Me.tbIP.TabIndex = 6
        '
        'tbUltimoClienteAsignado
        '
        Me.tbUltimoClienteAsignado.Enabled = False
        Me.tbUltimoClienteAsignado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUltimoClienteAsignado.Location = New System.Drawing.Point(556, 112)
        Me.tbUltimoClienteAsignado.Name = "tbUltimoClienteAsignado"
        Me.tbUltimoClienteAsignado.Size = New System.Drawing.Size(135, 22)
        Me.tbUltimoClienteAsignado.TabIndex = 14
        '
        'tbUltimaFechaLiberacion
        '
        Me.tbUltimaFechaLiberacion.Enabled = False
        Me.tbUltimaFechaLiberacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUltimaFechaLiberacion.Location = New System.Drawing.Point(556, 84)
        Me.tbUltimaFechaLiberacion.Name = "tbUltimaFechaLiberacion"
        Me.tbUltimaFechaLiberacion.Size = New System.Drawing.Size(135, 22)
        Me.tbUltimaFechaLiberacion.TabIndex = 12
        '
        'tbUltimaFechaAsignacion
        '
        Me.tbUltimaFechaAsignacion.Enabled = False
        Me.tbUltimaFechaAsignacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUltimaFechaAsignacion.Location = New System.Drawing.Point(556, 53)
        Me.tbUltimaFechaAsignacion.Name = "tbUltimaFechaAsignacion"
        Me.tbUltimaFechaAsignacion.Size = New System.Drawing.Size(135, 22)
        Me.tbUltimaFechaAsignacion.TabIndex = 10
        '
        'FrmCatalogoIP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(727, 236)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Consulta_Cat_Num_TelefonoBindingNavigator)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Name = "FrmCatalogoIP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de IPs"
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.ResumeLayout(False)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PerformLayout()
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents tbClave As System.Windows.Forms.TextBox
    Friend WithEvents tbIP As System.Windows.Forms.TextBox
    Friend WithEvents tbUltimoClienteAsignado As System.Windows.Forms.TextBox
    Friend WithEvents tbUltimaFechaLiberacion As System.Windows.Forms.TextBox
    Friend WithEvents tbUltimaFechaAsignacion As System.Windows.Forms.TextBox
    Friend WithEvents StatusLabel1 As System.Windows.Forms.Label
    Friend WithEvents ClaveLabel As System.Windows.Forms.Label
    Friend WithEvents Numero_telLabel As System.Windows.Forms.Label
    Friend WithEvents Ult_Cliente_AsignadoLabel As System.Windows.Forms.Label
    Friend WithEvents Ultima_FechaAsigLabel As System.Windows.Forms.Label
    Friend WithEvents Ultima_Fecha_LiberacionLabel As System.Windows.Forms.Label
End Class
