Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.Text
Imports System.Xml

Public Class FrmPrefijos

    Private actualizar As Boolean = False
    Private nuevo As Boolean = False
    Private identity As Integer = 0

    Private Sub FrmPrefijos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.llenaGridPrefijos()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Me.nuevo = True
        Me.btnModificar.Enabled = False
        Me.lblAdvertencia.Visible = True
        Me.txtValores.Visible = True
        Me.btnAceptar.Visible = True
        Me.Refresh()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.gvPrefijos.Rows.Count >= 1 Then
            Me.actualizar = True
            Me.btnAgregar.Enabled = False
            Me.lblAdvertencia.Visible = True
            Dim row As Integer = Convert.ToInt32(Me.gvPrefijos.CurrentRow.Index.ToString)
            Me.txtValores.Visible = True
            Me.btnAceptar.Visible = True
            Me.txtValores.Text = Me.gvPrefijos.Rows(row).Cells("Prefijo").Value.ToString
            Me.identity = Convert.ToInt32(Me.gvPrefijos.Rows(row).Cells("Clv_Prefijo").Value.ToString)
            Me.Refresh()
        Else
            MsgBox("No hay datos para modificar", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim msj As String
        Dim prefijo As String = Me.txtValores.Text.ToString
        If Me.actualizar = True Then
            msj = Me.Abc(Me.identity, prefijo, 2)
            Me.actualizar = False
            Me.btnAgregar.Enabled = True
        End If
        If nuevo = True Then
            msj = Me.Abc(Me.identity, prefijo, 1)
            Me.nuevo = False
            Me.btnModificar.Enabled = True
        End If
        Me.lblAdvertencia.Visible = False
        Me.llenaGridPrefijos()
        Me.txtValores.Visible = False
        Me.btnAceptar.Visible = False
        Me.txtValores.Text = ""
        MsgBox(msj, MsgBoxStyle.Information)
        Me.Refresh()
    End Sub

    Private Sub btnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitar.Click
        Dim row As Integer = Convert.ToInt32(Me.gvPrefijos.CurrentRow.Index.ToString)
        Me.identity = Convert.ToInt32(Me.gvPrefijos.Rows(row).Cells("Clv_Prefijo").Value.ToString)
        Dim prefijo As String = Me.gvPrefijos.Rows(row).Cells("Prefijo").Value.ToString()
        Dim valor As String = Me.Abc(Me.identity, prefijo, 3)
        MsgBox(valor, MsgBoxStyle.Information)
        Me.gvPrefijos.Rows.Remove(Me.gvPrefijos.Rows(row))
        Me.llenaGridPrefijos()
        Me.Refresh()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub llenaGridPrefijos()
        Dim conexion As New SqlConnection(MiConexion)
        Dim tabla As DataTable = New DataTable
        Dim com As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim bs As BindingSource = New BindingSource
        Try
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            com.CommandText = "Select * from Prefijos"
            com.Connection = conexion
            com.Connection.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
            If tabla.Rows.Count >= 1 Then
                Me.gvPrefijos.DataSource = bs
                Me.gvPrefijos.Columns("Clv_Prefijo").Visible = False
                Me.gvPrefijos.Columns("Prefijo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                Me.gvPrefijos.Rows(0).Cells("Prefijo").Selected = True
            Else
                Return
            End If
        Catch ex As Exception

        Finally
            com.Connection.Close()
            com.Dispose()
            tabla.Dispose()
        End Try
    End Sub

    Private Function Abc(ByVal indice As Integer, ByVal prefijo As String, ByVal op As Integer) As String
        Dim regresa As String
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("ModificaPrefijos", con)
        Try
            com.CommandType = CommandType.StoredProcedure
            com.Connection.Open()
            com.Parameters.Add(New SqlParameter("@Opcion", SqlDbType.Int))
            com.Parameters("@Opcion").Value = op
            com.Parameters.Add(New SqlParameter("@Prefijo", SqlDbType.VarChar, 10))
            com.Parameters("@Prefijo").Value = prefijo
            com.Parameters.Add(New SqlParameter("@Clv_Prefijo", SqlDbType.Int))
            com.Parameters("@Clv_Prefijo").Value = indice
            com.Parameters.Add(New SqlParameter("@Msj", SqlDbType.VarChar, 250))
            com.Parameters("@Msj").Value = ""
            com.Parameters("@Msj").Direction = ParameterDirection.Output
            com.ExecuteNonQuery()
            regresa = com.Parameters("@Msj").Value.ToString()
        Catch ex As Exception
            regresa = ex.Message.ToString
        Finally
            com.Connection.Close()
            com.Dispose()
        End Try
        Return regresa
    End Function
    
End Class