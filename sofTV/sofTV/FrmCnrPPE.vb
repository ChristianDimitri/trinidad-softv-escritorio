Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmCnrPPE

    Private Sub FrmCnrPPE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dtpFechaE.Value = DateTime.Today
        Me.dtpFechaH.Value = DateTime.Today
        Me.llenaGrid(1, 0, 0, "", 0, 0, DateTime.Today, DateTime.Today)
        Me.cbResultado.Items.Insert(0, "Pendiente")
        Me.cbResultado.Items.Insert(1, "Procesada")
        Me.cbResultado.Items.Insert(2, "Transación Fallida")

        UspDesactivaBotones(Me, Me.Name)

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

    End Sub

    Private Sub btnConsecutivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsecutivo.Click
        If IsNumeric(Me.txtConsecutivo.Text) = False Then Me.txtConsecutivo.Text = 0
        Dim cons As Long = Long.Parse(Me.txtConsecutivo.Text.ToString.Trim)
        Me.llenaGrid(8, cons, 0, "", 0, 0, DateTime.Today, DateTime.Today)
        Me.limpiaTodo()
    End Sub

    Private Sub btnContrato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContrato.Click
        If IsNumeric(Me.txtContrato.Text) = False Then Me.txtConsecutivo.Text = 0
        Dim contrato As Long = Long.Parse(Me.txtContrato.Text.ToString.Trim)
        Me.llenaGrid(2, 0, contrato, "", 0, 0, DateTime.Today, DateTime.Today)
        Me.limpiaTodo()
    End Sub

    Private Sub btnMac_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMac.Click
        Me.llenaGrid(3, 0, 0, Me.txtMac.Text.ToString, 0, 0, DateTime.Today, DateTime.Today)
        Me.limpiaTodo()
    End Sub

    Private Sub btnResultado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResultado.Click
        Dim resultado As Integer
        Dim text As String = Me.cbResultado.SelectedItem.ToString
        If text.Equals("Pendiente") Then
            resultado = 0
        ElseIf text.Equals("Procesada") Then
            resultado = 1
        ElseIf text.Equals("Transación Fallida") Then
            resultado = 2
        End If
        Me.llenaGrid(4, 0, 0, "", resultado, 0, DateTime.Today, DateTime.Today)
        Me.limpiaTodo()
    End Sub

    Private Sub btnClv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClv.Click
        If IsNumeric(Me.txtOrden.Text) = False Then Me.txtConsecutivo.Text = 0
        Dim clv As Long = Long.Parse(Me.txtOrden.Text.ToString.Trim)
        Me.llenaGrid(5, 0, 0, "", 0, clv, DateTime.Today, DateTime.Today)
        Me.limpiaTodo()
    End Sub

    Private Sub btnFechaE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFechaE.Click
        Me.llenaGrid(6, 0, 0, "", 0, 0, Me.dtpFechaE.Value, DateTime.Today)
        Me.limpiaTodo()
    End Sub

    Private Sub btnFechaH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFechaH.Click
        Me.llenaGrid(7, 0, 0, "", 0, 0, DateTime.Today, Me.dtpFechaH.Value)
        Me.limpiaTodo()
    End Sub

    Private Sub btnModifica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModifica.Click
        Me.dameDatosSeleccionados()
        FrmModificaCnrPPE.Show()
        Me.limpiaTodo()

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
   
    Private Sub gvPPE_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gvPPE.SelectionChanged
        Dim row As Integer = Convert.ToInt32(Me.gvPPE.CurrentRow.Index.ToString)
        Me.lblClv.Text = Me.gvPPE.Rows(row).Cells("Orden").Value.ToString
        Me.lblComando.Text = Me.gvPPE.Rows(row).Cells("Comando").Value.ToString
        Me.lblConsecutivo.Text = Me.gvPPE.Rows(row).Cells("Consecutivo").Value.ToString
        Me.lblContrato.Text = Me.gvPPE.Rows(row).Cells("Contrato").Value.ToString
        Me.lblDescripcion.Text = Me.gvPPE.Rows(row).Cells("Descripcion").Value.ToString
        Me.lblFechaE.Text = Me.gvPPE.Rows(row).Cells("Fecha Ejecucion").Value.ToString
        Me.lblFechaH.Text = Me.gvPPE.Rows(row).Cells("Fecha Habilitar").Value.ToString
        Me.lblMac.Text = Me.gvPPE.Rows(row).Cells("MAC Address").Value.ToString
        Me.lblPaquete.Text = Me.gvPPE.Rows(row).Cells("Paquete").Value.ToString
        Me.lblResultado.Text = Me.gvPPE.Rows(row).Cells("Resultado").Value.ToString
    End Sub

    Private Sub txtConsecutivo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtConsecutivo.KeyPress
        If e.KeyChar = Chr(13) Then
            Me.btnConsecutivo_Click(Nothing, e)
        End If
    End Sub

    Private Sub txtContrato_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContrato.KeyPress
        If e.KeyChar = Chr(13) Then
            Me.btnContrato_Click(Nothing, e)
        End If
    End Sub

    Private Sub txtMac_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMac.KeyPress
        If e.KeyChar = Chr(13) Then
            Me.btnMac_Click(Nothing, e)
        End If
    End Sub

    Private Sub txtOrden_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOrden.KeyPress
        If e.KeyChar = Chr(13) Then
            Me.btnClv_Click(Nothing, e)
        End If
    End Sub

    Public Sub llenaGrid(ByVal op As Integer, ByVal cons As Int64, ByVal contrato As Int64, ByVal mac As String, ByVal resultado As Integer, ByVal clvOrden As Int64, ByVal fechaE As DateTime, ByVal fechaH As DateTime)
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("MuestraCNR_PPE", con)
        com.CommandType = CommandType.StoredProcedure
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim bs As BindingSource = New BindingSource
        Dim tabla As DataTable = New DataTable
        com.Parameters.Add(New SqlParameter("@Op", SqlDbType.Int))
        com.Parameters("@Op").Value = op
        com.Parameters.Add(New SqlParameter("@Consecutivo", SqlDbType.BigInt))
        com.Parameters("@Consecutivo").Value = cons
        com.Parameters.Add(New SqlParameter("@Contrato", SqlDbType.BigInt))
        com.Parameters("@Contrato").Value = contrato
        com.Parameters.Add(New SqlParameter("@Mac", SqlDbType.NVarChar, 50))
        com.Parameters("@Mac").Value = mac
        com.Parameters.Add(New SqlParameter("@Resultado", SqlDbType.Int))
        com.Parameters("@Resultado").Value = resultado
        com.Parameters.Add(New SqlParameter("@Clv_Orden", SqlDbType.BigInt))
        com.Parameters("@Clv_Orden").Value = clvOrden
        com.Parameters.Add(New SqlParameter("@FechaEjec", SqlDbType.DateTime))
        com.Parameters("@FechaEjec").Value = fechaE
        com.Parameters.Add(New SqlParameter("@FechaHabilitar", SqlDbType.DateTime))
        com.Parameters("@FechaHabilitar").Value = fechaH
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
        If tabla.Rows.Count >= 1 Then
            Me.gvPPE.DataSource = bs
        Else
            MsgBox("No se encontraron resultados", MsgBoxStyle.Information)
            Me.llenaGrid(1, 0, 0, "", 0, 0, DateTime.Today, DateTime.Today)
        End If
    End Sub

    Private Sub dameDatosSeleccionados()
        FrmModificaCnrPPE.lblComando.Text = Me.lblComando.Text
        FrmModificaCnrPPE.lblResultado.Text = Me.lblResultado.Text
        FrmModificaCnrPPE.txtContrato.Text = Me.lblContrato.Text
        FrmModificaCnrPPE.txtMac.Text = Me.lblMac.Text
        FrmModificaCnrPPE.txtPaquete.Text = Me.lblPaquete.Text
        FrmModificaCnrPPE.antFechaH = Me.lblFechaH.Text.ToString.TrimEnd
        FrmModificaCnrPPE.Consecutivo = Long.Parse(Me.lblConsecutivo.Text.ToString)
    End Sub

    Private Sub limpiaTodo()
        Me.txtConsecutivo.Text = ""
        Me.txtContrato.Text = ""
        Me.txtMac.Text = ""
        Me.txtOrden.Text = ""
        Me.dtpFechaE.Value = DateTime.Today
        Me.dtpFechaH.Value = DateTime.Today
    End Sub

    Private Sub cbResultado_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbResultado.SelectedIndexChanged

    End Sub
End Class