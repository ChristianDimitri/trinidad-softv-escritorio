Imports System.Data.SqlClient
Public Class FrmTelsi
    Private Sub Inserta_Rel_Telefono_Report()

        Dim CON As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            CON.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "Inserta_Rel_Telefono_Report"
                .CommandTimeout = 0
                .Connection = CON
                .CommandType = CommandType.StoredProcedure
                '@clv_session bigint,@tel bit,@tel1 bit
                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = LocClv_session
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("@tel", SqlDbType.Bit)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = GloLocTel
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@tel1", SqlDbType.Bit)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = LocTodos
                .Parameters.Add(prm3)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub FrmTelsi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            'Me.CheckBox2.CheckState = CheckState.Unchecked
            GloLocTel = True
        Else
            GloLocTel = False
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            'Me.CheckBox1.CheckState = CheckState.Unchecked
            GloLocTel = False
        Else
            GloLocTel = True
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.CheckBox1.CheckState = CheckState.Checked And Me.CheckBox2.CheckState = CheckState.Checked Then
            LocTodos = 1
        Else
            LocTodos = 0
        End If
        If LocOp <> 7 Then
            If Me.CheckBox1.CheckState = CheckState.Unchecked And Me.CheckBox2.CheckState = CheckState.Unchecked Then
                MsgBox("Seleccione al menos opcion", MsgBoxStyle.Information)
                Exit Sub
            Else
                If bnd1 = True Then
                    FrmSelPeriodo.Show()
                Else
                    FrmSelPeriodo.Show()
                End If
            End If
        ElseIf LocOp = 7 Then
            Inserta_Rel_Telefono_Report()
            If IdSistema = "SA" Then
                bndAvisos2 = True
            Else
                FrmSelRecord.Show()
            End If
        End If
        Me.Close()
    End Sub
End Class