﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic
Public Class FrmImprimirQueja
    Private customersByCityReport As ReportDocument
    Private Sub FrmImprimirQueja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If GlobndImprimirQueja Then
            GlobndImprimirQueja = False
            ConfigureCrystalReports(0, "Imprimir Queja")
        End If
        If GlobndImprimirRobo Then
            GlobndImprimirRobo = False
            ConfigureCrystalReportsRobo("Imprimir Reportes de Robo de Señal")
        End If
    End Sub

    Private Sub ConfigureCrystalReportsRobo(ByVal Titulo As String)

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim CON As New SqlConnection(MiConexion)

        Me.Text = Titulo
        Dim reportPath As String = Nothing

        reportPath = RutaReportes + "\ReporteRoboSenal.rpt"

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.Int, LocClv_session)

        Dim lp As List(Of String) = New List(Of String)
        lp.Add("ReporteRoboSenal")

        Dim ds As DataSet = BaseII.ConsultaDS("ReporteRoboSenal", lp)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.Int, LocClv_session)
        BaseII.CreateMyParameter("@nombresCiudades", ParameterDirection.Output, SqlDbType.VarChar, 200)
        BaseII.ProcedimientoOutPut("ObtenerCiudadesFiltro")
        Dim nomCiudades As String = BaseII.dicoPar("@nombresCiudades").ToString

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.Int, LocClv_session)
        BaseII.CreateMyParameter("@nombresColonias", ParameterDirection.Output, SqlDbType.VarChar, 200)
        BaseII.ProcedimientoOutPut("ObtenerColoniasFiltro")
        Dim nomColonias As String = BaseII.dicoPar("@nombresColonias").ToString



        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadesFiltro").Text = "'" & nomCiudades & "'"
        customersByCityReport.DataDefinition.FormulaFields("ColoniasFiltro").Text = "'" & nomColonias & "'"
        'CON.Open()
        'Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
        'Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora_Ordenes, a)
        'CON.Close()

        'If a = 1 Then
        '    MsgBox("No se ha asignado una impresora para Quejas.")
        '    Exit Sub
        'Else
        '    customersByCityReport.PrintOptions.PrinterName = Impresora_Ordenes
        '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'End If
        CrystalReportViewer1.ReportSource = customersByCityReport



        customersByCityReport = Nothing

    End Sub

    Private Sub ConfigureCrystalReports(ByVal op As Integer, ByVal Titulo As String)

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"
        Dim a As Integer = 0
        Dim CON As New SqlConnection(MiConexion)

        Dim Impresora_Ordenes As String = Nothing
        Dim mySelectFormula As String = Titulo
        Dim OpOrdenar As String = "0"
        Me.Text = Titulo
        Dim reportPath As String = Nothing

        reportPath = RutaReportes + "\ReporteQuejas.rpt"

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, 1)
        BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, 0)
        BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, 0)
        BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, 0)
        BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, 0)
        BaseII.CreateMyParameter("@op6", SqlDbType.SmallInt, 0)
        BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, True)
        BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, False)
        BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, False)
        BaseII.CreateMyParameter("@StatusProc", SqlDbType.Bit, False)
        BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, FrmQuejasAntTel.Clv_quejaTextBox.Text)
        BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, FrmQuejasAntTel.Clv_quejaTextBox.Text)
        BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, Fec1Ini)
        BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, Fec1Fin)
        BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, Fec2Ini)
        BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, Fec2Fin)
        BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, OpOrdenar)
        BaseII.CreateMyParameter("@clv_Depto", SqlDbType.VarChar, 0)
        BaseII.CreateMyParameter("@Op7", SqlDbType.SmallInt, 0)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@clvProblema", SqlDbType.BigInt, 0)


        Dim lp As List(Of String) = New List(Of String)
        lp.Add("ReporteAreaTecnicaQuejas1")
        lp.Add("DameDatosGenerales_2")
        lp.Add("ClientesConElMismoPoste")
        Dim ds As DataSet = BaseII.ConsultaDS("ReporteQueja", lp)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)

        'CON.Open()
        'Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
        'Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora_Ordenes, a)
        'CON.Close()

        'If a = 1 Then
        '    MsgBox("No se ha asignado una impresora para Quejas.")
        '    Exit Sub
        'Else
        '    customersByCityReport.PrintOptions.PrinterName = Impresora_Ordenes
        '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'End If
        CrystalReportViewer1.ReportSource = customersByCityReport



        customersByCityReport = Nothing

    End Sub
End Class