<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTap
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim IngenieriaLabel As System.Windows.Forms.Label
        Dim NoCasasLabel As System.Windows.Forms.Label
        Dim NoNegociosLabel As System.Windows.Forms.Label
        Dim NoLotesLabel As System.Windows.Forms.Label
        Dim NoServiciosLabel As System.Windows.Forms.Label
        Dim FrenteANumeroLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim NombreLabel1 As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTap))
        Me.gbClave = New System.Windows.Forms.GroupBox()
        Me.cbPoste = New System.Windows.Forms.ComboBox()
        Me.cbSector = New System.Windows.Forms.ComboBox()
        Me.Clv_SectorTextBox1 = New System.Windows.Forms.TextBox()
        Me.tbClave = New System.Windows.Forms.TextBox()
        Me.tbIngenieria = New System.Windows.Forms.TextBox()
        Me.tbSalidas = New System.Windows.Forms.TextBox()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.gbDetalleClave = New System.Windows.Forms.GroupBox()
        Me.cbCalle = New System.Windows.Forms.ComboBox()
        Me.Clv_CalleTextBox1 = New System.Windows.Forms.TextBox()
        Me.cbColonia = New System.Windows.Forms.ComboBox()
        Me.Clv_ColoniaTextBox1 = New System.Windows.Forms.TextBox()
        Me.tbNoNegocios = New System.Windows.Forms.TextBox()
        Me.tbFrenteANumero = New System.Windows.Forms.TextBox()
        Me.tbNoServicios = New System.Windows.Forms.TextBox()
        Me.tbNoLotes = New System.Windows.Forms.TextBox()
        Me.tbNoCasas = New System.Windows.Forms.TextBox()
        Me.Clv_CalleTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_SectorTextBox = New System.Windows.Forms.TextBox()
        Me.ConsecutivoAux = New System.Windows.Forms.TextBox()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnTap = New System.Windows.Forms.BindingNavigator(Me.components)
        ClaveLabel = New System.Windows.Forms.Label()
        IngenieriaLabel = New System.Windows.Forms.Label()
        NoCasasLabel = New System.Windows.Forms.Label()
        NoNegociosLabel = New System.Windows.Forms.Label()
        NoLotesLabel = New System.Windows.Forms.Label()
        NoServiciosLabel = New System.Windows.Forms.Label()
        FrenteANumeroLabel = New System.Windows.Forms.Label()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        NombreLabel1 = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.gbClave.SuspendLayout()
        Me.gbDetalleClave.SuspendLayout()
        CType(Me.bnTap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTap.SuspendLayout()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.Location = New System.Drawing.Point(70, 31)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(50, 15)
        ClaveLabel.TabIndex = 2
        ClaveLabel.Text = "Clave :"
        '
        'IngenieriaLabel
        '
        IngenieriaLabel.AutoSize = True
        IngenieriaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IngenieriaLabel.Location = New System.Drawing.Point(409, 65)
        IngenieriaLabel.Name = "IngenieriaLabel"
        IngenieriaLabel.Size = New System.Drawing.Size(91, 15)
        IngenieriaLabel.TabIndex = 8
        IngenieriaLabel.Text = "Tap / Salidas"
        '
        'NoCasasLabel
        '
        NoCasasLabel.AutoSize = True
        NoCasasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NoCasasLabel.Location = New System.Drawing.Point(141, 177)
        NoCasasLabel.Name = "NoCasasLabel"
        NoCasasLabel.Size = New System.Drawing.Size(86, 15)
        NoCasasLabel.TabIndex = 16
        NoCasasLabel.Text = "# de Casas :"
        '
        'NoNegociosLabel
        '
        NoNegociosLabel.AutoSize = True
        NoNegociosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NoNegociosLabel.Location = New System.Drawing.Point(118, 203)
        NoNegociosLabel.Name = "NoNegociosLabel"
        NoNegociosLabel.Size = New System.Drawing.Size(107, 15)
        NoNegociosLabel.TabIndex = 18
        NoNegociosLabel.Text = "# de Negocios :"
        '
        'NoLotesLabel
        '
        NoLotesLabel.AutoSize = True
        NoLotesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NoLotesLabel.Location = New System.Drawing.Point(147, 229)
        NoLotesLabel.Name = "NoLotesLabel"
        NoLotesLabel.Size = New System.Drawing.Size(82, 15)
        NoLotesLabel.TabIndex = 20
        NoLotesLabel.Text = "# de Lotes :"
        '
        'NoServiciosLabel
        '
        NoServiciosLabel.AutoSize = True
        NoServiciosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NoServiciosLabel.Location = New System.Drawing.Point(120, 255)
        NoServiciosLabel.Name = "NoServiciosLabel"
        NoServiciosLabel.Size = New System.Drawing.Size(105, 15)
        NoServiciosLabel.TabIndex = 22
        NoServiciosLabel.Text = "# de Servicios :"
        '
        'FrenteANumeroLabel
        '
        FrenteANumeroLabel.AutoSize = True
        FrenteANumeroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FrenteANumeroLabel.Location = New System.Drawing.Point(100, 281)
        FrenteANumeroLabel.Name = "FrenteANumeroLabel"
        FrenteANumeroLabel.Size = New System.Drawing.Size(127, 15)
        FrenteANumeroLabel.TabIndex = 24
        FrenteANumeroLabel.Text = "Frente al Número :"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.Location = New System.Drawing.Point(18, 69)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(81, 15)
        Clv_TxtLabel.TabIndex = 11
        Clv_TxtLabel.Text = "Cuadrante :"
        '
        'NombreLabel1
        '
        NombreLabel1.AutoSize = True
        NombreLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel1.Location = New System.Drawing.Point(95, 34)
        NombreLabel1.Name = "NombreLabel1"
        NombreLabel1.Size = New System.Drawing.Size(71, 15)
        NombreLabel1.TabIndex = 26
        NombreLabel1.Text = "Barrio(s) :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.Location = New System.Drawing.Point(95, 94)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(65, 15)
        NOMBRELabel.TabIndex = 28
        NOMBRELabel.Text = "Calle(s) :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.Location = New System.Drawing.Point(208, 69)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(39, 15)
        Label1.TabIndex = 17
        Label1.Text = "Tap :"
        '
        'gbClave
        '
        Me.gbClave.Controls.Add(Label1)
        Me.gbClave.Controls.Add(Me.cbPoste)
        Me.gbClave.Controls.Add(Me.cbSector)
        Me.gbClave.Controls.Add(Me.Clv_SectorTextBox1)
        Me.gbClave.Controls.Add(Clv_TxtLabel)
        Me.gbClave.Controls.Add(Me.tbClave)
        Me.gbClave.Controls.Add(ClaveLabel)
        Me.gbClave.Controls.Add(IngenieriaLabel)
        Me.gbClave.Controls.Add(Me.tbIngenieria)
        Me.gbClave.Controls.Add(Me.tbSalidas)
        Me.gbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbClave.Location = New System.Drawing.Point(12, 46)
        Me.gbClave.Name = "gbClave"
        Me.gbClave.Size = New System.Drawing.Size(533, 135)
        Me.gbClave.TabIndex = 0
        Me.gbClave.TabStop = False
        Me.gbClave.Text = "Clave Técnica"
        '
        'cbPoste
        '
        Me.cbPoste.DisplayMember = "Descripcion"
        Me.cbPoste.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPoste.FormattingEnabled = True
        Me.cbPoste.Location = New System.Drawing.Point(211, 88)
        Me.cbPoste.Name = "cbPoste"
        Me.cbPoste.Size = New System.Drawing.Size(164, 23)
        Me.cbPoste.TabIndex = 16
        Me.cbPoste.ValueMember = "Id"
        '
        'cbSector
        '
        Me.cbSector.DisplayMember = "Descripcion"
        Me.cbSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSector.FormattingEnabled = True
        Me.cbSector.Location = New System.Drawing.Point(21, 88)
        Me.cbSector.Name = "cbSector"
        Me.cbSector.Size = New System.Drawing.Size(170, 23)
        Me.cbSector.TabIndex = 0
        Me.cbSector.ValueMember = "Clv_Sector"
        '
        'Clv_SectorTextBox1
        '
        Me.Clv_SectorTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SectorTextBox1.Location = New System.Drawing.Point(116, 88)
        Me.Clv_SectorTextBox1.Name = "Clv_SectorTextBox1"
        Me.Clv_SectorTextBox1.ReadOnly = True
        Me.Clv_SectorTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.Clv_SectorTextBox1.TabIndex = 13
        Me.Clv_SectorTextBox1.TabStop = False
        '
        'tbClave
        '
        Me.tbClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClave.Location = New System.Drawing.Point(132, 21)
        Me.tbClave.Name = "tbClave"
        Me.tbClave.ReadOnly = True
        Me.tbClave.Size = New System.Drawing.Size(325, 21)
        Me.tbClave.TabIndex = 3
        Me.tbClave.TabStop = False
        Me.tbClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbIngenieria
        '
        Me.tbIngenieria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbIngenieria.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIngenieria.Location = New System.Drawing.Point(392, 88)
        Me.tbIngenieria.Name = "tbIngenieria"
        Me.tbIngenieria.Size = New System.Drawing.Size(51, 21)
        Me.tbIngenieria.TabIndex = 1
        '
        'tbSalidas
        '
        Me.tbSalidas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbSalidas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSalidas.Location = New System.Drawing.Point(449, 88)
        Me.tbSalidas.Name = "tbSalidas"
        Me.tbSalidas.Size = New System.Drawing.Size(61, 21)
        Me.tbSalidas.TabIndex = 2
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(415, 518)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 10
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'gbDetalleClave
        '
        Me.gbDetalleClave.Controls.Add(Me.cbCalle)
        Me.gbDetalleClave.Controls.Add(Me.Clv_CalleTextBox1)
        Me.gbDetalleClave.Controls.Add(NOMBRELabel)
        Me.gbDetalleClave.Controls.Add(Me.cbColonia)
        Me.gbDetalleClave.Controls.Add(Me.Clv_ColoniaTextBox1)
        Me.gbDetalleClave.Controls.Add(NombreLabel1)
        Me.gbDetalleClave.Controls.Add(Me.tbNoNegocios)
        Me.gbDetalleClave.Controls.Add(Me.tbFrenteANumero)
        Me.gbDetalleClave.Controls.Add(FrenteANumeroLabel)
        Me.gbDetalleClave.Controls.Add(Me.tbNoServicios)
        Me.gbDetalleClave.Controls.Add(NoServiciosLabel)
        Me.gbDetalleClave.Controls.Add(Me.tbNoLotes)
        Me.gbDetalleClave.Controls.Add(NoLotesLabel)
        Me.gbDetalleClave.Controls.Add(NoNegociosLabel)
        Me.gbDetalleClave.Controls.Add(Me.tbNoCasas)
        Me.gbDetalleClave.Controls.Add(NoCasasLabel)
        Me.gbDetalleClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDetalleClave.Location = New System.Drawing.Point(12, 187)
        Me.gbDetalleClave.Name = "gbDetalleClave"
        Me.gbDetalleClave.Size = New System.Drawing.Size(533, 322)
        Me.gbDetalleClave.TabIndex = 3
        Me.gbDetalleClave.TabStop = False
        Me.gbDetalleClave.Text = "Datos del la Clave Técnica :"
        '
        'cbCalle
        '
        Me.cbCalle.DisplayMember = "Nombre"
        Me.cbCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCalle.FormattingEnabled = True
        Me.cbCalle.Location = New System.Drawing.Point(98, 113)
        Me.cbCalle.Name = "cbCalle"
        Me.cbCalle.Size = New System.Drawing.Size(324, 23)
        Me.cbCalle.TabIndex = 4
        Me.cbCalle.ValueMember = "Clv_Calle"
        '
        'Clv_CalleTextBox1
        '
        Me.Clv_CalleTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleTextBox1.Location = New System.Drawing.Point(389, 115)
        Me.Clv_CalleTextBox1.Name = "Clv_CalleTextBox1"
        Me.Clv_CalleTextBox1.ReadOnly = True
        Me.Clv_CalleTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.Clv_CalleTextBox1.TabIndex = 30
        Me.Clv_CalleTextBox1.TabStop = False
        '
        'cbColonia
        '
        Me.cbColonia.DisplayMember = "Nombre"
        Me.cbColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbColonia.FormattingEnabled = True
        Me.cbColonia.Location = New System.Drawing.Point(100, 53)
        Me.cbColonia.Name = "cbColonia"
        Me.cbColonia.Size = New System.Drawing.Size(322, 23)
        Me.cbColonia.TabIndex = 3
        Me.cbColonia.ValueMember = "Clv_Colonia"
        '
        'Clv_ColoniaTextBox1
        '
        Me.Clv_ColoniaTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ColoniaTextBox1.Location = New System.Drawing.Point(330, 53)
        Me.Clv_ColoniaTextBox1.Name = "Clv_ColoniaTextBox1"
        Me.Clv_ColoniaTextBox1.ReadOnly = True
        Me.Clv_ColoniaTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.Clv_ColoniaTextBox1.TabIndex = 28
        Me.Clv_ColoniaTextBox1.TabStop = False
        '
        'tbNoNegocios
        '
        Me.tbNoNegocios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNoNegocios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoNegocios.Location = New System.Drawing.Point(251, 197)
        Me.tbNoNegocios.Name = "tbNoNegocios"
        Me.tbNoNegocios.Size = New System.Drawing.Size(100, 21)
        Me.tbNoNegocios.TabIndex = 6
        '
        'tbFrenteANumero
        '
        Me.tbFrenteANumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbFrenteANumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbFrenteANumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFrenteANumero.Location = New System.Drawing.Point(251, 275)
        Me.tbFrenteANumero.Name = "tbFrenteANumero"
        Me.tbFrenteANumero.Size = New System.Drawing.Size(100, 21)
        Me.tbFrenteANumero.TabIndex = 9
        '
        'tbNoServicios
        '
        Me.tbNoServicios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNoServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoServicios.Location = New System.Drawing.Point(251, 249)
        Me.tbNoServicios.Name = "tbNoServicios"
        Me.tbNoServicios.Size = New System.Drawing.Size(100, 21)
        Me.tbNoServicios.TabIndex = 8
        '
        'tbNoLotes
        '
        Me.tbNoLotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNoLotes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoLotes.Location = New System.Drawing.Point(251, 223)
        Me.tbNoLotes.Name = "tbNoLotes"
        Me.tbNoLotes.Size = New System.Drawing.Size(100, 21)
        Me.tbNoLotes.TabIndex = 7
        '
        'tbNoCasas
        '
        Me.tbNoCasas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNoCasas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoCasas.Location = New System.Drawing.Point(251, 171)
        Me.tbNoCasas.Name = "tbNoCasas"
        Me.tbNoCasas.Size = New System.Drawing.Size(100, 21)
        Me.tbNoCasas.TabIndex = 5
        '
        'Clv_CalleTextBox
        '
        Me.Clv_CalleTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleTextBox.Location = New System.Drawing.Point(618, 662)
        Me.Clv_CalleTextBox.Name = "Clv_CalleTextBox"
        Me.Clv_CalleTextBox.ReadOnly = True
        Me.Clv_CalleTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_CalleTextBox.TabIndex = 15
        Me.Clv_CalleTextBox.TabStop = False
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(602, 662)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_ColoniaTextBox.TabIndex = 13
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(554, 662)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.ReadOnly = True
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(10, 22)
        Me.ConsecutivoTextBox.TabIndex = 7
        Me.ConsecutivoTextBox.TabStop = False
        '
        'Clv_SectorTextBox
        '
        Me.Clv_SectorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SectorTextBox.Location = New System.Drawing.Point(586, 662)
        Me.Clv_SectorTextBox.Name = "Clv_SectorTextBox"
        Me.Clv_SectorTextBox.ReadOnly = True
        Me.Clv_SectorTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_SectorTextBox.TabIndex = 5
        Me.Clv_SectorTextBox.TabStop = False
        '
        'ConsecutivoAux
        '
        Me.ConsecutivoAux.Location = New System.Drawing.Point(570, 662)
        Me.ConsecutivoAux.Name = "ConsecutivoAux"
        Me.ConsecutivoAux.ReadOnly = True
        Me.ConsecutivoAux.Size = New System.Drawing.Size(10, 20)
        Me.ConsecutivoAux.TabIndex = 35
        Me.ConsecutivoAux.TabStop = False
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(91, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'bnTap
        '
        Me.bnTap.AddNewItem = Nothing
        Me.bnTap.CountItem = Nothing
        Me.bnTap.DeleteItem = Nothing
        Me.bnTap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTap.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbGuardar})
        Me.bnTap.Location = New System.Drawing.Point(0, 0)
        Me.bnTap.MoveFirstItem = Nothing
        Me.bnTap.MoveLastItem = Nothing
        Me.bnTap.MoveNextItem = Nothing
        Me.bnTap.MovePreviousItem = Nothing
        Me.bnTap.Name = "bnTap"
        Me.bnTap.PositionItem = Nothing
        Me.bnTap.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTap.Size = New System.Drawing.Size(563, 25)
        Me.bnTap.TabIndex = 1
        Me.bnTap.TabStop = True
        Me.bnTap.Text = "BindingNavigator1"
        '
        'FrmTap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(563, 566)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.ConsecutivoAux)
        Me.Controls.Add(Me.bnTap)
        Me.Controls.Add(Me.gbDetalleClave)
        Me.Controls.Add(Me.gbClave)
        Me.Controls.Add(Me.Clv_SectorTextBox)
        Me.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.Controls.Add(Me.Clv_CalleTextBox)
        Me.Controls.Add(Me.ConsecutivoTextBox)
        Me.Name = "FrmTap"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Taps"
        Me.gbClave.ResumeLayout(False)
        Me.gbClave.PerformLayout()
        Me.gbDetalleClave.ResumeLayout(False)
        Me.gbDetalleClave.PerformLayout()
        CType(Me.bnTap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTap.ResumeLayout(False)
        Me.bnTap.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbClave As System.Windows.Forms.GroupBox
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents gbDetalleClave As System.Windows.Forms.GroupBox
    Friend WithEvents tbClave As System.Windows.Forms.TextBox
    Friend WithEvents tbIngenieria As System.Windows.Forms.TextBox
    Friend WithEvents tbSalidas As System.Windows.Forms.TextBox
    Friend WithEvents tbNoNegocios As System.Windows.Forms.TextBox
    Friend WithEvents tbFrenteANumero As System.Windows.Forms.TextBox
    Friend WithEvents tbNoServicios As System.Windows.Forms.TextBox
    Friend WithEvents tbNoLotes As System.Windows.Forms.TextBox
    Friend WithEvents tbNoCasas As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CalleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents cbSector As System.Windows.Forms.ComboBox
    Friend WithEvents cbColonia As System.Windows.Forms.ComboBox
    Friend WithEvents ConsecutivoAux As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbCalle As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_CalleTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbPoste As System.Windows.Forms.ComboBox
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnTap As System.Windows.Forms.BindingNavigator
End Class
