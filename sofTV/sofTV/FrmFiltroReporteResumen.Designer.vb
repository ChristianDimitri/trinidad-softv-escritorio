﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFiltroReporteResumen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtfin = New System.Windows.Forms.DateTimePicker()
        Me.dtini = New System.Windows.Forms.DateTimePicker()
        Me.rbsolicitud = New System.Windows.Forms.RadioButton()
        Me.rbejecucion = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btAceptar = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btDeseleccionaUno = New System.Windows.Forms.Button()
        Me.btSeleccionaUno = New System.Windows.Forms.Button()
        Me.btSeleccionarTodos = New System.Windows.Forms.Button()
        Me.lbseleccion = New System.Windows.Forms.ListBox()
        Me.lbtodos = New System.Windows.Forms.ListBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(525, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(15, 15)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "a"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(426, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(134, 15)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Fecha de Solicitud: "
        '
        'dtfin
        '
        Me.dtfin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtfin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtfin.Location = New System.Drawing.Point(546, 47)
        Me.dtfin.Name = "dtfin"
        Me.dtfin.Size = New System.Drawing.Size(94, 21)
        Me.dtfin.TabIndex = 21
        '
        'dtini
        '
        Me.dtini.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtini.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtini.Location = New System.Drawing.Point(426, 47)
        Me.dtini.Name = "dtini"
        Me.dtini.Size = New System.Drawing.Size(94, 21)
        Me.dtini.TabIndex = 20
        '
        'rbsolicitud
        '
        Me.rbsolicitud.AutoSize = True
        Me.rbsolicitud.Checked = True
        Me.rbsolicitud.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.rbsolicitud.Location = New System.Drawing.Point(33, 33)
        Me.rbsolicitud.Name = "rbsolicitud"
        Me.rbsolicitud.Size = New System.Drawing.Size(144, 19)
        Me.rbsolicitud.TabIndex = 25
        Me.rbsolicitud.TabStop = True
        Me.rbsolicitud.Text = "Fecha de Solicitud"
        Me.rbsolicitud.UseVisualStyleBackColor = True
        '
        'rbejecucion
        '
        Me.rbejecucion.AutoSize = True
        Me.rbejecucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.rbejecucion.Location = New System.Drawing.Point(190, 33)
        Me.rbejecucion.Name = "rbejecucion"
        Me.rbejecucion.Size = New System.Drawing.Size(151, 19)
        Me.rbejecucion.TabIndex = 26
        Me.rbejecucion.Text = "Fecha de Ejecución"
        Me.rbejecucion.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(541, 489)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(98, 31)
        Me.Button1.TabIndex = 35
        Me.Button1.Text = "Cancelar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(38, 98)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(207, 15)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Seleccione los trabajos a filtrar"
        '
        'btAceptar
        '
        Me.btAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAceptar.Location = New System.Drawing.Point(425, 489)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.Size = New System.Drawing.Size(98, 31)
        Me.btAceptar.TabIndex = 33
        Me.btAceptar.Text = "Aceptar"
        Me.btAceptar.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(297, 353)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(51, 23)
        Me.Button4.TabIndex = 32
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btDeseleccionaUno
        '
        Me.btDeseleccionaUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btDeseleccionaUno.Location = New System.Drawing.Point(297, 324)
        Me.btDeseleccionaUno.Name = "btDeseleccionaUno"
        Me.btDeseleccionaUno.Size = New System.Drawing.Size(51, 23)
        Me.btDeseleccionaUno.TabIndex = 31
        Me.btDeseleccionaUno.Text = "<"
        Me.btDeseleccionaUno.UseVisualStyleBackColor = True
        '
        'btSeleccionaUno
        '
        Me.btSeleccionaUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSeleccionaUno.Location = New System.Drawing.Point(297, 184)
        Me.btSeleccionaUno.Name = "btSeleccionaUno"
        Me.btSeleccionaUno.Size = New System.Drawing.Size(51, 23)
        Me.btSeleccionaUno.TabIndex = 30
        Me.btSeleccionaUno.Text = ">"
        Me.btSeleccionaUno.UseVisualStyleBackColor = True
        '
        'btSeleccionarTodos
        '
        Me.btSeleccionarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSeleccionarTodos.Location = New System.Drawing.Point(297, 213)
        Me.btSeleccionarTodos.Name = "btSeleccionarTodos"
        Me.btSeleccionarTodos.Size = New System.Drawing.Size(51, 23)
        Me.btSeleccionarTodos.TabIndex = 29
        Me.btSeleccionarTodos.Text = ">>"
        Me.btSeleccionarTodos.UseVisualStyleBackColor = True
        '
        'lbseleccion
        '
        Me.lbseleccion.DisplayMember = "DESCRIPCION"
        Me.lbseleccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbseleccion.FormattingEnabled = True
        Me.lbseleccion.ItemHeight = 15
        Me.lbseleccion.Location = New System.Drawing.Point(354, 124)
        Me.lbseleccion.Name = "lbseleccion"
        Me.lbseleccion.Size = New System.Drawing.Size(285, 349)
        Me.lbseleccion.TabIndex = 28
        Me.lbseleccion.ValueMember = "clv_trabajo"
        '
        'lbtodos
        '
        Me.lbtodos.DisplayMember = "DESCRIPCION"
        Me.lbtodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbtodos.FormattingEnabled = True
        Me.lbtodos.ItemHeight = 15
        Me.lbtodos.Location = New System.Drawing.Point(40, 122)
        Me.lbtodos.Name = "lbtodos"
        Me.lbtodos.Size = New System.Drawing.Size(251, 349)
        Me.lbtodos.TabIndex = 27
        Me.lbtodos.ValueMember = "clv_trabajo"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.RadioButton1.Location = New System.Drawing.Point(114, 31)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(96, 19)
        Me.RadioButton1.TabIndex = 38
        Me.RadioButton1.Text = "Ejecutadas"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Checked = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.RadioButton2.Location = New System.Drawing.Point(11, 31)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(97, 19)
        Me.RadioButton2.TabIndex = 37
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Pendientes"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(203, 17)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(217, 73)
        Me.GroupBox1.TabIndex = 39
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Status"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbejecucion)
        Me.GroupBox2.Controls.Add(Me.rbsolicitud)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.Location = New System.Drawing.Point(1, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(24, 17)
        Me.GroupBox2.TabIndex = 40
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Buscar por"
        Me.GroupBox2.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CheckBox2)
        Me.GroupBox3.Controls.Add(Me.CheckBox1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox3.Location = New System.Drawing.Point(25, 17)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(172, 73)
        Me.GroupBox3.TabIndex = 41
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Ordenes y/o quejas"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(96, 32)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(71, 19)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "Quejas"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(10, 32)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(80, 19)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Ordenes"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'FrmFiltroReporteResumen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(669, 530)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btAceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.btDeseleccionaUno)
        Me.Controls.Add(Me.btSeleccionaUno)
        Me.Controls.Add(Me.btSeleccionarTodos)
        Me.Controls.Add(Me.lbseleccion)
        Me.Controls.Add(Me.lbtodos)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtfin)
        Me.Controls.Add(Me.dtini)
        Me.Name = "FrmFiltroReporteResumen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Filtro de Resumen de Ordenes de Servicio"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtfin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtini As System.Windows.Forms.DateTimePicker
    Friend WithEvents rbsolicitud As System.Windows.Forms.RadioButton
    Friend WithEvents rbejecucion As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btAceptar As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents btDeseleccionaUno As System.Windows.Forms.Button
    Friend WithEvents btSeleccionaUno As System.Windows.Forms.Button
    Friend WithEvents btSeleccionarTodos As System.Windows.Forms.Button
    Friend WithEvents lbseleccion As System.Windows.Forms.ListBox
    Friend WithEvents lbtodos As System.Windows.Forms.ListBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
End Class
