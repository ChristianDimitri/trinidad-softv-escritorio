Imports System.Data.SqlClient
Public Class FrmDescuentoCombo
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim eResG As Integer = 0
    Dim eMsgG As String = ""
    Dim eOpBoton As Char = ""
    Dim eBndEsNuevo As Boolean = False
    Dim eBndHaCambiado As Boolean = False


    Private Sub FrmDescuentoCombo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            Session()
            If eOpcion = "N" Then
                eBndEsNuevo = True
                OpNuevo()
                Desaparecer()
                OpBotones1()
                ValidaGrid()

            End If
            If eOpcion = "M" Then
                OpModificar()
                OpBotones1()
                ValidaGrid()
            End If
            If eOpcion = "C" Then
                Me.BindingNavigator1.Enabled = False
                Me.CheckBox1.Enabled = False
                Me.Panel3.Enabled = False
                Me.Panel2.Enabled = False
                OpModificar()
            End If
            If eOpcion = "N" Or eOpcion = "M" Then
                UspDesactivaBotones(Me, Me.Name)
            End If
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Session()
        Dim CON1 As New SqlConnection(MiConexion)
        CON1.Open()
        Me.Dame_clv_session_ReportesTableAdapter.Connection = CON1
        Me.Dame_clv_session_ReportesTableAdapter.Fill(Me.DataSetEric2.Dame_clv_session_Reportes, eClv_Session)
        CON1.Close()
    End Sub

    Private Sub OpNuevo()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraTipoClientesTableAdapter.Connection = CON
            Me.MuestraTipoClientesTableAdapter.Fill(Me.DataSetEric2.MuestraTipoClientes, 0, 0)
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
            CON.Close()
            Me.BindingNavigatorDeleteItem.Enabled = False

            MuestraServicios()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub OpModificar()
        Me.TextBox5.Text = eNombreCombo
        Me.TextBox5.Enabled = False
        Try
            Dim CON0 As New SqlConnection(MiConexion)
            CON0.Open()
            Me.MuestraTipoClientesTableAdapter.Connection = CON0
            Me.MuestraTipoClientesTableAdapter.Fill(Me.DataSetEric2.MuestraTipoClientes, eClv_TipoCliente, 1)
            Me.MuestraTipServEricTableAdapter.Connection = CON0
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
            'Me.MuestraServiciosEricTableAdapter.Connection = CON0
            'Me.MuestraServiciosEricTableAdapter.Fill(Me.DataSetEric2.MuestraServiciosEric, CInt(Me.ConceptoComboBox.SelectedValue), 0, 0)
            Me.ConDetDescuentoCombo1TableAdapter.Connection = CON0
            Me.ConDetDescuentoCombo1TableAdapter.Fill(Me.DataSetEric2.ConDetDescuentoCombo1, 0, CInt(Me.Clv_TipoClienteTextBox.Text), Me.TextBox5.Text, eClv_Session, 0)

            CON0.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Agregar()
        Try

            If IsNumeric(Me.DescripcionComboBox1.SelectedValue) = False Then
                MsgBox("El tipo de servicio " & Me.ConceptoComboBox.Text & " no tiene servicios.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If DescripcionComboBox1.SelectedValue = 0 Then
                If rbPrincipal.Checked = True Then MessageBox.Show("Selecciona un Servicio Principal.")
                If rbAdicional.Checked = True Then MessageBox.Show("Selecciona un Servicio Adicional.")
                Exit Sub
            End If

            eRes = 0
            eMsg = ""
            If Me.TextBox1.Text.Length = 0 Then
                Me.TextBox1.Text = 0
            End If
            If Me.TextBox2.Text.Length = 0 Then
                Me.TextBox2.Text = 0
            End If
            If Me.TextBox3.Text.Length = 0 Then
                Me.TextBox3.Text = 0
            End If
            If Me.TextBox4.Text.Length = 0 Then
                Me.TextBox4.Text = 0
            End If
            If Me.TextBox6.Text.Length = 0 Then
                Me.TextBox6.Text = 0
            End If
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.AgrPreDetDescuentoComboTableAdapter.Connection = CON2
            Me.AgrPreDetDescuentoComboTableAdapter.Fill(Me.DataSetEric2.AgrPreDetDescuentoCombo, eClv_Session, CInt(Me.Clv_TipSerTextBox1.Text), CInt(Me.Clv_ServicioTextBox1.Text), Me.TextBox1.Text, Me.TextBox2.Text, Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox6.Text, 0, eRes, eMsg)
            Me.ConDetDescuentoCombo1TableAdapter.Connection = CON2
            Me.ConDetDescuentoCombo1TableAdapter.Fill(Me.DataSetEric2.ConDetDescuentoCombo1, 0, 0, "", eClv_Session, 1)
            CON2.Close()
            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Exclamation)
                PonerEnCeros()
            Else
                PonerEnCeros()
            End If


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Modificar()
        Try
            eRes = 0
            eMsg = ""
            If Me.ContTextBox.Text.Length = 0 Then
                Me.ContTextBox.Text = 0
            End If
            If Me.MensTextBox.Text.Length = 0 Then
                Me.MensTextBox.Text = 0
            End If
            If Me.RecoTextBox.Text.Length = 0 Then
                Me.RecoTextBox.Text = 0
            End If
            If Me.PuntosTextBox.Text.Length = 0 Then
                Me.PuntosTextBox.Text = 0
            End If
            If Me.LlamadaTextBox.Text.Length = 0 Then
                Me.LlamadaTextBox.Text = 0
            End If

            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            Me.AgrPreDetDescuentoComboTableAdapter.Connection = CON3
            Me.AgrPreDetDescuentoComboTableAdapter.Fill(Me.DataSetEric2.AgrPreDetDescuentoCombo, eClv_Session, CInt(Me.Clv_TipSerTextBox.Text), CInt(Me.Clv_ServicioTextBox.Text), Me.TextBox1.Text, Me.TextBox2.Text, Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox6.Text, 1, eRes, eMsg)
            Me.ConDetDescuentoCombo1TableAdapter.Connection = CON3
            Me.ConDetDescuentoCombo1TableAdapter.Fill(Me.DataSetEric2.ConDetDescuentoCombo1, 0, 0, "", eClv_Session, 1)
            CON3.Close()
            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Exclamation)
            Else
                PonerEnCeros()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Guardar()
        Try
            eRes = 0
            eMsg = ""
            If eBndEsNuevo = True Then
                eBndEsNuevo = False
                eOpcion = "N"
            End If
            Dim CON4 As New SqlConnection(MiConexion)
            If eOpcion = "M" Then
                DameClvDescuento()
                CON4.Open()
                Me.NueDetDescuentoComboTableAdapter.Connection = CON4
                Me.NueDetDescuentoComboTableAdapter.Fill(Me.DataSetEric2.NueDetDescuentoCombo, CInt(Me.txtClvDescuentoOriginal.Text), eClv_Session, Me.CheckBox1.Checked, eRes, eMsg)
                Me.ConDetDescuentoCombo1TableAdapter.Connection = CON4
                Me.ConDetDescuentoCombo1TableAdapter.Fill(Me.DataSetEric2.ConDetDescuentoCombo1, 0, CInt(Me.Clv_TipoClienteTextBox.Text), Me.TextBox5.Text, eClv_Session, 0)
                CON4.Close()
                If eRes = 1 Then
                    MsgBox(eMsg, MsgBoxStyle.Exclamation)
                Else
                    CON4.Open()
                    Me.ModDescuentoComboTableAdapter.Connection = CON4
                    Me.ModDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ModDescuentoCombo, eClv_Descuento, 0, Me.TextBox5.Text)
                    CON4.Close()
                    Me.DescripcionComboBox.Enabled = True
                    MsgBox(mensaje5, MsgBoxStyle.Information)
                    eBndCombo = True
                End If
            End If
            If eOpcion = "N" Then

                eResG = 0
                eMsgG = Nothing

                CON4.Open()
                Me.NueDescuentoComboTableAdapter.Connection = CON4
                Me.NueDescuentoComboTableAdapter.Fill(Me.DataSetEric2.NueDescuentoCombo, CInt(Me.Clv_TipoClienteTextBox.Text), Me.TextBox5.Text, eClv_Descuento, eResG, eMsgG)
                CON4.Close()

                If eResG = 1 Then
                    MsgBox(eMsgG, MsgBoxStyle.Exclamation)
                    Exit Sub
                End If

                CON4.Open()
                Me.NueDetDescuentoComboTableAdapter.Connection = CON4
                Me.NueDetDescuentoComboTableAdapter.Fill(Me.DataSetEric2.NueDetDescuentoCombo, eClv_Descuento, eClv_Session, Me.CheckBox1.Checked, eRes, eMsg)
                Me.ConDetDescuentoCombo1TableAdapter.Connection = CON4
                Me.ConDetDescuentoCombo1TableAdapter.Fill(Me.DataSetEric2.ConDetDescuentoCombo1, 0, CInt(Me.Clv_TipoClienteTextBox.Text), Me.TextBox5.Text, eClv_Session, 0)
                CON4.Close()
                If eRes = 1 Then
                    MsgBox(eMsg, MsgBoxStyle.Exclamation)
                    eOpcion = "N"
                    eBndEsNuevo = True

                Else
                    Me.DescripcionComboBox.Enabled = True
                    eOpcion = "M"
                    eBndEsNuevo = False
                    eBndCombo = True
                    MsgBox(mensaje5, MsgBoxStyle.Information)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Eliminar()
        Try
            Dim CON5 As New SqlConnection(MiConexion)
            CON5.Open()
            Me.EliPreDetDescuentoComboTableAdapter.Connection = CON5
            Me.EliPreDetDescuentoComboTableAdapter.Fill(Me.DataSetEric2.EliPreDetDescuentoCombo, eClv_Session, CInt(Me.Clv_ServicioTextBox.Text), 0)
            Me.ConDetDescuentoCombo1TableAdapter.Connection = CON5
            Me.ConDetDescuentoCombo1TableAdapter.Fill(Me.DataSetEric2.ConDetDescuentoCombo1, 0, 0, "", eClv_Session, 1)
            CON5.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ValidaGrid()
        If Me.ConDetDescuentoCombo1DataGridView.RowCount > 0 Then
            Me.Button2.Enabled = True
            Me.Button4.Enabled = True
            If eOpcion = "N" Then Me.DescripcionComboBox.Enabled = False
            Me.Label1.Enabled = False
        Else
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
            If eOpcion = "N" Then Me.DescripcionComboBox.Enabled = True
            Me.Label1.Enabled = True
        End If
    End Sub

    Private Sub PonerEnCeros()
        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
        Me.TextBox3.Text = ""
        Me.TextBox4.Text = ""
        Me.TextBox6.Text = ""
    End Sub

    Private Sub OpBotones1()
        Me.Button1.Enabled = True
        Me.Button2.Enabled = True
        Me.Button3.Enabled = False
        Me.Button4.Enabled = True
        Me.Button5.Enabled = False
    End Sub

    Private Sub OpBotones2()
        Me.Button1.Enabled = False
        Me.Button2.Enabled = False
        Me.Button3.Enabled = True
        Me.Button4.Enabled = False
        Me.Button5.Enabled = True
    End Sub

    Private Sub Aparecer()
        Me.ContTextBox.Visible = True
        Me.MensTextBox.Visible = True
        Me.RecoTextBox.Visible = True
        Me.PuntosTextBox.Visible = True

        Me.TextBox1.Visible = False
        Me.TextBox2.Visible = False
        Me.TextBox3.Visible = False
        Me.TextBox4.Visible = False

    End Sub

    Private Sub Desaparecer()
        Me.ContTextBox.Visible = False
        Me.MensTextBox.Visible = False
        Me.RecoTextBox.Visible = False
        Me.PuntosTextBox.Visible = False

        Me.TextBox1.Visible = True
        Me.TextBox2.Visible = True
        Me.TextBox3.Visible = True
        Me.TextBox4.Visible = True

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpBoton = "A"
        Agregar()
        OpBotones1()
        ValidaGrid()
        eBndHaCambiado = True
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConDetDescuentoCombo1DataGridView.RowCount = 0 Then
            MsgBox("No existen Servicios dentro del Combo a Modificar.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Me.TextBox1.Text = Me.ContTextBox.Text
        Me.TextBox2.Text = Me.MensTextBox.Text
        Me.TextBox3.Text = Me.RecoTextBox.Text
        Me.TextBox4.Text = Me.PuntosTextBox.Text
        Me.TextBox6.Text = Me.LlamadaTextBox.Text
        Desaparecer()
        ChecaSiEsTelefonia()

        Me.Panel3.Enabled = False

        eOpBoton = "M"
        OpBotones2()

        Me.ConDetDescuentoCombo1DataGridView.Enabled = False
        eBndHaCambiado = True
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConDetDescuentoCombo1DataGridView.RowCount = 0 Then
            MsgBox("No existen Servicios dentro del Combo a Guardar.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If eOpBoton = "M" Then

            Modificar()
        End If
        Me.Panel3.Enabled = True
        eOpBoton = "G"
        OpBotones1()
        Desaparecer()
        Me.ConDetDescuentoCombo1DataGridView.Enabled = True
        GuardarCombo()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.ConDetDescuentoCombo1DataGridView.RowCount = 0 Then
            MsgBox("No existen Servicios dentro del Combo a Eliminar.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        eOpBoton = "E"
        Eliminar()
        OpBotones1()
        ValidaGrid()
        eBndHaCambiado = True
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Panel3.Enabled = True
        eOpBoton = "C"
        OpBotones1()
        Desaparecer()
        PonerEnCeros()
        ChecaSiEsTelefonia1()
        Me.ConDetDescuentoCombo1DataGridView.Enabled = True

    End Sub

    'Private Sub Clv_TipSerTextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_TipSerTextBox1.TextChanged
    '    Try
    '        ChecaSiEsTelefonia1()

    '        If CInt(Me.Clv_TipSerTextBox1.Text) = 3 Then
    '            rbPrincipal.Enabled = True
    '            rbAdicional.Enabled = True
    '        Else
    '            rbPrincipal.Checked = True
    '            rbPrincipal.Enabled = False
    '            rbAdicional.Enabled = False
    '        End If

    '        Dim CON6 As New SqlConnection(MiConexion)
    '        CON6.Open()
    '        Me.MuestraServiciosEricTableAdapter.Connection = CON6
    '        If rbPrincipal.Checked = True Then Me.MuestraServiciosEricTableAdapter.Fill(Me.DataSetEric2.MuestraServiciosEric, CInt(Me.Clv_TipSerTextBox1.Text), 0, 7)
    '        If rbAdicional.Checked = True Then Me.MuestraServiciosEricTableAdapter.Fill(Me.DataSetEric2.MuestraServiciosEric, CInt(Me.Clv_TipSerTextBox1.Text), 0, 9)
    '        CON6.Close()

    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    End Try
    'End Sub

    Private Sub Clv_TipoClienteTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_TipoClienteTextBox.TextChanged
        If eBndHaCambiado = True Then
            eBndHaCambiado = False
            Dim r As Integer = 0
            r = MsgBox("Se han hecho modificaciones al combo y no has guardado. �Deseas guardar?", MsgBoxStyle.YesNo)
            If r = 6 Then
                GuardarCombo()
            End If
        End If

        If IsNumeric(Me.Clv_TipoClienteTextBox.Text) = True And Me.TextBox5.Text.Length > 0 Then
            DameClvDescuento()
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConDetDescuentoCombo1TableAdapter.Connection = CON
            Me.ConDetDescuentoCombo1TableAdapter.Fill(Me.DataSetEric2.ConDetDescuentoCombo1, 0, CInt(Me.Clv_TipoClienteTextBox.Text), Me.TextBox5.Text, eClv_Session, 0)
            CON.Close()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub

    Private Sub ContTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.ContTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub MensTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.MensTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub RecoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.RecoTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub PuntosTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.PuntosTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox3, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox4, Asc(LCase(e.KeyChar)), "L")))
    End Sub
    Private Sub TextBox6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox6, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub DameClvDescuento()
        Dim ClaveDescuento As Integer = 0
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameClv_DescuentoTableAdapter.Connection = CON
        Me.DameClv_DescuentoTableAdapter.Fill(Me.DataSetEric2.DameClv_Descuento, CInt(Me.Clv_TipoClienteTextBox.Text), Me.TextBox5.Text, ClaveDescuento)
        CON.Close()
        Me.txtClvDescuentoOriginal.Text = ClaveDescuento
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        GuardarCombo()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            eRes = 0
            eMsg = ""
            Dim CON5 As New SqlConnection(MiConexion)
            CON5.Open()
            'Me.BorDetDescuentoComboTableAdapter.Connection = CON5
            'Me.BorDetDescuentoComboTableAdapter.Fill(Me.DataSetEric2.BorDetDescuentoCombo, CInt(Me.txtClvDescuentoOriginal.Text), eClv_Session, 0, 0, eRes, eMsg)

            Me.ConDetDescuentoCombo1TableAdapter.Connection = CON5
            Me.ConDetDescuentoCombo1TableAdapter.Fill(Me.DataSetEric2.ConDetDescuentoCombo1, 0, CInt(Me.Clv_TipoClienteTextBox.Text), Me.TextBox5.Text, eClv_Session, 0)
            CON5.Close()

            BorDetDescuentoCombo(txtClvDescuentoOriginal.Text, eClv_Session, 0, 0)

            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Exclamation)
            Else
                MsgBox(mensaje6, MsgBoxStyle.Information)
                eBndCombo = True
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub GuardarCombo()
        If Me.ConDetDescuentoCombo1DataGridView.RowCount = 0 Then
            MsgBox("No existen Servicios dentro del Combo a Guardar.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If Me.TextBox5.Text.Length = 0 Then
            MsgBox("Captura el Nombre del Combo.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eBndHaCambiado = False
        Guardar()

        Me.Panel3.Enabled = True
        eOpBoton = "G"
        OpBotones1()
        Desaparecer()
        Me.ConDetDescuentoCombo1DataGridView.Enabled = True
    End Sub



    Private Sub ChecaSiEsTelefonia()

        If Me.Clv_TipSerTextBox.Text = 5 Then
            Me.Label3.Visible = True
            Me.LlamadaTextBox.Visible = True
            Me.TextBox6.Visible = True
        Else
            Me.Label3.Visible = False
            Me.LlamadaTextBox.Visible = False
            Me.TextBox6.Visible = False
        End If

    End Sub

    Private Sub ChecaSiEsTelefonia1()

        If Me.Clv_TipSerTextBox1.Text = 5 Then
            Me.Label3.Visible = True
            Me.LlamadaTextBox.Visible = True
            Me.TextBox6.Visible = True
        Else
            Me.Label3.Visible = False
            Me.LlamadaTextBox.Visible = False
            Me.TextBox6.Visible = False
        End If

    End Sub



    Private Sub rbPrincipal_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbPrincipal.CheckedChanged
        MuestraServicios()
    End Sub

    Private Sub MuestraServicios()

        Try
            If ConceptoComboBox.Text.Length = 0 Then Exit Sub

            ChecaSiEsTelefonia1()

            Dim CON6 As New SqlConnection(MiConexion)
            CON6.Open()
            Me.MuestraServiciosEricTableAdapter.Connection = CON6
            If rbPrincipal.Checked = True Then Me.MuestraServiciosEricTableAdapter.Fill(Me.DataSetEric2.MuestraServiciosEric, CInt(Me.ConceptoComboBox.SelectedValue), 0, 7)
            If rbAdicional.Checked = True Then Me.MuestraServiciosEricTableAdapter.Fill(Me.DataSetEric2.MuestraServiciosEric, CInt(Me.ConceptoComboBox.SelectedValue), 0, 9)
            CON6.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub



    Private Sub ConceptoComboBox_SelectedValueChanged(sender As System.Object, e As System.EventArgs) Handles ConceptoComboBox.SelectedValueChanged
        If ConceptoComboBox.Text.Length = 0 Then Exit Sub

        If CInt(Me.ConceptoComboBox.SelectedValue) = 3 Then
            rbPrincipal.Enabled = True
            rbAdicional.Enabled = True
            rbPrincipal.Checked = True
        Else
            rbPrincipal.Enabled = True
            rbAdicional.Enabled = False
            rbPrincipal.Checked = True
        End If
        MuestraServicios()
    End Sub

    Private Sub BorDetDescuentoCombo(ByVal Clv_Descuento As Integer, ByVal Clv_Session As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Descuento", SqlDbType.Int, Clv_Descuento)
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("BorDetDescuentoCombo")
        eRes = 0
        eMsj = String.Empty
        eRes = Integer.Parse(BaseII.dicoPar("@Res").ToString())
        eMsj = BaseII.dicoPar("@Msg")

    End Sub

End Class