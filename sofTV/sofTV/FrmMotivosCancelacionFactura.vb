Imports System.Data.SqlClient
Public Class FrmMotivosCancelacionFactura
    Private descr As String = Nothing
    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Fill(Me.DataSetEDGAR.CONMOTIVOSFACTURACANCELACION, New System.Nullable(Of Long)(CType(Clv_MotivoToolStripTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(BanderaToolStripTextBox.Text, Integer)))
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                descr = Me.DescripcionTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If opcion = "M" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, Me.DescripcionTextBox.Name, descr, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    ElseIf opcion = "N" Then
                        If eReImpresion = 0 Then
                            '     Me.Text = "Motivos de Cancelación de Facturas"
                            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Agrego Un Nuevo Motivo De Cancelación De La Factura", "", "Agrego Un Nuevo Motivo De Cancelación De La Factura: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                        Else
                            'Me.Text = "Motivos de ReImpresión de Facturas"
                            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Agrego Un Nuevo Motivo De Reimpresión De La Factura", "", "Agrego Un Nuevo Motivo De Reimpresión De La Factura: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                        End If
                    End If
                Case 1
                    If eReImpresion = 0 Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Se Elimino Motivo De Cancelación De La Factura", "", "Se Elimino un Motivo De Cancelación De La Factura: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    Else
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Se Elimino Motivo De Reimpresión De La Factura", "", "Se Elimino un Motivo De Reimpresión De La Factura: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    End If
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmMotivosCancelacionFactura_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)

        If eReImpresion = 0 Then
            Me.Text = "Motivos de Cancelación de Pagos"
        Else
            Me.Text = "Motivos de ReImpresión de Pagos"
        End If



        If opcion = "N" Then
            Me.CONMOTIVOSFACTURACANCELACIONBindingSource.AddNew()
            Me.BanderaTextBox.Text = eReImpresion
            Me.BindingNavigatorDeleteItem.Enabled = False
        End If

        If opcion = "M" Then
            Try
                Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Connection = CON
                Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Fill(Me.DataSetEDGAR.CONMOTIVOSFACTURACANCELACION, eClvMotivo, eReImpresion)
                damedatosbitacora()
                Me.BindingNavigatorDeleteItem.Enabled = True
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
        CON.Close()
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Validate()

            Me.CONMOTIVOSFACTURACANCELACIONBindingSource.EndEdit()
            Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Connection = CON
            Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Update(Me.DataSetEDGAR.CONMOTIVOSFACTURACANCELACION)
            CON.Close()
            Me.Close()
        Catch
            MsgBox("Debes de Introducir algún Motivo", , "Advertencia")
        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim r As Integer
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        r = MsgBox("¿Deseas Eliminar el Motivo?", MsgBoxStyle.YesNo, "Atención")
        If r = 6 Then
            Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Connection = CON
            Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Delete(eClvMotivo)
            Me.Close()
        Else
            Me.Close()
        End If
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Me.Validate()
        'Me.BanderaTextBox.Text = eReImpresion
        'Me.CONMOTIVOSFACTURACANCELACIONBindingSource.EndEdit()
        'Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Update(Me.DataSetEDGAR.CONMOTIVOSFACTURACANCELACION)
        'Me.Close()
    End Sub


    Private Sub CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionTextBox.Text.Length > 0 Then
            Me.Validate()
            Me.BanderaTextBox.Text = eReImpresion
            Me.CONMOTIVOSFACTURACANCELACIONBindingSource.EndEdit()
            CON.Open()
            Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Connection = CON
            Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Update(Me.DataSetEDGAR.CONMOTIVOSFACTURACANCELACION)
            CON.Close()
            guardabitacora(0)
            MsgBox(mensaje5)
            Me.Close()
        Else
            MsgBox("Introduce Primero la Descripción de un Motivo", , "Atención")
        End If
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Connection = CON
            Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.Delete(Me.Clv_MotivoTextBox.Text)
            guardabitacora(1)
            MsgBox(mensaje6)
            CON.Close()
            Me.Close()
        Catch
        End Try
    End Sub

    Private Sub DescripcionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescripcionTextBox.TextChanged

    End Sub
End Class