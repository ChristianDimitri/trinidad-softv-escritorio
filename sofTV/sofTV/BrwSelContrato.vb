Imports System.Data.SqlClient
Public Class BrwSelContrato

    Private Sub BrwSelContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        colorea(Me, Me.Name)
        'Me.DameClientesActivosTableAdapter.Connection = CON
        'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", "", "", "", 3)
        llenaComboColonias()
        llenaGridClientes(0, "", "", "", "", 3, Me.cmbColonias.SelectedValue)
        'CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.CONTRATOLabel1.Text.Length > 0 Then
            eGloContrato = Me.CONTRATOLabel1.Text
            eGloSubContrato = Me.CONTRATOLabel1.Text
            eContrato = Me.CONTRATOLabel1.Text
            Me.DialogResult = Windows.Forms.DialogResult.OK
            rNombre = Me.NOMBRELabel1.Text
        Else
            eGloContrato = 0
            eGloSubContrato = 0
            eContrato = 0
            rNombre = ""
        End If
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        eGloContrato = 0
        eGloSubContrato = 0
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                'Me.DameClientesActivosTableAdapter.Connection = CON
                'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
                llenaGridClientes(Me.TextBox1.Text, "", "", "", "", 0, Me.cmbColonias.SelectedValue)
            End If
        End If
        'CON.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If IsNumeric(Me.TextBox1.Text) = True Then
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            llenaGridClientes(Me.TextBox1.Text, "", "", "", "", 0, Me.cmbColonias.SelectedValue)
        End If
        'CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox2.Text.Length > 0 Then
                'Me.DameClientesActivosTableAdapter.Connection = CON
                'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, Me.TextBox2.Text, "", "", "", 1)
                llenaGridClientes(0, Me.TextBox2.Text, "", "", "", 1, Me.cmbColonias.SelectedValue)
            End If
        End If
        'CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Me.TextBox2.Text.Length > 0 Then
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, Me.TextBox2.Text, "", "", "", 1)
            llenaGridClientes(0, Me.TextBox2.Text, "", "", "", 1, Me.cmbColonias.SelectedValue)
        End If
        'CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            llenaGridClientes(0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2, Me.cmbColonias.SelectedValue)
        End If
        'CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        'Me.DameClientesActivosTableAdapter.Connection = CON
        'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        llenaGridClientes(0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2, Me.cmbColonias.SelectedValue)
        'CON.Close()
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            llenaGridClientes(0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2, Me.cmbColonias.SelectedValue)
        End If
        'CON.Close()
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            llenaGridClientes(0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2, Me.cmbColonias.SelectedValue)
        End If
        'CON.Close()
    End Sub

#Region "GRID DE CLIENTES"
    Private Sub llenaGridClientes(ByVal prmContrato As Long, ByVal prmNombre As String, ByVal prmCalle As String, ByVal prmNumero As String, ByVal prmCiudad As String, _
                                  ByVal prmOp As Integer, ByVal prmClvColonia As Integer)
        Dim clientes As New BaseIII
        Try
            clientes.limpiaParametros()
            clientes.CreateMyParameter("@contrato", SqlDbType.BigInt, prmContrato)
            clientes.CreateMyParameter("@nombre", SqlDbType.VarChar, prmNombre, 250)
            clientes.CreateMyParameter("@calle", SqlDbType.VarChar, prmCalle, 250)
            clientes.CreateMyParameter("@numero", SqlDbType.VarChar, prmNumero, 250)
            clientes.CreateMyParameter("@ciudad", SqlDbType.VarChar, prmCiudad, 250)
            clientes.CreateMyParameter("@op", SqlDbType.Int, prmOp)
            clientes.CreateMyParameter("@clvColonia", SqlDbType.Int, prmClvColonia)

            Me.DameClientesActivosDataGridView.DataSource = clientes.ConsultaDT("uspDameClientesActivos")

            If Me.DameClientesActivosDataGridView.RowCount > 0 Then
                Me.CONTRATOLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(0).Value.ToString
                Me.NOMBRELabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(1).Value.ToString
                Me.CALLELabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(2).Value.ToString
                Me.NUMEROLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(3).Value.ToString
                Me.COLONIALabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(4).Value.ToString
                Me.CIUDADLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(5).Value.ToString
                Me.SOLOINTERNETCheckBox.Checked = CBool(Me.DameClientesActivosDataGridView.SelectedCells(6).Value)
                Me.ESHOTELCheckBox.Checked = CBool(Me.DameClientesActivosDataGridView.SelectedCells(7).Value)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "COMBO COLONIAS"
    Private Sub llenaComboColonias()
        Dim colonias As New BaseIII
        Try
            colonias.limpiaParametros()
            Me.cmbColonias.DataSource = colonias.ConsultaDT("uspConsultaColonias")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub DameClientesActivosDataGridView_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DameClientesActivosDataGridView.CellClick
        Try
            Me.CONTRATOLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(0).Value.ToString
            Me.NOMBRELabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(1).Value.ToString
            Me.CALLELabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(2).Value.ToString
            Me.NUMEROLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(3).Value.ToString
            Me.COLONIALabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(4).Value.ToString
            Me.CIUDADLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(5).Value.ToString
            Me.SOLOINTERNETCheckBox.Checked = CBool(Me.DameClientesActivosDataGridView.SelectedCells(6).Value)
            Me.ESHOTELCheckBox.Checked = CBool(Me.DameClientesActivosDataGridView.SelectedCells(7).Value)
        Catch ex As Exception

        End Try
    End Sub
End Class