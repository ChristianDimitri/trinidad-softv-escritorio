<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwPromociones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgPromociones = New System.Windows.Forms.DataGridView()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.bBusNom = New System.Windows.Forms.Button()
        Me.bNuevo = New System.Windows.Forms.Button()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.cServicio = New System.Windows.Forms.ComboBox()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.bBusSer = New System.Windows.Forms.Button()
        Me.bModificar = New System.Windows.Forms.Button()
        Me.bConsultar = New System.Windows.Forms.Button()
        Me.bSalir = New System.Windows.Forms.Button()
        Me.cTipSer = New System.Windows.Forms.ComboBox()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pagos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaIni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaFin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vigente = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pregunta = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Clv_TipoCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgPromociones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgPromociones
        '
        Me.dgPromociones.AllowUserToAddRows = False
        Me.dgPromociones.AllowUserToDeleteRows = False
        Me.dgPromociones.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgPromociones.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgPromociones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPromociones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Nombre, Me.Pagos, Me.Precio, Me.FechaIni, Me.FechaFin, Me.Clv_Servicio, Me.Vigente, Me.Clave, Me.Pregunta, Me.Clv_TipoCliente, Me.Servicio, Me.SPrecio, Me.Concepto, Me.Descripcion})
        Me.dgPromociones.GridColor = System.Drawing.Color.WhiteSmoke
        Me.dgPromociones.Location = New System.Drawing.Point(294, 30)
        Me.dgPromociones.Name = "dgPromociones"
        Me.dgPromociones.ReadOnly = True
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgPromociones.RowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgPromociones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPromociones.Size = New System.Drawing.Size(534, 650)
        Me.dgPromociones.TabIndex = 0
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 59)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(163, 24)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Tipo de Servicio"
        '
        'bBusNom
        '
        Me.bBusNom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bBusNom.Location = New System.Drawing.Point(12, 174)
        Me.bBusNom.Name = "bBusNom"
        Me.bBusNom.Size = New System.Drawing.Size(75, 23)
        Me.bBusNom.TabIndex = 3
        Me.bBusNom.Text = "Buscar"
        Me.bBusNom.UseVisualStyleBackColor = True
        '
        'bNuevo
        '
        Me.bNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bNuevo.Location = New System.Drawing.Point(860, 30)
        Me.bNuevo.Name = "bNuevo"
        Me.bNuevo.Size = New System.Drawing.Size(136, 36)
        Me.bNuevo.TabIndex = 5
        Me.bNuevo.Text = "&NUEVO"
        Me.bNuevo.UseVisualStyleBackColor = True
        '
        'tbNombre
        '
        Me.tbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombre.Location = New System.Drawing.Point(16, 147)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(252, 21)
        Me.tbNombre.TabIndex = 6
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(13, 129)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(58, 15)
        Me.CMBLabel2.TabIndex = 7
        Me.CMBLabel2.Text = "Nombre"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(12, 9)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(224, 24)
        Me.CMBLabel3.TabIndex = 8
        Me.CMBLabel3.Text = "Buscar Promoción por:"
        '
        'cServicio
        '
        Me.cServicio.DisplayMember = "Descripcion"
        Me.cServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cServicio.FormattingEnabled = True
        Me.cServicio.Location = New System.Drawing.Point(16, 244)
        Me.cServicio.Name = "cServicio"
        Me.cServicio.Size = New System.Drawing.Size(252, 23)
        Me.cServicio.TabIndex = 9
        Me.cServicio.ValueMember = "Clv_Servicio"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(13, 226)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(58, 15)
        Me.CMBLabel4.TabIndex = 12
        Me.CMBLabel4.Text = "Servicio"
        '
        'bBusSer
        '
        Me.bBusSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bBusSer.Location = New System.Drawing.Point(12, 273)
        Me.bBusSer.Name = "bBusSer"
        Me.bBusSer.Size = New System.Drawing.Size(75, 23)
        Me.bBusSer.TabIndex = 10
        Me.bBusSer.Text = "Buscar"
        Me.bBusSer.UseVisualStyleBackColor = True
        '
        'bModificar
        '
        Me.bModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bModificar.Location = New System.Drawing.Point(860, 113)
        Me.bModificar.Name = "bModificar"
        Me.bModificar.Size = New System.Drawing.Size(136, 36)
        Me.bModificar.TabIndex = 13
        Me.bModificar.Text = "&MODIFICAR"
        Me.bModificar.UseVisualStyleBackColor = True
        '
        'bConsultar
        '
        Me.bConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bConsultar.Location = New System.Drawing.Point(860, 71)
        Me.bConsultar.Name = "bConsultar"
        Me.bConsultar.Size = New System.Drawing.Size(136, 36)
        Me.bConsultar.TabIndex = 14
        Me.bConsultar.Text = "&CONSULTAR"
        Me.bConsultar.UseVisualStyleBackColor = True
        '
        'bSalir
        '
        Me.bSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bSalir.Location = New System.Drawing.Point(860, 682)
        Me.bSalir.Name = "bSalir"
        Me.bSalir.Size = New System.Drawing.Size(136, 36)
        Me.bSalir.TabIndex = 15
        Me.bSalir.Text = "&SALIR"
        Me.bSalir.UseVisualStyleBackColor = True
        '
        'cTipSer
        '
        Me.cTipSer.DisplayMember = "Concepto"
        Me.cTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cTipSer.FormattingEnabled = True
        Me.cTipSer.Location = New System.Drawing.Point(16, 86)
        Me.cTipSer.Name = "cTipSer"
        Me.cTipSer.Size = New System.Drawing.Size(252, 23)
        Me.cTipSer.TabIndex = 16
        Me.cTipSer.ValueMember = "Clv_TipSer"
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 200
        '
        'Pagos
        '
        Me.Pagos.DataPropertyName = "Pagos"
        Me.Pagos.HeaderText = "Pagos"
        Me.Pagos.Name = "Pagos"
        Me.Pagos.ReadOnly = True
        Me.Pagos.Visible = False
        '
        'Precio
        '
        Me.Precio.DataPropertyName = "Precio"
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Name = "Precio"
        Me.Precio.ReadOnly = True
        Me.Precio.Visible = False
        '
        'FechaIni
        '
        Me.FechaIni.DataPropertyName = "FechaIni"
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.FechaIni.DefaultCellStyle = DataGridViewCellStyle2
        Me.FechaIni.HeaderText = "FechaIni"
        Me.FechaIni.Name = "FechaIni"
        Me.FechaIni.ReadOnly = True
        Me.FechaIni.Visible = False
        '
        'FechaFin
        '
        Me.FechaFin.DataPropertyName = "FechaFin"
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.FechaFin.DefaultCellStyle = DataGridViewCellStyle3
        Me.FechaFin.HeaderText = "FechaFin"
        Me.FechaFin.Name = "FechaFin"
        Me.FechaFin.ReadOnly = True
        Me.FechaFin.Visible = False
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.ReadOnly = True
        Me.Clv_Servicio.Visible = False
        '
        'Vigente
        '
        Me.Vigente.DataPropertyName = "Vigente"
        Me.Vigente.HeaderText = "Vigente"
        Me.Vigente.Name = "Vigente"
        Me.Vigente.ReadOnly = True
        Me.Vigente.Width = 80
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "Clave"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        Me.Clave.Visible = False
        '
        'Pregunta
        '
        Me.Pregunta.DataPropertyName = "Pregunta"
        Me.Pregunta.HeaderText = "Pregunta"
        Me.Pregunta.Name = "Pregunta"
        Me.Pregunta.ReadOnly = True
        Me.Pregunta.Visible = False
        '
        'Clv_TipoCliente
        '
        Me.Clv_TipoCliente.DataPropertyName = "Clv_TipoCliente"
        Me.Clv_TipoCliente.HeaderText = "Clv_TipoCliente"
        Me.Clv_TipoCliente.Name = "Clv_TipoCliente"
        Me.Clv_TipoCliente.ReadOnly = True
        Me.Clv_TipoCliente.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        Me.Servicio.Width = 200
        '
        'SPrecio
        '
        Me.SPrecio.DataPropertyName = "SPrecio"
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.SPrecio.DefaultCellStyle = DataGridViewCellStyle4
        Me.SPrecio.HeaderText = "SPrecio"
        Me.SPrecio.Name = "SPrecio"
        Me.SPrecio.ReadOnly = True
        Me.SPrecio.Visible = False
        '
        'Concepto
        '
        Me.Concepto.DataPropertyName = "Concepto"
        Me.Concepto.HeaderText = "Concepto"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.ReadOnly = True
        Me.Concepto.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'BrwPromociones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.cTipSer)
        Me.Controls.Add(Me.bSalir)
        Me.Controls.Add(Me.bConsultar)
        Me.Controls.Add(Me.bModificar)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.bBusSer)
        Me.Controls.Add(Me.cServicio)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.tbNombre)
        Me.Controls.Add(Me.bNuevo)
        Me.Controls.Add(Me.bBusNom)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.dgPromociones)
        Me.Name = "BrwPromociones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Promociones"
        CType(Me.dgPromociones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgPromociones As System.Windows.Forms.DataGridView
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents bBusNom As System.Windows.Forms.Button
    Friend WithEvents bNuevo As System.Windows.Forms.Button
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents cServicio As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents bBusSer As System.Windows.Forms.Button
    Friend WithEvents bModificar As System.Windows.Forms.Button
    Friend WithEvents bConsultar As System.Windows.Forms.Button
    Friend WithEvents bSalir As System.Windows.Forms.Button
    Friend WithEvents cTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pagos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaFin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vigente As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pregunta As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Clv_TipoCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPrecio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
