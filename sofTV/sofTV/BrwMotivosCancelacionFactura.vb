Imports System.Data.SqlClient
Public Class BrwMotivosCancelacionFactura

    Private Sub LlenaMotivos()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If eReImpresion = 0 Then
            Try
                Me.BuscaMotivosFacturaCanceladaTableAdapter.Connection = CON
                Me.BuscaMotivosFacturaCanceladaTableAdapter.Fill(Me.DataSetEDGAR.BuscaMotivosFacturaCancelada, 0, "", eReImpresion, 2)
            Catch
                'System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Else
            Try
                Me.BuscaMotivosFacturaCanceladaTableAdapter.Connection = CON
                Me.BuscaMotivosFacturaCanceladaTableAdapter.Fill(Me.DataSetEDGAR.BuscaMotivosFacturaCancelada, 0, "", eReImpresion, 2)
            Catch
                'System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try

        End If
        CON.Close()
    End Sub

    Private Sub Busca_Clave()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If eReImpresion = 0 Then
            Try
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BuscaMotivosFacturaCanceladaTableAdapter.Connection = CON
                    Me.BuscaMotivosFacturaCanceladaTableAdapter.Fill(Me.DataSetEDGAR.BuscaMotivosFacturaCancelada, Me.TextBox1.Text, "", eReImpresion, 0)
                Else
                    MsgBox("La Clave que busca no es valida")
                End If
                Me.TextBox1.Clear()
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try

        Else

            Try
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BuscaMotivosFacturaCanceladaTableAdapter.Connection = CON
                    Me.BuscaMotivosFacturaCanceladaTableAdapter.Fill(Me.DataSetEDGAR.BuscaMotivosFacturaCancelada, Me.TextBox1.Text, "", eReImpresion, 0)
                Else
                    MsgBox("La Clave que busca no es valida")
                End If
                Me.TextBox1.Clear()

            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
        CON.Close()
    End Sub

    Private Sub Busca_Motivo()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If eReImpresion = 0 Then
            Try
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BuscaMotivosFacturaCanceladaTableAdapter.Connection = CON
                    Me.BuscaMotivosFacturaCanceladaTableAdapter.Fill(Me.DataSetEDGAR.BuscaMotivosFacturaCancelada, 0, Me.TextBox2.Text, eReImpresion, 1)

                Else
                    MsgBox("El Motivo de Cancelación que busca no es valido")
                End If
                Me.TextBox2.Clear()
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Else
            Try
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BuscaMotivosFacturaCanceladaTableAdapter.Connection = CON
                    Me.BuscaMotivosFacturaCanceladaTableAdapter.Fill(Me.DataSetEDGAR.BuscaMotivosFacturaCancelada, 0, Me.TextBox2.Text, eReImpresion, 1)

                Else
                    MsgBox("El Motivo de Cancelación que busca no es valido")
                End If
                Me.TextBox2.Clear()

            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
        CON.Close()
    End Sub


    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.Busca_Clave()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Busca_Motivo()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.Busca_Clave()
        End If
    End Sub


    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.Busca_Motivo()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        eClvMotivo = 0
        FrmMotivosCancelacionFactura.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        If Me.DataGridView1.RowCount > 0 Then
            opcion = "M"
            eClvMotivo = Me.Clv_MotivoLabel1.Text
            FrmMotivosCancelacionFactura.Show()
        Else
            MsgBox("No Hay Motivos Registrados que puedan ser Modificados", , "Atención")
        End If


    End Sub

    Private Sub BrwMotivosCancelacionFactura_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        LlenaMotivos()
    End Sub

    Private Sub BrwMotivosCancelacionFactura_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        If eReImpresion = 0 Then
            Me.Text = "Catálogo de Motivos de Cancelación de Pagos"
            Me.Label2.Text = "Motivo de la Cancelación:"

        Else
            Me.Text = "Catálogo de Motivos de ReImpresión de Pagos"
            Me.Label2.Text = "Motivo de la ReImpresión:"
        End If

        LlenaMotivos()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

    End Sub
End Class