<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCEXTE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCEXTE))
        Me.ExtAdicLabel = New System.Windows.Forms.Label
        Me.CONCONEXBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONCONEXBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Button5 = New System.Windows.Forms.Button
        Me.ExtAdicTextBox = New System.Windows.Forms.TextBox
        CType(Me.CONCONEXBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCONEXBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'ExtAdicLabel
        '
        Me.ExtAdicLabel.AutoSize = True
        Me.ExtAdicLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExtAdicLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.ExtAdicLabel.Location = New System.Drawing.Point(30, 78)
        Me.ExtAdicLabel.Name = "ExtAdicLabel"
        Me.ExtAdicLabel.Size = New System.Drawing.Size(241, 15)
        Me.ExtAdicLabel.TabIndex = 12
        Me.ExtAdicLabel.Text = "Número de Extensiones por Instalar:"
        '
        'CONCONEXBindingNavigator
        '
        Me.CONCONEXBindingNavigator.AddNewItem = Nothing
        Me.CONCONEXBindingNavigator.CountItem = Nothing
        Me.CONCONEXBindingNavigator.DeleteItem = Nothing
        Me.CONCONEXBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONCONEXBindingNavigatorSaveItem})
        Me.CONCONEXBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCONEXBindingNavigator.MoveFirstItem = Nothing
        Me.CONCONEXBindingNavigator.MoveLastItem = Nothing
        Me.CONCONEXBindingNavigator.MoveNextItem = Nothing
        Me.CONCONEXBindingNavigator.MovePreviousItem = Nothing
        Me.CONCONEXBindingNavigator.Name = "CONCONEXBindingNavigator"
        Me.CONCONEXBindingNavigator.PositionItem = Nothing
        Me.CONCONEXBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCONEXBindingNavigator.Size = New System.Drawing.Size(434, 25)
        Me.CONCONEXBindingNavigator.TabIndex = 10
        Me.CONCONEXBindingNavigator.TabStop = True
        Me.CONCONEXBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(70, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Borrar"
        '
        'CONCONEXBindingNavigatorSaveItem
        '
        Me.CONCONEXBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCONEXBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCONEXBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCONEXBindingNavigatorSaveItem.Name = "CONCONEXBindingNavigatorSaveItem"
        Me.CONCONEXBindingNavigatorSaveItem.Size = New System.Drawing.Size(81, 22)
        Me.CONCONEXBindingNavigatorSaveItem.Text = "&Aceptar"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(286, 177)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ExtAdicTextBox
        '
        Me.ExtAdicTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExtAdicTextBox.Location = New System.Drawing.Point(277, 75)
        Me.ExtAdicTextBox.MaxLength = 3
        Me.ExtAdicTextBox.Name = "ExtAdicTextBox"
        Me.ExtAdicTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ExtAdicTextBox.TabIndex = 9
        '
        'FrmCEXTE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(434, 222)
        Me.Controls.Add(Me.CONCONEXBindingNavigator)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ExtAdicLabel)
        Me.Controls.Add(Me.ExtAdicTextBox)
        Me.Name = "FrmCEXTE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Extensiones por Instalar"
        CType(Me.CONCONEXBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCONEXBindingNavigator.ResumeLayout(False)
        Me.CONCONEXBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CONCONEXBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCONEXBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ExtAdicTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ExtAdicLabel As System.Windows.Forms.Label
End Class
