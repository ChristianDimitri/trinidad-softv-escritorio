<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCapArtiAcom
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.MuestraArtAcomBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New Softv.ProcedimientosArnoldo2()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Valida_Art_AcomBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLblTitulo = New System.Windows.Forms.Label()
        Me.ComboBox6 = New System.Windows.Forms.ComboBox()
        Me.MuestraArticulosClasificacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraArticulosAcometidaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameclaveBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_Articulos_AcometidaTableAdapter = New Softv.ProcedimientosArnoldo2TableAdapters.Muestra_Articulos_AcometidaTableAdapter()
        Me.Muestra_Articulos_ClasificacionTableAdapter = New Softv.ProcedimientosArnoldo2TableAdapters.Muestra_Articulos_ClasificacionTableAdapter()
        Me.Muestra_Art_AcomTableAdapter = New Softv.ProcedimientosArnoldo2TableAdapters.Muestra_Art_AcomTableAdapter()
        Me.Inserta_Precio_Art_AcomBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Precio_Art_AcomTableAdapter = New Softv.ProcedimientosArnoldo2TableAdapters.Inserta_Precio_Art_AcomTableAdapter()
        Me.Eliminar_Art_AcomBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Eliminar_Art_AcomTableAdapter = New Softv.ProcedimientosArnoldo2TableAdapters.Eliminar_Art_AcomTableAdapter()
        Me.Dame_claveTableAdapter = New Softv.ProcedimientosArnoldo2TableAdapters.Dame_claveTableAdapter()
        Me.Valida_Art_AcomTableAdapter = New Softv.ProcedimientosArnoldo2TableAdapters.Valida_Art_AcomTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.NoArticuloDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClaveDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConceptoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReferenciaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraArtAcomBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.Valida_Art_AcomBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraArticulosClasificacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraArticulosAcometidaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameclaveBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Precio_Art_AcomBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Eliminar_Art_AcomBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ComboBox1
        '
        Me.ComboBox1.CausesValidation = False
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(288, 143)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(298, 21)
        Me.ComboBox1.TabIndex = 3
        '
        'ComboBox3
        '
        Me.ComboBox3.CausesValidation = False
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(161, 143)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox3.TabIndex = 2
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(595, 143)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 4
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(707, 143)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(150, 20)
        Me.TextBox2.TabIndex = 5
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(216, 76)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(183, 16)
        Me.CMBLabel1.TabIndex = 5
        Me.CMBLabel1.Text = "Clasificación de Material:"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(189, 127)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(48, 16)
        Me.CMBLabel2.TabIndex = 6
        Me.CMBLabel2.Text = "Clave"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(335, 127)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(173, 16)
        Me.CMBLabel3.TabIndex = 7
        Me.CMBLabel3.Text = "Descripción del Articulo"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(618, 127)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(53, 16)
        Me.CMBLabel4.TabIndex = 8
        Me.CMBLabel4.Text = "Precio"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(725, 127)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(112, 16)
        Me.CMBLabel5.TabIndex = 9
        Me.CMBLabel5.Text = "No. por Default"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(141, 173)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(127, 30)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "&ELIMINAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(26, 173)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 30)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&AGREGAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NoArticuloDataGridViewTextBoxColumn, Me.ClaveDataGridViewTextBoxColumn, Me.ConceptoDataGridViewTextBoxColumn, Me.PrecioDataGridViewTextBoxColumn, Me.ReferenciaDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.MuestraArtAcomBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(-41, 2)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(1016, 458)
        Me.DataGridView1.TabIndex = 12
        '
        'MuestraArtAcomBindingSource
        '
        Me.MuestraArtAcomBindingSource.DataMember = "Muestra_Art_Acom"
        Me.MuestraArtAcomBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(851, 672)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DataGridView1)
        Me.Panel1.Controls.Add(Me.ComboBox2)
        Me.Panel1.Controls.Add(Me.ComboBox4)
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Location = New System.Drawing.Point(26, 209)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(961, 457)
        Me.Panel1.TabIndex = 8
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(24, 221)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(156, 21)
        Me.ComboBox2.TabIndex = 20
        Me.ComboBox2.TabStop = False
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(55, 169)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(156, 21)
        Me.ComboBox4.TabIndex = 17
        Me.ComboBox4.TabStop = False
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Valida_Art_AcomBindingSource, "Cantidad", True))
        Me.TextBox3.Location = New System.Drawing.Point(9, 418)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 20)
        Me.TextBox3.TabIndex = 17
        '
        'Valida_Art_AcomBindingSource
        '
        Me.Valida_Art_AcomBindingSource.DataMember = "Valida_Art_Acom"
        Me.Valida_Art_AcomBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'CMBLblTitulo
        '
        Me.CMBLblTitulo.AutoSize = True
        Me.CMBLblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblTitulo.Location = New System.Drawing.Point(311, 28)
        Me.CMBLblTitulo.Name = "CMBLblTitulo"
        Me.CMBLblTitulo.Size = New System.Drawing.Size(410, 24)
        Me.CMBLblTitulo.TabIndex = 16
        Me.CMBLblTitulo.Text = "Lista de Precios de Articulos de Instalación"
        '
        'ComboBox6
        '
        Me.ComboBox6.FormattingEnabled = True
        Me.ComboBox6.Location = New System.Drawing.Point(395, 71)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(375, 21)
        Me.ComboBox6.TabIndex = 1
        '
        'MuestraArticulosClasificacionBindingSource
        '
        Me.MuestraArticulosClasificacionBindingSource.DataMember = "Muestra_Articulos_Clasificacion"
        Me.MuestraArticulosClasificacionBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraArticulosAcometidaBindingSource
        '
        Me.MuestraArticulosAcometidaBindingSource.DataMember = "Muestra_Articulos_Acometida"
        Me.MuestraArticulosAcometidaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'DameclaveBindingSource
        '
        Me.DameclaveBindingSource.DataMember = "Dame_clave"
        Me.DameclaveBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Muestra_Articulos_AcometidaTableAdapter
        '
        Me.Muestra_Articulos_AcometidaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Articulos_ClasificacionTableAdapter
        '
        Me.Muestra_Articulos_ClasificacionTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Art_AcomTableAdapter
        '
        Me.Muestra_Art_AcomTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Precio_Art_AcomBindingSource
        '
        Me.Inserta_Precio_Art_AcomBindingSource.DataMember = "Inserta_Precio_Art_Acom"
        Me.Inserta_Precio_Art_AcomBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Precio_Art_AcomTableAdapter
        '
        Me.Inserta_Precio_Art_AcomTableAdapter.ClearBeforeFill = True
        '
        'Eliminar_Art_AcomBindingSource
        '
        Me.Eliminar_Art_AcomBindingSource.DataMember = "Eliminar_Art_Acom"
        Me.Eliminar_Art_AcomBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Eliminar_Art_AcomTableAdapter
        '
        Me.Eliminar_Art_AcomTableAdapter.ClearBeforeFill = True
        '
        'Dame_claveTableAdapter
        '
        Me.Dame_claveTableAdapter.ClearBeforeFill = True
        '
        'Valida_Art_AcomTableAdapter
        '
        Me.Valida_Art_AcomTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'NoArticuloDataGridViewTextBoxColumn
        '
        Me.NoArticuloDataGridViewTextBoxColumn.DataPropertyName = "NoArticulo"
        Me.NoArticuloDataGridViewTextBoxColumn.HeaderText = "NoArticulo"
        Me.NoArticuloDataGridViewTextBoxColumn.Name = "NoArticuloDataGridViewTextBoxColumn"
        Me.NoArticuloDataGridViewTextBoxColumn.ReadOnly = True
        Me.NoArticuloDataGridViewTextBoxColumn.Width = 5
        '
        'ClaveDataGridViewTextBoxColumn
        '
        Me.ClaveDataGridViewTextBoxColumn.DataPropertyName = "Clave"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.ClaveDataGridViewTextBoxColumn.HeaderText = "Clave"
        Me.ClaveDataGridViewTextBoxColumn.Name = "ClaveDataGridViewTextBoxColumn"
        Me.ClaveDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ConceptoDataGridViewTextBoxColumn
        '
        Me.ConceptoDataGridViewTextBoxColumn.DataPropertyName = "Concepto"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.NullValue = Nothing
        Me.ConceptoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.ConceptoDataGridViewTextBoxColumn.HeaderText = "Concepto"
        Me.ConceptoDataGridViewTextBoxColumn.Name = "ConceptoDataGridViewTextBoxColumn"
        Me.ConceptoDataGridViewTextBoxColumn.ReadOnly = True
        Me.ConceptoDataGridViewTextBoxColumn.Width = 300
        '
        'PrecioDataGridViewTextBoxColumn
        '
        Me.PrecioDataGridViewTextBoxColumn.DataPropertyName = "Precio"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = "0"
        Me.PrecioDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.PrecioDataGridViewTextBoxColumn.HeaderText = "Precio"
        Me.PrecioDataGridViewTextBoxColumn.Name = "PrecioDataGridViewTextBoxColumn"
        Me.PrecioDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ReferenciaDataGridViewTextBoxColumn
        '
        Me.ReferenciaDataGridViewTextBoxColumn.DataPropertyName = "Referencia"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = "0"
        Me.ReferenciaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.ReferenciaDataGridViewTextBoxColumn.HeaderText = "No. Por Default"
        Me.ReferenciaDataGridViewTextBoxColumn.Name = "ReferenciaDataGridViewTextBoxColumn"
        Me.ReferenciaDataGridViewTextBoxColumn.ReadOnly = True
        Me.ReferenciaDataGridViewTextBoxColumn.Width = 200
        '
        'FrmCapArtiAcom
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.ComboBox6)
        Me.Controls.Add(Me.CMBLblTitulo)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox1)
        Me.Name = "FrmCapArtiAcom"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lista de Precios de Articulos de Instalación"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraArtAcomBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Valida_Art_AcomBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraArticulosClasificacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraArticulosAcometidaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameclaveBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Precio_Art_AcomBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Eliminar_Art_AcomBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents MuestraArticulosAcometidaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Articulos_AcometidaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Articulos_AcometidaTableAdapter
    Friend WithEvents MuestraArticulosClasificacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Articulos_ClasificacionTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Articulos_ClasificacionTableAdapter
    Friend WithEvents MuestraArtAcomBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Art_AcomTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Art_AcomTableAdapter
    Friend WithEvents Inserta_Precio_Art_AcomBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Precio_Art_AcomTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Precio_Art_AcomTableAdapter
    Friend WithEvents Eliminar_Art_AcomBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Eliminar_Art_AcomTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Eliminar_Art_AcomTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBLblTitulo As System.Windows.Forms.Label
    Friend WithEvents DameclaveBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_claveTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Dame_claveTableAdapter
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Valida_Art_AcomBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_Art_AcomTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Valida_Art_AcomTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents NoArticuloDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClaveDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConceptoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReferenciaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
