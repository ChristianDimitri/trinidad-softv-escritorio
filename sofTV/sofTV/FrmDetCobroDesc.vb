Imports System.Data.SqlClient
Public Class FrmDetCobroDesc


    Private Sub FrmDetCobroDesc_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        Dim Clv_Orden As Integer = 0
        Dim Clv_Tipo As Integer = 0
        colorea(Me, Me.Name)
        CON.Open()
        Me.Muestra_Descr_CoDescTableAdapter.Connection = CON
        Me.Muestra_Descr_CoDescTableAdapter.Fill(Me.DataSetEric.Muestra_Descr_CoDesc, eGloContrato, Clv_Orden, Clv_Tipo)
        Me.TextBox1.Text = Clv_Orden.ToString
        Me.TextBox2.Text = Clv_Tipo.ToString
        Me.Valida_tipser_ordenTableAdapter.Connection = CON
        Me.Valida_tipser_ordenTableAdapter.Fill(Me.DataSetEric.Valida_tipser_orden, Clv_Orden, Clv_Tipo)
        CON.Close()
        If IsNumeric(Me.TextBox2.Text) = True Then
            If CInt(Me.TextBox2.Text) = 1 Then
                Me.CMBLabel1.Text = "No. de Orden que se est� cobrando:"
            ElseIf CInt(Me.TextBox2.Text) = 2 Then
                Me.CMBLabel1.Text = "No. de Queja que se est� cobrando:"
            End If
        End If
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub
End Class