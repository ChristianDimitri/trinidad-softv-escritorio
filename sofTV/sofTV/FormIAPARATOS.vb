﻿Imports System.Data.SqlClient

Public Class FormIAPARATOS


    Private Sub SP_CONSULTAIAPARATOS(ByVal oClv_Orden As Long, oClave As Long)
        Try

        
            '@Clv_Orden Bigint=0,@Clave Bigint=0,@Contratonet bigint =0 OUTPUT,@Clv_Aparato bigint=0 OUTPUT
            Dim oContratonet As Long = 0
            Dim oClv_Aparato As Long = 0
            Dim oStatus As String = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@Contratonet", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Clv_Aparato", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Status", ParameterDirection.Output, SqlDbType.VarChar, 1)
            BaseII.ProcedimientoOutPut("SP_CONSULTAIAPARATOS")
            oContratonet = BaseII.dicoPar("@Contratonet").ToString
            oClv_Aparato = BaseII.dicoPar("@Clv_Aparato").ToString
            oStatus = BaseII.dicoPar("@Status").ToString
            If oStatus.Length > 0 Then
                CMBoxEstadoAparato.SelectedValue = oStatus
            End If
            If oContratonet > 0 Then
                ComboBoxPorAsignar.SelectedValue = oContratonet
            End If
            If oClv_Aparato > 0 Then
                ComboBoxAparatosDisponibles.SelectedValue = oClv_Aparato
                consultaMacWanPorOrden(oClv_Orden, oClave, oClv_Aparato)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub consultaMacWanPorOrden(ByVal oClv_Orden As Long, oClave As Long, oClv_Aparato As Long)
        If ComboBoxTipoAparato.SelectedValue = "F" Or ComboBoxTipoAparato.SelectedValue = "S" Or ComboBoxTipoAparato.SelectedValue = "R" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
            BaseII.CreateMyParameter("@MacWan", ParameterDirection.Output, SqlDbType.VarChar, 50)
            BaseII.ProcedimientoOutPut("consultaMacWanPorOrden")
            TextBoxWan.Text = BaseII.dicoPar("@MacWan").ToString
        End If

    End Sub

    Private Sub Llena_AparatosporAsignar(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)

            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_EstadoAparato()
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()

            CMBoxEstadoAparato.DataSource = BaseII.ConsultaDT("SP_StatusAparatos")
            CMBoxEstadoAparato.DisplayMember = "Concepto"
            CMBoxEstadoAparato.ValueMember = "Clv_StatusCableModem"

            If CMBoxEstadoAparato.Items.Count > 0 Then
                CMBoxEstadoAparato.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_orden", SqlDbType.BigInt, gloClv_Orden)
            BaseII.CreateMyParameter("@idFibra", SqlDbType.BigInt, ComboBoxPorAsignar.SelectedValue)
            BaseII.CreateMyParameter("@trabajo", SqlDbType.VarChar, GLOTRABAJO, 10)
            ComboBoxTipoAparato.DataSource = BaseII.ConsultaDT("SP_DameLosPosiblesTiposAparato")
            ComboBoxTipoAparato.DisplayMember = "concepto"
            ComboBoxTipoAparato.ValueMember = "TipoAparato"
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Llena_AparatosporDisponibles(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            Dim TipoAparato As String = ""

            If Len(ComboBoxTipoAparato.SelectedValue) > 0 Then
                TipoAparato = ComboBoxTipoAparato.SelectedValue
            End If

            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, TipoAparato, 10)
            ComboBoxAparatosDisponibles.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxAparatosDisponibles.DisplayMember = "Descripcion"
            ComboBoxAparatosDisponibles.ValueMember = "ContratoAnt"

            If ComboBoxAparatosDisponibles.Items.Count > 0 Then
                ComboBoxAparatosDisponibles.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SP_GuardaIAPARATOS(oClave As Long, oTrabajo As String, oClv_Orden As Long, oContratonet As Long, oClv_Aparato As Long, oOpcion As String, oStatus As String)
        '@Clave bigint=0,@Trabajo varchar(10)='',@Clv_Orden bigint=0,@Contratonet Bigint=0,@Clv_Aparato bigint=0,@Opcion varchar(10)=''
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Trabajo", SqlDbType.VarChar, oTrabajo, 10)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, oStatus, 1)
        BaseII.Inserta("SP_GuardaIAPARATOS")
    End Sub

    Private Sub SP_GuardaMacWan(oClave As Long, oClv_Orden As Long, oClv_Aparato As Long, oMacLan As String, oMacWan As String)
        If ComboBoxTipoAparato.SelectedValue = "F" Or ComboBoxTipoAparato.SelectedValue = "S" Or ComboBoxTipoAparato.SelectedValue = "R" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
            BaseII.CreateMyParameter("@MacLan", SqlDbType.VarChar, oMacLan, 50)
            BaseII.CreateMyParameter("@MacWan", SqlDbType.VarChar, oMacWan, 50)
            BaseII.Inserta("SP_GuardaMacWan")
        End If

    End Sub

    Private Sub CONCCABMBindingNavigatorSaveItem_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub FormIAPARATOS_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        Llena_AparatosporAsignar(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, 0, gloClv_Orden, GloDetClave)
        Llena_AparatosporDisponibles(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
        Llena_EstadoAparato()
        Me.Text = GLONOMTRABAJO

        SP_CONSULTAIAPARATOS(gloClv_Orden, GloDetClave)
        LblEstadoAparato.Visible = False
        CMBoxEstadoAparato.Visible = False

        If GLOTRABAJO = "CANTE" Or GLOTRABAJO = "CALNB" Or GLOTRABAJO = "CAPAR" Then
            LblEstadoAparato.Visible = True
            CMBoxEstadoAparato.Visible = True

        End If

        If GLOTRABAJO <> "IAPAR" And GLOTRABAJO <> "ICAJA" And GLOTRABAJO <> "CAPAR" And GLOTRABAJO <> "CCAJA" And GLOTRABAJO <> "CANTE" And GLOTRABAJO <> "CALNB" Then
            ComboBoxTipoAparato.Visible = True
            Label2.Visible = True
        End If

        'Descomentar para solicitar la WAN
        'If ComboBoxTipoAparato.SelectedValue = "F" Or ComboBoxTipoAparato.SelectedValue = "S" Then
        '    'wan
        '    '    LabelWan.Visible = True
        '    '    TextBoxWan.Visible = True

        '    'buscar
        '    BtnBuscar.Visible = True
        '    BtnAceptar.Enabled = False

        'Else
        '    'wan
        '    '    LabelWan.Visible = False
        '    '    TextBoxWan.Visible = False

        '    'buscar
        '    BtnBuscar.Visible = False
        '    BtnAceptar.Enabled = True
        'End If

        If GLOTRABAJO = "CONUS" Or GLOTRABAJO = "CONUF" Or GLOTRABAJO = "CMINI" Or GLOTRABAJO = "CASAF" Or GLOTRABAJO = "CAFAS" Or GLOTRABAJO = "CAMAF" Or GLOTRABAJO = "CAFAM" Or GLOTRABAJO = "CAPAF" Or GLOTRABAJO = "CAPAD" Then
            'LblEstadoAparato.Visible = True
            'CMBoxEstadoAparato.Visible = True
            If GLOTRABAJO = "CONUS" Or GLOTRABAJO = "CONUF" Or GLOTRABAJO = "CMINI" Or GLOTRABAJO = "CASAF" Or GLOTRABAJO = "CAFAS" Or GLOTRABAJO = "CAMAF" Or GLOTRABAJO = "CAFAM" Or GLOTRABAJO = "CAPAF" Or GLOTRABAJO = "CAPAD" Then
                Label4.Text = "Aparato asignado actualmente"
                LblEstadoAparato.Text = "Seleccione el estado del aparato"
                Label1.Text = "Seleccione el aparato a instalar"
            End If


        End If

        If opcion = "M" Or opcion = "C" Then
            If FrmOrdSer.Panel6.Enabled = False Then
                ComboBoxAparatosDisponibles.Enabled = False
                ComboBoxPorAsignar.Enabled = False
                BtnAceptar.Enabled = False
                CMBoxEstadoAparato.Enabled = False
            End If
        End If


    End Sub

    Private Sub BtnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles BtnAceptar.Click
        If GLOTRABAJO = "CANTE" Or GLOTRABAJO = "CALNB" Or GLOTRABAJO = "CAPAR" Then
            If CMBoxEstadoAparato.SelectedIndex = -1 Then
                MsgBox("Seleccione el Estado del Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Nuevo Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
        ElseIf GLOTRABAJO = "CONUS" Or GLOTRABAJO = "CONUF" Or GLOTRABAJO = "CMINI" Or GLOTRABAJO = "CASAF" Or GLOTRABAJO = "CAFAS" Or GLOTRABAJO = "CAMAF" Or GLOTRABAJO = "CAFAM" Or GLOTRABAJO = "CAPAF" Or GLOTRABAJO = "CAPAD" Then
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Nuevo Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
        Else
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Instalar", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
        End If

        'Descomentar para solicitar la WAN
        'If ComboBoxTipoAparato.SelectedValue = "F" Or ComboBoxTipoAparato.SelectedValue = "S" Or ComboBoxTipoAparato.SelectedValue = "R" Then
        '    If TextBoxWan.Text.Length = 0 Then
        '        MsgBox("Falta la dirección MAC WAN", MsgBoxStyle.Information, "Información")
        '        Exit Sub
        '    End If

        '    If TextBoxWan.Text.Length <> 12 Then
        '        MsgBox("Formato incorrecto de la dirección MAC WAN", MsgBoxStyle.Information, "Información")
        '        Exit Sub
        '    End If

        '    If TextBoxWan.Text = ComboBoxAparatosDisponibles.Text Then
        '        MsgBox("La dirección MAC LAN y MAC WAN no pueder ser iguales", MsgBoxStyle.Information, "Información")
        '        Exit Sub
        '    End If

        '    If Not validaWAN() Then
        '        MsgBox("Ya existe esa MAC WAN registrada con otro equipo", MsgBoxStyle.Information, "Información")
        '        Exit Sub
        '    End If
        'End If

        SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxAparatosDisponibles.SelectedValue, opcion, CMBoxEstadoAparato.SelectedValue)

        'Descomentar para solicitar la WAN
        'If ComboBoxTipoAparato.SelectedValue = "F" Or ComboBoxTipoAparato.SelectedValue = "S" Or ComboBoxTipoAparato.SelectedValue = "R" Then
        '    SP_GuardaMacWan(GloDetClave, gloClv_Orden, ComboBoxAparatosDisponibles.SelectedValue, ComboBoxAparatosDisponibles.Text, TextBoxWan.Text)
        'End If

        Me.Close()
    End Sub

    Private Function validaWAN() As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, ComboBoxAparatosDisponibles.SelectedValue)
        BaseII.CreateMyParameter("@MacWan", SqlDbType.VarChar, TextBoxWan.Text, 50)
        BaseII.CreateMyParameter("@correcta", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("validaWAN")
        Return CBool(BaseII.dicoPar("@correcta").ToString)
    End Function

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub ComboBoxTipoAparato_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxTipoAparato.SelectedValueChanged
        Llena_AparatosporDisponibles(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)

        'Descomentar para solicitar la WAN
        'Try
        '    If ComboBoxTipoAparato.SelectedValue = "F" Or ComboBoxTipoAparato.SelectedValue = "S" Then
        '        'LabelWan.Visible = True
        '        'TextBoxWan.Visible = True
        '        BtnBuscar.Visible = True
        '        BtnAceptar.Enabled = False

        '    Else
        '        'LabelWan.Visible = False
        '        'TextBoxWan.Visible = False
        '        BtnBuscar.Visible = False
        '        BtnAceptar.Enabled = True
        '    End If
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub ComboBoxPorAsignar_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxPorAsignar.SelectedValueChanged
        Llena_EstadoAparato()
    End Sub

    Private Sub ComboBoxAparatosDisponibles_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxAparatosDisponibles.SelectedValueChanged
        'If ComboBoxTipoAparato.SelectedValue = "F" Or ComboBoxTipoAparato.SelectedValue = "S" Or ComboBoxTipoAparato.SelectedValue = "R" Then
        '    If IsNumeric(ComboBoxAparatosDisponibles.SelectedValue) Then
        '        If ComboBoxAparatosDisponibles.SelectedValue > 0 Then
        '            consultaMacWanPorOrden(gloClv_Orden, GloDetClave, ComboBoxAparatosDisponibles.SelectedValue)
        '        End If
        '    End If
        'End If

        'If ComboBoxTipoAparato.SelectedValue = "F" Or ComboBoxTipoAparato.SelectedValue = "S" Then
        '    BtnBuscar.Visible = True
        '    BtnAceptar.Enabled = False
        'Else
        '    BtnBuscar.Visible = False
        '    BtnAceptar.Enabled = True
        'End If
    End Sub

    'Private Sub BtnBuscar_Click(sender As Object, e As EventArgs) Handles BtnBuscar.Click
    '    If IsNumeric(ComboBoxAparatosDisponibles.SelectedValue) Then
    '        BaseII.limpiaParametros()
    '        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, ComboBoxPorAsignar.SelectedValue)
    '        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, ComboBoxAparatosDisponibles.SelectedValue)
    '        BaseII.CreateMyParameter("@Mac", SqlDbType.VarChar, ComboBoxAparatosDisponibles.Text, 20)
    '        BaseII.CreateMyParameter("@consecutivo", ParameterDirection.Output, SqlDbType.BigInt)
    '        BaseII.ProcedimientoOutPut("insertaComandoBuscar")

    '        Dim frm1 As New FrmBuscarOnu
    '        frm1.Consecutivo = CInt(BaseII.dicoPar("@consecutivo").ToString)


    '        frm1.ShowDialog()



    '        If frm1.Encontrada = 1 Then
    '            BtnBuscar.Visible = False
    '            BtnAceptar.Enabled = True
    '        End If
    '    End If

    'End Sub

End Class