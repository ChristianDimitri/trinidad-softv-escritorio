﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConsultaCambiosEsq
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxSoloInternet = New System.Windows.Forms.CheckBox()
        Me.LabelCelular = New System.Windows.Forms.Label()
        Me.LabelTelefono = New System.Windows.Forms.Label()
        Me.LabelCiudad = New System.Windows.Forms.Label()
        Me.LabelColonia = New System.Windows.Forms.Label()
        Me.LabelNumero = New System.Windows.Forms.Label()
        Me.LabelCalle = New System.Windows.Forms.Label()
        Me.LabelNombre = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.LbDiasPrepNew = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Lbstatus = New System.Windows.Forms.Label()
        Me.LbEsqNew = New System.Windows.Forms.Label()
        Me.LbDiasPrepant = New System.Windows.Forms.Label()
        Me.LbFecha = New System.Windows.Forms.Label()
        Me.LbEsqant = New System.Windows.Forms.Label()
        Me.LbUsuario = New System.Windows.Forms.Label()
        Me.LbContrato = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Salir = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.CheckBoxSoloInternet)
        Me.GroupBox1.Controls.Add(Me.LabelCelular)
        Me.GroupBox1.Controls.Add(Me.LabelTelefono)
        Me.GroupBox1.Controls.Add(Me.LabelCiudad)
        Me.GroupBox1.Controls.Add(Me.LabelColonia)
        Me.GroupBox1.Controls.Add(Me.LabelNumero)
        Me.GroupBox1.Controls.Add(Me.LabelCalle)
        Me.GroupBox1.Controls.Add(Me.LabelNombre)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(801, 133)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'CheckBoxSoloInternet
        '
        Me.CheckBoxSoloInternet.AutoSize = True
        Me.CheckBoxSoloInternet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSoloInternet.Enabled = False
        Me.CheckBoxSoloInternet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxSoloInternet.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBoxSoloInternet.Location = New System.Drawing.Point(469, 20)
        Me.CheckBoxSoloInternet.Name = "CheckBoxSoloInternet"
        Me.CheckBoxSoloInternet.Size = New System.Drawing.Size(108, 19)
        Me.CheckBoxSoloInternet.TabIndex = 16
        Me.CheckBoxSoloInternet.Text = "Sólo Internet"
        Me.CheckBoxSoloInternet.UseVisualStyleBackColor = True
        Me.CheckBoxSoloInternet.Visible = False
        '
        'LabelCelular
        '
        Me.LabelCelular.Location = New System.Drawing.Point(112, 97)
        Me.LabelCelular.Name = "LabelCelular"
        Me.LabelCelular.Size = New System.Drawing.Size(230, 19)
        Me.LabelCelular.TabIndex = 15
        Me.LabelCelular.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTelefono
        '
        Me.LabelTelefono.Location = New System.Drawing.Point(456, 97)
        Me.LabelTelefono.Name = "LabelTelefono"
        Me.LabelTelefono.Size = New System.Drawing.Size(207, 19)
        Me.LabelTelefono.TabIndex = 14
        Me.LabelTelefono.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCiudad
        '
        Me.LabelCiudad.Location = New System.Drawing.Point(511, 67)
        Me.LabelCiudad.Name = "LabelCiudad"
        Me.LabelCiudad.Size = New System.Drawing.Size(177, 19)
        Me.LabelCiudad.TabIndex = 13
        Me.LabelCiudad.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelColonia
        '
        Me.LabelColonia.Location = New System.Drawing.Point(112, 67)
        Me.LabelColonia.Name = "LabelColonia"
        Me.LabelColonia.Size = New System.Drawing.Size(300, 19)
        Me.LabelColonia.TabIndex = 12
        Me.LabelColonia.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNumero
        '
        Me.LabelNumero.Location = New System.Drawing.Point(465, 42)
        Me.LabelNumero.Name = "LabelNumero"
        Me.LabelNumero.Size = New System.Drawing.Size(112, 19)
        Me.LabelNumero.TabIndex = 11
        Me.LabelNumero.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCalle
        '
        Me.LabelCalle.Location = New System.Drawing.Point(112, 42)
        Me.LabelCalle.Name = "LabelCalle"
        Me.LabelCalle.Size = New System.Drawing.Size(300, 19)
        Me.LabelCalle.TabIndex = 10
        Me.LabelCalle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNombre
        '
        Me.LabelNombre.Location = New System.Drawing.Point(112, 23)
        Me.LabelNombre.Name = "LabelNombre"
        Me.LabelNombre.Size = New System.Drawing.Size(347, 19)
        Me.LabelNombre.TabIndex = 9
        Me.LabelNombre.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(24, 97)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 19)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Celular: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(350, 97)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(100, 19)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Teléfono: "
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label12.Location = New System.Drawing.Point(429, 67)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 19)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Ciudad: "
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label13.Location = New System.Drawing.Point(21, 67)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(85, 19)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Colonia: "
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(429, 42)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(30, 19)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "#: "
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label15.Location = New System.Drawing.Point(6, 39)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 19)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Calle: "
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label16.Location = New System.Drawing.Point(6, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 19)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Nombre: "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.LbDiasPrepNew)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Lbstatus)
        Me.GroupBox2.Controls.Add(Me.LbEsqNew)
        Me.GroupBox2.Controls.Add(Me.LbDiasPrepant)
        Me.GroupBox2.Controls.Add(Me.LbFecha)
        Me.GroupBox2.Controls.Add(Me.LbEsqant)
        Me.GroupBox2.Controls.Add(Me.LbUsuario)
        Me.GroupBox2.Controls.Add(Me.LbContrato)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(10, 151)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(801, 133)
        Me.GroupBox2.TabIndex = 23
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Información del Cambio"
        '
        'LbDiasPrepNew
        '
        Me.LbDiasPrepNew.Location = New System.Drawing.Point(712, 63)
        Me.LbDiasPrepNew.Name = "LbDiasPrepNew"
        Me.LbDiasPrepNew.Size = New System.Drawing.Size(37, 19)
        Me.LbDiasPrepNew.TabIndex = 17
        Me.LbDiasPrepNew.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(612, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 19)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Dias Prepago: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Lbstatus
        '
        Me.Lbstatus.Location = New System.Drawing.Point(112, 86)
        Me.Lbstatus.Name = "Lbstatus"
        Me.Lbstatus.Size = New System.Drawing.Size(79, 19)
        Me.Lbstatus.TabIndex = 15
        Me.Lbstatus.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LbEsqNew
        '
        Me.LbEsqNew.Location = New System.Drawing.Point(454, 64)
        Me.LbEsqNew.Name = "LbEsqNew"
        Me.LbEsqNew.Size = New System.Drawing.Size(158, 19)
        Me.LbEsqNew.TabIndex = 14
        Me.LbEsqNew.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LbDiasPrepant
        '
        Me.LbDiasPrepant.Location = New System.Drawing.Point(712, 21)
        Me.LbDiasPrepant.Name = "LbDiasPrepant"
        Me.LbDiasPrepant.Size = New System.Drawing.Size(37, 19)
        Me.LbDiasPrepant.TabIndex = 13
        Me.LbDiasPrepant.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LbFecha
        '
        Me.LbFecha.Location = New System.Drawing.Point(112, 65)
        Me.LbFecha.Name = "LbFecha"
        Me.LbFecha.Size = New System.Drawing.Size(204, 19)
        Me.LbFecha.TabIndex = 12
        Me.LbFecha.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LbEsqant
        '
        Me.LbEsqant.Location = New System.Drawing.Point(454, 21)
        Me.LbEsqant.Name = "LbEsqant"
        Me.LbEsqant.Size = New System.Drawing.Size(158, 19)
        Me.LbEsqant.TabIndex = 11
        Me.LbEsqant.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LbUsuario
        '
        Me.LbUsuario.Location = New System.Drawing.Point(112, 45)
        Me.LbUsuario.Name = "LbUsuario"
        Me.LbUsuario.Size = New System.Drawing.Size(204, 19)
        Me.LbUsuario.TabIndex = 10
        Me.LbUsuario.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LbContrato
        '
        Me.LbContrato.Location = New System.Drawing.Point(112, 23)
        Me.LbContrato.Name = "LbContrato"
        Me.LbContrato.Size = New System.Drawing.Size(204, 19)
        Me.LbContrato.TabIndex = 9
        Me.LbContrato.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(24, 84)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(82, 19)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Status: "
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label11.Location = New System.Drawing.Point(328, 64)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(126, 19)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Esquema Nuevo: "
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label17.Location = New System.Drawing.Point(612, 23)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(101, 19)
        Me.Label17.TabIndex = 5
        Me.Label17.Text = "Dias Prepago: "
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label18.Location = New System.Drawing.Point(21, 62)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(85, 19)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "Fecha: "
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label19
        '
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label19.Location = New System.Drawing.Point(328, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(129, 19)
        Me.Label19.TabIndex = 3
        Me.Label19.Text = "Esquema Anterior: "
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label20.Location = New System.Drawing.Point(6, 42)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(100, 19)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Usuario: "
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label21
        '
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label21.Location = New System.Drawing.Point(6, 20)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(100, 19)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Contrato: "
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Salir
        '
        Me.Salir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Salir.Location = New System.Drawing.Point(738, 304)
        Me.Salir.Name = "Salir"
        Me.Salir.Size = New System.Drawing.Size(75, 24)
        Me.Salir.TabIndex = 24
        Me.Salir.Text = "Salir"
        Me.Salir.UseVisualStyleBackColor = True
        '
        'FrmConsultaCambiosEsq
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(833, 348)
        Me.Controls.Add(Me.Salir)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmConsultaCambiosEsq"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consulta Cambios Esquema"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxSoloInternet As System.Windows.Forms.CheckBox
    Friend WithEvents LabelCelular As System.Windows.Forms.Label
    Friend WithEvents LabelTelefono As System.Windows.Forms.Label
    Friend WithEvents LabelCiudad As System.Windows.Forms.Label
    Friend WithEvents LabelColonia As System.Windows.Forms.Label
    Friend WithEvents LabelNumero As System.Windows.Forms.Label
    Friend WithEvents LabelCalle As System.Windows.Forms.Label
    Friend WithEvents LabelNombre As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Lbstatus As System.Windows.Forms.Label
    Friend WithEvents LbEsqNew As System.Windows.Forms.Label
    Friend WithEvents LbDiasPrepant As System.Windows.Forms.Label
    Friend WithEvents LbFecha As System.Windows.Forms.Label
    Friend WithEvents LbEsqant As System.Windows.Forms.Label
    Friend WithEvents LbUsuario As System.Windows.Forms.Label
    Friend WithEvents LbContrato As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents LbDiasPrepNew As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Salir As System.Windows.Forms.Button
End Class
