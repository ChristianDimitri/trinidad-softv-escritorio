﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.IO

Public Class FrmTarifasPagare
    Private Sub FrmTarifasPagare_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        MuestraTipoServicioPagare(2)
        MuestraMarcasAparatosPagare()
        If OpPagare = "N" Then
            Me.TipServCombo.Enabled = True
            Me.NormalRadioBut.Enabled = True
            Me.HDRadioBut.Enabled = True
            Me.AparatoCombo.Enabled = True
        ElseIf OpPagare = "M" Then
            Me.TipServCombo.Enabled = False
            Me.NormalRadioBut.Enabled = False
            Me.HDRadioBut.Enabled = False
            Me.AparatoCombo.Enabled = False
            ConCostosPagare()
        End If
    End Sub

    Private Sub MuestraTipoServicioPagare(ByVal OPCION As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraTipoServicioPagare ")
        StrSQL.Append(CStr(OPCION))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            TipServCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MuestraMarcasAparatosPagare()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraMarcasAparatosPagare ")
        StrSQL.Append(CStr(TipServCombo.SelectedValue) & ", ")
        If Me.NormalRadioBut.Checked Then
            StrSQL.Append(CStr("1"))
        ElseIf Me.HDRadioBut.Checked Then
            StrSQL.Append(CStr("2"))
        Else
            StrSQL.Append(CStr("0"))
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            AparatoCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub GuardaCostosPagare()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("GuardaCostosPagare", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ID", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = IdPagare
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@TIPSER", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Me.TipServCombo.SelectedValue
        CMD.Parameters.Add(PRM2)

        Dim PRM4 As New SqlParameter("@IDARTICULO", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = Me.AparatoCombo.SelectedValue
        CMD.Parameters.Add(PRM4)

        Me.CostoText.Text = 0
        Dim PRM5 As New SqlParameter("@COSTOARTICULO", SqlDbType.Money)
        PRM5.Direction = ParameterDirection.Input
        PRM5.Value = CostoText.Text
        CMD.Parameters.Add(PRM5)

        Me.CostoAdicText.Text = 0
        Dim PRM10 As New SqlParameter("@COSTOARTICULOADIC", SqlDbType.Money)
        PRM10.Direction = ParameterDirection.Input
        PRM10.Value = CostoAdicText.Text
        CMD.Parameters.Add(PRM10)

        'Me.RentaPrincipalText.Text = 0
        Dim PRM8 As New SqlParameter("@RENTAPRINCIPAL", SqlDbType.Money)
        PRM8.Direction = ParameterDirection.Input
        PRM8.Value = Me.RentaPrincipalText.Text
        CMD.Parameters.Add(PRM8)

        'Me.RentaAdicionalText.Text = 0
        Dim PRM9 As New SqlParameter("@RENTAADICIONAL", SqlDbType.Money)
        PRM9.Direction = ParameterDirection.Input
        PRM9.Value = Me.RentaAdicionalText.Text
        CMD.Parameters.Add(PRM9)

        Dim PRM6 As New SqlParameter("@OPCION", SqlDbType.VarChar, 1)
        PRM6.Direction = ParameterDirection.Input
        PRM6.Value = OpPagare
        CMD.Parameters.Add(PRM6)

        Dim PRM7 As New SqlParameter("@EXISTENCIA", SqlDbType.Bit)
        PRM7.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM7)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            If PRM7.Value = True Then
                MsgBox("El registro ya fue dado de alta anteriormente", MsgBoxStyle.Information)
                Exit Sub
            Else
                MsgBox("Registro almacenado Satisfactoriamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub

    Private Sub ConCostosPagare()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConCostosPagare", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@OP", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = 2
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@TIPSER", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = 0
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@ID", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = IdPagare
        CMD.Parameters.Add(PRM3)
        Dim SqlReader As SqlDataReader

        Try
            CON.Open()
            SqlReader = CMD.ExecuteReader

            While (SqlReader.Read)
                Me.TipServCombo.SelectedValue = SqlReader(0).ToString
                Me.AparatoCombo.SelectedValue = SqlReader(1).ToString
                'Me.CostoText.Text = SqlReader(2).ToString
                'Me.CostoText.Text = CType(Me.CostoText.Text, Double)
                Me.RentaPrincipalText.Text = SqlReader(2).ToString
                Me.RentaPrincipalText.Text = CType(Me.RentaPrincipalText.Text, Double)
                Me.RentaAdicionalText.Text = SqlReader(3).ToString
                Me.RentaAdicionalText.Text = CType(Me.RentaAdicionalText.Text, Double)
                'Me.CostoAdicText.Text = SqlReader(3).ToString
                'Me.CostoAdicText.Text = CType(Me.CostoAdicText.Text, Double)
                If SqlReader(4).ToString = "False" Then
                    NormalRadioBut.Checked = True
                Else
                    HDRadioBut.Checked = True
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub

    Private Sub TipServCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipServCombo.SelectedIndexChanged
        If Me.TipServCombo.SelectedValue = 2 Then
            Me.NormalRadioBut.Visible = False
            Me.HDRadioBut.Visible = False
            Me.CMBLabel3.Visible = False
        Else
            Me.NormalRadioBut.Visible = True
            Me.HDRadioBut.Visible = True
            Me.CMBLabel3.Visible = True
        End If
        MuestraMarcasAparatosPagare()
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub



    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        'If Me.CostoText.TextLength = 0 Then
        '    MsgBox("Capture el costo del Aparato", MsgBoxStyle.Exclamation)
        '    Exit Sub
        'End If
        'If Me.CostoAdicText.TextLength = 0 Then
        '    MsgBox("Capture el costo del Aparato Adicional", MsgBoxStyle.Exclamation)
        '    Exit Sub
        'End If
        If Me.RentaPrincipalText.TextLength = 0 Then
            MsgBox("Capture el costo de la renta mensual del Aparato Principal", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If Me.RentaAdicionalText.TextLength = 0 Then
            MsgBox("Capture el costo de la renta mensual del Aparato Adicional", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If Me.AparatoCombo.SelectedValue > 0 Then
            GuardaCostosPagare()
            BndActualizaPagare = True
            Me.Close()
        Else
            MsgBox("No existen aparatos en la Lista", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
    End Sub

    Private Sub NormalRadioBut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NormalRadioBut.CheckedChanged
        If Me.Visible = True Then
            Me.AparatoCombo.SelectedValue = 0
            MuestraMarcasAparatosPagare()
        End If
    End Sub

    Private Sub HDRadioBut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HDRadioBut.CheckedChanged
        If Me.Visible = True Then
            Me.AparatoCombo.SelectedValue = 0
            MuestraMarcasAparatosPagare()
        End If
    End Sub
End Class