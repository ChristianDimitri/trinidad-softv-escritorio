Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelDatosP


    Private Sub CMBLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel2.Click

    End Sub

    Private Sub FrmSelDatosP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        Me.Muestra_Colonia_RepTableAdapter.Connection = CON
        Me.Muestra_Colonia_RepTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Colonia_Rep)
        Me.Muestra_Calle_Asociada2TableAdapter.Connection = CON
        Me.Muestra_Calle_Asociada2TableAdapter.Fill(Me.DataSetarnoldo.Muestra_Calle_Asociada2, CInt(Me.ComboBox2.SelectedValue))
        

        CON.Close()


        MuestraSectorPenetracion()

        MuestraTapPenetracion()

        If eTipoPen = 1 Then
            Me.Panel2.Enabled = False
            Me.Panel2.Visible = False
            Me.Panel3.Enabled = False
            Me.Panel3.Visible = False
        ElseIf eTipoPen = 2 Then
            Me.Panel1.Enabled = False
            Me.Panel1.Visible = False
            Me.Panel3.Enabled = False
            Me.Panel3.Visible = False
        ElseIf eTipoPen = 3 Then
            Me.Panel1.Enabled = False
            Me.Panel1.Visible = False
            Me.Panel2.Enabled = False
            Me.Panel2.Visible = False
        End If

    End Sub

    

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If eTipoPen = 1 Then

            'If Me.ComboBox1.Text = "" Then
            '    MsgBox("Seleccione un Servicio.", MsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If Me.ComboBox2.Text = "" Then
                MsgBox("Seleccione un Barrio.", MsgBoxStyle.Information)
                Exit Sub
            End If

        ElseIf eTipoPen = 2 Then

            'If Me.ComboBox1.Text = "" Then
            '    MsgBox("Seleccione un Servicio.", MsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If Me.ComboBox4.Text = "" Then
                MsgBox("Seleccione un Sector.", MsgBoxStyle.Information)
                Exit Sub
            End If

        ElseIf eTipoPen = 3 Then

            'If Me.ComboBox1.Text = "" Then
            '    MsgBox("Seleccione un Servicio.", MsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If Me.ComboBox4.Text = "" Then
                MsgBox("Seleccione un Tap.", MsgBoxStyle.Information)
                Exit Sub
            End If

        End If


        'Locclv_txt = Me.ComboBox1.SelectedValue
        Locclv_colonia = Me.ComboBox2.SelectedValue
        If Me.ComboBox3.Text.Length > 0 Then Locclv_calle = Me.ComboBox3.SelectedValue
        eClv_Sector = Me.ComboBox4.SelectedValue

        If eTipoPen = 1 Then
            'Locclv_txt = Me.ComboBox1.SelectedValue
            Locclv_colonia = Me.ComboBox2.SelectedValue
            If Me.ComboBox3.Text.Length > 0 Then Locclv_calle = Me.ComboBox3.SelectedValue
            LocDesPC = Me.ComboBox2.Text
            LocDesPCa = Me.ComboBox3.Text


            eBndPenetracion = False
            RTapPenetracion = False

            If Me.ComboBox2.SelectedValue = 0 And Me.ComboBox3.SelectedValue = 0 Then
                eBndPenetracion = True
            End If

        ElseIf eTipoPen = 2 Then
            RTapPenetracion = False
            LocDesPC = Me.ComboBox4.Text
            LocDesPCa = ""

        ElseIf eTipoPen = 3 Then
            If Me.ComboBox5.SelectedValue = 0 Then
                RTapPenetracion = True
            End If
            Locclv_Tap = Me.ComboBox5.SelectedValue
            LocDesPC = Me.ComboBox5.Text
            LocDesPCa = ""
        End If

        LocDesP = Me.ComboBox1.Text
        Locbndpen1 = True
        eTipSer = GloClv_tipser2
        Dame_clv_session_Reportes()
        FrmSelServicioE.Show()
        Me.Close()

    End Sub

    Private Sub MuestraSectorPenetracion()

        Dim conexion As New SqlConnection(MiConexion)
        Dim dataAdapter As New SqlDataAdapter("EXEC MuestraSectorPenetracion", conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox4.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
    Private Sub MuestraTapPenetracion()
        BaseII.limpiaParametros()
        Me.ComboBox5.DataSource = BaseII.ConsultaDT("MuestraTapPenetracion")
    End Sub
    Private Sub Dame_clv_session_Reportes()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = CLng(parametro.Value.ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.Muestra_Calle_Asociada2TableAdapter.Connection = con
        Me.Muestra_Calle_Asociada2TableAdapter.Fill(Me.DataSetarnoldo.Muestra_Calle_Asociada2, CInt(Me.ComboBox2.SelectedValue))
        con.Close()
    End Sub
End Class