﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCatalogoIP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TipoCobroLabel1 = New System.Windows.Forms.Label()
        Me.NombreLabel = New System.Windows.Forms.Label()
        Me.Clv_calleLabel1 = New System.Windows.Forms.Label()
        Me.bBuscarIP = New System.Windows.Forms.Button()
        Me.bBuscarClave = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.laUltimoClienteAsignado = New System.Windows.Forms.Label()
        Me.laUltimaFechaLiberacion = New System.Windows.Forms.Label()
        Me.laUltimaFechaAsignacion = New System.Windows.Forms.Label()
        Me.laStatus = New System.Windows.Forms.Label()
        Me.laClave = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.laIP = New System.Windows.Forms.Label()
        Me.tbIP = New System.Windows.Forms.TextBox()
        Me.tbClave = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.gridIP = New System.Windows.Forms.DataGridView()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimaFechaAsignacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimaFechaLiberacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoClienteAsignado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bSalir = New System.Windows.Forms.Button()
        Me.bModificar = New System.Windows.Forms.Button()
        Me.bConsultar = New System.Windows.Forms.Button()
        Me.bNuevo = New System.Windows.Forms.Button()
        Me.bStatus = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbStatus = New System.Windows.Forms.ComboBox()
        Me.Panel1.SuspendLayout()
        CType(Me.gridIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(18, 245)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(165, 15)
        Me.Label8.TabIndex = 27
        Me.Label8.Text = "Ultimo Cliente Asignado:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(17, 197)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(190, 15)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Ultima Fecha De Liberación:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(16, 149)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 15)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Ultima Fecha De Asignación:"
        '
        'TipoCobroLabel1
        '
        Me.TipoCobroLabel1.AutoSize = True
        Me.TipoCobroLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoCobroLabel1.ForeColor = System.Drawing.Color.White
        Me.TipoCobroLabel1.Location = New System.Drawing.Point(17, 101)
        Me.TipoCobroLabel1.Name = "TipoCobroLabel1"
        Me.TipoCobroLabel1.Size = New System.Drawing.Size(51, 15)
        Me.TipoCobroLabel1.TabIndex = 19
        Me.TipoCobroLabel1.Text = "Status:"
        '
        'NombreLabel
        '
        Me.NombreLabel.AutoSize = True
        Me.NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreLabel.ForeColor = System.Drawing.Color.White
        Me.NombreLabel.Location = New System.Drawing.Point(17, 59)
        Me.NombreLabel.Name = "NombreLabel"
        Me.NombreLabel.Size = New System.Drawing.Size(89, 15)
        Me.NombreLabel.TabIndex = 3
        Me.NombreLabel.Text = "Dirección IP:"
        '
        'Clv_calleLabel1
        '
        Me.Clv_calleLabel1.AutoSize = True
        Me.Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Me.Clv_calleLabel1.Location = New System.Drawing.Point(17, 30)
        Me.Clv_calleLabel1.Name = "Clv_calleLabel1"
        Me.Clv_calleLabel1.Size = New System.Drawing.Size(50, 15)
        Me.Clv_calleLabel1.TabIndex = 1
        Me.Clv_calleLabel1.Text = "Clave :"
        '
        'bBuscarIP
        '
        Me.bBuscarIP.BackColor = System.Drawing.Color.DarkOrange
        Me.bBuscarIP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bBuscarIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bBuscarIP.ForeColor = System.Drawing.Color.Black
        Me.bBuscarIP.Location = New System.Drawing.Point(16, 129)
        Me.bBuscarIP.Name = "bBuscarIP"
        Me.bBuscarIP.Size = New System.Drawing.Size(88, 23)
        Me.bBuscarIP.TabIndex = 15
        Me.bBuscarIP.Text = "&Buscar"
        Me.bBuscarIP.UseVisualStyleBackColor = False
        '
        'bBuscarClave
        '
        Me.bBuscarClave.BackColor = System.Drawing.Color.DarkOrange
        Me.bBuscarClave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bBuscarClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bBuscarClave.ForeColor = System.Drawing.Color.Black
        Me.bBuscarClave.Location = New System.Drawing.Point(127, 53)
        Me.bBuscarClave.Name = "bBuscarClave"
        Me.bBuscarClave.Size = New System.Drawing.Size(88, 23)
        Me.bBuscarClave.TabIndex = 12
        Me.bBuscarClave.Text = "&Buscar"
        Me.bBuscarClave.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.laUltimoClienteAsignado)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.laUltimaFechaLiberacion)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.laUltimaFechaAsignacion)
        Me.Panel1.Controls.Add(Me.TipoCobroLabel1)
        Me.Panel1.Controls.Add(Me.laStatus)
        Me.Panel1.Controls.Add(Me.laClave)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.NombreLabel)
        Me.Panel1.Controls.Add(Me.laIP)
        Me.Panel1.Controls.Add(Me.Clv_calleLabel1)
        Me.Panel1.Location = New System.Drawing.Point(16, 240)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(243, 296)
        Me.Panel1.TabIndex = 16
        '
        'laUltimoClienteAsignado
        '
        Me.laUltimoClienteAsignado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laUltimoClienteAsignado.Location = New System.Drawing.Point(27, 265)
        Me.laUltimoClienteAsignado.Name = "laUltimoClienteAsignado"
        Me.laUltimoClienteAsignado.Size = New System.Drawing.Size(202, 23)
        Me.laUltimoClienteAsignado.TabIndex = 28
        '
        'laUltimaFechaLiberacion
        '
        Me.laUltimaFechaLiberacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laUltimaFechaLiberacion.Location = New System.Drawing.Point(27, 217)
        Me.laUltimaFechaLiberacion.Name = "laUltimaFechaLiberacion"
        Me.laUltimaFechaLiberacion.Size = New System.Drawing.Size(202, 23)
        Me.laUltimaFechaLiberacion.TabIndex = 26
        '
        'laUltimaFechaAsignacion
        '
        Me.laUltimaFechaAsignacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laUltimaFechaAsignacion.Location = New System.Drawing.Point(24, 169)
        Me.laUltimaFechaAsignacion.Name = "laUltimaFechaAsignacion"
        Me.laUltimaFechaAsignacion.Size = New System.Drawing.Size(215, 23)
        Me.laUltimaFechaAsignacion.TabIndex = 24
        '
        'laStatus
        '
        Me.laStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laStatus.Location = New System.Drawing.Point(24, 121)
        Me.laStatus.Name = "laStatus"
        Me.laStatus.Size = New System.Drawing.Size(148, 23)
        Me.laStatus.TabIndex = 20
        '
        'laClave
        '
        Me.laClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laClave.Location = New System.Drawing.Point(74, 32)
        Me.laClave.Name = "laClave"
        Me.laClave.Size = New System.Drawing.Size(100, 23)
        Me.laClave.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(4, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(191, 18)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos de la Dirección IP"
        '
        'laIP
        '
        Me.laIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laIP.Location = New System.Drawing.Point(21, 79)
        Me.laIP.Name = "laIP"
        Me.laIP.Size = New System.Drawing.Size(210, 20)
        Me.laIP.TabIndex = 22
        '
        'tbIP
        '
        Me.tbIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIP.Location = New System.Drawing.Point(16, 102)
        Me.tbIP.Name = "tbIP"
        Me.tbIP.Size = New System.Drawing.Size(243, 21)
        Me.tbIP.TabIndex = 14
        '
        'tbClave
        '
        Me.tbClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClave.Location = New System.Drawing.Point(16, 55)
        Me.tbClave.Name = "tbClave"
        Me.tbClave.Size = New System.Drawing.Size(88, 21)
        Me.tbClave.TabIndex = 11
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(203, 20)
        Me.CMBLabel1.TabIndex = 9
        Me.CMBLabel1.Text = "Buscar Dirección IP por:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Clave :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 15)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Dirección IP:"
        '
        'gridIP
        '
        Me.gridIP.AllowUserToAddRows = False
        Me.gridIP.AllowUserToDeleteRows = False
        Me.gridIP.AllowUserToOrderColumns = True
        Me.gridIP.AllowUserToResizeRows = False
        Me.gridIP.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridIP.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.gridIP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridIP.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clave, Me.IP, Me.Status, Me.UltimaFechaAsignacion, Me.UltimaFechaLiberacion, Me.UltimoClienteAsignado})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridIP.DefaultCellStyle = DataGridViewCellStyle6
        Me.gridIP.Location = New System.Drawing.Point(265, 12)
        Me.gridIP.Name = "gridIP"
        Me.gridIP.ReadOnly = True
        Me.gridIP.RowHeadersVisible = False
        Me.gridIP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gridIP.Size = New System.Drawing.Size(566, 524)
        Me.gridIP.TabIndex = 17
        Me.gridIP.TabStop = False
        '
        'Clave
        '
        Me.Clave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        '
        'IP
        '
        Me.IP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.IP.HeaderText = "IP"
        Me.IP.Name = "IP"
        Me.IP.ReadOnly = True
        '
        'Status
        '
        Me.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'UltimaFechaAsignacion
        '
        Me.UltimaFechaAsignacion.HeaderText = "UltimaFechaAsignacion"
        Me.UltimaFechaAsignacion.Name = "UltimaFechaAsignacion"
        Me.UltimaFechaAsignacion.ReadOnly = True
        Me.UltimaFechaAsignacion.Visible = False
        '
        'UltimaFechaLiberacion
        '
        Me.UltimaFechaLiberacion.HeaderText = "UltimaFechaLiberacion"
        Me.UltimaFechaLiberacion.Name = "UltimaFechaLiberacion"
        Me.UltimaFechaLiberacion.ReadOnly = True
        Me.UltimaFechaLiberacion.Visible = False
        '
        'UltimoClienteAsignado
        '
        Me.UltimoClienteAsignado.HeaderText = "UltimoClienteAsignado"
        Me.UltimoClienteAsignado.Name = "UltimoClienteAsignado"
        Me.UltimoClienteAsignado.ReadOnly = True
        Me.UltimoClienteAsignado.Visible = False
        '
        'bSalir
        '
        Me.bSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.bSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bSalir.ForeColor = System.Drawing.Color.Black
        Me.bSalir.Location = New System.Drawing.Point(837, 500)
        Me.bSalir.Name = "bSalir"
        Me.bSalir.Size = New System.Drawing.Size(136, 36)
        Me.bSalir.TabIndex = 23
        Me.bSalir.Text = "&SALIR"
        Me.bSalir.UseVisualStyleBackColor = False
        '
        'bModificar
        '
        Me.bModificar.BackColor = System.Drawing.Color.Orange
        Me.bModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bModificar.ForeColor = System.Drawing.Color.Black
        Me.bModificar.Location = New System.Drawing.Point(837, 96)
        Me.bModificar.Name = "bModificar"
        Me.bModificar.Size = New System.Drawing.Size(136, 36)
        Me.bModificar.TabIndex = 26
        Me.bModificar.Text = "&MODIFICAR"
        Me.bModificar.UseVisualStyleBackColor = False
        Me.bModificar.Visible = False
        '
        'bConsultar
        '
        Me.bConsultar.BackColor = System.Drawing.Color.Orange
        Me.bConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bConsultar.ForeColor = System.Drawing.Color.Black
        Me.bConsultar.Location = New System.Drawing.Point(837, 54)
        Me.bConsultar.Name = "bConsultar"
        Me.bConsultar.Size = New System.Drawing.Size(136, 36)
        Me.bConsultar.TabIndex = 25
        Me.bConsultar.Text = "&CONSULTA"
        Me.bConsultar.UseVisualStyleBackColor = False
        '
        'bNuevo
        '
        Me.bNuevo.BackColor = System.Drawing.Color.Orange
        Me.bNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bNuevo.ForeColor = System.Drawing.Color.Black
        Me.bNuevo.Location = New System.Drawing.Point(837, 12)
        Me.bNuevo.Name = "bNuevo"
        Me.bNuevo.Size = New System.Drawing.Size(136, 36)
        Me.bNuevo.TabIndex = 24
        Me.bNuevo.Text = "&NUEVO"
        Me.bNuevo.UseVisualStyleBackColor = False
        Me.bNuevo.Visible = False
        '
        'bStatus
        '
        Me.bStatus.BackColor = System.Drawing.Color.DarkOrange
        Me.bStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bStatus.ForeColor = System.Drawing.Color.Black
        Me.bStatus.Location = New System.Drawing.Point(16, 211)
        Me.bStatus.Name = "bStatus"
        Me.bStatus.Size = New System.Drawing.Size(88, 23)
        Me.bStatus.TabIndex = 28
        Me.bStatus.Text = "&Buscar"
        Me.bStatus.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 166)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 15)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Status:"
        '
        'cbStatus
        '
        Me.cbStatus.DisplayMember = "Status"
        Me.cbStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbStatus.FormattingEnabled = True
        Me.cbStatus.Location = New System.Drawing.Point(16, 184)
        Me.cbStatus.Name = "cbStatus"
        Me.cbStatus.Size = New System.Drawing.Size(243, 21)
        Me.cbStatus.TabIndex = 29
        '
        'BrwCatalogoIP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(985, 544)
        Me.Controls.Add(Me.cbStatus)
        Me.Controls.Add(Me.bStatus)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.bModificar)
        Me.Controls.Add(Me.bConsultar)
        Me.Controls.Add(Me.bNuevo)
        Me.Controls.Add(Me.bSalir)
        Me.Controls.Add(Me.gridIP)
        Me.Controls.Add(Me.bBuscarIP)
        Me.Controls.Add(Me.bBuscarClave)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.tbIP)
        Me.Controls.Add(Me.tbClave)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Name = "BrwCatalogoIP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogos IPs"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.gridIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bBuscarIP As System.Windows.Forms.Button
    Friend WithEvents bBuscarClave As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents laUltimoClienteAsignado As System.Windows.Forms.Label
    Friend WithEvents laUltimaFechaLiberacion As System.Windows.Forms.Label
    Friend WithEvents laUltimaFechaAsignacion As System.Windows.Forms.Label
    Friend WithEvents laStatus As System.Windows.Forms.Label
    Friend WithEvents laClave As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents laIP As System.Windows.Forms.Label
    Friend WithEvents tbIP As System.Windows.Forms.TextBox
    Friend WithEvents tbClave As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gridIP As System.Windows.Forms.DataGridView
    Friend WithEvents bSalir As System.Windows.Forms.Button
    Friend WithEvents bModificar As System.Windows.Forms.Button
    Friend WithEvents bConsultar As System.Windows.Forms.Button
    Friend WithEvents bNuevo As System.Windows.Forms.Button
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimaFechaAsignacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimaFechaLiberacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimoClienteAsignado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bStatus As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TipoCobroLabel1 As System.Windows.Forms.Label
    Friend WithEvents NombreLabel As System.Windows.Forms.Label
    Friend WithEvents Clv_calleLabel1 As System.Windows.Forms.Label
End Class
