<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEncargadosEmails
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_EncargadoLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEncargadosEmails))
        Me.DataSetEric = New sofTV.DataSetEric
        Me.ConEncargadosEmailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConEncargadosEmailsTableAdapter = New sofTV.DataSetEricTableAdapters.ConEncargadosEmailsTableAdapter
        Me.ConEncargadosEmailsBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ConEncargadosEmailsBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Clv_EncargadoTextBox = New System.Windows.Forms.TextBox
        Me.NombreTextBox = New System.Windows.Forms.TextBox
        Me.EmailTextBox = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Clv_EncargadoLabel = New System.Windows.Forms.Label
        NombreLabel = New System.Windows.Forms.Label
        EmailLabel = New System.Windows.Forms.Label
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConEncargadosEmailsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConEncargadosEmailsBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConEncargadosEmailsBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_EncargadoLabel
        '
        Clv_EncargadoLabel.AutoSize = True
        Clv_EncargadoLabel.Location = New System.Drawing.Point(122, 191)
        Clv_EncargadoLabel.Name = "Clv_EncargadoLabel"
        Clv_EncargadoLabel.Size = New System.Drawing.Size(80, 13)
        Clv_EncargadoLabel.TabIndex = 2
        Clv_EncargadoLabel.Text = "Clv Encargado:"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(23, 14)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(67, 16)
        NombreLabel.TabIndex = 4
        NombreLabel.Text = "Nombre:"
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = True
        EmailLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EmailLabel.Location = New System.Drawing.Point(23, 70)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(51, 16)
        EmailLabel.TabIndex = 6
        EmailLabel.Text = "Email:"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConEncargadosEmailsBindingSource
        '
        Me.ConEncargadosEmailsBindingSource.DataMember = "ConEncargadosEmails"
        Me.ConEncargadosEmailsBindingSource.DataSource = Me.DataSetEric
        '
        'ConEncargadosEmailsTableAdapter
        '
        Me.ConEncargadosEmailsTableAdapter.ClearBeforeFill = True
        '
        'ConEncargadosEmailsBindingNavigator
        '
        Me.ConEncargadosEmailsBindingNavigator.AddNewItem = Nothing
        Me.ConEncargadosEmailsBindingNavigator.BindingSource = Me.ConEncargadosEmailsBindingSource
        Me.ConEncargadosEmailsBindingNavigator.CountItem = Nothing
        Me.ConEncargadosEmailsBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConEncargadosEmailsBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConEncargadosEmailsBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConEncargadosEmailsBindingNavigatorSaveItem})
        Me.ConEncargadosEmailsBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConEncargadosEmailsBindingNavigator.MoveFirstItem = Nothing
        Me.ConEncargadosEmailsBindingNavigator.MoveLastItem = Nothing
        Me.ConEncargadosEmailsBindingNavigator.MoveNextItem = Nothing
        Me.ConEncargadosEmailsBindingNavigator.MovePreviousItem = Nothing
        Me.ConEncargadosEmailsBindingNavigator.Name = "ConEncargadosEmailsBindingNavigator"
        Me.ConEncargadosEmailsBindingNavigator.PositionItem = Nothing
        Me.ConEncargadosEmailsBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConEncargadosEmailsBindingNavigator.Size = New System.Drawing.Size(502, 25)
        Me.ConEncargadosEmailsBindingNavigator.TabIndex = 2
        Me.ConEncargadosEmailsBindingNavigator.TabStop = True
        Me.ConEncargadosEmailsBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConEncargadosEmailsBindingNavigatorSaveItem
        '
        Me.ConEncargadosEmailsBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConEncargadosEmailsBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConEncargadosEmailsBindingNavigatorSaveItem.Name = "ConEncargadosEmailsBindingNavigatorSaveItem"
        Me.ConEncargadosEmailsBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.ConEncargadosEmailsBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_EncargadoTextBox
        '
        Me.Clv_EncargadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConEncargadosEmailsBindingSource, "Clv_Encargado", True))
        Me.Clv_EncargadoTextBox.Location = New System.Drawing.Point(125, 188)
        Me.Clv_EncargadoTextBox.Name = "Clv_EncargadoTextBox"
        Me.Clv_EncargadoTextBox.ReadOnly = True
        Me.Clv_EncargadoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_EncargadoTextBox.TabIndex = 3
        Me.Clv_EncargadoTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConEncargadosEmailsBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(26, 33)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(421, 22)
        Me.NombreTextBox.TabIndex = 0
        '
        'EmailTextBox
        '
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConEncargadosEmailsBindingSource, "Email", True))
        Me.EmailTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailTextBox.Location = New System.Drawing.Point(26, 89)
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(310, 22)
        Me.EmailTextBox.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.EmailTextBox)
        Me.Panel1.Controls.Add(EmailLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Location = New System.Drawing.Point(-1, 23)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(506, 206)
        Me.Panel1.TabIndex = 8
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(335, 152)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmEncargadosEmails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(502, 228)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ConEncargadosEmailsBindingNavigator)
        Me.Controls.Add(Clv_EncargadoLabel)
        Me.Controls.Add(Me.Clv_EncargadoTextBox)
        Me.Name = "FrmEncargadosEmails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Encargado de Fallas de Interface"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConEncargadosEmailsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConEncargadosEmailsBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConEncargadosEmailsBindingNavigator.ResumeLayout(False)
        Me.ConEncargadosEmailsBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConEncargadosEmailsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConEncargadosEmailsTableAdapter As sofTV.DataSetEricTableAdapters.ConEncargadosEmailsTableAdapter
    Friend WithEvents ConEncargadosEmailsBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConEncargadosEmailsBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_EncargadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
