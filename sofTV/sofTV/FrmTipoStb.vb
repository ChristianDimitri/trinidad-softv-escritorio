﻿Public Class FrmTipoStb

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If CheckBox1.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False Then
            MsgBox("Seleccione por lo menos una Opción", MsgBoxStyle.Information)
            GloTipoCablemodemR = 0
            Exit Sub
        ElseIf CheckBox1.Checked = True Then
            GloTipoCablemodemR = 1
        ElseIf CheckBox3.Checked = True Then
            GloTipoCablemodemR = 2
        ElseIf CheckBox4.Checked = True Then
            GloTipoCablemodemR = 3
        End If

        LocGloOpRep = 27
        FrmImprimirFac.Show()

        Me.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GloTipoCablemodemR = 0
        Me.Close()
    End Sub

    Private Sub FrmTipoCablemodem_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            CheckBox1.Checked = False
            CheckBox3.Visible = True
            CheckBox4.Visible = True
        ElseIf CheckBox2.Checked = False Then
            CheckBox1.Checked = True
            CheckBox3.Visible = False
            CheckBox4.Visible = False
        End If

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            CheckBox2.Checked = False
            CheckBox3.Visible = False
            CheckBox4.Visible = False
        ElseIf CheckBox1.Checked = False Then
            CheckBox2.Checked = True
            CheckBox3.Visible = True
            CheckBox4.Visible = True
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.Checked = True Then
            CheckBox3.Checked = False
        ElseIf CheckBox4.Checked = False Then
            CheckBox3.Checked = True
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            CheckBox4.Checked = False
        ElseIf CheckBox3.Checked = False Then
            CheckBox4.Checked = True
        End If
    End Sub

End Class