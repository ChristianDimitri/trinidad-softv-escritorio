﻿Public Class BrwCatalogoIP

    Private Sub bNuevo_Click(sender As Object, e As EventArgs) Handles bNuevo.Click
        BanderaCatalogoIP = "N"
        FrmCatalogoIP.ShowDialog()
        TraerTodasIPs()
    End Sub

    Private Sub BrwCatalogoIP_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        cbStatus.DataSource = BaseII.ConsultaDT("GetAllStatusIP")

        TraerTodasIPs()
        LlenarLabels()
    End Sub

    Private Sub bModificar_Click(sender As Object, e As EventArgs) Handles bModificar.Click
        BanderaCatalogoIP = "M"
        IdIP = Convert.ToInt32(gridIP("Clave", gridIP.CurrentRow.Index).Value)
        FrmCatalogoIP.ShowDialog()
        TraerTodasIPs()
    End Sub

    Private Sub BrwCatalogoIP_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated

    End Sub

    Private Sub TraerTodasIPs()
        BaseII.limpiaParametros()
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("GetAllCatalogoIP")
        Dim dr As DataRow
        gridIP.Rows.Clear()
        For Each dr In dt.Rows
            gridIP.Rows.Add()
            gridIP("Clave", gridIP.Rows.Count - 1).Value = dr("IdIP").ToString()
            gridIP("IP", gridIP.Rows.Count - 1).Value = dr("IP").ToString()
            gridIP("Status", gridIP.Rows.Count - 1).Value = dr("Status").ToString()
            gridIP("UltimaFechaAsignacion", gridIP.Rows.Count - 1).Value = dr("UltimaFechaAsignacion").ToString()
            gridIP("UltimaFechaLiberacion", gridIP.Rows.Count - 1).Value = dr("UltimaFechaLiberacion").ToString()
            gridIP("UltimoClienteAsignado", gridIP.Rows.Count - 1).Value = dr("UltimoClienteAsignado").ToString()
        Next
    End Sub

    Private Sub TraerIPsByClave(clave As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Id", SqlDbType.Int, clave)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("GetCatalogoIPById")
        Dim dr As DataRow
        gridIP.Rows.Clear()
        For Each dr In dt.Rows
            gridIP.Rows.Add()
            gridIP("Clave", gridIP.Rows.Count - 1).Value = dr("IdIP").ToString()
            gridIP("IP", gridIP.Rows.Count - 1).Value = dr("IP").ToString()
            gridIP("Status", gridIP.Rows.Count - 1).Value = dr("Status").ToString()
            gridIP("UltimaFechaAsignacion", gridIP.Rows.Count - 1).Value = dr("UltimaFechaAsignacion").ToString()
            gridIP("UltimaFechaLiberacion", gridIP.Rows.Count - 1).Value = dr("UltimaFechaLiberacion").ToString()
            gridIP("UltimoClienteAsignado", gridIP.Rows.Count - 1).Value = dr("UltimoClienteAsignado").ToString()
        Next
    End Sub

    Private Sub TraerIPsByIP(IP As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IP", SqlDbType.NVarChar, IP)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("GetCatalogoIPByIP")
        Dim dr As DataRow
        gridIP.Rows.Clear()
        For Each dr In dt.Rows
            gridIP.Rows.Add()
            gridIP("Clave", gridIP.Rows.Count - 1).Value = dr("IdIP").ToString()
            gridIP("IP", gridIP.Rows.Count - 1).Value = dr("IP").ToString()
            gridIP("Status", gridIP.Rows.Count - 1).Value = dr("Status").ToString()
            gridIP("UltimaFechaAsignacion", gridIP.Rows.Count - 1).Value = dr("UltimaFechaAsignacion").ToString()
            gridIP("UltimaFechaLiberacion", gridIP.Rows.Count - 1).Value = dr("UltimaFechaLiberacion").ToString()
            gridIP("UltimoClienteAsignado", gridIP.Rows.Count - 1).Value = dr("UltimoClienteAsignado").ToString()
        Next
    End Sub

    Private Sub TraerIPsByStatus(Status As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Status", SqlDbType.NVarChar, Status)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("GetCatalogoIPByStatus")
        Dim dr As DataRow
        gridIP.Rows.Clear()
        For Each dr In dt.Rows
            gridIP.Rows.Add()
            gridIP("Clave", gridIP.Rows.Count - 1).Value = dr("IdIP").ToString()
            gridIP("IP", gridIP.Rows.Count - 1).Value = dr("IP").ToString()
            gridIP("Status", gridIP.Rows.Count - 1).Value = dr("Status").ToString()
            gridIP("UltimaFechaAsignacion", gridIP.Rows.Count - 1).Value = dr("UltimaFechaAsignacion").ToString()
            gridIP("UltimaFechaLiberacion", gridIP.Rows.Count - 1).Value = dr("UltimaFechaLiberacion").ToString()
            gridIP("UltimoClienteAsignado", gridIP.Rows.Count - 1).Value = dr("UltimoClienteAsignado").ToString()
        Next
    End Sub

    Private Sub gridIP_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles gridIP.CellContentClick

    End Sub

    Private Sub LlenarLabels()
        If gridIP.Rows.Count = 0 Then
            Exit Sub
        End If
        If gridIP(0, 0).Value <> Nothing Then
            laClave.Text = gridIP("Clave", gridIP.CurrentRow.Index).Value.ToString()
            laIP.Text = gridIP("IP", gridIP.CurrentRow.Index).Value.ToString()
            laStatus.Text = gridIP("Status", gridIP.CurrentRow.Index).Value.ToString()
            laUltimaFechaAsignacion.Text = gridIP("UltimaFechaAsignacion", gridIP.CurrentRow.Index).Value.ToString()
            laUltimaFechaLiberacion.Text = gridIP("UltimaFechaLiberacion", gridIP.CurrentRow.Index).Value.ToString()
            laUltimoClienteAsignado.Text = gridIP("UltimoClienteAsignado", gridIP.CurrentRow.Index).Value.ToString()
        End If
    End Sub

    Private Sub gridIP_SelectionChanged(sender As Object, e As EventArgs) Handles gridIP.SelectionChanged
        LlenarLabels()
    End Sub

    Private Sub bBuscarClave_Click(sender As Object, e As EventArgs) Handles bBuscarClave.Click
        If tbClave.Text.Trim() = "" Then
            Exit Sub
        End If

        TraerIPsByClave(Convert.ToInt32(tbClave.Text.Trim()))
        LlenarLabels()
    End Sub

    Private Sub bBuscarIP_Click(sender As Object, e As EventArgs) Handles bBuscarIP.Click
        If tbIP.Text.Trim() = "" Then
            Exit Sub
        End If

        TraerIPsByIP(tbIP.Text.Trim())
        LlenarLabels()
    End Sub

    Private Sub bStatus_Click(sender As Object, e As EventArgs) Handles bStatus.Click
        If cbStatus.Text.Trim() = "" Then
            Exit Sub
        End If

        TraerIPsByStatus(cbStatus.Text.Trim())
        LlenarLabels()
    End Sub

    Private Sub bSalir_Click(sender As Object, e As EventArgs) Handles bSalir.Click
        Me.Close()
    End Sub

    Private Sub bConsultar_Click(sender As Object, e As EventArgs) Handles bConsultar.Click
        BanderaCatalogoIP = "C"
        IdIP = Convert.ToInt32(gridIP("Clave", gridIP.CurrentRow.Index).Value)
        FrmCatalogoIP.ShowDialog()
        TraerTodasIPs()
    End Sub
End Class