﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSeriesFolio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBSerie = New System.Windows.Forms.Label()
        Me.Cmb_serie = New System.Windows.Forms.ComboBox()
        Me.CMBfolio = New System.Windows.Forms.Label()
        Me.Txt_Serie = New System.Windows.Forms.TextBox()
        Me.Btn_Imprimir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Btn_Salir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CMBSerie
        '
        Me.CMBSerie.AutoSize = True
        Me.CMBSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBSerie.Location = New System.Drawing.Point(163, 58)
        Me.CMBSerie.Name = "CMBSerie"
        Me.CMBSerie.Size = New System.Drawing.Size(90, 15)
        Me.CMBSerie.TabIndex = 0
        Me.CMBSerie.Text = "Dosificación:"
        '
        'Cmb_serie
        '
        Me.Cmb_serie.FormattingEnabled = True
        Me.Cmb_serie.Location = New System.Drawing.Point(259, 58)
        Me.Cmb_serie.Name = "Cmb_serie"
        Me.Cmb_serie.Size = New System.Drawing.Size(199, 21)
        Me.Cmb_serie.TabIndex = 1
        '
        'CMBfolio
        '
        Me.CMBfolio.AutoSize = True
        Me.CMBfolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBfolio.Location = New System.Drawing.Point(31, 93)
        Me.CMBfolio.Name = "CMBfolio"
        Me.CMBfolio.Size = New System.Drawing.Size(222, 15)
        Me.CMBfolio.TabIndex = 2
        Me.CMBfolio.Text = "¿Cuantos folios deseas imprimir?"
        '
        'Txt_Serie
        '
        Me.Txt_Serie.Location = New System.Drawing.Point(259, 92)
        Me.Txt_Serie.Name = "Txt_Serie"
        Me.Txt_Serie.Size = New System.Drawing.Size(75, 20)
        Me.Txt_Serie.TabIndex = 3
        '
        'Btn_Imprimir
        '
        Me.Btn_Imprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Imprimir.Location = New System.Drawing.Point(135, 153)
        Me.Btn_Imprimir.Name = "Btn_Imprimir"
        Me.Btn_Imprimir.Size = New System.Drawing.Size(147, 42)
        Me.Btn_Imprimir.TabIndex = 4
        Me.Btn_Imprimir.Text = "&Imprimir"
        Me.Btn_Imprimir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Btn_Salir
        '
        Me.Btn_Salir.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Salir.Location = New System.Drawing.Point(311, 153)
        Me.Btn_Salir.Name = "Btn_Salir"
        Me.Btn_Salir.Size = New System.Drawing.Size(147, 42)
        Me.Btn_Salir.TabIndex = 5
        Me.Btn_Salir.Text = "&Salir"
        Me.Btn_Salir.UseVisualStyleBackColor = True
        '
        'FrmSeriesFolio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(537, 235)
        Me.Controls.Add(Me.Btn_Salir)
        Me.Controls.Add(Me.Btn_Imprimir)
        Me.Controls.Add(Me.Txt_Serie)
        Me.Controls.Add(Me.CMBfolio)
        Me.Controls.Add(Me.Cmb_serie)
        Me.Controls.Add(Me.CMBSerie)
        Me.MaximizeBox = False
        Me.Name = "FrmSeriesFolio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Series  Folio"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBSerie As System.Windows.Forms.Label
    Friend WithEvents Cmb_serie As System.Windows.Forms.ComboBox
    Friend WithEvents CMBfolio As System.Windows.Forms.Label
    Friend WithEvents Txt_Serie As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Imprimir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Btn_Salir As System.Windows.Forms.Button
End Class
