Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class FrmDatosBancoProspectos
    'datos bitacora
    Private Nombre As String = Nothing
    Private Banco As String = Nothing
    Private CuentaBancaria As String = Nothing
    Private TipoCuenta As String = Nothing
    Private Vencimiento As String = Nothing
    Private codigoseguridad As String = Nothing
    Private customersByCityReport As ReportDocument
    'findatosbitacora

    'Private Sub ConfigureCrystalReportcargosautomaticos()
    '    Try
    '        Dim impresora As String = Nothing
    '        customersByCityReport = New ReportDocument
    '        Dim connectionInfo As New ConnectionInfo

    '        connectionInfo.ServerName = GloServerName
    '        connectionInfo.DatabaseName = GloDatabaseName
    '        connectionInfo.UserID = GloUserID
    '        connectionInfo.Password = GloPassword

    '        Dim reportPath As String = Nothing
    '        Dim Subtitulo As String = Nothing
    '        Dim mySelectFormula As String = Nothing

    '        reportPath = RutaReportes + "\Solicitudcargoauto.rpt"
    '        '  mySelectFormula = "Listado de Cambios En Bitacora Del Sistema"


    '        customersByCityReport.Load(reportPath)


    '        SetDBLogonForReport(connectionInfo, customersByCityReport)


    '        'op, Loc1clv_sistema, Loc1Fechaini, Loc1Fechafin, Loc1horaini, Loc1horafin, Loc1clv_usuario, Loc1clv_pantalla, Loc1clv_control

    '        '(@op int
    '        customersByCityReport.SetParameterValue(0, Contrato)
    '        ',@clv_sistema varchar(max),@fecha1 varchar(max),@fecha2 varchar(max),@hora1 varchar(max),@hora2 varchar(max),@clv_usuario varchar(max),@clv_pantalla varchar(max),@clv_control varchar(max))


    '        'Subtitulo = "Sucursal:" & GloSucusal
    '        customersByCityReport.PrintOptions.PrinterName = ImpresoraContatos
    '        customersByCityReport.PrintToPrinter(1, True, 1, 1)

    '        customersByCityReport = Nothing

    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub
    'Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    Try
    '        Dim myTables As Tables = myReportDocument.Database.Tables
    '        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '        For Each myTable In myTables
    '            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '            myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '            myTable.ApplyLogOnInfo(myTableLogonInfo)
    '            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '        Next
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Private Sub damedatosbitacora()
        Try
            If OpcionCli = "M " Then
                Nombre = Me.NOMBRETextBox.Text
                Banco = Me.ComboBox1.Text
                CuentaBancaria = Me.CUENTA_BANCOTextBox.Text
                TipoCuenta = Me.TIPODECUENTA.Text
                Vencimiento = Me.MaskedTextBox1.Text
                codigoseguridad = Me.CODIGOSEGURIDADTextBox.Text
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub guardabitacora(ByVal Contrato As Integer)
        Try
            If OpcionCli = "M" Then
                'Nombre = Me.NOMBRETextBox.Text
                bitsist(GloUsuario, Contrato, LocGloSistema, Me.Name, Me.NOMBRETextBox.Name, Nombre, Me.NOMBRETextBox.Text, LocClv_Ciudad)
                'Banco = Me.ComboBox1.Text
                bitsist(GloUsuario, Contrato, LocGloSistema, Me.Name, "Comboboxbanco", Banco, Me.ComboBox1.Text, LocClv_Ciudad)
                'CuentaBancaria = Me.CUENTA_BANCOTextBox.Text
                bitsist(GloUsuario, Contrato, LocGloSistema, Me.Name, Me.CUENTA_BANCOTextBox.Name, CuentaBancaria, Me.CUENTA_BANCOTextBox.Text, LocClv_Ciudad)
                'TipoCuenta = Me.TIPODECUENTA.Text
                bitsist(GloUsuario, Contrato, LocGloSistema, Me.Name, Me.TIPODECUENTA.Name, TipoCuenta, Me.TIPODECUENTA.Text, LocClv_Ciudad)
                'Vencimiento = Me.MaskedTextBox1.Text
                bitsist(GloUsuario, Contrato, LocGloSistema, Me.Name, "VencimientoMasked", Vencimiento, Me.MaskedTextBox1.Text, LocClv_Ciudad)
                'codigoseguridad = Me.CODIGOSEGURIDADTextBox.Text
                bitsist(GloUsuario, Contrato, LocGloSistema, Me.Name, Me.CODIGOSEGURIDADTextBox.Name, codigoseguridad, Me.CODIGOSEGURIDADTextBox.Text, LocClv_Ciudad)
            ElseIf OpcionCli = "N" Then
                bitsist(GloUsuario, Contrato, LocGloSistema, Me.Name, "Se Dieron de Alta los Datos Bancarios del Prospecto", "", "Se Dieron De Alta Los Datos Bancarios Del Prospecto", LocClv_Ciudad)
            End If

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CONRELCLIBANCOBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONRELCLIBANCOBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexionProspectos)



        Try
            'Validacion nueva
            If Me.TxtConfCuentaBanc.Text.Length = 0 Then
                MsgBox("Se Requiere la Confirmaci�n De La Cuenta", MsgBoxStyle.Information)
                Exit Sub
            ElseIf Me.TxtConfCuentaBanc.Text.Length > 0 Then
                If Me.CUENTA_BANCOTextBox.Text.Equals(Me.TxtConfCuentaBanc.Text) = False Then
                    MsgBox("La Cuenta De Banco y Su Confirmaci�n No Son Iguales", MsgBoxStyle.Information)
                    Me.TxtConfCuentaBanc.Clear()
                    Exit Sub
                End If
            End If

            If Me.TxtConfirmacionCodigo.Text.Length = 0 Then
                MsgBox("Se Requiere la Confirmaci�n Del C�digo de Seguridad", MsgBoxStyle.Information)
                Exit Sub
            ElseIf Me.TxtConfirmacionCodigo.Text.Length > 0 Then
                If Me.CODIGOSEGURIDADTextBox.Text.Equals(Me.TxtConfirmacionCodigo.Text) = False Then
                    MsgBox("El C�digo De Seguridad No Es Igual Al De Su Confirmacion", MsgBoxStyle.Information)
                    Me.TxtConfirmacionCodigo.Clear()
                    Exit Sub
                End If
            End If
            'Fin VAlidacion nueva

            If Me.NOMBRETextBox.Text.Length = 0 Then
                MsgBox("Se Requiere el Nombre Por Favor", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Me.ComboBox1.SelectedValue.ToString.Length = 0 Then
                MsgBox("Se Requiere el Banco Por Favor", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Me.CUENTA_BANCOTextBox.Text.Length < 15 Then
                MsgBox("Se Requiere La Cuenta De Banco Por Favor. (16 D�gitos)", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Me.TIPODECUENTA.Text.Length = 0 Then
                MsgBox("Se Requiere El Tipo De Cuenta Por Favor", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Me.MaskedTextBox1.Text.Length <> 5 Then
                MsgBox("Se Requiere El Vencimiento Por Favor. (MM/AA Ejem. 01/07)", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Me.TIPODECUENTA.Text = "TARJETA DE CREDITO" And Me.CODIGOSEGURIDADTextBox.Text.Length = 0 Then
                MsgBox("Se Requiere el C�digo De Seguridad Por Favor", MsgBoxStyle.Information)
                Exit Sub
            End If



            CON.Open()
            Me.Validate()
            Me.CONRELCLIBANCOBindingSource.EndEdit()
            Me.CONRELCLIBANCOTableAdapter.Connection = CON
            Me.CONRELCLIBANCOTableAdapter.Update(Me.NewSofTvDataSet.CONRELCLIBANCO)
            MsgBox(mensaje5)
            guardabitacora(Contrato)
            'If IdSistema = "VA" Then
            '    VALIDAIMPRESION()
            'End If
            'If LOCBNDREPDATOSBANC = True Then
            '    LOCBNDREPDATOSBANC = False
            '    If IdSistema = "VA" Then
            '        ConfigureCrystalReportcargosautomaticos()
            '    End If
            'End If
            GloCmdBanco = 1

            Me.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            CON.Close()
        End Try


    End Sub

    Private Sub Busca()
        Dim CON As New SqlConnection(MiConexionProspectos)
        CON.Open()
        Try
            Me.CONRELCLIBANCOTableAdapter.Connection = CON
            Me.CONRELCLIBANCOTableAdapter.Fill(Me.NewSofTvDataSet.CONRELCLIBANCO, New System.Nullable(Of Long)(CType(Contrato, Long)))
            If Me.CUENTA_BANCOTextBox.Text.Length > 0 Then
                Me.TxtConfCuentaBanc.Text = Me.CUENTA_BANCOTextBox.Text
            End If
            If Me.CODIGOSEGURIDADTextBox.Text.Length > 0 Then
                Me.TxtConfirmacionCodigo.Text = Me.CODIGOSEGURIDADTextBox.Text
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmDatosBanco_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        'If IdSistema = "VA" Then
        '    VALIDAIMPRESION()
        '    If LOCBNDREPDATOSBANC = True Then
        '        LOCBNDREPDATOSBANC = False
        '        Me.Button1.Visible = True
        '    Else
        '        Me.Button1.Visible = False
        '    End If
        'ElseIf IdSistema <> "VA" Then
        '    Me.Button1.Visible = False
        'End If

        colorea(Me, Me.Name)
        If OpcionCli = "C" Then
            Me.CONRELCLIBANCOBindingNavigator.Enabled = False
            Me.Panel1.Enabled = False
        ElseIf OpcionCli = "M" Then
            Me.CONRELCLIBANCOBindingNavigator.Enabled = True
            Me.Panel1.Enabled = True
        End If

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRATIPOSDECUENTA' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRATIPOSDECUENTATableAdapter.Connection = CON
        Me.MUESTRATIPOSDECUENTATableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATIPOSDECUENTA)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRABANCOS' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRABANCOSTableAdapter.Connection = CON
        Me.MUESTRABANCOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRABANCOS)
        Busca()
        If IsNumeric(Me.ContratoTextBox.Text) = False Then
            Me.CONRELCLIBANCOBindingSource.AddNew()
            Me.ContratoTextBox.Text = Contrato
        End If
        damedatosbitacora()
        CON.Close()
        If OpcionCli = "N" Or OpcionCli = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click

        If IdSistema = "AG" And GloTipoUsuario <> 40 Then
            MsgBox("Tu tipo de perfil no permite realizar esta acci�n.", MsgBoxStyle.Information)
            Me.Close()
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexionProspectos)
        CON.Open()
        Me.CONRELCLIBANCOTableAdapter.Connection = CON
        Me.CONRELCLIBANCOTableAdapter.Delete(Contrato)
        MsgBox(mensaje6)
        CON.Close()
        bitsist(GloUsuario, Contrato, LocGloSistema, Me.Name, "Se Dieron de Baja los Datos Bancarios del Prospecto", "", "Se Dieron De Baja Los Datos Bancarios Del Prospecto", LocClv_Ciudad)
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub NOMBRETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NOMBRETextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NOMBRETextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NOMBRETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMBRETextBox.TextChanged

    End Sub

    Private Sub TIPODECUENTA_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TIPODECUENTA.SelectedIndexChanged

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'VALIDAIMPRESION()
        'If LOCBNDREPDATOSBANC = True Then
        '    LOCBNDREPDATOSBANC = False
        '    If IdSistema = "VA" Then
        '        ConfigureCrystalReportcargosautomaticos()
        '    End If

        'End If
        'Me.Close()
    End Sub
    'Private Sub VALIDAIMPRESION()
    '    Dim CON2 As New SqlConnection(MiConexionProspectos)
    '    Dim comando As SqlClient.SqlCommand
    '    Dim error1 As Integer = 0

    '    comando = New SqlClient.SqlCommand
    '    CON2.Open()
    '    With comando
    '        .Connection = CON2
    '        .CommandText = "VALDADATOSBANCARIOS"
    '        .CommandType = CommandType.StoredProcedure
    '        .CommandTimeout = 0
    '        ' Create a SqlParameter for each parameter in the stored procedure.
    '        Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
    '        Dim prm1 As New SqlParameter("@error", SqlDbType.Int)



    '        '@clv_cobro bigint,@contrato bigint,@monto decimal(18,2),@clv_servicio bigint,@error int output

    '        prm.Direction = ParameterDirection.Input
    '        prm1.Direction = ParameterDirection.Output


    '        prm.Value = Contrato
    '        prm1.Value = 0

    '        .Parameters.Add(prm)
    '        .Parameters.Add(prm1)

    '        Dim i As Integer = comando.ExecuteNonQuery()
    '        error1 = prm1.Value
    '    End With
    '    CON2.Close()

    '    If error1 = 0 Then
    '        LOCBNDREPDATOSBANC = True
    '    ElseIf error1 = 1 Then
    '        LOCBNDREPDATOSBANC = False
    '    End If

    'End Sub

    Private Sub CUENTA_BANCOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUENTA_BANCOTextBox.TextChanged
        If Len(Me.CUENTA_BANCOTextBox.Text) = 0 Then
            Me.LblConfCuentaBanc.Visible = False
            Me.TxtConfCuentaBanc.Visible = False
            Me.TxtConfCuentaBanc.Clear()
        ElseIf Len(Me.CUENTA_BANCOTextBox.Text) > 0 Then
            Me.TxtConfCuentaBanc.Clear()
            Me.LblConfCuentaBanc.Visible = True
            Me.TxtConfCuentaBanc.Visible = True
        End If
    End Sub

    Private Sub CODIGOSEGURIDADTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODIGOSEGURIDADTextBox.TextChanged
        If Len(Me.CODIGOSEGURIDADTextBox.Text) = 0 Then
            Me.LblConfiCodSeg.Visible = False
            Me.TxtConfirmacionCodigo.Visible = False
            Me.TxtConfirmacionCodigo.Clear()
        ElseIf Len(Me.CODIGOSEGURIDADTextBox.Text) > 0 Then
            Me.TxtConfirmacionCodigo.Clear()
            Me.LblConfiCodSeg.Visible = True
            Me.TxtConfirmacionCodigo.Visible = True
        End If
    End Sub
End Class