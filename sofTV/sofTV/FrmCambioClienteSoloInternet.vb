Imports System.Data.SqlClient
Public Class FrmCambioClienteSoloInternet

    Private Sub FrmCambioClienteSoloInternet_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If LocbndProceso = True Then
            LocbndProceso = False
            Me.TextBox1.Text = GLOCONTRATOSEL
        End If
        If LocbndProceso1 = True Then
            LocbndProceso1 = False
            Me.TextBox1.Text = 0
        End If
    End Sub

    Private Sub FrmCambioClienteSoloInternet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.TextBox1.Clear()
        If LocProceso = 1 Then
            GloClv_TipSer = 1000
            Me.Text = "Proceso De Cambio de Cliente Normal A Internet"
            Me.CMBLabel1.Text = "Proceso De Cambio de Cliente Normal A Internet"
        ElseIf LocProceso = 0 Then
            GloClv_TipSer = 1001
            Me.Text = "Proceso De Cambio de Solo Internet A Cliente Normal"

            Me.CMBLabel1.Text = "Proceso De Cambio de Solo Internet A Cliente Normal"
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FrmSelCliente.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        generaOrden()
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then

        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
    Private Sub generaOrden()
        Dim op As Integer = 0
        Dim error1 As Integer = 0
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If LocProceso = 1 Then
            op = 2
        ElseIf LocProceso = 0 Then
            op = 1
        End If
        Me.Crea_Orden_Proceso_Cambio_ClienteTableAdapter.Connection = CON
        Me.Crea_Orden_Proceso_Cambio_ClienteTableAdapter.Fill(Me.ProcedimientosArnoldo2.Crea_Orden_Proceso_Cambio_Cliente, Me.TextBox1.Text, op, error1)
        CON.Close()

        Select Case error1
            Case 0
                bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, Me.Text, "", Me.CMBLabel1.Text, GloSucursal, LocClv_Ciudad)
                MsgBox("Cliente Procesado Con Exito", MsgBoxStyle.Information)
            Case 1
                MsgBox("El Cliente No Tiene Todos Los Servicio Dados De Baja", MsgBoxStyle.Information)
            Case 2
                MsgBox("No se ha Generado La Orden de Servicio", MsgBoxStyle.Information)
        End Select

    End Sub

   

End Class