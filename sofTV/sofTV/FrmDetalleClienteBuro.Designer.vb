﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetalleClienteBuro
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CMBLabel1 As System.Windows.Forms.Label
        Dim CMBCliente As System.Windows.Forms.Label
        Dim CMBLabel3 As System.Windows.Forms.Label
        Dim CMBLabel4 As System.Windows.Forms.Label
        Dim CMBLabel5 As System.Windows.Forms.Label
        Dim CMBLabel6 As System.Windows.Forms.Label
        Dim CMBLabel2 As System.Windows.Forms.Label
        Dim CMBLabel7 As System.Windows.Forms.Label
        Dim CMBLabel8 As System.Windows.Forms.Label
        Dim CMBLabel9 As System.Windows.Forms.Label
        Me.txtAdeudoPorMensualidades = New System.Windows.Forms.TextBox()
        Me.txtAdeudoPorPagare = New System.Windows.Forms.TextBox()
        Me.txtFechaReporte = New System.Windows.Forms.TextBox()
        Me.txtFechaRetiro = New System.Windows.Forms.TextBox()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtClvBuro = New System.Windows.Forms.TextBox()
        Me.txtDe = New System.Windows.Forms.TextBox()
        Me.txtA = New System.Windows.Forms.TextBox()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnQuitarBuro = New System.Windows.Forms.Button()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        CMBLabel1 = New System.Windows.Forms.Label()
        CMBCliente = New System.Windows.Forms.Label()
        CMBLabel3 = New System.Windows.Forms.Label()
        CMBLabel4 = New System.Windows.Forms.Label()
        CMBLabel5 = New System.Windows.Forms.Label()
        CMBLabel6 = New System.Windows.Forms.Label()
        CMBLabel2 = New System.Windows.Forms.Label()
        CMBLabel7 = New System.Windows.Forms.Label()
        CMBLabel8 = New System.Windows.Forms.Label()
        CMBLabel9 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        CMBLabel1.AutoSize = True
        CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel1.Location = New System.Drawing.Point(12, 22)
        CMBLabel1.Name = "CMBLabel1"
        CMBLabel1.Size = New System.Drawing.Size(65, 15)
        CMBLabel1.TabIndex = 14
        CMBLabel1.Text = "Contrato:"
        '
        'CMBCliente
        '
        CMBCliente.AutoSize = True
        CMBCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBCliente.Location = New System.Drawing.Point(12, 48)
        CMBCliente.Name = "CMBCliente"
        CMBCliente.Size = New System.Drawing.Size(56, 15)
        CMBCliente.TabIndex = 15
        CMBCliente.Text = "Cliente:"
        '
        'CMBLabel3
        '
        CMBLabel3.AutoSize = True
        CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel3.Location = New System.Drawing.Point(376, 100)
        CMBLabel3.Name = "CMBLabel3"
        CMBLabel3.Size = New System.Drawing.Size(115, 15)
        CMBLabel3.TabIndex = 17
        CMBLabel3.Text = "Fecha De Retiro:"
        '
        'CMBLabel4
        '
        CMBLabel4.AutoSize = True
        CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel4.Location = New System.Drawing.Point(12, 100)
        CMBLabel4.Name = "CMBLabel4"
        CMBLabel4.Size = New System.Drawing.Size(127, 15)
        CMBLabel4.TabIndex = 18
        CMBLabel4.Text = "Fecha De Reporte:"
        '
        'CMBLabel5
        '
        CMBLabel5.AutoSize = True
        CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel5.Location = New System.Drawing.Point(12, 121)
        CMBLabel5.Name = "CMBLabel5"
        CMBLabel5.Size = New System.Drawing.Size(187, 15)
        CMBLabel5.TabIndex = 19
        CMBLabel5.Text = "Adeudo Por Mensualidades:"
        '
        'CMBLabel6
        '
        CMBLabel6.AutoSize = True
        CMBLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel6.Location = New System.Drawing.Point(375, 121)
        CMBLabel6.Name = "CMBLabel6"
        CMBLabel6.Size = New System.Drawing.Size(217, 15)
        CMBLabel6.TabIndex = 20
        CMBLabel6.Text = "Adeudo Por Pagare De Aparatos:"
        '
        'CMBLabel2
        '
        CMBLabel2.AutoSize = True
        CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel2.Location = New System.Drawing.Point(376, 70)
        CMBLabel2.Name = "CMBLabel2"
        CMBLabel2.Size = New System.Drawing.Size(216, 15)
        CMBLabel2.TabIndex = 27
        CMBLabel2.Text = "Clave Buro De Dias Con Adeudo:"
        '
        'CMBLabel7
        '
        CMBLabel7.AutoSize = True
        CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel7.Location = New System.Drawing.Point(12, 74)
        CMBLabel7.Name = "CMBLabel7"
        CMBLabel7.Size = New System.Drawing.Size(187, 15)
        CMBLabel7.TabIndex = 29
        CMBLabel7.Text = "Rango de Dias Con Adeudo:"
        '
        'CMBLabel8
        '
        CMBLabel8.AutoSize = True
        CMBLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel8.Location = New System.Drawing.Point(279, 71)
        CMBLabel8.Name = "CMBLabel8"
        CMBLabel8.Size = New System.Drawing.Size(15, 15)
        CMBLabel8.TabIndex = 32
        CMBLabel8.Text = "A"
        '
        'CMBLabel9
        '
        CMBLabel9.AutoSize = True
        CMBLabel9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel9.Location = New System.Drawing.Point(12, 147)
        CMBLabel9.Name = "CMBLabel9"
        CMBLabel9.Size = New System.Drawing.Size(95, 15)
        CMBLabel9.TabIndex = 35
        CMBLabel9.Text = "Adeudo Total:"
        '
        'txtAdeudoPorMensualidades
        '
        Me.txtAdeudoPorMensualidades.Enabled = False
        Me.txtAdeudoPorMensualidades.Location = New System.Drawing.Point(206, 121)
        Me.txtAdeudoPorMensualidades.Name = "txtAdeudoPorMensualidades"
        Me.txtAdeudoPorMensualidades.Size = New System.Drawing.Size(163, 20)
        Me.txtAdeudoPorMensualidades.TabIndex = 21
        '
        'txtAdeudoPorPagare
        '
        Me.txtAdeudoPorPagare.Enabled = False
        Me.txtAdeudoPorPagare.Location = New System.Drawing.Point(598, 121)
        Me.txtAdeudoPorPagare.Name = "txtAdeudoPorPagare"
        Me.txtAdeudoPorPagare.Size = New System.Drawing.Size(163, 20)
        Me.txtAdeudoPorPagare.TabIndex = 22
        '
        'txtFechaReporte
        '
        Me.txtFechaReporte.Enabled = False
        Me.txtFechaReporte.Location = New System.Drawing.Point(206, 95)
        Me.txtFechaReporte.Name = "txtFechaReporte"
        Me.txtFechaReporte.Size = New System.Drawing.Size(163, 20)
        Me.txtFechaReporte.TabIndex = 23
        '
        'txtFechaRetiro
        '
        Me.txtFechaRetiro.Enabled = False
        Me.txtFechaRetiro.Location = New System.Drawing.Point(598, 95)
        Me.txtFechaRetiro.Name = "txtFechaRetiro"
        Me.txtFechaRetiro.Size = New System.Drawing.Size(163, 20)
        Me.txtFechaRetiro.TabIndex = 24
        '
        'txtContrato
        '
        Me.txtContrato.Enabled = False
        Me.txtContrato.Location = New System.Drawing.Point(83, 17)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.Size = New System.Drawing.Size(163, 20)
        Me.txtContrato.TabIndex = 25
        '
        'txtNombre
        '
        Me.txtNombre.Enabled = False
        Me.txtNombre.Location = New System.Drawing.Point(83, 43)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(678, 20)
        Me.txtNombre.TabIndex = 26
        '
        'txtClvBuro
        '
        Me.txtClvBuro.Enabled = False
        Me.txtClvBuro.Location = New System.Drawing.Point(598, 69)
        Me.txtClvBuro.Name = "txtClvBuro"
        Me.txtClvBuro.Size = New System.Drawing.Size(163, 20)
        Me.txtClvBuro.TabIndex = 28
        '
        'txtDe
        '
        Me.txtDe.Enabled = False
        Me.txtDe.Location = New System.Drawing.Point(206, 69)
        Me.txtDe.Name = "txtDe"
        Me.txtDe.Size = New System.Drawing.Size(66, 20)
        Me.txtDe.TabIndex = 30
        '
        'txtA
        '
        Me.txtA.Enabled = False
        Me.txtA.Location = New System.Drawing.Point(304, 69)
        Me.txtA.Name = "txtA"
        Me.txtA.Size = New System.Drawing.Size(66, 20)
        Me.txtA.TabIndex = 31
        '
        'btnCerrar
        '
        Me.btnCerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCerrar.Location = New System.Drawing.Point(598, 156)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(163, 36)
        Me.btnCerrar.TabIndex = 33
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnQuitarBuro
        '
        Me.btnQuitarBuro.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitarBuro.Location = New System.Drawing.Point(425, 156)
        Me.btnQuitarBuro.Name = "btnQuitarBuro"
        Me.btnQuitarBuro.Size = New System.Drawing.Size(167, 36)
        Me.btnQuitarBuro.TabIndex = 34
        Me.btnQuitarBuro.Text = "Quitar De Buro"
        Me.btnQuitarBuro.UseVisualStyleBackColor = True
        '
        'txtTotal
        '
        Me.txtTotal.Enabled = False
        Me.txtTotal.Location = New System.Drawing.Point(206, 147)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(163, 20)
        Me.txtTotal.TabIndex = 36
        '
        'FrmDetalleClienteBuro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 204)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(CMBLabel9)
        Me.Controls.Add(Me.btnQuitarBuro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(CMBLabel8)
        Me.Controls.Add(Me.txtA)
        Me.Controls.Add(Me.txtDe)
        Me.Controls.Add(CMBLabel7)
        Me.Controls.Add(Me.txtClvBuro)
        Me.Controls.Add(CMBLabel2)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txtContrato)
        Me.Controls.Add(Me.txtFechaRetiro)
        Me.Controls.Add(Me.txtFechaReporte)
        Me.Controls.Add(Me.txtAdeudoPorPagare)
        Me.Controls.Add(Me.txtAdeudoPorMensualidades)
        Me.Controls.Add(CMBLabel6)
        Me.Controls.Add(CMBLabel5)
        Me.Controls.Add(CMBLabel4)
        Me.Controls.Add(CMBLabel3)
        Me.Controls.Add(CMBCliente)
        Me.Controls.Add(CMBLabel1)
        Me.MaximumSize = New System.Drawing.Size(800, 242)
        Me.MinimumSize = New System.Drawing.Size(800, 242)
        Me.Name = "FrmDetalleClienteBuro"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Detalle Cliente Buro"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtAdeudoPorMensualidades As System.Windows.Forms.TextBox
    Friend WithEvents txtAdeudoPorPagare As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaReporte As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaRetiro As System.Windows.Forms.TextBox
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtClvBuro As System.Windows.Forms.TextBox
    Friend WithEvents txtDe As System.Windows.Forms.TextBox
    Friend WithEvents txtA As System.Windows.Forms.TextBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnQuitarBuro As System.Windows.Forms.Button
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
End Class
