'
'    MediaFile class for C#
'		Version: 1.0		Date: 2002/04/09
'
'
'    Copyright � 2002, The KPD-Team
'    All rights reserved.
'    http://www.mentalis.org/
'
'  Redistribution and use in source and binary forms, with or without
'  modification, are permitted provided that the following conditions
'  are met:
'
'    - Redistributions of source code must retain the above copyright
'       notice, this list of conditions and the following disclaimer. 
'
'    - Neither the name of the KPD-Team, nor the names of its contributors
'       may be used to endorse or promote products derived from this
'       software without specific prior written permission. 
'
'  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
'  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
'  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
'  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
'  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
'  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
'  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
'  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
'  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
'  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
'  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
'  OF THE POSSIBILITY OF SUCH DAMAGE.
'

Imports System
Imports System.IO
Imports System.Text
Imports System.Windows.Forms
Imports System.Runtime.InteropServices

'// <summary>Defines a set of classes that can be used to play media files in any .NET language.</summary>
Namespace Org.Mentalis.Multimedia
    '/// <summary>
    '/// Defines the different states a media file can be in.
    '/// </summary>
    Public Enum StatusInfo
        '/// <summary>The media file is playing.</summary>
        Playing
        '/// <summary>The media file is paused.</summary>
		Paused
        '/// <summary>The media file is stopped.</summary>
        Stopped
        '/// <summary>The media file status is unknown.</summary>
		Unknown
    End Enum
    '/// <summary>
    '/// An abstract base class that defines methods that should be available for any MCI file type.
    '/// </summary>
    Public MustInherit Class MediaFile
        Implements IDisposable
        '/// <summary>
        '/// The mciSendString function sends a command string to an MCI device. The device that the command is sent to is specified in the command string.
        '/// </summary>
        '/// <param name="lpstrCommand">Address of a null-terminated string that specifies an MCI command string.</param>
        '/// <param name="lpstrReturnString">Address of a buffer that receives return information. If no return information is needed, this parameter can be null (Nothing in VB.NET).</param>
        '/// <param name="uReturnLength">Size, in characters, of the return buffer specified by the lpszReturnString parameter.</param>
        '/// <param name="hwndCallback">Handle of a callback window if the notify flag was specified in the command string.</param>
        '/// <returns>Returns zero if successful or an error otherwise. The low-order word of the returned doubleword value contains the error return value. If the error is device-specific, the high-order word of the return value is the driver identifier; otherwise, the high-order word is zero.<br>To retrieve a text description of mciSendString return values, pass the return value to the mciGetErrorString function.</br></returns>
        Protected Declare Ansi Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As StringBuilder, ByVal uReturnLength As Integer, ByVal hwndCallback As IntPtr) As Integer
        '/// <summary>
        '/// The mciGetErrorString function retrieves a string that describes the specified MCI error code.
        '/// </summary>
        '/// <param name="dwError">Error code returned by the mciSendCommand or mciSendString function.</param>
        '/// <param name="lpstrBuffer">Address of a buffer that receives a null-terminated string describing the specified error.</param>
        '/// <param name="uLength">Length of the buffer, in characters, pointed to by the lpszErrorText parameter.</param>
        '/// <returns>Returns TRUE if successful or FALSE if the error code is not known.</returns>
        Protected Declare Ansi Function mciGetErrorString Lib "winmm.dll" Alias "mciGetErrorStringA" (ByVal dwError As Integer, ByVal lpstrBuffer As StringBuilder, ByVal uLength As Integer) As Integer
        '/// <summary>
        '/// The GetShortPathName function obtains the short path form of a specified input path.
        '/// </summary>
        '/// <param name="lpszLongPath">Pointer to a null-terminated path string. The function obtains the short form of this path.</param>
        '/// <param name="lpszShortPath">Pointer to a buffer to receive the null-terminated short form of the path specified by lpszLongPath.</param>
        '/// <param name="cchBuffer">Specifies the size, in characters, of the buffer pointed to by lpszShortPath.</param>
        '/// <returns>If the function succeeds, the return value is the length, in characters, of the string copied to lpszShortPath, not including the terminating null character.<br>If the function fails due to the lpszShortPath buffer being too small to contain the short path string, the return value is the size, in characters, of the short path string. You need to call the function with a short path buffer that is at least as large as the short path string.</br><br>If the function fails for any other reason, the return value is zero. To get extended error information, call GetLastError.</br></returns>
        Protected Declare Ansi Function GetShortPathName Lib "kernel32.dll" Alias "GetShortPathNameA" (ByVal lpszLongPath As String, ByVal lpszShortPath As StringBuilder, ByVal cchBuffer As Integer) As Integer
        '/// <summary>
        '/// Initializes a new instance of the MediaFile class.
        '/// </summary>
        '/// <param name="file">The media file to open.</param>
        '/// <exception cref="ArgumentNullException">The file parameter is null (Nothing in VB.NET).</exception>
        '/// <exception cref="FileNotFoundException">The specified file could not be found.</exception>
        '/// <exception cref="MediaException">An error occured while opening the specified file.</exception>
        Public Sub New(ByVal file As String)
            Me.new(file, Nothing)
        End Sub
        '/// <summary>
        '/// Initializes a new instance of the MediaFile class.
        '/// </summary>
        '/// <param name="file">The media file to open.</param>
        '/// <param name="owner">The owner of the media file.</param>
        '/// <exception cref="ArgumentNullException">The file parameter is null (Nothing in VB.NET).</exception>
        '/// <exception cref="FileNotFoundException">The specified file could not be found.</exception>
        '/// <exception cref="MediaException">An error occured while opening the specified file.</exception>
        '/// <remarks>The <c>owner</c> parameter can be set to null (Nothing in VB.NET).</remarks>
        Public Sub New(ByVal file As String, ByVal owner As IWin32Window)
            If file Is Nothing Then Throw New ArgumentNullException("Parameter 'file' cannot be null!")
            If Not System.IO.File.Exists(file) Then Throw New FileNotFoundException()
            Me.Owner = owner
            Open(file)
        End Sub
        '/// <summary>
        '/// Converts a long path into a short path.
        '/// </summary>
        '/// <param name="file">The long path to convert.</param>
        '/// <returns>The short path -or- an empty string (="") if an error occurs.</returns>
        Protected Function GetShortPath(ByVal file As String) As String
            Dim buffer As StringBuilder = New StringBuilder(file.Length + 1)
            Dim ret As Integer = GetShortPathName(file, buffer, buffer.Capacity)
            If ret = 0 Then  ' Error
                Return ""
            Else   ' Success
                Return buffer.ToString()
            End If
        End Function
        '/// <summary>
        '/// Gets the filename of the media file.
        '/// </summary>
        '/// <value>A string containing the full path of the media file.</value>
        Public ReadOnly Property Filename() As String
            Get
                Return m_File
            End Get
        End Property
        '/// <summary>
        '/// Specifies the MCI string that should be used when opening the media file.
        '/// </summary>
        '/// <returns>An MCI string that should be used when opening the media file.</returns>
        '/// <remarks>Since this string depends on the media type, this method must be overridden in subclasses.</remarks>
        Protected MustOverride Function GetOpenString() As String
        '/// <summary>
        '/// Opens the specified media file and creates an Alias for it.
        '/// </summary>
        '/// <param name="file">The file to open.</param>
        Protected Sub Open(ByVal file As String)
            m_File = GetShortPath(file)
            AliasName = Guid.NewGuid().ToString("N")
            Dim ret As Integer
            ret = mciSendString(GetOpenString(), Nothing, 0, IntPtr.Zero)
            If ret <> 0 Then Throw New MediaException(GetMciError(ret))
        End Sub
        '/// <summary>
        '/// Converts an MCI error code into a string.
        '/// </summary>
        '/// <param name="errorCode">The error code to convert.</param>
        '/// <returns>A string representation of the specified error code -or- an empty string (="") when an error occurs.</returns>
        Protected Function GetMciError(ByVal errorCode As Integer) As String
            Dim buffer As StringBuilder = New StringBuilder(255)
            If mciGetErrorString(errorCode, buffer, buffer.Capacity) = 0 Then Return ""
            Return buffer.ToString()
        End Function
        '/// <summary>
        '/// Disposes of the unmanaged resources (other than memory) used by the MediaFile object.
        '/// </summary>
        Public Sub Dispose() Implements IDisposable.Dispose
            If (Disposed) Then Return
            mciSendString("CLOSE " + AliasName, Nothing, 0, IntPtr.Zero)
            m_Disposed = True
        End Sub
        '/// <summary>
        '/// Specifies the MCI string that should be used when playing the media file.
        '/// </summary>
        '/// <param name="from">The start position from where to play (measured in milliseconds).</param>
        '/// <param name="length">The length (measured in milliseconds) of the media to play.</param>
        '/// <returns>An MCI string that should be used when playing the media file.</returns>
        Protected Function GetPlayString(ByVal from As Integer, ByVal length As Integer) As String
            Dim ret As String = "PLAY " + AliasName + " FROM " + from.ToString() + " TO " + (from + length).ToString()
            If Repeat Then ret += " REPEAT"
            Return ret
        End Function
        '/// <summary>
        '/// Starts playing a file.
        '/// </summary>
        '/// <exception cref="MediaException">An error occured when starting playing the file.</exception>
        Public Sub Play()
            Play(0)
        End Sub
        '/// <summary>
        '/// Starts playing a file.
        '/// </summary>
        '/// <param name="from">The start position from where to play (measured in milliseconds).</param>
        '/// <exception cref="MediaException">An error occured when starting playing the file.</exception>
        Public Sub Play(ByVal from As Integer)
            Play(from, Length - from)
        End Sub
        '/// <summary>
        '/// Starts playing a file.
        '/// </summary>
        '/// <param name="from">The start position from where to play (measured in milliseconds).</param>
        '/// <param name="length">The length (measured in milliseconds) of the media to play.</param>
        '/// <exception cref="MediaException">An error occured while starting playing the file.</exception>
        Public Sub Play(ByVal from As Integer, ByVal length As Integer)
            If from < 0 OrElse length > length OrElse length < 0 OrElse from + length > length Then Throw New ArgumentException("Invalid parameter(s) specified.")
            Dim ret As Integer = mciSendString(GetPlayString(from, length), Nothing, 0, IntPtr.Zero)
            If ret <> 0 Then Throw New MediaException(GetMciError(ret))
        End Sub
        '/// <summary>
        '/// Stops playing a file and rewinds.
        '/// </summary>
        '/// <exception cref="MediaException">An error occured while stopping the file.</exception>
        Public Sub StopPlay()
            Dim ret As Integer = mciSendString("STOP " + AliasName, Nothing, 0, IntPtr.Zero)
            If ret <> 0 Then Throw New MediaException(GetMciError(ret))
        End Sub
        '/// <summary>
        '/// Gets whether the object has been disposed or not.
        '/// </summary>
        '/// <value>True if the object has been disposed, false otherwise.</value>
        Public ReadOnly Property Disposed() As Boolean
            Get
                Return m_Disposed
            End Get
        End Property
        '/// <summary>
        '/// Gets or sets the paused state of the media file.
        '/// </summary>
        '/// <value>A boolean specifying whether the media file is paused or not.</value>
        '/// <exception cref="MediaException">An error occured while accessing the media.</exception>
        Public Property Paused() As Boolean
            Get
                Return Status = StatusInfo.Paused
            End Get
            Set(ByVal Value As Boolean)
                If Value <> Paused Then
                    Dim ret As Integer
                    If Value Then 'PAUSE
                        ret = mciSendString("PAUSE " + AliasName, Nothing, 0, IntPtr.Zero)
                        If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                    Else 'RESUME
                        ret = mciSendString("RESUME " + AliasName, Nothing, 0, IntPtr.Zero)
                        If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                    End If
                End If
            End Set
        End Property
        '/// <summary>
        '/// Gets the status of the media file.
        '/// </summary>
        '/// <value>One of the StatusInfo values.</value>
        '/// <exception cref="MediaException">An error occured while accessing the media.</exception>
        Public ReadOnly Property Status() As StatusInfo
            Get
                Dim buffer As StringBuilder = New StringBuilder(255)
                Dim ret As Integer = mciSendString("STATUS " + AliasName + " MODE", buffer, buffer.Capacity, IntPtr.Zero)
                If ret <> 0 Then
                    Return StatusInfo.Unknown
                Else
                    Select Case buffer.ToString().ToLower()
                        Case "playing"
                            Return StatusInfo.Playing
                        Case "paused"
                            Return StatusInfo.Paused
                        Case "stopped"
                            Return StatusInfo.Stopped
                        Case Else
                            Return StatusInfo.Unknown
                    End Select
                End If
            End Get
        End Property
        '/// <summary>
        '/// Gets or sets the time format to use.
        '/// </summary>
        '/// <value>A string specifying the time format that has to be used.</value>
        '/// <remarks>This value can be 'ms' for milliseconds or 'frames' for frames (however 'frames' is not supported on all media types).</remarks>
        '/// <exception cref="ArgumentNullException">The specified value is null (Nothing in VB.NET).</exception>
        '/// <exception cref="MediaException">An error occured while accessing the media.</exception>
        Protected Property TimeFormat() As String
            Get
                Return m_TimeFormat
            End Get
            Set(ByVal Value As String)
                If Value Is Nothing Then Throw New ArgumentNullException()
                Dim ret As Integer = mciSendString("SET " + AliasName + " TIME FORMAT " + Value, Nothing, 0, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                m_TimeFormat = Value
            End Set
        End Property
        '/// <summary>
        '/// Gets the length of the file.
        '/// </summary>
        '/// <value>An integer that holds the length of the file.</value>
        '/// <remarks>The length is measured in milliseconds.</remarks>
        '/// <exception cref="MediaException">An error occured while accessing the media.</exception>
        Public ReadOnly Property Length() As Integer
            Get
                Dim buffer As StringBuilder = New StringBuilder(260)
                Dim ret As Integer = mciSendString("STATUS " + AliasName + " LENGTH", buffer, buffer.Capacity, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                Return Integer.Parse(buffer.ToString())
            End Get
        End Property
        '/// <summary>
        '/// Gets or sets the position in the media file.
        '/// </summary>
        '/// <value>An integer that specifies the position in the media file.</value>
        '/// <exception cref="MediaException">An error occured while accessing the media.</exception>
        '/// <remarks>The position is measured in milliseconds.</remarks>
        Public Property Position() As Integer
            Get
                Dim buffer As StringBuilder = New StringBuilder(260)
                Dim ret As Integer = mciSendString("STATUS " + AliasName + " POSITION", buffer, buffer.Capacity, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                Return Integer.Parse(buffer.ToString())
            End Get
            Set(ByVal Value As Integer)
                Dim playing As Boolean = (Me.Status = StatusInfo.Playing)
                Dim ret As Integer = mciSendString("SEEK " + AliasName + " TO " + Value.ToString(), Nothing, 0, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                If playing Then
                    ret = mciSendString("SEEK " + AliasName + " TO " + Value.ToString(), Nothing, 0, IntPtr.Zero)
                    If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                End If
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the volume of the sound of the media file.
        '/// </summary>
        '/// <value>An integer that specifies the volume of the sound of the media file.</value>
        '/// <exception cref="MediaException">An error occured while accessing the media.</exception>
        Public Property Volume() As Integer
            Get
                Dim buffer As StringBuilder = New StringBuilder(260)
                Dim ret As Integer = mciSendString("STATUS " + AliasName + " VOLUME", buffer, buffer.Capacity, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                Return Integer.Parse(buffer.ToString())
            End Get
            Set(ByVal Value As Integer)
                Dim ret As Integer = mciSendString("SETAUDIO " + AliasName + " VOLUME TO " + Value.ToString(), Nothing, 0, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the playback speed of the media file.
        '/// </summary>
        '/// <value>An integer that specifies the playback speed of the media file.</value>
        Public Property PlaybackSpeed() As Integer
            Get
                Return m_PlaybackSpeed
            End Get
            Set(ByVal Value As Integer)
                Dim ret As Integer = mciSendString("SET " + AliasName + " SPEED " + Value.ToString(), Nothing, 0, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                m_PlaybackSpeed = Value
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the alias of the media file.
        '/// </summary>
        '/// <value>A string that holds the alias of the media file.</value>
        Protected Property AliasName() As String
            Get
                Return m_Alias
            End Get
            Set(ByVal Value As String)
                m_Alias = Value
            End Set
        End Property
        '/// <summary>
        '/// Gets the full path of the media file.
        '/// </summary>
        '/// <value>A string that holds the full path of the media file.</value>
        Public ReadOnly Property File() As String
            Get
                Return m_File
            End Get
        End Property
        '/// <summary>
        '/// Gets or sets the owner window of this media file.
        '/// </summary>
        '/// <value>An instance of an IWin32Window object that's the parent of the media file.</value>
        Protected Property Owner() As IWin32Window
            Get
                Return m_Owner
            End Get
            Set(ByVal Value As IWin32Window)
                m_Owner = Value
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets whether the file should be repeated when it has reached the end.
        '/// </summary>
        '/// <value>A boolean that specifies whether the file should be repeated when it has reached the end.</value>
        Public Property Repeat() As Boolean
            Get
                Return m_Repeat
            End Get
            Set(ByVal Value As Boolean)
                m_Repeat = Value
            End Set
        End Property
        '/// <summary>
        '/// Destructs the MediaFile object by calling the Dispose method.
        '/// </summary>
        Protected Overrides Sub Finalize()
            Dispose()
        End Sub
        'private variables
        '/// <summary>Holds the value of the Owner property.</summary>
        Private m_Owner As IWin32Window
        '/// <summary>Holds the value of the File property.</summary>
        Private m_File As String
        '/// <summary>Holds the value of the Alias property.</summary>
        Private m_Alias As String
        '/// <summary>Holds the value of the Disposed property.</summary>
        Private m_Disposed As Boolean
        '/// <summary>Holds the value of the TimeFormat property.</summary>
        Private m_TimeFormat As String
        '/// <summary>Holds the value of the PlaybackSpeed property.</summary>
        Private m_PlaybackSpeed As Integer = 100
        '/// <summary>Holds the value of the Repeat property.</summary>
        Private m_Repeat As Boolean = False
    End Class
End Namespace
