Imports System.Data.SqlClient
Public Class FrmLoginPremium
    Dim eBandera As Integer = 0

    Private Sub Accion()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DAMECLAVEUSUARIOTableAdapter.Connection = CON
        Me.DAMECLAVEUSUARIOTableAdapter.Fill(Me.DataSetEric.DAMECLAVEUSUARIO, Me.Clv_UsuarioTextBox.Text, eClv_Usuario)
        'Me.VerAcceso_ChecaTableAdapter.Fill(Me.EricDataSet.VerAcceso_Checa, Me.Login.Text, Me.Pass.Text, 1, eBandera)
        Me.VerAcceso_ChecaActivaTableAdapter.Connection = CON
        Me.VerAcceso_ChecaActivaTableAdapter.Fill(Me.DataSetEric.VerAcceso_ChecaActiva, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, eBandera)
        CON.Close()
        eBandera = 1
        If eBandera = 1 Then
            FrmPaqueteActivado.Show()
            Me.Close()
        Else
            MsgBox("Acceso Denegado.", , "Error")
            Me.Clv_UsuarioTextBox.Clear()
            Me.PasaporteTextBox.Clear()
            'Me.Login.Clear()
            'Me.Pass.Clear()
        End If
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Accion()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub PasaporteTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasaporteTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Accion()
        End If
    End Sub


    Private Sub FrmLoginPremiun_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eAccesoAdmin = True
        If eAccesoAdmin = True Then
            FrmPaqueteActivado.Show()
            Me.Close()
        Else
            colorea(Me, Me.Name)
        End If

    End Sub

End Class