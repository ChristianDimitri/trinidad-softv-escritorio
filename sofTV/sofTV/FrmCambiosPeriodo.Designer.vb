﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambiosPeriodo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label19 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxSoloInternet = New System.Windows.Forms.CheckBox()
        Me.LabelCelular = New System.Windows.Forms.Label()
        Me.LabelTelefono = New System.Windows.Forms.Label()
        Me.LabelCiudad = New System.Windows.Forms.Label()
        Me.LabelColonia = New System.Windows.Forms.Label()
        Me.LabelNumero = New System.Windows.Forms.Label()
        Me.LabelCalle = New System.Windows.Forms.Label()
        Me.LabelNombre = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBoxContrato = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbperiodos = New System.Windows.Forms.ComboBox()
        Me.TextPeriodoactual = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Label19 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label19
        '
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Label19.Location = New System.Drawing.Point(22, 18)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(272, 17)
        Label19.TabIndex = 107
        Label19.Text = "Periodo Actual"
        Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(434, 19)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(347, 16)
        Label20.TabIndex = 106
        Label20.Text = "Cambio al Periodo"
        Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Label20.Visible = False
        '
        'Label1
        '
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(315, 35)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(135, 23)
        Label1.TabIndex = 108
        Label1.Text = "----------------->"
        Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.CheckBoxSoloInternet)
        Me.GroupBox1.Controls.Add(Me.LabelCelular)
        Me.GroupBox1.Controls.Add(Me.LabelTelefono)
        Me.GroupBox1.Controls.Add(Me.LabelCiudad)
        Me.GroupBox1.Controls.Add(Me.LabelColonia)
        Me.GroupBox1.Controls.Add(Me.LabelNumero)
        Me.GroupBox1.Controls.Add(Me.LabelCalle)
        Me.GroupBox1.Controls.Add(Me.LabelNombre)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 55)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(801, 133)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'CheckBoxSoloInternet
        '
        Me.CheckBoxSoloInternet.AutoSize = True
        Me.CheckBoxSoloInternet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSoloInternet.Enabled = False
        Me.CheckBoxSoloInternet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxSoloInternet.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBoxSoloInternet.Location = New System.Drawing.Point(469, 20)
        Me.CheckBoxSoloInternet.Name = "CheckBoxSoloInternet"
        Me.CheckBoxSoloInternet.Size = New System.Drawing.Size(108, 19)
        Me.CheckBoxSoloInternet.TabIndex = 16
        Me.CheckBoxSoloInternet.Text = "Sólo Internet"
        Me.CheckBoxSoloInternet.UseVisualStyleBackColor = True
        Me.CheckBoxSoloInternet.Visible = False
        '
        'LabelCelular
        '
        Me.LabelCelular.Location = New System.Drawing.Point(112, 97)
        Me.LabelCelular.Name = "LabelCelular"
        Me.LabelCelular.Size = New System.Drawing.Size(230, 19)
        Me.LabelCelular.TabIndex = 15
        Me.LabelCelular.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTelefono
        '
        Me.LabelTelefono.Location = New System.Drawing.Point(456, 97)
        Me.LabelTelefono.Name = "LabelTelefono"
        Me.LabelTelefono.Size = New System.Drawing.Size(207, 19)
        Me.LabelTelefono.TabIndex = 14
        Me.LabelTelefono.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCiudad
        '
        Me.LabelCiudad.Location = New System.Drawing.Point(511, 67)
        Me.LabelCiudad.Name = "LabelCiudad"
        Me.LabelCiudad.Size = New System.Drawing.Size(177, 19)
        Me.LabelCiudad.TabIndex = 13
        Me.LabelCiudad.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelColonia
        '
        Me.LabelColonia.Location = New System.Drawing.Point(112, 67)
        Me.LabelColonia.Name = "LabelColonia"
        Me.LabelColonia.Size = New System.Drawing.Size(300, 19)
        Me.LabelColonia.TabIndex = 12
        Me.LabelColonia.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNumero
        '
        Me.LabelNumero.Location = New System.Drawing.Point(465, 42)
        Me.LabelNumero.Name = "LabelNumero"
        Me.LabelNumero.Size = New System.Drawing.Size(112, 19)
        Me.LabelNumero.TabIndex = 11
        Me.LabelNumero.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCalle
        '
        Me.LabelCalle.Location = New System.Drawing.Point(112, 42)
        Me.LabelCalle.Name = "LabelCalle"
        Me.LabelCalle.Size = New System.Drawing.Size(300, 19)
        Me.LabelCalle.TabIndex = 10
        Me.LabelCalle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNombre
        '
        Me.LabelNombre.Location = New System.Drawing.Point(112, 23)
        Me.LabelNombre.Name = "LabelNombre"
        Me.LabelNombre.Size = New System.Drawing.Size(347, 19)
        Me.LabelNombre.TabIndex = 9
        Me.LabelNombre.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(24, 97)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 19)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Celular: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(350, 97)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(100, 19)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Teléfono: "
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label12.Location = New System.Drawing.Point(429, 67)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 19)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Ciudad: "
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label13.Location = New System.Drawing.Point(21, 67)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(85, 19)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Colonia: "
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(429, 42)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(30, 19)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "#: "
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label15.Location = New System.Drawing.Point(6, 39)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 19)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Calle: "
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label16.Location = New System.Drawing.Point(6, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 19)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Nombre: "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(18, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 23)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "Contrato:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextBoxContrato
        '
        Me.TextBoxContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxContrato.Location = New System.Drawing.Point(104, 28)
        Me.TextBoxContrato.Name = "TextBoxContrato"
        Me.TextBoxContrato.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxContrato.TabIndex = 22
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.cbperiodos)
        Me.GroupBox2.Controls.Add(Label1)
        Me.GroupBox2.Controls.Add(Me.TextPeriodoactual)
        Me.GroupBox2.Controls.Add(Label19)
        Me.GroupBox2.Controls.Add(Label20)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 200)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(801, 84)
        Me.GroupBox2.TabIndex = 25
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cambio de Periodo"
        '
        'cbperiodos
        '
        Me.cbperiodos.DisplayMember = "DESCRIPCION"
        Me.cbperiodos.FormattingEnabled = True
        Me.cbperiodos.Location = New System.Drawing.Point(480, 37)
        Me.cbperiodos.Name = "cbperiodos"
        Me.cbperiodos.Size = New System.Drawing.Size(301, 23)
        Me.cbperiodos.TabIndex = 109
        Me.cbperiodos.ValueMember = "CLV_Periodo"
        '
        'TextPeriodoactual
        '
        Me.TextPeriodoactual.Location = New System.Drawing.Point(24, 37)
        Me.TextPeriodoactual.Name = "TextPeriodoactual"
        Me.TextPeriodoactual.ReadOnly = True
        Me.TextPeriodoactual.Size = New System.Drawing.Size(267, 21)
        Me.TextPeriodoactual.TabIndex = 26
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.LightSlateGray
        Me.Button2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Button2.ForeColor = System.Drawing.SystemColors.Window
        Me.Button2.Location = New System.Drawing.Point(720, 306)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(93, 35)
        Me.Button2.TabIndex = 27
        Me.Button2.Text = "CANCELAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.LightSlateGray
        Me.Button1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.SystemColors.Window
        Me.Button1.Location = New System.Drawing.Point(621, 306)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 35)
        Me.Button1.TabIndex = 26
        Me.Button1.Text = "ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(210, 28)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(28, 19)
        Me.Button3.TabIndex = 28
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'FrmCambiosPeriodo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(825, 353)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TextBoxContrato)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCambiosPeriodo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambios de Periodo"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxSoloInternet As System.Windows.Forms.CheckBox
    Friend WithEvents LabelCelular As System.Windows.Forms.Label
    Friend WithEvents LabelTelefono As System.Windows.Forms.Label
    Friend WithEvents LabelCiudad As System.Windows.Forms.Label
    Friend WithEvents LabelColonia As System.Windows.Forms.Label
    Friend WithEvents LabelNumero As System.Windows.Forms.Label
    Friend WithEvents LabelCalle As System.Windows.Forms.Label
    Friend WithEvents LabelNombre As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBoxContrato As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TextPeriodoactual As System.Windows.Forms.TextBox
    Friend WithEvents cbperiodos As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
