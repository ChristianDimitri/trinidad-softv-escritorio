Imports System.Data.SqlClient
Public Class FrmGenerales_Internet
    Private IpServerC4 As String = Nothing
    Private Comunidad As String = Nothing
    Private NumeroDeDias As String = Nothing
    Private PaquetesAsignados As String = Nothing

    Private Sub BuscaDatosBitacora()
        Try
            IpServerC4 = Me.TextBox2.Text
            Comunidad = Me.TextBox1.Text
            NumeroDeDias = Me.Num_dia_habilitarTextBox.Text
            PaquetesAsignados = Me.Paq_defaultTextBox.Text
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub GuadaDatosBitacora(ByVal Op As String)
        Try
            If Op = 0 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Ip Server C4", IpServerC4, Me.TextBox2.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Comunidad", Comunidad, Me.TextBox1.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Dias de Paquete de Prueba", NumeroDeDias, Me.Num_dia_habilitarTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Paquete(s) Asignado(s) Por Default", PaquetesAsignados, Me.Paq_defaultTextBox.Text, LocClv_Ciudad)
            ElseIf Op = 1 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimin� Generales de Internet", "IpServerC4: " + IpServerC4 + " / Comunidad: " + Comunidad + " / D�as de Paquete de Prueba: " + NumeroDeDias + " / Paquete(s) Asignado(s) Por Default: " + PaquetesAsignados, "", LocClv_Ciudad)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONGenerales_InternetTableAdapter.Connection = CON
            Me.CONGenerales_InternetTableAdapter.Fill(Me.DataSetEDGAR.CONGenerales_Internet, 1)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
        BuscaDatosBitacora()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmGenerales_Internet_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Busca()
        If (Me.Clv_IdTextBox.Text = "") Then
            Me.CONGenerales_InternetBindingSource.AddNew()
        End If
        UspDesactivaBotones(Me, Me.Name)

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub
    Private Sub CONGenerales_InternetBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONGenerales_InternetBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONGenerales_InternetBindingSource.EndEdit()
        Me.CONGenerales_InternetTableAdapter.Connection = CON
        Me.CONGenerales_InternetTableAdapter.Update(Me.DataSetEDGAR.CONGenerales_Internet)

        MsgBox(mensaje5)
        CON.Close()
        GuadaDatosBitacora(0)
        Me.Close()
    End Sub
    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        GuadaDatosBitacora(1)
        CON.Open()

        Me.CONGenerales_InternetTableAdapter.Connection = CON
        Me.CONGenerales_InternetTableAdapter.Delete(Me.Clv_IdTextBox.Text)
        CON.Close()

        MsgBox(mensaje6)
        Me.Close()
    End Sub
End Class