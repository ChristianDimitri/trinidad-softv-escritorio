﻿Imports System.Data.SqlClient

Public Class FormModeloCaja

    Private Sub LLENA_cajas()

        Dim dTable As New DataTable
        Dim i As Integer = 0
        BaseII.limpiaParametros()
        Me.ComboBox1.DataSource = BaseII.ConsultaDT("SP_MUESTRATipoCaja")
        Me.ComboBox1.ValueMember = "NOARTICULO"
        Me.ComboBox1.DisplayMember = "CAJA"

        'DataGridView1.Columns.Item(7).Visible = False
    End Sub

    Private Sub FormModeloCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LLENA_cajas()
        If GloCajaPropia = 1 Then
            Label59.Visible = True
            CheckBox3.Visible = True
            CheckBox4.Visible = True
            Label57.Visible = True
            Label58.Visible = True
            TextBox31.Visible = True
            TextBox35.Visible = True

            Label1.Visible = False
            ComboBox1.Visible = False
        ElseIf GloCajaPropia = 0 Then
            Label59.Visible = False
            CheckBox3.Visible = False
            CheckBox4.Visible = False
            Label57.Visible = False
            Label58.Visible = False
            TextBox31.Visible = False
            TextBox35.Visible = False

            Label1.Visible = True
            ComboBox1.Visible = True
        End If
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        LocNoArticuloCajas = 0
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If GloCajaPropia = 0 Then
            If ComboBox1.SelectedIndex >= 1 Then
                LocNoArticuloCajas = ComboBox1.SelectedValue
                GloClvCajaDig = 0
                GloClvTarjDig = 0
                Me.Close()
            Else
                LocNoArticuloCajas = 0
                MsgBox("Seleccione un modelo de STB por favor ", MsgBoxStyle.Information, "Importante")
            End If
        ElseIf GloCajaPropia = 1 Then
            If CheckBox3.Checked = True And CheckBox4.Checked = False Then
                GloCajaOTarjeta = 1
            ElseIf CheckBox3.Checked = False And CheckBox4.Checked = True Then
                GloCajaOTarjeta = 2
            ElseIf CheckBox3.Checked = True And CheckBox4.Checked = True Then
                GloCajaOTarjeta = 3
            End If

            If GloCajaOTarjeta = 1 Then
                'If IsNumeric(TextBox31.Text) Then
                ValidaSerieCajaBaja()
                If GloCajaenBaja = 1 Then
                    GloClvCajaDig = TextBox31.Text
                    Me.Close()
                Else
                    MsgBox("La serie que capturo no existe o se encuentra instalada actualmente", MsgBoxStyle.Information)
                    Exit Sub
                End If
                'Else
                ' MsgBox("El numero de serie de la caja es incorrecto", MsgBoxStyle.Information)
                ' End If
            ElseIf GloCajaOTarjeta = 2 Then
                'If IsNumeric(TextBox35.Text) Then
                If ComboBox1.SelectedValue = 0 Then
                    MsgBox("Debe Seleccionar la Marca del STB antes de continuar", MsgBoxStyle.Information)
                    Exit Sub
                End If
                ValidaSerieCajaBaja()
                If GloCajaenBaja = 1 Then
                    LocNoArticuloCajas = ComboBox1.SelectedValue
                    GloClvTarjDig = TextBox35.Text
                    Me.Close()
                Else
                    MsgBox("La serie que capturo no existe o se encuentra instalada actualmente", MsgBoxStyle.Information)
                    GloGuardarDig = False
                    Exit Sub
                End If
                'Else
                ' MsgBox("El numero de serie de la Tarjeta es incorrecto", MsgBoxStyle.Information)
                'End If
            ElseIf GloCajaOTarjeta = 3 Then
                'If IsNumeric(TextBox31.Text) And IsNumeric(TextBox35.Text) Then
                ValidaSerieCajaBaja()
                If GloCajaenBaja = 1 Then
                    GloClvCajaDig = TextBox31.Text
                    GloClvTarjDig = TextBox35.Text
                    Me.Close()
                Else
                    MsgBox("La serie que capturo no existe o se encuentra instalada actualmente", MsgBoxStyle.Information)
                    GloGuardarDig = False
                    Exit Sub
                End If
                'Else
                'MsgBox("El numero de serie de la caja o la tarjeta es incorrecto", MsgBoxStyle.Information)
                'End If
            End If
        End If
        
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            TextBox31.Enabled = True
            Label1.Visible = False
            ComboBox1.Visible = False
        ElseIf CheckBox3.Checked = False Then
            TextBox31.Enabled = False
        End If

        If CheckBox4.Checked = True And CheckBox3.Checked = False Then
            Label1.Visible = True
            ComboBox1.Visible = True
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.Checked = True Then
            TextBox35.Enabled = True
        ElseIf CheckBox4.Checked = False Then
            TextBox35.Enabled = False
            Label1.Visible = False
            ComboBox1.Visible = False
        End If

        If CheckBox4.Checked = True And CheckBox3.Checked = False Then
            Label1.Visible = True
            ComboBox1.Visible = True
        End If
    End Sub

    Private Sub ValidaSerieCajaBaja()
        If TextBox31.Text = "" Then
            TextBox31.Text = 0
        End If
        If TextBox35.Text = "" Then
            TextBox35.Text = 0
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CajaOTarj", SqlDbType.Int, GloCajaOTarjeta)
        BaseII.CreateMyParameter("@SerieCaja", SqlDbType.VarChar, TextBox31.Text, 50)
        BaseII.CreateMyParameter("@SerieTarj", SqlDbType.VarChar, TextBox35.Text, 50)
        BaseII.CreateMyParameter("@Estaenbaja", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ValidaSerieCajaBaja")
        GloCajaenBaja = 0
        GloCajaenBaja = BaseII.dicoPar("@Estaenbaja").ToString
    End Sub
End Class