﻿Imports System.Collections.Generic
Imports System.Data.SqlClient

Public Class BrwCatalogoGastos

    Private Sub BrwCatalogoGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name) ''SE INVOCA EL COLOREA PARA DARLE FORMATO A LOS CONTROLES

        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A LLENAR EL COMBO (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbBuscaActivo.DisplayMember = "DESCRIPCION"
        Me.cmbBuscaActivo.ValueMember = "ACTIVO"
        Me.cmbBuscaActivo.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboActivo")
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A LLENAR EL COMBO (FIN)

        '''''LLENAMOS EL GRID (INICIO)
        Busqueda(0, "", 0, 0)  '''OP=0
        '''''LLENAMOS EL GRID (FIN)        

        '''''LLENAMOS LAS ETIQUETAS QUE SE ENCUENTRAN DENTRO DEL PANEL DE INFORMACIÓN GENERAL (INICIO)
        Try
            If Me.dgvTipoGastos.RowCount > 0 Then
                Me.CMBlblIdTipoPago.Text = Me.dgvTipoGastos.SelectedCells(0).Value
                Me.CMBlblDescripcion.Text = Me.dgvTipoGastos.SelectedCells(1).Value
                Me.cbMuestraActivo.Checked = CBool(Me.dgvTipoGastos.SelectedCells(2).Value)
            End If
        Catch ex As Exception

        End Try
        '''''LLENAMOS LAS ETIQUETAS QUE SE ENCUENTRAN DENTRO DEL PANEL DE INFORMACIÓN GENERAL (FIN)
    End Sub

    Private Sub btnBuscaDescripcion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscaDescripcion.Click
        '''''REALIZAMOS LA BÚSQUEDA POR DESCRIPCIÓN OP=2
        If Me.cmbBuscaActivo.Text = "ACTIVO" Or Me.cmbBuscaActivo.Text = "NO ACTIVO" Then
            If Me.txtBuscaDescripcion.Text.Length = 0 Then
                Busqueda(0, "", CBool(Me.cmbBuscaActivo.SelectedValue), 0)
            Else
                Busqueda(0, Me.txtBuscaDescripcion.Text, CBool(Me.cmbBuscaActivo.SelectedValue), 2)
            End If
        Else
            If Me.txtBuscaDescripcion.Text.Length = 0 Then
                Busqueda(0, "", 0, 0)
            Else
                Busqueda(0, Me.txtBuscaDescripcion.Text, 0, 2)
            End If
        End If
        Me.txtBuscaDescripcion.Clear()
    End Sub

    Private Sub Busqueda(ByVal prmidTipoGasto As Long, ByVal prmDescripcion As String, ByVal prmActivo As Boolean, ByVal prmOp As Integer)
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A LLENAR EL DATA GRDI (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idTipoGasto", SqlDbType.BigInt, prmidTipoGasto)
        ControlEfectivoClass.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@ACTIVO", SqlDbType.Bit, prmActivo)
        ControlEfectivoClass.CreateMyParameter("@OP", SqlDbType.Int, prmOp)

        Me.dgvTipoGastos.DataSource = ControlEfectivoClass.ConsultaDT("uspConsultaTipoGastos")
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A LLENAR EL DATA GRDI (FIN)
    End Sub

    Private Sub cmbBuscaActivo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscaActivo.SelectedIndexChanged
        If Me.cmbBuscaActivo.Text = "ACTIVO" Or Me.cmbBuscaActivo.Text = "NO ACTIVO" Then ''SE REALIZA LA BÚSQUEDA FILTRANDO EL STATUS DEL GASTO
            Busqueda(0, String.Empty, Me.cmbBuscaActivo.SelectedValue, 3)
        Else
            Busqueda(0, String.Empty, 0, 0)
        End If
    End Sub

    Private Sub dgvTipoGastos_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvTipoGastos.CurrentCellChanged
        Try '' SE LLENAN LAS ETIQUETAS DE INFORMACIÓN DEL PANEL OBTENIENDO LOS VALORES DE LA FILA SELECCIONADA AL MOMENTO
            If Me.dgvTipoGastos.RowCount > 0 Then
                Me.CMBlblIdTipoPago.Text = Me.dgvTipoGastos.SelectedCells(0).Value
                Me.CMBlblDescripcion.Text = Me.dgvTipoGastos.SelectedCells(1).Value
                Me.cbMuestraActivo.Checked = CBool(Me.dgvTipoGastos.SelectedCells(2).Value)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        ''SE MANDA LLAMAR EL FORMULARIO DE LOS GASTOS
        Dim FRM As New FrmCatalogoGastos
        FRM.opcionGastos = "N" ''EN ESTE CASO ES PARA DAR DE ALTA UN NUEVO REGISTRO Y SE MANDA LA OPCIÓN "N"(NUEVO)
        FRM.ShowDialog()
        Busqueda(0, String.Empty, 0, 0) ''SE ACTUALIZA EL GRID
    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulta.Click
        ''SE MANDA LLAMAR EL FORMULARIO DE LOS GASTOS
        Dim FRM As New FrmCatalogoGastos
        FRM.opcionGastos = "C" ''EN ESTE CASO ES PARA CONSULTAR UN REGISTRO Y SE MANDA LA OPCIÓN "C"(CONSULTAR)
        FRM.idTipoGasto = Me.dgvTipoGastos.SelectedCells(0).Value ''SE MANDA EL VALOR (ID) PARA REALIZAR LA BÚSQUEDA
        FRM.ShowDialog()
        Busqueda(0, String.Empty, 0, 0) ''SE ACTUALIZA EL GRID
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        ''SE MANDA LLAMAR EL FORMULARIO DE LOS GASTOS
        Dim FRM As New FrmCatalogoGastos
        FRM.opcionGastos = "M" ''EN ESTE CASO ES PARA MODIFICAR UN REGISTRO Y SE MANDA LA OPCIÓN "M"(MODIFICAR)
        FRM.idTipoGasto = Me.dgvTipoGastos.SelectedCells(0).Value ''SE MANDA EL VALOR (ID) PARA REALIZAR LA BÚSQUEDA
        FRM.ShowDialog()
        Busqueda(0, String.Empty, 0, 0) ''SE ACTUALIZA EL GRID
    End Sub
End Class