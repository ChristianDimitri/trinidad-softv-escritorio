Imports System.Data.SqlClient
Public Class FrmMotivosCancelacion
    Private motivo As String = Nothing
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                motivo = Me.MOTCANTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If opcion = "M" Then
                        'motivo = Me.MOTCANTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.MOTCANTextBox.Name, motivo, Me.MOTCANTextBox.Text, LocClv_Ciudad)
                    ElseIf opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Agrego Un Nuevo Motivo De Cancelaci�n", "", "Agrego Un Nuevo Motivo De Cancelaci�n: " + Me.MOTCANTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimin� Un Motivo De Cancelaci�n ", "", "Se Elimin� Un Motivo De Cancelaci�n: " + Me.MOTCANTextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub CONMotivoCancelacionBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONMotivoCancelacionBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Len(Trim(Me.MOTCANTextBox.Text)) > 0 Then
            Me.Validate()
            Me.CONMotivoCancelacionBindingSource.EndEdit()
            Me.CONMotivoCancelacionTableAdapter.Connection = CON
            Me.CONMotivoCancelacionTableAdapter.Update(Me.NewSofTvDataSet.CONMotivoCancelacion)
            MsgBox("Se Guardo con Ex�to", MsgBoxStyle.Information)
            guardabitacora(0)
            GloBnd = True
            Me.Close()
        Else
            MsgBox("Capture el concepto ", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub

    Private Sub Busca()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONMotivoCancelacionTableAdapter.Connection = CON
            Me.CONMotivoCancelacionTableAdapter.Fill(Me.NewSofTvDataSet.CONMotivoCancelacion, New System.Nullable(Of Integer)(CType(gloClave, Integer)))
            damedatosbitacora()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub FrmMotivosCancelacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca()
        If opcion = "N" Then
            Me.CONMotivoCancelacionBindingSource.AddNew()
        ElseIf opcion = "C" Then
            Me.Panel1.Enabled = False
        End If
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.Clv_MOTCANTextBox.Text) = False Then Me.Clv_MOTCANTextBox.Text = 0
        If CLng(Me.Clv_MOTCANTextBox.Text) > 4 Then
            Me.CONMotivoCancelacionTableAdapter.Connection = CON
            Me.CONMotivoCancelacionTableAdapter.Delete(Me.Clv_MOTCANTextBox.Text)
            guardabitacora(1)
            MsgBox(mensaje6)
            GloBnd = True
            Me.Close()
        Else
            MsgBox("Es un concepto fijo por lo cual no se puede eliminar ", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub



    Private Sub CONMotivoCancelacionBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONMotivoCancelacionBindingNavigator.RefreshItems

    End Sub

    Private Sub MOTCANTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MOTCANTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.MOTCANTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub MOTCANTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MOTCANTextBox.TextChanged

    End Sub
End Class