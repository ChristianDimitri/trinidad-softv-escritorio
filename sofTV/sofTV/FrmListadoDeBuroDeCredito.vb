﻿Imports System.Collections.Generic
Public Class FrmListadoDeBuroDeCredito

    Private Sub FrmListadoDeBuroDeCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenarGrid()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub llenarGrid()
        BaseII.limpiaParametros()
        Dim ds As DataSet = BaseII.ConsultaDS("USP_ListadoBuroGrid")
        dgvListadoBuro.DataSource = ds
        dgvListadoBuro.DataMember = "Table"
    End Sub

    Private Sub btnGenerarListado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarListado.Click
        If (txtRutaDestino.Text.Length > 0) Then
            Dim ab As New GeneraArchivoBuroDeCredito.ArchivoBuro()
            ab.Directorio = txtRutaDestino.Text
            ab.GenerarArchivoBuro()

        Else
            MessageBox.Show("Seleccione La Ruta Destino Del Listado Que Se Va a Generar")
        End If
    End Sub

    Private Sub btnSeleccionarRuta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionarRuta.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            txtRutaDestino.Text = FolderBrowserDialog1.SelectedPath

        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportarListado.Click
        Dim frm As New FrmFiltroListadoBuro

        If (frm.ShowDialog = DialogResult.OK) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@x", SqlDbType.Xml, frm.GeneraXml())
            Dim lt As New List(Of String)
            lt.Add("Datos")
            Dim ds As DataSet = BaseII.ConsultaDS("USP_ListadoBuroReporte", lt)
            Dim dico As New Dictionary(Of String, String)
            dico.Add("Cliente", GloEmpresa)
            'BaseII.llamarReporteCentralizado(RutaReportes + "\ReporteListadoBuro", ds, dico, True)
        End If

    End Sub
End Class