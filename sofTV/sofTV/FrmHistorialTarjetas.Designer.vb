﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmHistorialTarjetas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvHistorialTarjetas = New System.Windows.Forms.DataGridView()
        Me.COLID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColClvRECIBO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColContrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColFechaImpresion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Colimporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Colcancelada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTXT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDireccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColColonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColMunicipio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColEntreCalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTelefono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCantidadLetra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lbColonia = New System.Windows.Forms.Label()
        Me.lbDireccion = New System.Windows.Forms.Label()
        Me.lbNombre = New System.Windows.Forms.Label()
        Me.lbContrato = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.dgvHistorialTarjetas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvHistorialTarjetas
        '
        Me.dgvHistorialTarjetas.AllowUserToAddRows = False
        Me.dgvHistorialTarjetas.AllowUserToDeleteRows = False
        Me.dgvHistorialTarjetas.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHistorialTarjetas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvHistorialTarjetas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHistorialTarjetas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COLID, Me.ColClvRECIBO, Me.ColContrato, Me.ColFechaImpresion, Me.Colimporte, Me.Colcancelada, Me.ColTXT, Me.ColNombre, Me.ColDireccion, Me.ColColonia, Me.ColMunicipio, Me.ColEstado, Me.ColCP, Me.ColEntreCalle, Me.ColTelefono, Me.ColCantidadLetra, Me.ColTipo})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHistorialTarjetas.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvHistorialTarjetas.Location = New System.Drawing.Point(12, 167)
        Me.dgvHistorialTarjetas.Name = "dgvHistorialTarjetas"
        Me.dgvHistorialTarjetas.ReadOnly = True
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHistorialTarjetas.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvHistorialTarjetas.Size = New System.Drawing.Size(703, 135)
        Me.dgvHistorialTarjetas.TabIndex = 0
        '
        'COLID
        '
        Me.COLID.DataPropertyName = "ID"
        Me.COLID.HeaderText = "ID"
        Me.COLID.Name = "COLID"
        Me.COLID.ReadOnly = True
        Me.COLID.Visible = False
        '
        'ColClvRECIBO
        '
        Me.ColClvRECIBO.DataPropertyName = "ClvRECIBO"
        Me.ColClvRECIBO.HeaderText = "ClvRECIBO"
        Me.ColClvRECIBO.Name = "ColClvRECIBO"
        Me.ColClvRECIBO.ReadOnly = True
        Me.ColClvRECIBO.Visible = False
        '
        'ColContrato
        '
        Me.ColContrato.DataPropertyName = "Contrato"
        Me.ColContrato.HeaderText = "Contrato"
        Me.ColContrato.Name = "ColContrato"
        Me.ColContrato.ReadOnly = True
        Me.ColContrato.Visible = False
        '
        'ColFechaImpresion
        '
        Me.ColFechaImpresion.DataPropertyName = "FechaImpresion"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.ColFechaImpresion.DefaultCellStyle = DataGridViewCellStyle2
        Me.ColFechaImpresion.FillWeight = 150.0!
        Me.ColFechaImpresion.HeaderText = "Fecha de Impresion"
        Me.ColFechaImpresion.Name = "ColFechaImpresion"
        Me.ColFechaImpresion.ReadOnly = True
        Me.ColFechaImpresion.Width = 130
        '
        'Colimporte
        '
        Me.Colimporte.DataPropertyName = "importe"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Colimporte.DefaultCellStyle = DataGridViewCellStyle3
        Me.Colimporte.HeaderText = "Importe"
        Me.Colimporte.Name = "Colimporte"
        Me.Colimporte.ReadOnly = True
        Me.Colimporte.Width = 130
        '
        'Colcancelada
        '
        Me.Colcancelada.DataPropertyName = "cancelada"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Colcancelada.DefaultCellStyle = DataGridViewCellStyle4
        Me.Colcancelada.HeaderText = "Cancelada"
        Me.Colcancelada.Name = "Colcancelada"
        Me.Colcancelada.ReadOnly = True
        Me.Colcancelada.Width = 130
        '
        'ColTXT
        '
        Me.ColTXT.DataPropertyName = "TXT"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.ColTXT.DefaultCellStyle = DataGridViewCellStyle5
        Me.ColTXT.HeaderText = "Código"
        Me.ColTXT.Name = "ColTXT"
        Me.ColTXT.ReadOnly = True
        Me.ColTXT.Width = 130
        '
        'ColNombre
        '
        Me.ColNombre.DataPropertyName = "Nombre"
        Me.ColNombre.HeaderText = "Nombre"
        Me.ColNombre.Name = "ColNombre"
        Me.ColNombre.ReadOnly = True
        Me.ColNombre.Visible = False
        '
        'ColDireccion
        '
        Me.ColDireccion.DataPropertyName = "Direccion"
        Me.ColDireccion.HeaderText = "Direccion"
        Me.ColDireccion.Name = "ColDireccion"
        Me.ColDireccion.ReadOnly = True
        Me.ColDireccion.Visible = False
        '
        'ColColonia
        '
        Me.ColColonia.DataPropertyName = "Colonia"
        Me.ColColonia.HeaderText = "Colonia"
        Me.ColColonia.Name = "ColColonia"
        Me.ColColonia.ReadOnly = True
        Me.ColColonia.Visible = False
        '
        'ColMunicipio
        '
        Me.ColMunicipio.DataPropertyName = "Municipio"
        Me.ColMunicipio.HeaderText = "Municipio"
        Me.ColMunicipio.Name = "ColMunicipio"
        Me.ColMunicipio.ReadOnly = True
        Me.ColMunicipio.Visible = False
        '
        'ColEstado
        '
        Me.ColEstado.DataPropertyName = "Estado"
        Me.ColEstado.HeaderText = "Estado"
        Me.ColEstado.Name = "ColEstado"
        Me.ColEstado.ReadOnly = True
        Me.ColEstado.Visible = False
        '
        'ColCP
        '
        Me.ColCP.DataPropertyName = "CP"
        Me.ColCP.HeaderText = "CP"
        Me.ColCP.Name = "ColCP"
        Me.ColCP.ReadOnly = True
        Me.ColCP.Visible = False
        '
        'ColEntreCalle
        '
        Me.ColEntreCalle.DataPropertyName = "EntreCalle"
        Me.ColEntreCalle.HeaderText = "EntreCalle"
        Me.ColEntreCalle.Name = "ColEntreCalle"
        Me.ColEntreCalle.ReadOnly = True
        Me.ColEntreCalle.Visible = False
        '
        'ColTelefono
        '
        Me.ColTelefono.DataPropertyName = "Telefono"
        Me.ColTelefono.HeaderText = "Telefono"
        Me.ColTelefono.Name = "ColTelefono"
        Me.ColTelefono.ReadOnly = True
        Me.ColTelefono.Visible = False
        '
        'ColCantidadLetra
        '
        Me.ColCantidadLetra.DataPropertyName = "CantidadLetra"
        Me.ColCantidadLetra.HeaderText = "CantidadLetra"
        Me.ColCantidadLetra.Name = "ColCantidadLetra"
        Me.ColCantidadLetra.ReadOnly = True
        Me.ColCantidadLetra.Visible = False
        '
        'ColTipo
        '
        Me.ColTipo.DataPropertyName = "Tipo"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.ColTipo.DefaultCellStyle = DataGridViewCellStyle6
        Me.ColTipo.HeaderText = "Tipo de Tarjeta"
        Me.ColTipo.Name = "ColTipo"
        Me.ColTipo.ReadOnly = True
        Me.ColTipo.Width = 130
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Orange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(595, 315)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(120, 36)
        Me.Button4.TabIndex = 13
        Me.Button4.Text = "&ACEPTAR"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 15)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Contrato :"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lbColonia)
        Me.Panel1.Controls.Add(Me.lbDireccion)
        Me.Panel1.Controls.Add(Me.lbNombre)
        Me.Panel1.Controls.Add(Me.lbContrato)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(703, 141)
        Me.Panel1.TabIndex = 15
        '
        'lbColonia
        '
        Me.lbColonia.AutoSize = True
        Me.lbColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbColonia.Location = New System.Drawing.Point(91, 105)
        Me.lbColonia.Name = "lbColonia"
        Me.lbColonia.Size = New System.Drawing.Size(0, 15)
        Me.lbColonia.TabIndex = 21
        '
        'lbDireccion
        '
        Me.lbDireccion.AutoSize = True
        Me.lbDireccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbDireccion.Location = New System.Drawing.Point(91, 74)
        Me.lbDireccion.Name = "lbDireccion"
        Me.lbDireccion.Size = New System.Drawing.Size(0, 15)
        Me.lbDireccion.TabIndex = 20
        '
        'lbNombre
        '
        Me.lbNombre.AutoSize = True
        Me.lbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbNombre.Location = New System.Drawing.Point(91, 43)
        Me.lbNombre.Name = "lbNombre"
        Me.lbNombre.Size = New System.Drawing.Size(0, 15)
        Me.lbNombre.TabIndex = 19
        '
        'lbContrato
        '
        Me.lbContrato.AutoSize = True
        Me.lbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbContrato.Location = New System.Drawing.Point(91, 13)
        Me.lbContrato.Name = "lbContrato"
        Me.lbContrato.Size = New System.Drawing.Size(0, 15)
        Me.lbContrato.TabIndex = 18
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(21, 105)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 15)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Barrio :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 15)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Dirección :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(16, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 15)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Nombre :"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmHistorialTarjetas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(727, 361)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.dgvHistorialTarjetas)
        Me.Name = "FrmHistorialTarjetas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Historial de Tarjetas Impresas"
        CType(Me.dgvHistorialTarjetas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvHistorialTarjetas As System.Windows.Forms.DataGridView
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lbColonia As System.Windows.Forms.Label
    Friend WithEvents lbDireccion As System.Windows.Forms.Label
    Friend WithEvents lbNombre As System.Windows.Forms.Label
    Friend WithEvents lbContrato As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents COLID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColClvRECIBO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColContrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColFechaImpresion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Colimporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Colcancelada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTXT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDireccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColColonia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColMunicipio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColEntreCalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTelefono As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCantidadLetra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTipo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
