﻿Imports System.Text
Imports System.Data.SqlClient

Public Class FrmCambiosPeriodo

    Private Sub TextBoxContrato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxContrato.KeyDown
        If (e.KeyCode <> Keys.Enter) Then
            Exit Sub
        End If
        If IsNumeric(Me.TextBoxContrato.Text) = False Then
            Exit Sub
        End If
        If CInt(Me.TextBoxContrato.Text) <= 0 Then
            Exit Sub
        End If
        Dim bnd As Integer = 0
        bnd = Usp_guardacambioperiodo(TextBoxContrato.Text)
        If bnd = 1 Then

            MuestraInfoCliente(TextBoxContrato.Text)
            eContrato = TextBoxContrato.Text

            TextPeriodoactual.Text = MUESTRAPERIODOASIGNADO(eContrato)

        Else
            MsgBox("Debe tener el servicio Digital activo para poder hacer el cambio", MsgBoxStyle.Information, "Cambios de Periodo")
            Limpiar()
        End If
        MUESTRACATALOGOPERIODO(eContrato)


    End Sub

    Private Sub TextBoxContrato_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxContrato.TextChanged
        Limpiar()
    End Sub

    Private Sub Limpiar()
        TextPeriodoactual.Text = ""
        'cbperiodos.Dispose()
        Me.LabelNombre.Text = ""
        Me.LabelCalle.Text = ""
        Me.LabelNumero.Text = ""
        Me.LabelColonia.Text = ""
        Me.LabelCiudad.Text = ""
        Me.LabelTelefono.Text = ""
        Me.LabelCelular.Text = ""
        Me.CheckBoxSoloInternet.Checked = False
        'tvTV.Nodes.Clear()
        'tvNET.Nodes.Clear()
        'tvDIG.Nodes.Clear()
        'tvTel.Nodes.Clear()
        'lblTV.Text = ""
        'lblNET.Text = ""
        'lblDIG.Text = ""
        'lblTel.Text = ""
        'eContratoNet = 0
        eClv_Servicio = 0
        'tcServicios.Enabled = False
        'nudSinPago.Value = 0
        'nudConPago.Value = 0
        'nudExt.Value = 0
        'nudDiasPrepago2.Value = 15
        'cmbEsquemaPago2.SelectedIndex = -1
        'cbTipoContratacion2.SelectedIndex = -1
        'BorReconSession(eClv_Session)
        GroupBox1.Enabled = False

    End Sub

    Private Sub MuestraInfoCliente(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraInfoCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                Me.LabelNombre.Text = reader(0).ToString()
                Me.LabelCalle.Text = reader(1).ToString()
                Me.LabelNumero.Text = reader(2).ToString()
                Me.LabelColonia.Text = reader(3).ToString()
                Me.LabelCiudad.Text = reader(4).ToString()
                Me.LabelTelefono.Text = reader(5).ToString()
                Me.LabelCelular.Text = reader(6).ToString()
                Me.CheckBoxSoloInternet.Checked = reader(7).ToString
            End While

            'UspMuestraTipoEsquema(Contrato)
            'MUESTRATipoContratacionClientes(Contrato, Me.cmbEsquemaPago.SelectedValue)
            'UspMuestraTipoEsquema_Opcion(Contrato)
            'GroupBox2.Enabled = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Function MUESTRAPERIODOASIGNADO(ByVal OContrato As Long) As String
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, OContrato)
        BaseII.CreateMyParameter("@Periodo", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("MUESTRAPERIODOASIGNADO")
        Return BaseII.dicoPar("@Periodo").ToString
    End Function

    Private Sub MUESTRACATALOGOPERIODO(ByVal PContrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, PContrato)
        'BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        cbperiodos.DataSource = BaseII.ConsultaDT("MUESTRACATALOGOPERIODO")
    End Sub

    Private Function Usp_guardacambioperiodo(ByVal CONTRATO As Long, ByVal Usuario As String, ByVal periodoant As String, ByVal periodonew As Integer) As String
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, CONTRATO)
        BaseII.CreateMyParameter("@Usuario", SqlDbType.VarChar, Usuario, 50)
        BaseII.CreateMyParameter("@periodoant", SqlDbType.VarChar, periodoant, 50)
        BaseII.CreateMyParameter("@periodonew", SqlDbType.BigInt, periodonew)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 800)
        BaseII.ProcedimientoOutPut("Usp_guardacambioperiodo")
        Return BaseII.dicoPar("@MSJ").ToString
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim otXT As String = ""
        otXT = Usp_guardacambioperiodo(eContrato, GloUsuario, TextPeriodoactual.Text, cbperiodos.SelectedValue)
        MsgBox(otXT, MsgBoxStyle.Information, "Cambio Periodo")
        Me.Close()

    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        GloClv_TipSer = 3
        Dim FrmCli As New FrmSelClienteActivos
        FrmCli.ShowDialog()
        'FrmSelCliente.Show()
    End Sub

    Private Sub FrmCambiosPeriodo_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        If LocbndProceso = True Then
            LocbndProceso = False
            Me.TextBoxContrato.Text = GLOCONTRATOSEL
            MuestraInfoCliente(TextBoxContrato.Text)
            eContrato = TextBoxContrato.Text

            TextPeriodoactual.Text = MUESTRAPERIODOASIGNADO(eContrato)
            MUESTRACATALOGOPERIODO(eContrato)
        End If

    End Sub

    Private Function Usp_guardacambioperiodo(ByVal oCONTRATO As Long) As Integer
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oCONTRATO)
        BaseII.CreateMyParameter("@BND", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("SP_ValidaClientesActivos")
        Return BaseII.dicoPar("@BND").ToString
    End Function

End Class