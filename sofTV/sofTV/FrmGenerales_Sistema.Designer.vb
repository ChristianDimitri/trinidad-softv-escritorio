<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGenerales_Sistema
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim PortLabel As System.Windows.Forms.Label
        Dim HostLabel As System.Windows.Forms.Label
        Dim PasswordLabel As System.Windows.Forms.Label
        Dim CuentaLabel As System.Windows.Forms.Label
        Dim MesesLabel As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim ImportePorExtrasLabel As System.Windows.Forms.Label
        Dim CargaDeTrabajoLabel As System.Windows.Forms.Label
        Dim Carga_trab_insLabel As System.Windows.Forms.Label
        Dim NumeroExtLabel As System.Windows.Forms.Label
        Dim CostoExtLabel As System.Windows.Forms.Label
        Dim ImpresoraOrdenesLabel As System.Windows.Forms.Label
        Dim Puntos1Label As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim Label34 As System.Windows.Forms.Label
        Dim Label33 As System.Windows.Forms.Label
        Dim Label32 As System.Windows.Forms.Label
        Dim Si_se_generaLabel As System.Windows.Forms.Label
        Dim MensajeLabel As System.Windows.Forms.Label
        Dim ActivoLabel As System.Windows.Forms.Label
        Dim Label38 As System.Windows.Forms.Label
        Dim Label41 As System.Windows.Forms.Label
        Dim Label39 As System.Windows.Forms.Label
        Dim TELefonosLabel As System.Windows.Forms.Label
        Dim RfcLabel As System.Windows.Forms.Label
        Dim CiudadLabel As System.Windows.Forms.Label
        Dim ColoniaLabel As System.Windows.Forms.Label
        Dim DireccionLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Id_sucursalLabel As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label61 As System.Windows.Forms.Label
        Dim Label63 As System.Windows.Forms.Label
        Dim Label69 As System.Windows.Forms.Label
        Dim Label72 As System.Windows.Forms.Label
        Dim Label125 As System.Windows.Forms.Label
        Dim Clave01900Label As System.Windows.Forms.Label
        Dim Clave01800Label As System.Windows.Forms.Label
        Dim Clave045Label As System.Windows.Forms.Label
        Dim Clave044Label As System.Windows.Forms.Label
        Dim Pais_LocalLabel As System.Windows.Forms.Label
        Dim Numero_LocalLabel As System.Windows.Forms.Label
        Dim RutaLabel As System.Windows.Forms.Label
        Dim FormatoLabel As System.Windows.Forms.Label
        Dim Numero_InicioLabel As System.Windows.Forms.Label
        Dim Fecha_InicioLabel As System.Windows.Forms.Label
        Dim Nom_ArchivoLabel As System.Windows.Forms.Label
        Dim Label46 As System.Windows.Forms.Label
        Dim Periodo_inicialLabel As System.Windows.Forms.Label
        Dim Fecha_FacturasLabel As System.Windows.Forms.Label
        Dim Label52 As System.Windows.Forms.Label
        Dim Label53 As System.Windows.Forms.Label
        Dim LblEdoClvAdic As System.Windows.Forms.Label
        Dim LblCdClvAdic As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label89 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ConsultaCpGeneBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Consulta_ImpresorasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConMesesCobroAdeudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGeneralCorreoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_ImpresorasTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_ImpresorasTableAdapter()
        Me.Inserta_Mod_Cp_GenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Mod_Cp_GenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Mod_Cp_GenTableAdapter()
        Me.Consulta_Cp_GeneTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_Cp_GeneTableAdapter()
        Me.ConMesesCobroAdeudoTableAdapter = New sofTV.DataSetEricTableAdapters.ConMesesCobroAdeudoTableAdapter()
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter()
        Me.ConGeneralCorreoTableAdapter = New sofTV.DataSetEricTableAdapters.ConGeneralCorreoTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Consulta_Generales_Filtros_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Generales_Filtros_OrdenesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Generales_Filtros_OrdenesTableAdapter()
        Me.Inserta_Generales_Filtros_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Generales_Filtros_OrdenesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Generales_Filtros_OrdenesTableAdapter()
        Me.COnsultaGralfacturaglobalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.COnsulta_Gral_factura_globalTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter()
        Me.Inserta_Gral_factura_globalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Gral_factura_globalTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.CuentaTextBox = New System.Windows.Forms.TextBox()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.HostTextBox = New System.Windows.Forms.TextBox()
        Me.PortTextBox = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.DataGridViewCobro = New System.Windows.Forms.DataGridView()
        Me.Clv_TipoCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Meses = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Habilitado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ComboBoxCobro = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MesesTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.tbPuntosAnti = New System.Windows.Forms.TextBox()
        Me.CONSULTAGENERALESDESCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.rb30 = New System.Windows.Forms.RadioButton()
        Me.rb15 = New System.Windows.Forms.RadioButton()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.MUESTRAPERIODOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtDiaCorte = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.PanelBasico = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Puntos1TextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralAntiguedadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown6 = New System.Windows.Forms.NumericUpDown()
        Me.Puntos2TextBox = New System.Windows.Forms.TextBox()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Puntos5TextBox = New System.Windows.Forms.TextBox()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown8 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown7 = New System.Windows.Forms.NumericUpDown()
        Me.Puntos10TextBox = New System.Windows.Forms.TextBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.ImportePorExtrasTextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Carga_trab_insTextBox = New System.Windows.Forms.TextBox()
        Me.CargaDeTrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CostoExtTextBox = New System.Windows.Forms.TextBox()
        Me.NumeroExtTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraOrdenesTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_cobrodepositoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Si_se_generaCheckBox = New System.Windows.Forms.CheckBox()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.ConGeneralAlertaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.CONSULTA_General_HsbcBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetyahve = New sofTV.DataSetyahve()
        Me.CONSULTA_General_SantanderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONSULTA_General_XmlBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PanelDatos = New System.Windows.Forms.Panel()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.TextBoxNumeroExt = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.TextBoxNumeroInt = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Id_sucursalTextBox = New System.Windows.Forms.TextBox()
        Me.RfcTextBox = New System.Windows.Forms.TextBox()
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.TELefonosTextBox = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.MensajeTextBox = New System.Windows.Forms.TextBox()
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.ConGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_cobrodepositobuenoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Rel_PaquetesDigTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGeneralAlertaTableAdapter = New sofTV.DataSetLidia2TableAdapters.ConGeneralAlertaTableAdapter()
        Me.NueGeneralAlertaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueGeneralAlertaTableAdapter = New sofTV.DataSetLidia2TableAdapters.NueGeneralAlertaTableAdapter()
        Me.Dame_Direcciones_IpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Direcciones_IpTableAdapter = New sofTV.DataSetLidia2TableAdapters.Dame_Direcciones_IpTableAdapter()
        Me.Consulta_cobrodepositoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositoTableAdapter()
        Me.Consulta_cobrodepositobuenoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositobuenoTableAdapter()
        Me.Consulta_Rel_PaquetesDigTelTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_Rel_PaquetesDigTelTableAdapter()
        Me.CONSULTA_General_XmlTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_XmlTableAdapter()
        Me.CONSULTA_General_SantanderTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_SantanderTableAdapter()
        Me.CONSULTA_General_HsbcTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_HsbcTableAdapter()
        Me.Muestra_ciudadTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_ciudadTableAdapter()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage15 = New System.Windows.Forms.TabPage()
        Me.PnQuejas = New System.Windows.Forms.Panel()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.TreeViewOrdenes = New System.Windows.Forms.TreeView()
        Me.LblNotas = New System.Windows.Forms.Label()
        Me.LblFamilia = New System.Windows.Forms.Label()
        Me.LblTecnico = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.TreViewTecnicos = New System.Windows.Forms.TreeView()
        Me.CmbTecnicos = New System.Windows.Forms.ComboBox()
        Me.MuestraTecnicosByFamiliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CmBDepto = New System.Windows.Forms.ComboBox()
        Me.MuestratecnicosDepartamentosAlmacenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabPage16 = New System.Windows.Forms.TabPage()
        Me.LabelNota = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Porcentaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TabPage17 = New System.Windows.Forms.TabPage()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.DDescoSusp = New System.Windows.Forms.TextBox()
        Me.DContrata = New System.Windows.Forms.TextBox()
        Me.TabPage18 = New System.Windows.Forms.TabPage()
        Me.TextIEPS = New System.Windows.Forms.TextBox()
        Me.TextIva = New System.Windows.Forms.TextBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.LblPorIeps = New System.Windows.Forms.Label()
        Me.NumericUpDownIeps = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownIva = New System.Windows.Forms.NumericUpDown()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.ChkCalculo1 = New System.Windows.Forms.CheckBox()
        Me.LabelIEPS = New System.Windows.Forms.Label()
        Me.CtaIepsText = New System.Windows.Forms.TextBox()
        Me.LabelCtaIEPS = New System.Windows.Forms.Label()
        Me.CheckIEPS = New System.Windows.Forms.CheckBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.tpRecontratacion = New System.Windows.Forms.TabPage()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.btnAceptarRecon = New System.Windows.Forms.Button()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.cbAplica = New System.Windows.Forms.CheckBox()
        Me.txtMeses = New System.Windows.Forms.TextBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.tpEstadosCuenta = New System.Windows.Forms.TabPage()
        Me.gbMensaje = New System.Windows.Forms.GroupBox()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.cbDeco = New System.Windows.Forms.CheckBox()
        Me.cbCorreo = New System.Windows.Forms.CheckBox()
        Me.dtHora = New System.Windows.Forms.DateTimePicker()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.tbMensajeDeco = New System.Windows.Forms.TextBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.tbMensaje = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.tbAsunto = New System.Windows.Forms.TextBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtDiaEdoCuenta = New System.Windows.Forms.TextBox()
        Me.cbPeriodo = New System.Windows.Forms.ComboBox()
        Me.tbPuntos = New System.Windows.Forms.TabPage()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.tcPuntos = New System.Windows.Forms.TabControl()
        Me.tbPuntosRangos = New System.Windows.Forms.TabPage()
        Me.bnEliminarRango = New System.Windows.Forms.Button()
        Me.bnAgregarRango = New System.Windows.Forms.Button()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.tbRangoFin = New System.Windows.Forms.TextBox()
        Me.dgRangos = New System.Windows.Forms.DataGridView()
        Me.IdRango = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Rango = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.tbRangoIni = New System.Windows.Forms.TextBox()
        Me.tbPuntosServicios = New System.Windows.Forms.TabPage()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.cbTipSerPuntosAnti = New System.Windows.Forms.ComboBox()
        Me.cbServicioPuntosAnti = New System.Windows.Forms.ComboBox()
        Me.bnEliminarPuntosServicio = New System.Windows.Forms.Button()
        Me.bnAgregarPuntosServicio = New System.Windows.Forms.Button()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.dgPuntosServicios = New System.Windows.Forms.DataGridView()
        Me.IdPuntos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Puntos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tboxPuntosAnti = New System.Windows.Forms.TextBox()
        Me.cbRangosPuntosAnti = New System.Windows.Forms.ComboBox()
        Me.tbPuntosServiciosAdic = New System.Windows.Forms.TabPage()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Label118 = New System.Windows.Forms.Label()
        Me.tbPuntosAntiServicios = New System.Windows.Forms.TextBox()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.cbTipSerPuntosAntiServicios = New System.Windows.Forms.ComboBox()
        Me.cbServicioPuntosAntiServicios = New System.Windows.Forms.ComboBox()
        Me.bnEliminarPuntosAntiServicios = New System.Windows.Forms.Button()
        Me.bnAgregarPuntosAntiServicios = New System.Windows.Forms.Button()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.dgPagosAntiServAdic = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PagosPuntuales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbPuntosAntiServiciosPagos = New System.Windows.Forms.TextBox()
        Me.cbRangosPuntosAntiServicios = New System.Windows.Forms.ComboBox()
        Me.tpProrroga = New System.Windows.Forms.TabPage()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.bnGuardarProrroga = New System.Windows.Forms.Button()
        Me.dgvUsuarios = New System.Windows.Forms.DataGridView()
        Me.ClaveUsuarios = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreUsuarios = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActivoUsuarios = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgvTipoUsuario = New System.Windows.Forms.DataGridView()
        Me.Clv_TipoUsuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionTipoUsuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActivoTipoUsuario = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TabOxxo = New System.Windows.Forms.TabPage()
        Me.txtPrefijo = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.txtComisionOxxo = New System.Windows.Forms.TextBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.TpCobroMaterial = New System.Windows.Forms.TabPage()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.dgvRangosMaterial = New System.Windows.Forms.DataGridView()
        Me.identity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RangoIni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RangoFinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumPagos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblRangoFin = New System.Windows.Forms.Label()
        Me.lblRangoIni = New System.Windows.Forms.Label()
        Me.txtRango2 = New System.Windows.Forms.TextBox()
        Me.txtRango1 = New System.Windows.Forms.TextBox()
        Me.nudPagos = New System.Windows.Forms.NumericUpDown()
        Me.lblPagos = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TpBonificaciones = New System.Windows.Forms.TabPage()
        Me.tbBonificacionMax = New System.Windows.Forms.TextBox()
        Me.bnGuardarBonificacionMax = New System.Windows.Forms.Button()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.TbResumenEjec = New System.Windows.Forms.TabPage()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.email = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.habilitar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TbPagosContrataDig = New System.Windows.Forms.TabPage()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.CheckBox9 = New System.Windows.Forms.CheckBox()
        Me.NumericUpDown11 = New System.Windows.Forms.NumericUpDown()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.NumericUpDown10 = New System.Windows.Forms.NumericUpDown()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.ConsultaRelTecnicosQuejasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRelQuejasTecFamiliaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ConGeneralTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralTableAdapter()
        Me.ConGeneralAntiguedadTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralAntiguedadTableAdapter()
        Me.MUESTRAPERIODOSTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.MUESTRAPERIODOSTableAdapter()
        Me.CONSULTAGENERALESDESCTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.CONSULTAGENERALESDESCTableAdapter()
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Quejas_Tec_FamiliaTableAdapter()
        Me.ConsultaRel_Tecnicos_QuejasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRel_Tecnicos_QuejasTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Tecnicos_QuejasTableAdapter()
        Me.Muestra_tecnicosDepartamentos_AlmacenTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_tecnicosDepartamentos_AlmacenTableAdapter()
        Me.Muestra_TecnicosByFamiliTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_TecnicosByFamiliTableAdapter()
        Me.NueGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MODIFCAGENERALESDESCBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueGeneralBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEGeneralAntiguedadBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_Rel_PaquetesDigTelDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumericUpDown9 = New System.Windows.Forms.NumericUpDown()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Clave01900TextBox = New System.Windows.Forms.TextBox()
        Me.Clave01800TextBox = New System.Windows.Forms.TextBox()
        Me.Clave045TextBox = New System.Windows.Forms.TextBox()
        Me.Clave044TextBox = New System.Windows.Forms.TextBox()
        Me.Pais_LocalTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_LocalTextBox = New System.Windows.Forms.TextBox()
        Me.RutaTextBox = New System.Windows.Forms.TextBox()
        Me.FormatoTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_InicioTextBox = New System.Windows.Forms.TextBox()
        Me.Periodo_finalTextBox = New System.Windows.Forms.TextBox()
        Me.Periodo_inicialTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_FacturasDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Fecha_InicioDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Nom_ArchivoTextBox = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.TextBoxserie = New System.Windows.Forms.TextBox()
        Me.TextBoxfolio = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.TxtEdoClvAdic = New System.Windows.Forms.TextBox()
        Me.TxtCdClvAdic = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter9 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter10 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter11 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter12 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter13 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter14 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter15 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter16 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter17 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter18 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        PortLabel = New System.Windows.Forms.Label()
        HostLabel = New System.Windows.Forms.Label()
        PasswordLabel = New System.Windows.Forms.Label()
        CuentaLabel = New System.Windows.Forms.Label()
        MesesLabel = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        ImportePorExtrasLabel = New System.Windows.Forms.Label()
        CargaDeTrabajoLabel = New System.Windows.Forms.Label()
        Carga_trab_insLabel = New System.Windows.Forms.Label()
        NumeroExtLabel = New System.Windows.Forms.Label()
        CostoExtLabel = New System.Windows.Forms.Label()
        ImpresoraOrdenesLabel = New System.Windows.Forms.Label()
        Puntos1Label = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label29 = New System.Windows.Forms.Label()
        Label34 = New System.Windows.Forms.Label()
        Label33 = New System.Windows.Forms.Label()
        Label32 = New System.Windows.Forms.Label()
        Si_se_generaLabel = New System.Windows.Forms.Label()
        MensajeLabel = New System.Windows.Forms.Label()
        ActivoLabel = New System.Windows.Forms.Label()
        Label38 = New System.Windows.Forms.Label()
        Label41 = New System.Windows.Forms.Label()
        Label39 = New System.Windows.Forms.Label()
        TELefonosLabel = New System.Windows.Forms.Label()
        RfcLabel = New System.Windows.Forms.Label()
        CiudadLabel = New System.Windows.Forms.Label()
        ColoniaLabel = New System.Windows.Forms.Label()
        DireccionLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Id_sucursalLabel = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label61 = New System.Windows.Forms.Label()
        Label63 = New System.Windows.Forms.Label()
        Label69 = New System.Windows.Forms.Label()
        Label72 = New System.Windows.Forms.Label()
        Label125 = New System.Windows.Forms.Label()
        Clave01900Label = New System.Windows.Forms.Label()
        Clave01800Label = New System.Windows.Forms.Label()
        Clave045Label = New System.Windows.Forms.Label()
        Clave044Label = New System.Windows.Forms.Label()
        Pais_LocalLabel = New System.Windows.Forms.Label()
        Numero_LocalLabel = New System.Windows.Forms.Label()
        RutaLabel = New System.Windows.Forms.Label()
        FormatoLabel = New System.Windows.Forms.Label()
        Numero_InicioLabel = New System.Windows.Forms.Label()
        Fecha_InicioLabel = New System.Windows.Forms.Label()
        Nom_ArchivoLabel = New System.Windows.Forms.Label()
        Label46 = New System.Windows.Forms.Label()
        Periodo_inicialLabel = New System.Windows.Forms.Label()
        Fecha_FacturasLabel = New System.Windows.Forms.Label()
        Label52 = New System.Windows.Forms.Label()
        Label53 = New System.Windows.Forms.Label()
        LblEdoClvAdic = New System.Windows.Forms.Label()
        LblCdClvAdic = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label89 = New System.Windows.Forms.Label()
        CType(Me.ConsultaCpGeneBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_ImpresorasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMesesCobroAdeudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralCorreoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Mod_Cp_GenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.COnsultaGralfacturaglobalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Gral_factura_globalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.DataGridViewCobro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CONSULTAGENERALESDESCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAPERIODOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelBasico.SuspendLayout()
        Me.Panel13.SuspendLayout()
        CType(Me.ConGeneralAntiguedadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.Consulta_cobrodepositoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.ConGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTA_General_HsbcBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTA_General_SantanderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTA_General_XmlBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDatos.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.ConGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_cobrodepositobuenoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_PaquetesDigTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Direcciones_IpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage8.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage15.SuspendLayout()
        Me.PnQuejas.SuspendLayout()
        CType(Me.MuestraTecnicosByFamiliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestratecnicosDepartamentosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage16.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage17.SuspendLayout()
        Me.TabPage18.SuspendLayout()
        CType(Me.NumericUpDownIeps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDownIva, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpRecontratacion.SuspendLayout()
        Me.tpEstadosCuenta.SuspendLayout()
        Me.gbMensaje.SuspendLayout()
        Me.tbPuntos.SuspendLayout()
        Me.tcPuntos.SuspendLayout()
        Me.tbPuntosRangos.SuspendLayout()
        CType(Me.dgRangos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPuntosServicios.SuspendLayout()
        CType(Me.dgPuntosServicios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPuntosServiciosAdic.SuspendLayout()
        CType(Me.dgPagosAntiServAdic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpProrroga.SuspendLayout()
        CType(Me.dgvUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTipoUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabOxxo.SuspendLayout()
        Me.TpCobroMaterial.SuspendLayout()
        Me.Panel14.SuspendLayout()
        CType(Me.dgvRangosMaterial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPagos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TpBonificaciones.SuspendLayout()
        Me.TbResumenEjec.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TbPagosContrataDig.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.NumericUpDown11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUpDown10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRelTecnicosQuejasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRelQuejasTecFamiliaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRel_Tecnicos_QuejasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MODIFCAGENERALESDESCBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEGeneralAntiguedadBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_PaquetesDigTelDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PortLabel
        '
        PortLabel.AutoSize = True
        PortLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PortLabel.Location = New System.Drawing.Point(336, 210)
        PortLabel.Name = "PortLabel"
        PortLabel.Size = New System.Drawing.Size(37, 15)
        PortLabel.TabIndex = 35
        PortLabel.Text = "Port:"
        '
        'HostLabel
        '
        HostLabel.AutoSize = True
        HostLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HostLabel.Location = New System.Drawing.Point(333, 183)
        HostLabel.Name = "HostLabel"
        HostLabel.Size = New System.Drawing.Size(40, 15)
        HostLabel.TabIndex = 33
        HostLabel.Text = "Host:"
        '
        'PasswordLabel
        '
        PasswordLabel.AutoSize = True
        PasswordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PasswordLabel.Location = New System.Drawing.Point(300, 156)
        PasswordLabel.Name = "PasswordLabel"
        PasswordLabel.Size = New System.Drawing.Size(73, 15)
        PasswordLabel.TabIndex = 31
        PasswordLabel.Text = "Password:"
        '
        'CuentaLabel
        '
        CuentaLabel.AutoSize = True
        CuentaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CuentaLabel.Location = New System.Drawing.Point(317, 129)
        CuentaLabel.Name = "CuentaLabel"
        CuentaLabel.Size = New System.Drawing.Size(56, 15)
        CuentaLabel.TabIndex = 29
        CuentaLabel.Text = "Cuenta:"
        '
        'MesesLabel
        '
        MesesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MesesLabel.Location = New System.Drawing.Point(728, 554)
        MesesLabel.Name = "MesesLabel"
        MesesLabel.Size = New System.Drawing.Size(21, 10)
        MesesLabel.TabIndex = 4
        MesesLabel.Text = "Número de Meses :"
        MesesLabel.Visible = False
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.Location = New System.Drawing.Point(330, 151)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(98, 15)
        Label20.TabIndex = 20
        Label20.Text = "Desde el Dia :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.Location = New System.Drawing.Point(334, 178)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(94, 15)
        Label19.TabIndex = 21
        Label19.Text = "Hasta el Dia :"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Location = New System.Drawing.Point(593, 254)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(52, 15)
        Label22.TabIndex = 29
        Label22.Text = "Precio:"
        Label22.Visible = False
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.Location = New System.Drawing.Point(304, 301)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(124, 15)
        Label16.TabIndex = 20
        Label16.Text = "Por Pronto Pago : "
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Location = New System.Drawing.Point(582, 292)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(132, 15)
        Label21.TabIndex = 30
        Label21.Text = "Precio Suspendido:"
        Label21.Visible = False
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.Location = New System.Drawing.Point(334, 448)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(95, 15)
        Label15.TabIndex = 21
        Label15.Text = "Día de Corte :"
        Label15.Visible = False
        '
        'ImportePorExtrasLabel
        '
        ImportePorExtrasLabel.AutoSize = True
        ImportePorExtrasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImportePorExtrasLabel.Location = New System.Drawing.Point(286, 61)
        ImportePorExtrasLabel.Name = "ImportePorExtrasLabel"
        ImportePorExtrasLabel.Size = New System.Drawing.Size(306, 15)
        ImportePorExtrasLabel.TabIndex = 16
        ImportePorExtrasLabel.Text = "Importe de la mensualidad de la TV Adicional :"
        ImportePorExtrasLabel.Visible = False
        '
        'CargaDeTrabajoLabel
        '
        CargaDeTrabajoLabel.AutoSize = True
        CargaDeTrabajoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CargaDeTrabajoLabel.Location = New System.Drawing.Point(32, 24)
        CargaDeTrabajoLabel.Name = "CargaDeTrabajoLabel"
        CargaDeTrabajoLabel.Size = New System.Drawing.Size(66, 15)
        CargaDeTrabajoLabel.TabIndex = 0
        CargaDeTrabajoLabel.Text = "General :"
        '
        'Carga_trab_insLabel
        '
        Carga_trab_insLabel.AutoSize = True
        Carga_trab_insLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Carga_trab_insLabel.Location = New System.Drawing.Point(13, 51)
        Carga_trab_insLabel.Name = "Carga_trab_insLabel"
        Carga_trab_insLabel.Size = New System.Drawing.Size(85, 15)
        Carga_trab_insLabel.TabIndex = 2
        Carga_trab_insLabel.Text = "Instalación :"
        '
        'NumeroExtLabel
        '
        NumeroExtLabel.AutoSize = True
        NumeroExtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeroExtLabel.Location = New System.Drawing.Point(17, 21)
        NumeroExtLabel.Name = "NumeroExtLabel"
        NumeroExtLabel.Size = New System.Drawing.Size(160, 15)
        NumeroExtLabel.TabIndex = 0
        NumeroExtLabel.Text = "Numero de Exteciones :"
        NumeroExtLabel.Visible = False
        '
        'CostoExtLabel
        '
        CostoExtLabel.AutoSize = True
        CostoExtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CostoExtLabel.Location = New System.Drawing.Point(35, 50)
        CostoExtLabel.Name = "CostoExtLabel"
        CostoExtLabel.Size = New System.Drawing.Size(143, 15)
        CostoExtLabel.TabIndex = 2
        CostoExtLabel.Text = "Costo por  Exteción : "
        CostoExtLabel.Visible = False
        '
        'ImpresoraOrdenesLabel
        '
        ImpresoraOrdenesLabel.AutoSize = True
        ImpresoraOrdenesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImpresoraOrdenesLabel.Location = New System.Drawing.Point(217, 192)
        ImpresoraOrdenesLabel.Name = "ImpresoraOrdenesLabel"
        ImpresoraOrdenesLabel.Size = New System.Drawing.Size(175, 15)
        ImpresoraOrdenesLabel.TabIndex = 21
        ImpresoraOrdenesLabel.Text = "Impresora Recone&xiones :"
        '
        'Puntos1Label
        '
        Puntos1Label.AutoSize = True
        Puntos1Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos1Label.Location = New System.Drawing.Point(41, 49)
        Puntos1Label.Name = "Puntos1Label"
        Puntos1Label.Size = New System.Drawing.Size(55, 15)
        Puntos1Label.TabIndex = 23
        Puntos1Label.Text = "Puntos "
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.Location = New System.Drawing.Point(151, 49)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(15, 15)
        Label24.TabIndex = 30
        Label24.Text = "a"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.Location = New System.Drawing.Point(227, 49)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(46, 15)
        Label25.TabIndex = 32
        Label25.Text = "años :"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.Location = New System.Drawing.Point(40, 76)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(55, 15)
        Label28.TabIndex = 33
        Label28.Text = "Puntos "
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label27.Location = New System.Drawing.Point(151, 76)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(15, 15)
        Label27.TabIndex = 35
        Label27.Text = "a"
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.Location = New System.Drawing.Point(227, 76)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(46, 15)
        Label26.TabIndex = 37
        Label26.Text = "años :"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.Location = New System.Drawing.Point(40, 102)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(55, 15)
        Label31.TabIndex = 38
        Label31.Text = "Puntos "
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.Location = New System.Drawing.Point(151, 102)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(15, 15)
        Label30.TabIndex = 40
        Label30.Text = "a"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label29.Location = New System.Drawing.Point(227, 102)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(46, 15)
        Label29.TabIndex = 42
        Label29.Text = "años :"
        '
        'Label34
        '
        Label34.AutoSize = True
        Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label34.Location = New System.Drawing.Point(40, 128)
        Label34.Name = "Label34"
        Label34.Size = New System.Drawing.Size(55, 15)
        Label34.TabIndex = 43
        Label34.Text = "Puntos "
        '
        'Label33
        '
        Label33.AutoSize = True
        Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label33.Location = New System.Drawing.Point(151, 128)
        Label33.Name = "Label33"
        Label33.Size = New System.Drawing.Size(15, 15)
        Label33.TabIndex = 45
        Label33.Text = "a"
        '
        'Label32
        '
        Label32.AutoSize = True
        Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label32.Location = New System.Drawing.Point(227, 128)
        Label32.Name = "Label32"
        Label32.Size = New System.Drawing.Size(46, 15)
        Label32.TabIndex = 47
        Label32.Text = "años :"
        '
        'Si_se_generaLabel
        '
        Si_se_generaLabel.AutoSize = True
        Si_se_generaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Si_se_generaLabel.Location = New System.Drawing.Point(276, 97)
        Si_se_generaLabel.Name = "Si_se_generaLabel"
        Si_se_generaLabel.Size = New System.Drawing.Size(174, 16)
        Si_se_generaLabel.TabIndex = 52
        Si_se_generaLabel.Text = "Generar Factura Global "
        '
        'MensajeLabel
        '
        MensajeLabel.AutoSize = True
        MensajeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensajeLabel.Location = New System.Drawing.Point(210, 120)
        MensajeLabel.Name = "MensajeLabel"
        MensajeLabel.Size = New System.Drawing.Size(66, 15)
        MensajeLabel.TabIndex = 54
        MensajeLabel.Text = "Mensaje:"
        '
        'ActivoLabel
        '
        ActivoLabel.AutoSize = True
        ActivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ActivoLabel.Location = New System.Drawing.Point(210, 299)
        ActivoLabel.Name = "ActivoLabel"
        ActivoLabel.Size = New System.Drawing.Size(48, 15)
        ActivoLabel.TabIndex = 56
        ActivoLabel.Text = "Activo:"
        '
        'Label38
        '
        Label38.AutoSize = True
        Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label38.Location = New System.Drawing.Point(197, 158)
        Label38.Name = "Label38"
        Label38.Size = New System.Drawing.Size(70, 15)
        Label38.TabIndex = 54
        Label38.Text = "Mensaje :"
        '
        'Label41
        '
        Label41.AutoSize = True
        Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label41.Location = New System.Drawing.Point(197, 102)
        Label41.Name = "Label41"
        Label41.Size = New System.Drawing.Size(99, 15)
        Label41.TabIndex = 58
        Label41.Text = "Monto Límite :"
        '
        'Label39
        '
        Label39.AutoSize = True
        Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label39.Location = New System.Drawing.Point(202, 275)
        Label39.Name = "Label39"
        Label39.Size = New System.Drawing.Size(27, 15)
        Label39.TabIndex = 60
        Label39.Text = "Ip :"
        '
        'TELefonosLabel
        '
        TELefonosLabel.AutoSize = True
        TELefonosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TELefonosLabel.Location = New System.Drawing.Point(182, 409)
        TELefonosLabel.Name = "TELefonosLabel"
        TELefonosLabel.Size = New System.Drawing.Size(78, 15)
        TELefonosLabel.TabIndex = 13
        TELefonosLabel.Text = "Teléfonos :"
        '
        'RfcLabel
        '
        RfcLabel.AutoSize = True
        RfcLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RfcLabel.Location = New System.Drawing.Point(218, 382)
        RfcLabel.Name = "RfcLabel"
        RfcLabel.Size = New System.Drawing.Size(37, 15)
        RfcLabel.TabIndex = 11
        RfcLabel.Text = "NIT :"
        '
        'CiudadLabel
        '
        CiudadLabel.AutoSize = True
        CiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CiudadLabel.Location = New System.Drawing.Point(204, 355)
        CiudadLabel.Name = "CiudadLabel"
        CiudadLabel.Size = New System.Drawing.Size(56, 15)
        CiudadLabel.TabIndex = 9
        CiudadLabel.Text = "Ciudad:"
        '
        'ColoniaLabel
        '
        ColoniaLabel.AutoSize = True
        ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ColoniaLabel.Location = New System.Drawing.Point(196, 327)
        ColoniaLabel.Name = "ColoniaLabel"
        ColoniaLabel.Size = New System.Drawing.Size(54, 15)
        ColoniaLabel.TabIndex = 7
        ColoniaLabel.Text = "Barrio :"
        '
        'DireccionLabel
        '
        DireccionLabel.AutoSize = True
        DireccionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DireccionLabel.Location = New System.Drawing.Point(188, 204)
        DireccionLabel.Name = "DireccionLabel"
        DireccionLabel.Size = New System.Drawing.Size(76, 15)
        DireccionLabel.TabIndex = 5
        DireccionLabel.Text = "Dirección :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(188, 184)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(72, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Empresa :"
        '
        'Id_sucursalLabel
        '
        Id_sucursalLabel.AutoSize = True
        Id_sucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Id_sucursalLabel.Location = New System.Drawing.Point(134, 153)
        Id_sucursalLabel.Name = "Id_sucursalLabel"
        Id_sucursalLabel.Size = New System.Drawing.Size(126, 15)
        Id_sucursalLabel.TabIndex = 0
        Id_sucursalLabel.Text = "Clave de la Plaza :"
        '
        'Label12
        '
        Label12.Location = New System.Drawing.Point(730, 211)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(100, 23)
        Label12.TabIndex = 41
        AddHandler Label12.Click, AddressOf Me.Label12_Click
        '
        'Label61
        '
        Label61.AutoSize = True
        Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label61.Location = New System.Drawing.Point(132, 339)
        Label61.Name = "Label61"
        Label61.Size = New System.Drawing.Size(351, 15)
        Label61.TabIndex = 25
        Label61.Text = "Dias Proporcionales Para Desconexion y Suspención:"
        '
        'Label63
        '
        Label63.AutoSize = True
        Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label63.Location = New System.Drawing.Point(212, 258)
        Label63.Name = "Label63"
        Label63.Size = New System.Drawing.Size(271, 15)
        Label63.TabIndex = 24
        Label63.Text = "Dias Propoecionales Para Contratación : "
        '
        'Label69
        '
        Label69.AutoSize = True
        Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label69.Location = New System.Drawing.Point(253, 107)
        Label69.Name = "Label69"
        Label69.Size = New System.Drawing.Size(47, 15)
        Label69.TabIndex = 53
        Label69.Text = "I.V.A. :"
        '
        'Label72
        '
        Label72.AutoSize = True
        Label72.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label72.Location = New System.Drawing.Point(371, 482)
        Label72.Name = "Label72"
        Label72.Size = New System.Drawing.Size(58, 15)
        Label72.TabIndex = 45
        Label72.Text = "Cobrar :"
        Label72.Visible = False
        '
        'Label125
        '
        Label125.AutoSize = True
        Label125.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label125.Location = New System.Drawing.Point(304, 374)
        Label125.Name = "Label125"
        Label125.Size = New System.Drawing.Size(124, 15)
        Label125.TabIndex = 47
        Label125.Text = "Por Pronto Pago : "
        '
        'Clave01900Label
        '
        Clave01900Label.AutoSize = True
        Clave01900Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave01900Label.Location = New System.Drawing.Point(63, 424)
        Clave01900Label.Name = "Clave01900Label"
        Clave01900Label.Size = New System.Drawing.Size(86, 13)
        Clave01900Label.TabIndex = 39
        '
        'Clave01800Label
        '
        Clave01800Label.AutoSize = True
        Clave01800Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave01800Label.Location = New System.Drawing.Point(63, 398)
        Clave01800Label.Name = "Clave01800Label"
        Clave01800Label.Size = New System.Drawing.Size(86, 13)
        Clave01800Label.TabIndex = 37
        '
        'Clave045Label
        '
        Clave045Label.AutoSize = True
        Clave045Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave045Label.Location = New System.Drawing.Point(63, 372)
        Clave045Label.Name = "Clave045Label"
        Clave045Label.Size = New System.Drawing.Size(72, 13)
        Clave045Label.TabIndex = 35
        '
        'Clave044Label
        '
        Clave044Label.AutoSize = True
        Clave044Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave044Label.Location = New System.Drawing.Point(63, 346)
        Clave044Label.Name = "Clave044Label"
        Clave044Label.Size = New System.Drawing.Size(72, 13)
        Clave044Label.TabIndex = 33
        '
        'Pais_LocalLabel
        '
        Pais_LocalLabel.AutoSize = True
        Pais_LocalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Pais_LocalLabel.Location = New System.Drawing.Point(63, 320)
        Pais_LocalLabel.Name = "Pais_LocalLabel"
        Pais_LocalLabel.Size = New System.Drawing.Size(74, 13)
        Pais_LocalLabel.TabIndex = 31
        '
        'Numero_LocalLabel
        '
        Numero_LocalLabel.AutoSize = True
        Numero_LocalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_LocalLabel.Location = New System.Drawing.Point(63, 294)
        Numero_LocalLabel.Name = "Numero_LocalLabel"
        Numero_LocalLabel.Size = New System.Drawing.Size(93, 13)
        Numero_LocalLabel.TabIndex = 29
        '
        'RutaLabel
        '
        RutaLabel.AutoSize = True
        RutaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RutaLabel.Location = New System.Drawing.Point(63, 268)
        RutaLabel.Name = "RutaLabel"
        RutaLabel.Size = New System.Drawing.Size(42, 13)
        RutaLabel.TabIndex = 27
        '
        'FormatoLabel
        '
        FormatoLabel.AutoSize = True
        FormatoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FormatoLabel.Location = New System.Drawing.Point(63, 242)
        FormatoLabel.Name = "FormatoLabel"
        FormatoLabel.Size = New System.Drawing.Size(60, 13)
        FormatoLabel.TabIndex = 25
        '
        'Numero_InicioLabel
        '
        Numero_InicioLabel.AutoSize = True
        Numero_InicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_InicioLabel.Location = New System.Drawing.Point(63, 216)
        Numero_InicioLabel.Name = "Numero_InicioLabel"
        Numero_InicioLabel.Size = New System.Drawing.Size(93, 13)
        Numero_InicioLabel.TabIndex = 23
        '
        'Fecha_InicioLabel
        '
        Fecha_InicioLabel.AutoSize = True
        Fecha_InicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_InicioLabel.Location = New System.Drawing.Point(63, 134)
        Fecha_InicioLabel.Name = "Fecha_InicioLabel"
        Fecha_InicioLabel.Size = New System.Drawing.Size(85, 13)
        Fecha_InicioLabel.TabIndex = 21
        '
        'Nom_ArchivoLabel
        '
        Nom_ArchivoLabel.AutoSize = True
        Nom_ArchivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Nom_ArchivoLabel.Location = New System.Drawing.Point(63, 107)
        Nom_ArchivoLabel.Name = "Nom_ArchivoLabel"
        Nom_ArchivoLabel.Size = New System.Drawing.Size(127, 13)
        Nom_ArchivoLabel.TabIndex = 19
        '
        'Label46
        '
        Label46.AutoSize = True
        Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label46.Location = New System.Drawing.Point(63, 189)
        Label46.Name = "Label46"
        Label46.Size = New System.Drawing.Size(85, 13)
        Label46.TabIndex = 41
        '
        'Periodo_inicialLabel
        '
        Periodo_inicialLabel.AutoSize = True
        Periodo_inicialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Periodo_inicialLabel.Location = New System.Drawing.Point(305, 188)
        Periodo_inicialLabel.Name = "Periodo_inicialLabel"
        Periodo_inicialLabel.Size = New System.Drawing.Size(17, 13)
        Periodo_inicialLabel.TabIndex = 49
        '
        'Fecha_FacturasLabel
        '
        Fecha_FacturasLabel.AutoSize = True
        Fecha_FacturasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_FacturasLabel.Location = New System.Drawing.Point(63, 162)
        Fecha_FacturasLabel.Name = "Fecha_FacturasLabel"
        Fecha_FacturasLabel.Size = New System.Drawing.Size(181, 13)
        Fecha_FacturasLabel.TabIndex = 47
        '
        'Label52
        '
        Label52.AutoSize = True
        Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label52.Location = New System.Drawing.Point(543, 229)
        Label52.Name = "Label52"
        Label52.Size = New System.Drawing.Size(44, 13)
        Label52.TabIndex = 72
        '
        'Label53
        '
        Label53.AutoSize = True
        Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label53.Location = New System.Drawing.Point(543, 255)
        Label53.Name = "Label53"
        Label53.Size = New System.Drawing.Size(42, 13)
        Label53.TabIndex = 73
        '
        'LblEdoClvAdic
        '
        LblEdoClvAdic.AutoSize = True
        LblEdoClvAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblEdoClvAdic.Location = New System.Drawing.Point(542, 320)
        LblEdoClvAdic.Name = "LblEdoClvAdic"
        LblEdoClvAdic.Size = New System.Drawing.Size(54, 13)
        LblEdoClvAdic.TabIndex = 77
        '
        'LblCdClvAdic
        '
        LblCdClvAdic.AutoSize = True
        LblCdClvAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblCdClvAdic.Location = New System.Drawing.Point(542, 346)
        LblCdClvAdic.Name = "LblCdClvAdic"
        LblCdClvAdic.Size = New System.Drawing.Size(118, 13)
        LblCdClvAdic.TabIndex = 78
        '
        'Label11
        '
        Label11.BackColor = System.Drawing.Color.Black
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.White
        Label11.Location = New System.Drawing.Point(147, 53)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(510, 24)
        Label11.TabIndex = 150
        Label11.Text = "Configuración de Rangos de Montos por Cobro de Material Diferido"
        Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label89
        '
        Label89.AutoSize = True
        Label89.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label89.Location = New System.Drawing.Point(194, 118)
        Label89.Name = "Label89"
        Label89.Size = New System.Drawing.Size(429, 15)
        Label89.TabIndex = 87
        Label89.Text = "Bonificación máxima permitida a usuarios Cajero sin Autorización"
        '
        'ConsultaCpGeneBindingSource
        '
        Me.ConsultaCpGeneBindingSource.DataMember = "Consulta_Cp_Gene"
        Me.ConsultaCpGeneBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_ImpresorasBindingSource
        '
        Me.Consulta_ImpresorasBindingSource.DataMember = "Consulta_Impresoras"
        Me.Consulta_ImpresorasBindingSource.DataSource = Me.DataSetarnoldo
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConMesesCobroAdeudoBindingSource
        '
        Me.ConMesesCobroAdeudoBindingSource.DataMember = "ConMesesCobroAdeudo"
        Me.ConMesesCobroAdeudoBindingSource.DataSource = Me.DataSetEric
        '
        'ConGeneralCorreoBindingSource
        '
        Me.ConGeneralCorreoBindingSource.DataMember = "ConGeneralCorreo"
        Me.ConGeneralCorreoBindingSource.DataSource = Me.DataSetEric
        '
        'Consulta_ImpresorasTableAdapter
        '
        Me.Consulta_ImpresorasTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Mod_Cp_GenBindingSource
        '
        Me.Inserta_Mod_Cp_GenBindingSource.DataMember = "Inserta_Mod_Cp_Gen"
        Me.Inserta_Mod_Cp_GenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Mod_Cp_GenTableAdapter
        '
        Me.Inserta_Mod_Cp_GenTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Cp_GeneTableAdapter
        '
        Me.Consulta_Cp_GeneTableAdapter.ClearBeforeFill = True
        '
        'ConMesesCobroAdeudoTableAdapter
        '
        Me.ConMesesCobroAdeudoTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConGeneralCorreoTableAdapter
        '
        Me.ConGeneralCorreoTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_Generales_Filtros_OrdenesBindingSource
        '
        Me.Consulta_Generales_Filtros_OrdenesBindingSource.DataMember = "Consulta_Generales_Filtros_Ordenes"
        Me.Consulta_Generales_Filtros_OrdenesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Generales_Filtros_OrdenesTableAdapter
        '
        Me.Consulta_Generales_Filtros_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Generales_Filtros_OrdenesBindingSource
        '
        Me.Inserta_Generales_Filtros_OrdenesBindingSource.DataMember = "Inserta_Generales_Filtros_Ordenes"
        Me.Inserta_Generales_Filtros_OrdenesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Generales_Filtros_OrdenesTableAdapter
        '
        Me.Inserta_Generales_Filtros_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'COnsultaGralfacturaglobalBindingSource
        '
        Me.COnsultaGralfacturaglobalBindingSource.DataMember = "COnsulta_Gral_factura_global"
        Me.COnsultaGralfacturaglobalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'COnsulta_Gral_factura_globalTableAdapter
        '
        Me.COnsulta_Gral_factura_globalTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Gral_factura_globalBindingSource
        '
        Me.Inserta_Gral_factura_globalBindingSource.DataMember = "Inserta_Gral_factura_global"
        Me.Inserta_Gral_factura_globalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Gral_factura_globalTableAdapter
        '
        Me.Inserta_Gral_factura_globalTableAdapter.ClearBeforeFill = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.Button12)
        Me.Panel4.Controls.Add(CuentaLabel)
        Me.Panel4.Controls.Add(Me.CuentaTextBox)
        Me.Panel4.Controls.Add(PasswordLabel)
        Me.Panel4.Controls.Add(Me.PasswordTextBox)
        Me.Panel4.Controls.Add(HostLabel)
        Me.Panel4.Controls.Add(Me.HostTextBox)
        Me.Panel4.Controls.Add(PortLabel)
        Me.Panel4.Controls.Add(Me.PortTextBox)
        Me.Panel4.Controls.Add(Me.Label17)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(865, 592)
        Me.Panel4.TabIndex = 32
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.SystemColors.Control
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.Black
        Me.Button12.Location = New System.Drawing.Point(694, 543)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(136, 36)
        Me.Button12.TabIndex = 29
        Me.Button12.Text = "&GUARDAR"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'CuentaTextBox
        '
        Me.CuentaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Cuenta", True))
        Me.CuentaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CuentaTextBox.Location = New System.Drawing.Point(379, 126)
        Me.CuentaTextBox.Name = "CuentaTextBox"
        Me.CuentaTextBox.Size = New System.Drawing.Size(208, 21)
        Me.CuentaTextBox.TabIndex = 30
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Password", True))
        Me.PasswordTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasswordTextBox.Location = New System.Drawing.Point(379, 153)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.Size = New System.Drawing.Size(208, 21)
        Me.PasswordTextBox.TabIndex = 32
        '
        'HostTextBox
        '
        Me.HostTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Host", True))
        Me.HostTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HostTextBox.Location = New System.Drawing.Point(379, 180)
        Me.HostTextBox.Name = "HostTextBox"
        Me.HostTextBox.Size = New System.Drawing.Size(208, 21)
        Me.HostTextBox.TabIndex = 34
        '
        'PortTextBox
        '
        Me.PortTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Port", True))
        Me.PortTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PortTextBox.Location = New System.Drawing.Point(379, 207)
        Me.PortTextBox.Name = "PortTextBox"
        Me.PortTextBox.Size = New System.Drawing.Size(127, 21)
        Me.PortTextBox.TabIndex = 36
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Black
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(50, 32)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(63, 20)
        Me.Label17.TabIndex = 29
        Me.Label17.Text = "Correo"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.Label60)
        Me.Panel3.Controls.Add(Me.DataGridViewCobro)
        Me.Panel3.Controls.Add(Me.ComboBoxCobro)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Button11)
        Me.Panel3.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel3.Controls.Add(Me.ConceptoComboBox)
        Me.Panel3.Controls.Add(MesesLabel)
        Me.Panel3.Controls.Add(Me.MesesTextBox)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(865, 592)
        Me.Panel3.TabIndex = 31
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(224, 97)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(110, 15)
        Me.Label60.TabIndex = 25
        Me.Label60.Text = "Tipo de Servicio"
        '
        'DataGridViewCobro
        '
        Me.DataGridViewCobro.AllowUserToAddRows = False
        Me.DataGridViewCobro.AllowUserToDeleteRows = False
        Me.DataGridViewCobro.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridViewCobro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewCobro.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_TipoCliente, Me.Descripcion, Me.Meses, Me.Habilitado})
        Me.DataGridViewCobro.Location = New System.Drawing.Point(227, 158)
        Me.DataGridViewCobro.Name = "DataGridViewCobro"
        Me.DataGridViewCobro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewCobro.Size = New System.Drawing.Size(458, 301)
        Me.DataGridViewCobro.TabIndex = 24
        '
        'Clv_TipoCliente
        '
        Me.Clv_TipoCliente.DataPropertyName = "Clv_TipoCliente"
        Me.Clv_TipoCliente.HeaderText = "Clv_TipoCliente"
        Me.Clv_TipoCliente.Name = "Clv_TipoCliente"
        Me.Clv_TipoCliente.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Tipo Cliente"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Width = 200
        '
        'Meses
        '
        Me.Meses.DataPropertyName = "Meses"
        Me.Meses.HeaderText = "Meses"
        Me.Meses.Name = "Meses"
        '
        'Habilitado
        '
        Me.Habilitado.DataPropertyName = "Habilitado"
        Me.Habilitado.HeaderText = "Habilitado"
        Me.Habilitado.Name = "Habilitado"
        '
        'ComboBoxCobro
        '
        Me.ComboBoxCobro.DisplayMember = "Concepto"
        Me.ComboBoxCobro.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCobro.FormattingEnabled = True
        Me.ComboBoxCobro.Location = New System.Drawing.Point(227, 115)
        Me.ComboBoxCobro.Name = "ComboBoxCobro"
        Me.ComboBoxCobro.Size = New System.Drawing.Size(262, 23)
        Me.ComboBoxCobro.TabIndex = 23
        Me.ComboBoxCobro.ValueMember = "Clv_TipSer"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Black
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(37, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(246, 20)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Contratación a Plazo Forzoso"
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.SystemColors.Control
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.Black
        Me.Button11.Location = New System.Drawing.Point(692, 528)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(136, 36)
        Me.Button11.TabIndex = 21
        Me.Button11.Text = "&GUARDAR"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipServEricBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(755, 551)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 21)
        Me.Clv_TipSerTextBox.TabIndex = 7
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(778, 551)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(10, 23)
        Me.ConceptoComboBox.TabIndex = 6
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        Me.ConceptoComboBox.Visible = False
        '
        'MesesTextBox
        '
        Me.MesesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMesesCobroAdeudoBindingSource, "Meses", True))
        Me.MesesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesTextBox.Location = New System.Drawing.Point(755, 551)
        Me.MesesTextBox.Name = "MesesTextBox"
        Me.MesesTextBox.Size = New System.Drawing.Size(17, 21)
        Me.MesesTextBox.TabIndex = 5
        Me.MesesTextBox.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.Label124)
        Me.Panel1.Controls.Add(Me.tbPuntosAnti)
        Me.Panel1.Controls.Add(Label125)
        Me.Panel1.Controls.Add(Label72)
        Me.Panel1.Controls.Add(Me.rb30)
        Me.Panel1.Controls.Add(Me.rb15)
        Me.Panel1.Controls.Add(Me.Label71)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.txtDiaCorte)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.TextBox15)
        Me.Panel1.Controls.Add(Label21)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TextBox11)
        Me.Panel1.Controls.Add(Label20)
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Label16)
        Me.Panel1.Controls.Add(Me.TextBox14)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Label12)
        Me.Panel1.Controls.Add(Me.TextBox16)
        Me.Panel1.Controls.Add(Me.TextBox13)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.TextBox7)
        Me.Panel1.Controls.Add(Label22)
        Me.Panel1.Controls.Add(Label19)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(865, 592)
        Me.Panel1.TabIndex = 29
        '
        'Label124
        '
        Me.Label124.BackColor = System.Drawing.Color.Black
        Me.Label124.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label124.ForeColor = System.Drawing.Color.White
        Me.Label124.Location = New System.Drawing.Point(246, 336)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(330, 23)
        Me.Label124.TabIndex = 48
        Me.Label124.Text = "Días Limite para Puntos de Antigüedad  :"
        Me.Label124.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tbPuntosAnti
        '
        Me.tbPuntosAnti.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosAnt1", True))
        Me.tbPuntosAnti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPuntosAnti.Location = New System.Drawing.Point(434, 372)
        Me.tbPuntosAnti.MaxLength = 3
        Me.tbPuntosAnti.Name = "tbPuntosAnti"
        Me.tbPuntosAnti.Size = New System.Drawing.Size(100, 21)
        Me.tbPuntosAnti.TabIndex = 46
        '
        'CONSULTAGENERALESDESCBindingSource
        '
        Me.CONSULTAGENERALESDESCBindingSource.DataMember = "CONSULTAGENERALESDESC"
        Me.CONSULTAGENERALESDESCBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'rb30
        '
        Me.rb30.AutoSize = True
        Me.rb30.Location = New System.Drawing.Point(435, 505)
        Me.rb30.Name = "rb30"
        Me.rb30.Size = New System.Drawing.Size(115, 19)
        Me.rb30.TabIndex = 44
        Me.rb30.Text = "Mes completo"
        Me.rb30.UseVisualStyleBackColor = True
        Me.rb30.Visible = False
        '
        'rb15
        '
        Me.rb15.AutoSize = True
        Me.rb15.Checked = True
        Me.rb15.Location = New System.Drawing.Point(435, 480)
        Me.rb15.Name = "rb15"
        Me.rb15.Size = New System.Drawing.Size(72, 19)
        Me.rb15.TabIndex = 43
        Me.rb15.TabStop = True
        Me.rb15.Text = "15 días"
        Me.rb15.UseVisualStyleBackColor = True
        Me.rb15.Visible = False
        '
        'Label71
        '
        Me.Label71.BackColor = System.Drawing.Color.Black
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.ForeColor = System.Drawing.Color.White
        Me.Label71.Location = New System.Drawing.Point(247, 407)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(330, 23)
        Me.Label71.TabIndex = 42
        Me.Label71.Text = "Pagos Diferidos :"
        Me.Label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label71.Visible = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.MUESTRAPERIODOSBindingSource, "Habilitar", True))
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(504, 77)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(104, 19)
        Me.CheckBox1.TabIndex = 11
        Me.CheckBox1.Text = "Deshabilitar"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'MUESTRAPERIODOSBindingSource
        '
        Me.MUESTRAPERIODOSBindingSource.DataMember = "MUESTRAPERIODOS"
        Me.MUESTRAPERIODOSBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Black
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(34, 29)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(144, 20)
        Me.Label18.TabIndex = 16
        Me.Label18.Text = "Periodo de Corte"
        '
        'txtDiaCorte
        '
        Me.txtDiaCorte.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosAnt1", True))
        Me.txtDiaCorte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiaCorte.Location = New System.Drawing.Point(435, 442)
        Me.txtDiaCorte.MaxLength = 3
        Me.txtDiaCorte.Name = "txtDiaCorte"
        Me.txtDiaCorte.Size = New System.Drawing.Size(100, 21)
        Me.txtDiaCorte.TabIndex = 16
        Me.txtDiaCorte.Visible = False
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Black
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(246, 263)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(330, 23)
        Me.Label7.TabIndex = 39
        Me.Label7.Text = "Días Limite para Puntos  :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(585, 310)
        Me.TextBox15.MaxLength = 3
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(110, 21)
        Me.TextBox15.TabIndex = 32
        Me.TextBox15.TabStop = False
        Me.TextBox15.Visible = False
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Black
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(246, 200)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(330, 23)
        Me.Label6.TabIndex = 38
        Me.Label6.Text = "Proceso de Desconexión :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox11
        '
        Me.TextBox11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosPP1", True))
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.Location = New System.Drawing.Point(434, 299)
        Me.TextBox11.MaxLength = 3
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(100, 21)
        Me.TextBox11.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Black
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(247, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(329, 23)
        Me.Label5.TabIndex = 37
        Me.Label5.Text = "Clientes que Contraten :"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox14
        '
        Me.TextBox14.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaInicial1", True))
        Me.TextBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox14.Location = New System.Drawing.Point(434, 148)
        Me.TextBox14.MaxLength = 3
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(100, 21)
        Me.TextBox14.TabIndex = 12
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.SystemColors.Control
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(694, 515)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 17
        Me.Button6.Text = "&GUARDAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(586, 270)
        Me.TextBox16.MaxLength = 3
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(109, 21)
        Me.TextBox16.TabIndex = 31
        Me.TextBox16.TabStop = False
        Me.TextBox16.Visible = False
        '
        'TextBox13
        '
        Me.TextBox13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaFinal1", True))
        Me.TextBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox13.Location = New System.Drawing.Point(434, 175)
        Me.TextBox13.MaxLength = 3
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(100, 21)
        Me.TextBox13.TabIndex = 13
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRAPERIODOSBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(250, 77)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(205, 23)
        Me.ComboBox1.TabIndex = 10
        Me.ComboBox1.ValueMember = "Clv_Periodo"
        '
        'TextBox7
        '
        Me.TextBox7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaCorte", True))
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(434, 233)
        Me.TextBox7.MaxLength = 3
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(100, 21)
        Me.TextBox7.TabIndex = 14
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Black
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(246, 51)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(75, 20)
        Me.Label23.TabIndex = 34
        Me.Label23.Text = "Periodo:"
        '
        'PanelBasico
        '
        Me.PanelBasico.BackColor = System.Drawing.Color.Transparent
        Me.PanelBasico.Controls.Add(Me.Panel13)
        Me.PanelBasico.Controls.Add(Me.CheckBox3)
        Me.PanelBasico.Controls.Add(Me.Label3)
        Me.PanelBasico.Controls.Add(Me.CheckBox2)
        Me.PanelBasico.Controls.Add(Me.Label35)
        Me.PanelBasico.Controls.Add(Me.Panel5)
        Me.PanelBasico.Controls.Add(Me.ImportePorExtrasTextBox)
        Me.PanelBasico.Controls.Add(ImportePorExtrasLabel)
        Me.PanelBasico.Controls.Add(Me.GroupBox2)
        Me.PanelBasico.Controls.Add(Me.Button3)
        Me.PanelBasico.Controls.Add(Me.GroupBox3)
        Me.PanelBasico.Controls.Add(Me.ImpresoraOrdenesTextBox)
        Me.PanelBasico.Controls.Add(ImpresoraOrdenesLabel)
        Me.PanelBasico.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelBasico.Location = New System.Drawing.Point(3, 3)
        Me.PanelBasico.Name = "PanelBasico"
        Me.PanelBasico.Size = New System.Drawing.Size(859, 586)
        Me.PanelBasico.TabIndex = 3
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.Label4)
        Me.Panel13.Controls.Add(Me.Puntos1TextBox)
        Me.Panel13.Controls.Add(Label30)
        Me.Panel13.Controls.Add(Me.NumericUpDown2)
        Me.Panel13.Controls.Add(Me.NumericUpDown5)
        Me.Panel13.Controls.Add(Puntos1Label)
        Me.Panel13.Controls.Add(Label25)
        Me.Panel13.Controls.Add(Me.NumericUpDown6)
        Me.Panel13.Controls.Add(Label24)
        Me.Panel13.Controls.Add(Label29)
        Me.Panel13.Controls.Add(Me.Puntos2TextBox)
        Me.Panel13.Controls.Add(Label28)
        Me.Panel13.Controls.Add(Label31)
        Me.Panel13.Controls.Add(Label34)
        Me.Panel13.Controls.Add(Me.NumericUpDown1)
        Me.Panel13.Controls.Add(Me.Puntos5TextBox)
        Me.Panel13.Controls.Add(Label32)
        Me.Panel13.Controls.Add(Label26)
        Me.Panel13.Controls.Add(Me.NumericUpDown4)
        Me.Panel13.Controls.Add(Me.NumericUpDown8)
        Me.Panel13.Controls.Add(Me.NumericUpDown3)
        Me.Panel13.Controls.Add(Label33)
        Me.Panel13.Controls.Add(Me.NumericUpDown7)
        Me.Panel13.Controls.Add(Me.Puntos10TextBox)
        Me.Panel13.Controls.Add(Label27)
        Me.Panel13.Enabled = False
        Me.Panel13.Location = New System.Drawing.Point(28, 229)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(21, 24)
        Me.Panel13.TabIndex = 52
        Me.Panel13.Visible = False
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Black
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(28, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(358, 20)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Puntos Por Antigüedad"
        '
        'Puntos1TextBox
        '
        Me.Puntos1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos1", True))
        Me.Puntos1TextBox.Location = New System.Drawing.Point(279, 46)
        Me.Puntos1TextBox.MaxLength = 3
        Me.Puntos1TextBox.Name = "Puntos1TextBox"
        Me.Puntos1TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos1TextBox.TabIndex = 18
        '
        'ConGeneralAntiguedadBindingSource
        '
        Me.ConGeneralAntiguedadBindingSource.DataMember = "ConGeneralAntiguedad"
        Me.ConGeneralAntiguedadBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown2.Location = New System.Drawing.Point(171, 46)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(52, 21)
        Me.NumericUpDown2.TabIndex = 17
        '
        'NumericUpDown5
        '
        Me.NumericUpDown5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown5.Location = New System.Drawing.Point(96, 99)
        Me.NumericUpDown5.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown5.Name = "NumericUpDown5"
        Me.NumericUpDown5.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown5.TabIndex = 22
        '
        'NumericUpDown6
        '
        Me.NumericUpDown6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown6.Location = New System.Drawing.Point(173, 99)
        Me.NumericUpDown6.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown6.Name = "NumericUpDown6"
        Me.NumericUpDown6.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown6.TabIndex = 23
        '
        'Puntos2TextBox
        '
        Me.Puntos2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos2", True))
        Me.Puntos2TextBox.Location = New System.Drawing.Point(279, 73)
        Me.Puntos2TextBox.MaxLength = 3
        Me.Puntos2TextBox.Name = "Puntos2TextBox"
        Me.Puntos2TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos2TextBox.TabIndex = 21
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.Location = New System.Drawing.Point(96, 46)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(52, 21)
        Me.NumericUpDown1.TabIndex = 16
        '
        'Puntos5TextBox
        '
        Me.Puntos5TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos5", True))
        Me.Puntos5TextBox.Location = New System.Drawing.Point(279, 99)
        Me.Puntos5TextBox.MaxLength = 3
        Me.Puntos5TextBox.Name = "Puntos5TextBox"
        Me.Puntos5TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos5TextBox.TabIndex = 24
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown4.Location = New System.Drawing.Point(173, 73)
        Me.NumericUpDown4.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown4.TabIndex = 20
        '
        'NumericUpDown8
        '
        Me.NumericUpDown8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown8.Location = New System.Drawing.Point(173, 125)
        Me.NumericUpDown8.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown8.Name = "NumericUpDown8"
        Me.NumericUpDown8.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown8.TabIndex = 26
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown3.Location = New System.Drawing.Point(96, 73)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown3.TabIndex = 19
        '
        'NumericUpDown7
        '
        Me.NumericUpDown7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown7.Location = New System.Drawing.Point(97, 125)
        Me.NumericUpDown7.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown7.Name = "NumericUpDown7"
        Me.NumericUpDown7.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown7.TabIndex = 25
        '
        'Puntos10TextBox
        '
        Me.Puntos10TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos10", True))
        Me.Puntos10TextBox.Location = New System.Drawing.Point(279, 125)
        Me.Puntos10TextBox.MaxLength = 3
        Me.Puntos10TextBox.Name = "Puntos10TextBox"
        Me.Puntos10TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos10TextBox.TabIndex = 27
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(293, 307)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(263, 19)
        Me.CheckBox3.TabIndex = 50
        Me.CheckBox3.Text = "Instalación De Filtro De Solo Internet"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Black
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(24, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(190, 20)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Servicios de Televisión"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(294, 281)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(232, 19)
        Me.CheckBox2.TabIndex = 49
        Me.CheckBox2.Text = "Retiro De Filtro De Solo Internet"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.BackColor = System.Drawing.Color.Black
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(285, 258)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(339, 20)
        Me.Label35.TabIndex = 48
        Me.Label35.Text = "Generar Orden De Cambio De Clientes"
        '
        'Panel5
        '
        Me.Panel5.Location = New System.Drawing.Point(761, 69)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(72, 100)
        Me.Panel5.TabIndex = 51
        '
        'ImportePorExtrasTextBox
        '
        Me.ImportePorExtrasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ImportePorExtras", True))
        Me.ImportePorExtrasTextBox.Location = New System.Drawing.Point(593, 55)
        Me.ImportePorExtrasTextBox.MaxLength = 10
        Me.ImportePorExtrasTextBox.Name = "ImportePorExtrasTextBox"
        Me.ImportePorExtrasTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ImportePorExtrasTextBox.TabIndex = 10
        Me.ImportePorExtrasTextBox.Visible = False
        '
        'ConGeneralBindingSource
        '
        Me.ConGeneralBindingSource.AllowNew = False
        Me.ConGeneralBindingSource.DataMember = "ConGeneral"
        Me.ConGeneralBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Carga_trab_insLabel)
        Me.GroupBox2.Controls.Add(Me.Carga_trab_insTextBox)
        Me.GroupBox2.Controls.Add(CargaDeTrabajoLabel)
        Me.GroupBox2.Controls.Add(Me.CargaDeTrabajoTextBox)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(290, 85)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(302, 84)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Carga de trabajo"
        '
        'Carga_trab_insTextBox
        '
        Me.Carga_trab_insTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "carga_trab_ins", True))
        Me.Carga_trab_insTextBox.Location = New System.Drawing.Point(104, 48)
        Me.Carga_trab_insTextBox.MaxLength = 2
        Me.Carga_trab_insTextBox.Name = "Carga_trab_insTextBox"
        Me.Carga_trab_insTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Carga_trab_insTextBox.TabIndex = 14
        '
        'CargaDeTrabajoTextBox
        '
        Me.CargaDeTrabajoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "CargaDeTrabajo", True))
        Me.CargaDeTrabajoTextBox.Location = New System.Drawing.Point(104, 21)
        Me.CargaDeTrabajoTextBox.MaxLength = 2
        Me.CargaDeTrabajoTextBox.Name = "CargaDeTrabajoTextBox"
        Me.CargaDeTrabajoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CargaDeTrabajoTextBox.TabIndex = 13
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.Control
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(694, 543)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 20
        Me.Button3.Text = "&GUARDAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(CostoExtLabel)
        Me.GroupBox3.Controls.Add(Me.CostoExtTextBox)
        Me.GroupBox3.Controls.Add(NumeroExtLabel)
        Me.GroupBox3.Controls.Add(Me.NumeroExtTextBox)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(314, 348)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(295, 83)
        Me.GroupBox3.TabIndex = 11
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Hoteles"
        Me.GroupBox3.Visible = False
        '
        'CostoExtTextBox
        '
        Me.CostoExtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "CostoExt", True))
        Me.CostoExtTextBox.Location = New System.Drawing.Point(183, 47)
        Me.CostoExtTextBox.MaxLength = 10
        Me.CostoExtTextBox.Name = "CostoExtTextBox"
        Me.CostoExtTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CostoExtTextBox.TabIndex = 12
        Me.CostoExtTextBox.Visible = False
        '
        'NumeroExtTextBox
        '
        Me.NumeroExtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "NumeroExt", True))
        Me.NumeroExtTextBox.Location = New System.Drawing.Point(183, 18)
        Me.NumeroExtTextBox.MaxLength = 3
        Me.NumeroExtTextBox.Name = "NumeroExtTextBox"
        Me.NumeroExtTextBox.Size = New System.Drawing.Size(100, 21)
        Me.NumeroExtTextBox.TabIndex = 11
        Me.NumeroExtTextBox.Visible = False
        '
        'ImpresoraOrdenesTextBox
        '
        Me.ImpresoraOrdenesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ImpresoraOrdenes", True))
        Me.ImpresoraOrdenesTextBox.Location = New System.Drawing.Point(398, 191)
        Me.ImpresoraOrdenesTextBox.MaxLength = 150
        Me.ImpresoraOrdenesTextBox.Name = "ImpresoraOrdenesTextBox"
        Me.ImpresoraOrdenesTextBox.Size = New System.Drawing.Size(345, 21)
        Me.ImpresoraOrdenesTextBox.TabIndex = 15
        '
        'Consulta_cobrodepositoBindingSource
        '
        Me.Consulta_cobrodepositoBindingSource.DataMember = "Consulta_cobrodeposito"
        Me.Consulta_cobrodepositoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Transparent
        Me.Panel6.Controls.Add(Me.RadioButton2)
        Me.Panel6.Controls.Add(Me.RadioButton1)
        Me.Panel6.Controls.Add(Si_se_generaLabel)
        Me.Panel6.Controls.Add(Me.Si_se_generaCheckBox)
        Me.Panel6.Controls.Add(Me.Button15)
        Me.Panel6.Controls.Add(Me.Label36)
        Me.Panel6.Controls.Add(Me.TextBox2)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(0, 0)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(865, 592)
        Me.Panel6.TabIndex = 52
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(462, 145)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(80, 19)
        Me.RadioButton2.TabIndex = 55
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Mensual"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(341, 145)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(64, 19)
        Me.RadioButton1.TabIndex = 54
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Diaria"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Si_se_generaCheckBox
        '
        Me.Si_se_generaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.COnsultaGralfacturaglobalBindingSource, "si_se_genera", True))
        Me.Si_se_generaCheckBox.Location = New System.Drawing.Point(256, 94)
        Me.Si_se_generaCheckBox.Name = "Si_se_generaCheckBox"
        Me.Si_se_generaCheckBox.Size = New System.Drawing.Size(51, 24)
        Me.Si_se_generaCheckBox.TabIndex = 53
        '
        'Button15
        '
        Me.Button15.BackColor = System.Drawing.SystemColors.Control
        Me.Button15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button15.ForeColor = System.Drawing.Color.Black
        Me.Button15.Location = New System.Drawing.Point(694, 543)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(136, 36)
        Me.Button15.TabIndex = 50
        Me.Button15.Text = "&GUARDAR"
        Me.Button15.UseVisualStyleBackColor = False
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.Black
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(42, 27)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(281, 20)
        Me.Label36.TabIndex = 49
        Me.Label36.Text = "Generales de Facturas Globales."
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.COnsultaGralfacturaglobalBindingSource, "Tipo", True))
        Me.TextBox2.Location = New System.Drawing.Point(595, 145)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(112, 21)
        Me.TextBox2.TabIndex = 54
        Me.TextBox2.Visible = False
        '
        'ConGeneralAlertaBindingSource
        '
        Me.ConGeneralAlertaBindingSource.DataMember = "ConGeneralAlerta"
        Me.ConGeneralAlertaBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONSULTA_General_HsbcBindingSource
        '
        Me.CONSULTA_General_HsbcBindingSource.DataMember = "CONSULTA_General_Hsbc"
        Me.CONSULTA_General_HsbcBindingSource.DataSource = Me.DataSetyahve
        '
        'DataSetyahve
        '
        Me.DataSetyahve.DataSetName = "DataSetyahve"
        Me.DataSetyahve.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONSULTA_General_SantanderBindingSource
        '
        Me.CONSULTA_General_SantanderBindingSource.DataMember = "CONSULTA_General_Santander"
        Me.CONSULTA_General_SantanderBindingSource.DataSource = Me.DataSetyahve
        '
        'CONSULTA_General_XmlBindingSource
        '
        Me.CONSULTA_General_XmlBindingSource.DataMember = "CONSULTA_General_Xml"
        Me.CONSULTA_General_XmlBindingSource.DataSource = Me.DataSetyahve
        '
        'PanelDatos
        '
        Me.PanelDatos.AutoScroll = True
        Me.PanelDatos.BackColor = System.Drawing.Color.Transparent
        Me.PanelDatos.Controls.Add(Me.Label56)
        Me.PanelDatos.Controls.Add(Me.TextBoxNumeroExt)
        Me.PanelDatos.Controls.Add(Me.Label55)
        Me.PanelDatos.Controls.Add(Me.TextBoxNumeroInt)
        Me.PanelDatos.Controls.Add(Me.Label14)
        Me.PanelDatos.Controls.Add(Me.Label2)
        Me.PanelDatos.Controls.Add(ColoniaLabel)
        Me.PanelDatos.Controls.Add(Me.TextBox1)
        Me.PanelDatos.Controls.Add(Me.CiudadTextBox)
        Me.PanelDatos.Controls.Add(Me.ColoniaTextBox)
        Me.PanelDatos.Controls.Add(Me.Button5)
        Me.PanelDatos.Controls.Add(CiudadLabel)
        Me.PanelDatos.Controls.Add(Me.Id_sucursalTextBox)
        Me.PanelDatos.Controls.Add(DireccionLabel)
        Me.PanelDatos.Controls.Add(Id_sucursalLabel)
        Me.PanelDatos.Controls.Add(Me.RfcTextBox)
        Me.PanelDatos.Controls.Add(TELefonosLabel)
        Me.PanelDatos.Controls.Add(Me.DireccionTextBox)
        Me.PanelDatos.Controls.Add(Me.NombreTextBox)
        Me.PanelDatos.Controls.Add(RfcLabel)
        Me.PanelDatos.Controls.Add(Me.TELefonosTextBox)
        Me.PanelDatos.Controls.Add(NombreLabel)
        Me.PanelDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelDatos.Location = New System.Drawing.Point(3, 3)
        Me.PanelDatos.Name = "PanelDatos"
        Me.PanelDatos.Size = New System.Drawing.Size(859, 586)
        Me.PanelDatos.TabIndex = 68
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(162, 273)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(103, 15)
        Me.Label56.TabIndex = 23
        Me.Label56.Text = "Num. Exterior :"
        '
        'TextBoxNumeroExt
        '
        Me.TextBoxNumeroExt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNumeroExt.Location = New System.Drawing.Point(266, 267)
        Me.TextBoxNumeroExt.MaxLength = 20
        Me.TextBoxNumeroExt.Name = "TextBoxNumeroExt"
        Me.TextBoxNumeroExt.Size = New System.Drawing.Size(204, 21)
        Me.TextBoxNumeroExt.TabIndex = 22
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(161, 300)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(99, 15)
        Me.Label55.TabIndex = 21
        Me.Label55.Text = "Num. Interior :"
        '
        'TextBoxNumeroInt
        '
        Me.TextBoxNumeroInt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNumeroInt.Location = New System.Drawing.Point(266, 294)
        Me.TextBoxNumeroInt.MaxLength = 20
        Me.TextBoxNumeroInt.Name = "TextBoxNumeroInt"
        Me.TextBoxNumeroInt.Size = New System.Drawing.Size(204, 21)
        Me.TextBoxNumeroInt.TabIndex = 20
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(164, 436)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(100, 15)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Código Postal:"
        Me.Label14.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Black
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(26, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(182, 20)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Datos de la Empresa "
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaCpGeneBindingSource, "Codigo_Postal", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox1.Location = New System.Drawing.Point(266, 430)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(204, 21)
        Me.TextBox1.TabIndex = 18
        Me.TextBox1.Visible = False
        '
        'CiudadTextBox
        '
        Me.CiudadTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ciudad", True))
        Me.CiudadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CiudadTextBox.Location = New System.Drawing.Point(266, 349)
        Me.CiudadTextBox.MaxLength = 180
        Me.CiudadTextBox.Name = "CiudadTextBox"
        Me.CiudadTextBox.Size = New System.Drawing.Size(331, 21)
        Me.CiudadTextBox.TabIndex = 14
        '
        'ColoniaTextBox
        '
        Me.ColoniaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "colonia", True))
        Me.ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColoniaTextBox.Location = New System.Drawing.Point(266, 321)
        Me.ColoniaTextBox.MaxLength = 150
        Me.ColoniaTextBox.Name = "ColoniaTextBox"
        Me.ColoniaTextBox.Size = New System.Drawing.Size(331, 21)
        Me.ColoniaTextBox.TabIndex = 13
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(693, 506)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 17
        Me.Button5.Text = "&GUARDAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Id_sucursalTextBox
        '
        Me.Id_sucursalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Id_sucursalTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Id_sucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "id_sucursal", True))
        Me.Id_sucursalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Id_sucursalTextBox.Location = New System.Drawing.Point(266, 151)
        Me.Id_sucursalTextBox.MaxLength = 2
        Me.Id_sucursalTextBox.Name = "Id_sucursalTextBox"
        Me.Id_sucursalTextBox.ReadOnly = True
        Me.Id_sucursalTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Id_sucursalTextBox.TabIndex = 10
        '
        'RfcTextBox
        '
        Me.RfcTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RfcTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RfcTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "rfc", True))
        Me.RfcTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RfcTextBox.Location = New System.Drawing.Point(266, 376)
        Me.RfcTextBox.MaxLength = 20
        Me.RfcTextBox.Name = "RfcTextBox"
        Me.RfcTextBox.Size = New System.Drawing.Size(204, 21)
        Me.RfcTextBox.TabIndex = 15
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "direccion", True))
        Me.DireccionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DireccionTextBox.Location = New System.Drawing.Point(266, 204)
        Me.DireccionTextBox.MaxLength = 250
        Me.DireccionTextBox.Multiline = True
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(488, 57)
        Me.DireccionTextBox.TabIndex = 12
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(266, 178)
        Me.NombreTextBox.MaxLength = 150
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(488, 21)
        Me.NombreTextBox.TabIndex = 11
        '
        'TELefonosTextBox
        '
        Me.TELefonosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TELefonosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "TELefonos", True))
        Me.TELefonosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELefonosTextBox.Location = New System.Drawing.Point(266, 403)
        Me.TELefonosTextBox.MaxLength = 50
        Me.TELefonosTextBox.Name = "TELefonosTextBox"
        Me.TELefonosTextBox.Size = New System.Drawing.Size(204, 21)
        Me.TELefonosTextBox.TabIndex = 16
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Transparent
        Me.Panel8.Controls.Add(Me.TreeView1)
        Me.Panel8.Controls.Add(Me.TextBox6)
        Me.Panel8.Controls.Add(Me.Button21)
        Me.Panel8.Controls.Add(Me.Button20)
        Me.Panel8.Controls.Add(Label39)
        Me.Panel8.Controls.Add(Me.TextBox5)
        Me.Panel8.Controls.Add(Label41)
        Me.Panel8.Controls.Add(Me.TextBox4)
        Me.Panel8.Controls.Add(Label38)
        Me.Panel8.Controls.Add(Me.TextBox3)
        Me.Panel8.Controls.Add(Me.CheckBox4)
        Me.Panel8.Controls.Add(Me.Button19)
        Me.Panel8.Controls.Add(Me.Label40)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel8.Location = New System.Drawing.Point(0, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(865, 592)
        Me.Panel8.TabIndex = 54
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(415, 291)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(312, 127)
        Me.TreeView1.TabIndex = 62
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(505, 332)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 21)
        Me.TextBox6.TabIndex = 74
        '
        'Button21
        '
        Me.Button21.BackColor = System.Drawing.SystemColors.Control
        Me.Button21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button21.ForeColor = System.Drawing.Color.Black
        Me.Button21.Location = New System.Drawing.Point(212, 370)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(176, 26)
        Me.Button21.TabIndex = 64
        Me.Button21.Text = "&Quitar Ip de la Lista"
        Me.Button21.UseVisualStyleBackColor = False
        '
        'Button20
        '
        Me.Button20.BackColor = System.Drawing.SystemColors.Control
        Me.Button20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button20.ForeColor = System.Drawing.Color.Black
        Me.Button20.Location = New System.Drawing.Point(212, 332)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(174, 26)
        Me.Button20.TabIndex = 63
        Me.Button20.Text = "&Agregar Ip a la Lista"
        Me.Button20.UseVisualStyleBackColor = False
        '
        'TextBox5
        '
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(210, 293)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox5.Size = New System.Drawing.Size(110, 21)
        Me.TextBox5.TabIndex = 61
        '
        'TextBox4
        '
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAlertaBindingSource, "Monto", True))
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(205, 120)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox4.Size = New System.Drawing.Size(110, 21)
        Me.TextBox4.TabIndex = 59
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAlertaBindingSource, "Mensaje", True))
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(205, 176)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox3.Size = New System.Drawing.Size(517, 88)
        Me.TextBox3.TabIndex = 55
        '
        'CheckBox4
        '
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.Location = New System.Drawing.Point(409, 110)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(161, 24)
        Me.CheckBox4.TabIndex = 57
        Me.CheckBox4.Text = "Activar Alerta"
        '
        'Button19
        '
        Me.Button19.BackColor = System.Drawing.SystemColors.Control
        Me.Button19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button19.ForeColor = System.Drawing.Color.Black
        Me.Button19.Location = New System.Drawing.Point(694, 543)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(136, 36)
        Me.Button19.TabIndex = 54
        Me.Button19.Text = "&GUARDAR"
        Me.Button19.UseVisualStyleBackColor = False
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Black
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(46, 26)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(66, 20)
        Me.Label40.TabIndex = 52
        Me.Label40.Text = "Alertas"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.Controls.Add(MensajeLabel)
        Me.Panel7.Controls.Add(Me.MensajeTextBox)
        Me.Panel7.Controls.Add(ActivoLabel)
        Me.Panel7.Controls.Add(Me.ActivoCheckBox)
        Me.Panel7.Controls.Add(Me.Button16)
        Me.Panel7.Controls.Add(Me.Label37)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(865, 592)
        Me.Panel7.TabIndex = 53
        '
        'MensajeTextBox
        '
        Me.MensajeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeTextBox.Location = New System.Drawing.Point(218, 138)
        Me.MensajeTextBox.Multiline = True
        Me.MensajeTextBox.Name = "MensajeTextBox"
        Me.MensajeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.MensajeTextBox.Size = New System.Drawing.Size(517, 153)
        Me.MensajeTextBox.TabIndex = 55
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(263, 295)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ActivoCheckBox.TabIndex = 57
        '
        'Button16
        '
        Me.Button16.BackColor = System.Drawing.SystemColors.Control
        Me.Button16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button16.ForeColor = System.Drawing.Color.Black
        Me.Button16.Location = New System.Drawing.Point(694, 543)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(136, 36)
        Me.Button16.TabIndex = 54
        Me.Button16.Text = "&GUARDAR"
        Me.Button16.UseVisualStyleBackColor = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Black
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(55, 32)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(163, 20)
        Me.Label37.TabIndex = 52
        Me.Label37.Text = "Mensaje en Tickets"
        '
        'ConGeneralMsjTicketsBindingSource
        '
        Me.ConGeneralMsjTicketsBindingSource.DataMember = "ConGeneralMsjTickets"
        '
        'Consulta_cobrodepositobuenoBindingSource
        '
        Me.Consulta_cobrodepositobuenoBindingSource.DataMember = "Consulta_cobrodepositobueno"
        Me.Consulta_cobrodepositobuenoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Consulta_Rel_PaquetesDigTelBindingSource
        '
        Me.Consulta_Rel_PaquetesDigTelBindingSource.DataMember = "Consulta_Rel_PaquetesDigTel"
        Me.Consulta_Rel_PaquetesDigTelBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConGeneralAlertaTableAdapter
        '
        Me.ConGeneralAlertaTableAdapter.ClearBeforeFill = True
        '
        'NueGeneralAlertaBindingSource
        '
        Me.NueGeneralAlertaBindingSource.DataMember = "NueGeneralAlerta"
        Me.NueGeneralAlertaBindingSource.DataSource = Me.DataSetLidia2
        '
        'NueGeneralAlertaTableAdapter
        '
        Me.NueGeneralAlertaTableAdapter.ClearBeforeFill = True
        '
        'Dame_Direcciones_IpBindingSource
        '
        Me.Dame_Direcciones_IpBindingSource.DataMember = "Dame_Direcciones_Ip"
        Me.Dame_Direcciones_IpBindingSource.DataSource = Me.DataSetLidia2
        '
        'Dame_Direcciones_IpTableAdapter
        '
        Me.Dame_Direcciones_IpTableAdapter.ClearBeforeFill = True
        '
        'Consulta_cobrodepositoTableAdapter
        '
        Me.Consulta_cobrodepositoTableAdapter.ClearBeforeFill = True
        '
        'Consulta_cobrodepositobuenoTableAdapter
        '
        Me.Consulta_cobrodepositobuenoTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Rel_PaquetesDigTelTableAdapter
        '
        Me.Consulta_Rel_PaquetesDigTelTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_XmlTableAdapter
        '
        Me.CONSULTA_General_XmlTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_SantanderTableAdapter
        '
        Me.CONSULTA_General_SantanderTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_HsbcTableAdapter
        '
        Me.CONSULTA_General_HsbcTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ciudadTableAdapter1
        '
        Me.Muestra_ciudadTableAdapter1.ClearBeforeFill = True
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.Panel8)
        Me.TabPage8.Location = New System.Drawing.Point(4, 64)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(865, 592)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = "Alertas"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.Panel7)
        Me.TabPage7.Location = New System.Drawing.Point(4, 64)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(865, 592)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Mensaje en Tickets"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Panel6)
        Me.TabPage6.Location = New System.Drawing.Point(4, 64)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(865, 592)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Facturas Globales"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Panel4)
        Me.TabPage5.Location = New System.Drawing.Point(4, 64)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(865, 592)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Correo"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Panel3)
        Me.TabPage4.Location = New System.Drawing.Point(4, 64)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(865, 592)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Plazo Forzoso"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Panel1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 64)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(865, 592)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Periodo de Corte"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.PanelBasico)
        Me.TabPage2.Location = New System.Drawing.Point(4, 64)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(865, 592)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Servicios de Televisión"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.PanelDatos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 64)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(865, 592)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos de la Empresa"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage15)
        Me.TabControl1.Controls.Add(Me.TabPage16)
        Me.TabControl1.Controls.Add(Me.TabPage17)
        Me.TabControl1.Controls.Add(Me.TabPage18)
        Me.TabControl1.Controls.Add(Me.tpRecontratacion)
        Me.TabControl1.Controls.Add(Me.tpEstadosCuenta)
        Me.TabControl1.Controls.Add(Me.tbPuntos)
        Me.TabControl1.Controls.Add(Me.tpProrroga)
        Me.TabControl1.Controls.Add(Me.TabOxxo)
        Me.TabControl1.Controls.Add(Me.TpCobroMaterial)
        Me.TabControl1.Controls.Add(Me.TpBonificaciones)
        Me.TabControl1.Controls.Add(Me.TbResumenEjec)
        Me.TabControl1.Controls.Add(Me.TbPagosContrataDig)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(873, 660)
        Me.TabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
        Me.TabControl1.TabIndex = 72
        '
        'TabPage15
        '
        Me.TabPage15.AutoScroll = True
        Me.TabPage15.Controls.Add(Me.PnQuejas)
        Me.TabPage15.Location = New System.Drawing.Point(4, 64)
        Me.TabPage15.Name = "TabPage15"
        Me.TabPage15.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage15.Size = New System.Drawing.Size(865, 592)
        Me.TabPage15.TabIndex = 14
        Me.TabPage15.Text = "Órdenes y Quejas"
        Me.TabPage15.UseVisualStyleBackColor = True
        '
        'PnQuejas
        '
        Me.PnQuejas.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnQuejas.Controls.Add(Me.Label59)
        Me.PnQuejas.Controls.Add(Me.Button10)
        Me.PnQuejas.Controls.Add(Me.Button9)
        Me.PnQuejas.Controls.Add(Me.TreeViewOrdenes)
        Me.PnQuejas.Controls.Add(Me.LblNotas)
        Me.PnQuejas.Controls.Add(Me.LblFamilia)
        Me.PnQuejas.Controls.Add(Me.LblTecnico)
        Me.PnQuejas.Controls.Add(Me.Label57)
        Me.PnQuejas.Controls.Add(Me.TreViewTecnicos)
        Me.PnQuejas.Controls.Add(Me.CmbTecnicos)
        Me.PnQuejas.Controls.Add(Me.CmBDepto)
        Me.PnQuejas.Controls.Add(Me.Button4)
        Me.PnQuejas.Controls.Add(Me.Button2)
        Me.PnQuejas.Location = New System.Drawing.Point(6, 6)
        Me.PnQuejas.Name = "PnQuejas"
        Me.PnQuejas.Size = New System.Drawing.Size(850, 581)
        Me.PnQuejas.TabIndex = 0
        '
        'Label59
        '
        Me.Label59.BackColor = System.Drawing.Color.Black
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.White
        Me.Label59.Location = New System.Drawing.Point(25, 120)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(304, 23)
        Me.Label59.TabIndex = 66
        Me.Label59.Text = "Técnicos a mostrar en Órdenes"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(335, 184)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(76, 32)
        Me.Button10.TabIndex = 65
        Me.Button10.Text = "&Eliminar"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(335, 146)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(76, 32)
        Me.Button9.TabIndex = 64
        Me.Button9.Text = "&Agregar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'TreeViewOrdenes
        '
        Me.TreeViewOrdenes.Location = New System.Drawing.Point(25, 146)
        Me.TreeViewOrdenes.Name = "TreeViewOrdenes"
        Me.TreeViewOrdenes.Size = New System.Drawing.Size(304, 365)
        Me.TreeViewOrdenes.TabIndex = 63
        '
        'LblNotas
        '
        Me.LblNotas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNotas.ForeColor = System.Drawing.Color.Red
        Me.LblNotas.Location = New System.Drawing.Point(300, 525)
        Me.LblNotas.Name = "LblNotas"
        Me.LblNotas.Size = New System.Drawing.Size(296, 50)
        Me.LblNotas.TabIndex = 62
        Me.LblNotas.Text = "Nota: Si requiere agregar TODOS los Técnicos de un Departamento, NO seleccione ni" & _
    "ngún Técnico."
        Me.LblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblFamilia
        '
        Me.LblFamilia.AutoSize = True
        Me.LblFamilia.Location = New System.Drawing.Point(196, 37)
        Me.LblFamilia.Name = "LblFamilia"
        Me.LblFamilia.Size = New System.Drawing.Size(98, 15)
        Me.LblFamilia.TabIndex = 61
        Me.LblFamilia.Text = "Departamento"
        '
        'LblTecnico
        '
        Me.LblTecnico.AutoSize = True
        Me.LblTecnico.Location = New System.Drawing.Point(237, 66)
        Me.LblTecnico.Name = "LblTecnico"
        Me.LblTecnico.Size = New System.Drawing.Size(57, 15)
        Me.LblTecnico.TabIndex = 60
        Me.LblTecnico.Text = "Técnico"
        '
        'Label57
        '
        Me.Label57.BackColor = System.Drawing.Color.Black
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.White
        Me.Label57.Location = New System.Drawing.Point(449, 120)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(304, 23)
        Me.Label57.TabIndex = 59
        Me.Label57.Text = "Técnicos a mostrar en Quejas"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TreViewTecnicos
        '
        Me.TreViewTecnicos.Location = New System.Drawing.Point(449, 146)
        Me.TreViewTecnicos.Name = "TreViewTecnicos"
        Me.TreViewTecnicos.Size = New System.Drawing.Size(304, 365)
        Me.TreViewTecnicos.TabIndex = 58
        '
        'CmbTecnicos
        '
        Me.CmbTecnicos.DataSource = Me.MuestraTecnicosByFamiliBindingSource
        Me.CmbTecnicos.DisplayMember = "Tecnico"
        Me.CmbTecnicos.FormattingEnabled = True
        Me.CmbTecnicos.Location = New System.Drawing.Point(300, 58)
        Me.CmbTecnicos.Name = "CmbTecnicos"
        Me.CmbTecnicos.Size = New System.Drawing.Size(296, 23)
        Me.CmbTecnicos.TabIndex = 57
        Me.CmbTecnicos.ValueMember = "Clv_tecnico"
        '
        'MuestraTecnicosByFamiliBindingSource
        '
        Me.MuestraTecnicosByFamiliBindingSource.DataMember = "Muestra_TecnicosByFamili"
        Me.MuestraTecnicosByFamiliBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'CmBDepto
        '
        Me.CmBDepto.DataSource = Me.MuestratecnicosDepartamentosAlmacenBindingSource
        Me.CmBDepto.DisplayMember = "Puesto"
        Me.CmBDepto.FormattingEnabled = True
        Me.CmBDepto.Location = New System.Drawing.Point(300, 29)
        Me.CmBDepto.Name = "CmBDepto"
        Me.CmBDepto.Size = New System.Drawing.Size(296, 23)
        Me.CmBDepto.TabIndex = 56
        Me.CmBDepto.ValueMember = "Clv_Puesto"
        '
        'MuestratecnicosDepartamentosAlmacenBindingSource
        '
        Me.MuestratecnicosDepartamentosAlmacenBindingSource.DataMember = "Muestra_tecnicosDepartamentos_Almacen"
        Me.MuestratecnicosDepartamentosAlmacenBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(759, 184)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(76, 32)
        Me.Button4.TabIndex = 55
        Me.Button4.Text = "&Eliminar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(759, 146)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(76, 32)
        Me.Button2.TabIndex = 54
        Me.Button2.Text = "&Agregar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TabPage16
        '
        Me.TabPage16.Controls.Add(Me.LabelNota)
        Me.TabPage16.Controls.Add(Me.Label58)
        Me.TabPage16.Controls.Add(Me.Button7)
        Me.TabPage16.Controls.Add(Me.DataGridView1)
        Me.TabPage16.Location = New System.Drawing.Point(4, 64)
        Me.TabPage16.Name = "TabPage16"
        Me.TabPage16.Size = New System.Drawing.Size(865, 592)
        Me.TabPage16.TabIndex = 15
        Me.TabPage16.Text = "Cartera Ejecutiva"
        Me.TabPage16.UseVisualStyleBackColor = True
        '
        'LabelNota
        '
        Me.LabelNota.ForeColor = System.Drawing.Color.Red
        Me.LabelNota.Location = New System.Drawing.Point(226, 399)
        Me.LabelNota.Name = "LabelNota"
        Me.LabelNota.Size = New System.Drawing.Size(452, 22)
        Me.LabelNota.TabIndex = 55
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.BackColor = System.Drawing.Color.Black
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.White
        Me.Label58.Location = New System.Drawing.Point(31, 21)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(260, 20)
        Me.Label58.TabIndex = 54
        Me.Label58.Text = "Generales de Cartera Ejecutiva"
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.SystemColors.Control
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(694, 543)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 36)
        Me.Button7.TabIndex = 18
        Me.Button7.Text = "&GUARDAR"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Grupo, Me.Grupo, Me.Porcentaje, Me.Activo})
        Me.DataGridView1.Location = New System.Drawing.Point(226, 119)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(452, 268)
        Me.DataGridView1.TabIndex = 0
        '
        'Clv_Grupo
        '
        Me.Clv_Grupo.DataPropertyName = "Clv_Grupo"
        Me.Clv_Grupo.HeaderText = "Clv_Grupo"
        Me.Clv_Grupo.Name = "Clv_Grupo"
        Me.Clv_Grupo.Visible = False
        '
        'Grupo
        '
        Me.Grupo.DataPropertyName = "Grupo"
        Me.Grupo.HeaderText = "Grupo"
        Me.Grupo.Name = "Grupo"
        Me.Grupo.Width = 150
        '
        'Porcentaje
        '
        Me.Porcentaje.DataPropertyName = "Porcentaje"
        Me.Porcentaje.HeaderText = "Porcentaje (%)"
        Me.Porcentaje.Name = "Porcentaje"
        Me.Porcentaje.Width = 150
        '
        'Activo
        '
        Me.Activo.DataPropertyName = "Activo"
        Me.Activo.HeaderText = "Activo"
        Me.Activo.Name = "Activo"
        '
        'TabPage17
        '
        Me.TabPage17.Controls.Add(Me.Button13)
        Me.TabPage17.Controls.Add(Me.Label64)
        Me.TabPage17.Controls.Add(Me.DDescoSusp)
        Me.TabPage17.Controls.Add(Me.DContrata)
        Me.TabPage17.Controls.Add(Label61)
        Me.TabPage17.Controls.Add(Label63)
        Me.TabPage17.Location = New System.Drawing.Point(4, 64)
        Me.TabPage17.Name = "TabPage17"
        Me.TabPage17.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage17.Size = New System.Drawing.Size(865, 592)
        Me.TabPage17.TabIndex = 17
        Me.TabPage17.Text = "Proporcionales"
        Me.TabPage17.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.SystemColors.Control
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.Black
        Me.Button13.Location = New System.Drawing.Point(679, 512)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(136, 36)
        Me.Button13.TabIndex = 27
        Me.Button13.Text = "&GUARDAR"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.BackColor = System.Drawing.Color.Black
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(56, 79)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(183, 20)
        Me.Label64.TabIndex = 26
        Me.Label64.Text = "Pagos Proporcionales"
        '
        'DDescoSusp
        '
        Me.DDescoSusp.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosAnt1", True))
        Me.DDescoSusp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DDescoSusp.Location = New System.Drawing.Point(489, 336)
        Me.DDescoSusp.MaxLength = 3
        Me.DDescoSusp.Name = "DDescoSusp"
        Me.DDescoSusp.Size = New System.Drawing.Size(100, 21)
        Me.DDescoSusp.TabIndex = 23
        '
        'DContrata
        '
        Me.DContrata.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosPP1", True))
        Me.DContrata.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DContrata.Location = New System.Drawing.Point(489, 258)
        Me.DContrata.MaxLength = 3
        Me.DContrata.Name = "DContrata"
        Me.DContrata.Size = New System.Drawing.Size(100, 21)
        Me.DContrata.TabIndex = 22
        '
        'TabPage18
        '
        Me.TabPage18.Controls.Add(Me.TextIEPS)
        Me.TabPage18.Controls.Add(Me.TextIva)
        Me.TabPage18.Controls.Add(Me.Label68)
        Me.TabPage18.Controls.Add(Me.LblPorIeps)
        Me.TabPage18.Controls.Add(Me.NumericUpDownIeps)
        Me.TabPage18.Controls.Add(Me.NumericUpDownIva)
        Me.TabPage18.Controls.Add(Me.Button14)
        Me.TabPage18.Controls.Add(Me.ChkCalculo1)
        Me.TabPage18.Controls.Add(Me.LabelIEPS)
        Me.TabPage18.Controls.Add(Me.CtaIepsText)
        Me.TabPage18.Controls.Add(Me.LabelCtaIEPS)
        Me.TabPage18.Controls.Add(Me.CheckIEPS)
        Me.TabPage18.Controls.Add(Label69)
        Me.TabPage18.Controls.Add(Me.Label67)
        Me.TabPage18.Location = New System.Drawing.Point(4, 64)
        Me.TabPage18.Name = "TabPage18"
        Me.TabPage18.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage18.Size = New System.Drawing.Size(865, 592)
        Me.TabPage18.TabIndex = 18
        Me.TabPage18.Text = "Impuestos"
        Me.TabPage18.UseVisualStyleBackColor = True
        '
        'TextIEPS
        '
        Me.TextIEPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextIEPS.Enabled = False
        Me.TextIEPS.Location = New System.Drawing.Point(383, 233)
        Me.TextIEPS.Name = "TextIEPS"
        Me.TextIEPS.Size = New System.Drawing.Size(47, 21)
        Me.TextIEPS.TabIndex = 65
        Me.TextIEPS.Visible = False
        '
        'TextIva
        '
        Me.TextIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextIva.Enabled = False
        Me.TextIva.Location = New System.Drawing.Point(383, 105)
        Me.TextIva.Name = "TextIva"
        Me.TextIva.Size = New System.Drawing.Size(47, 21)
        Me.TextIva.TabIndex = 64
        Me.TextIva.Visible = False
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(358, 107)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(19, 15)
        Me.Label68.TabIndex = 63
        Me.Label68.Text = "%"
        '
        'LblPorIeps
        '
        Me.LblPorIeps.AutoSize = True
        Me.LblPorIeps.Location = New System.Drawing.Point(358, 235)
        Me.LblPorIeps.Name = "LblPorIeps"
        Me.LblPorIeps.Size = New System.Drawing.Size(19, 15)
        Me.LblPorIeps.TabIndex = 62
        Me.LblPorIeps.Text = "%"
        Me.LblPorIeps.Visible = False
        '
        'NumericUpDownIeps
        '
        Me.NumericUpDownIeps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDownIeps.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDownIeps.Location = New System.Drawing.Point(306, 233)
        Me.NumericUpDownIeps.Name = "NumericUpDownIeps"
        Me.NumericUpDownIeps.Size = New System.Drawing.Size(46, 22)
        Me.NumericUpDownIeps.TabIndex = 61
        Me.NumericUpDownIeps.Visible = False
        '
        'NumericUpDownIva
        '
        Me.NumericUpDownIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDownIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDownIva.Location = New System.Drawing.Point(306, 105)
        Me.NumericUpDownIva.Name = "NumericUpDownIva"
        Me.NumericUpDownIva.Size = New System.Drawing.Size(46, 22)
        Me.NumericUpDownIva.TabIndex = 60
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.SystemColors.Control
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.Black
        Me.Button14.Location = New System.Drawing.Point(383, 386)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(136, 36)
        Me.Button14.TabIndex = 59
        Me.Button14.Text = "&GUARDAR"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'ChkCalculo1
        '
        Me.ChkCalculo1.AutoSize = True
        Me.ChkCalculo1.Location = New System.Drawing.Point(306, 188)
        Me.ChkCalculo1.Name = "ChkCalculo1"
        Me.ChkCalculo1.Size = New System.Drawing.Size(369, 19)
        Me.ChkCalculo1.TabIndex = 58
        Me.ChkCalculo1.Text = "Calcular Iva Incluyendo Impuesto Taza de Regulación"
        Me.ChkCalculo1.UseVisualStyleBackColor = True
        Me.ChkCalculo1.Visible = False
        '
        'LabelIEPS
        '
        Me.LabelIEPS.AutoSize = True
        Me.LabelIEPS.Location = New System.Drawing.Point(94, 235)
        Me.LabelIEPS.Name = "LabelIEPS"
        Me.LabelIEPS.Size = New System.Drawing.Size(206, 15)
        Me.LabelIEPS.TabIndex = 57
        Me.LabelIEPS.Text = "Impuesto Taza de Regulación :"
        '
        'CtaIepsText
        '
        Me.CtaIepsText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CtaIepsText.Location = New System.Drawing.Point(306, 277)
        Me.CtaIepsText.Name = "CtaIepsText"
        Me.CtaIepsText.Size = New System.Drawing.Size(136, 21)
        Me.CtaIepsText.TabIndex = 56
        Me.CtaIepsText.Visible = False
        '
        'LabelCtaIEPS
        '
        Me.LabelCtaIEPS.AutoSize = True
        Me.LabelCtaIEPS.Location = New System.Drawing.Point(65, 279)
        Me.LabelCtaIEPS.Name = "LabelCtaIEPS"
        Me.LabelCtaIEPS.Size = New System.Drawing.Size(235, 15)
        Me.LabelCtaIEPS.TabIndex = 55
        Me.LabelCtaIEPS.Text = "Cta. Impuesto Taza de Regulación :"
        Me.LabelCtaIEPS.Visible = False
        '
        'CheckIEPS
        '
        Me.CheckIEPS.AutoSize = True
        Me.CheckIEPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckIEPS.ForeColor = System.Drawing.Color.Red
        Me.CheckIEPS.Location = New System.Drawing.Point(306, 152)
        Me.CheckIEPS.Name = "CheckIEPS"
        Me.CheckIEPS.Size = New System.Drawing.Size(297, 20)
        Me.CheckIEPS.TabIndex = 54
        Me.CheckIEPS.Text = "Habilitar Impuesto Taza de Regulación"
        Me.CheckIEPS.UseVisualStyleBackColor = True
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.BackColor = System.Drawing.Color.Black
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.ForeColor = System.Drawing.SystemColors.Window
        Me.Label67.Location = New System.Drawing.Point(133, 47)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(115, 18)
        Me.Label67.TabIndex = 0
        Me.Label67.Text = "IMPUESTOS: "
        '
        'tpRecontratacion
        '
        Me.tpRecontratacion.Controls.Add(Me.Label66)
        Me.tpRecontratacion.Controls.Add(Me.btnAceptarRecon)
        Me.tpRecontratacion.Controls.Add(Me.Label70)
        Me.tpRecontratacion.Controls.Add(Me.cbAplica)
        Me.tpRecontratacion.Controls.Add(Me.txtMeses)
        Me.tpRecontratacion.Controls.Add(Me.Label65)
        Me.tpRecontratacion.Location = New System.Drawing.Point(4, 64)
        Me.tpRecontratacion.Name = "tpRecontratacion"
        Me.tpRecontratacion.Size = New System.Drawing.Size(865, 592)
        Me.tpRecontratacion.TabIndex = 19
        Me.tpRecontratacion.Text = "Recontratación"
        Me.tpRecontratacion.UseVisualStyleBackColor = True
        '
        'Label66
        '
        Me.Label66.Location = New System.Drawing.Point(454, 163)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(70, 18)
        Me.Label66.TabIndex = 62
        Me.Label66.Text = "Meses"
        '
        'btnAceptarRecon
        '
        Me.btnAceptarRecon.BackColor = System.Drawing.SystemColors.Control
        Me.btnAceptarRecon.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptarRecon.ForeColor = System.Drawing.Color.Black
        Me.btnAceptarRecon.Location = New System.Drawing.Point(350, 287)
        Me.btnAceptarRecon.Name = "btnAceptarRecon"
        Me.btnAceptarRecon.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptarRecon.TabIndex = 61
        Me.btnAceptarRecon.Text = "&GUARDAR"
        Me.btnAceptarRecon.UseVisualStyleBackColor = False
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.BackColor = System.Drawing.Color.Black
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.ForeColor = System.Drawing.SystemColors.Window
        Me.Label70.Location = New System.Drawing.Point(41, 37)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(193, 18)
        Me.Label70.TabIndex = 60
        Me.Label70.Text = "RECONTRATACIONES: "
        '
        'cbAplica
        '
        Me.cbAplica.AutoSize = True
        Me.cbAplica.Location = New System.Drawing.Point(556, 163)
        Me.cbAplica.Name = "cbAplica"
        Me.cbAplica.Size = New System.Drawing.Size(151, 19)
        Me.cbAplica.TabIndex = 3
        Me.cbAplica.Text = "Aplica Proporcional"
        Me.cbAplica.UseVisualStyleBackColor = True
        Me.cbAplica.Visible = False
        '
        'txtMeses
        '
        Me.txtMeses.Location = New System.Drawing.Point(350, 163)
        Me.txtMeses.Name = "txtMeses"
        Me.txtMeses.Size = New System.Drawing.Size(98, 21)
        Me.txtMeses.TabIndex = 2
        '
        'Label65
        '
        Me.Label65.Location = New System.Drawing.Point(239, 108)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(383, 52)
        Me.Label65.TabIndex = 0
        Me.Label65.Text = "A partir de la última fecha de Baja, número de meses que deben de pasar para que " & _
    "se tome como Contratación:"
        '
        'tpEstadosCuenta
        '
        Me.tpEstadosCuenta.Controls.Add(Me.gbMensaje)
        Me.tpEstadosCuenta.Controls.Add(Me.Label79)
        Me.tpEstadosCuenta.Controls.Add(Me.Label78)
        Me.tpEstadosCuenta.Controls.Add(Me.Label80)
        Me.tpEstadosCuenta.Controls.Add(Me.btnGuardar)
        Me.tpEstadosCuenta.Controls.Add(Me.txtDiaEdoCuenta)
        Me.tpEstadosCuenta.Controls.Add(Me.cbPeriodo)
        Me.tpEstadosCuenta.Location = New System.Drawing.Point(4, 64)
        Me.tpEstadosCuenta.Name = "tpEstadosCuenta"
        Me.tpEstadosCuenta.Size = New System.Drawing.Size(865, 592)
        Me.tpEstadosCuenta.TabIndex = 21
        Me.tpEstadosCuenta.Text = "Estados de Cuenta"
        Me.tpEstadosCuenta.UseVisualStyleBackColor = True
        '
        'gbMensaje
        '
        Me.gbMensaje.Controls.Add(Me.Label88)
        Me.gbMensaje.Controls.Add(Me.Label87)
        Me.gbMensaje.Controls.Add(Me.cbDeco)
        Me.gbMensaje.Controls.Add(Me.cbCorreo)
        Me.gbMensaje.Controls.Add(Me.dtHora)
        Me.gbMensaje.Controls.Add(Me.Label86)
        Me.gbMensaje.Controls.Add(Me.tbMensajeDeco)
        Me.gbMensaje.Controls.Add(Me.Label85)
        Me.gbMensaje.Controls.Add(Me.Label84)
        Me.gbMensaje.Controls.Add(Me.Label83)
        Me.gbMensaje.Controls.Add(Me.tbMensaje)
        Me.gbMensaje.Controls.Add(Me.Label82)
        Me.gbMensaje.Controls.Add(Me.tbAsunto)
        Me.gbMensaje.Controls.Add(Me.Label81)
        Me.gbMensaje.Location = New System.Drawing.Point(19, 168)
        Me.gbMensaje.Name = "gbMensaje"
        Me.gbMensaje.Size = New System.Drawing.Size(830, 344)
        Me.gbMensaje.TabIndex = 42
        Me.gbMensaje.TabStop = False
        Me.gbMensaje.Text = "Mensajes"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(30, 70)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(52, 15)
        Me.Label88.TabIndex = 52
        Me.Label88.Text = "Activo :"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Location = New System.Drawing.Point(441, 70)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(52, 15)
        Me.Label87.TabIndex = 51
        Me.Label87.Text = "Activo :"
        '
        'cbDeco
        '
        Me.cbDeco.AutoSize = True
        Me.cbDeco.Location = New System.Drawing.Point(499, 70)
        Me.cbDeco.Name = "cbDeco"
        Me.cbDeco.Size = New System.Drawing.Size(15, 14)
        Me.cbDeco.TabIndex = 50
        Me.cbDeco.UseVisualStyleBackColor = True
        '
        'cbCorreo
        '
        Me.cbCorreo.AutoSize = True
        Me.cbCorreo.Location = New System.Drawing.Point(88, 70)
        Me.cbCorreo.Name = "cbCorreo"
        Me.cbCorreo.Size = New System.Drawing.Size(15, 14)
        Me.cbCorreo.TabIndex = 49
        Me.cbCorreo.UseVisualStyleBackColor = True
        '
        'dtHora
        '
        Me.dtHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtHora.Location = New System.Drawing.Point(499, 305)
        Me.dtHora.MaxDate = New Date(1900, 1, 1, 22, 0, 0, 0)
        Me.dtHora.MinDate = New Date(1900, 1, 1, 7, 0, 0, 0)
        Me.dtHora.Name = "dtHora"
        Me.dtHora.ShowUpDown = True
        Me.dtHora.Size = New System.Drawing.Size(118, 21)
        Me.dtHora.TabIndex = 48
        Me.dtHora.Value = New Date(1900, 1, 1, 7, 0, 0, 0)
        '
        'Label86
        '
        Me.Label86.Location = New System.Drawing.Point(420, 305)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(73, 21)
        Me.Label86.TabIndex = 47
        Me.Label86.Text = "Hora :"
        Me.Label86.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'tbMensajeDeco
        '
        Me.tbMensajeDeco.Location = New System.Drawing.Point(499, 105)
        Me.tbMensajeDeco.MaxLength = 1000
        Me.tbMensajeDeco.Multiline = True
        Me.tbMensajeDeco.Name = "tbMensajeDeco"
        Me.tbMensajeDeco.Size = New System.Drawing.Size(315, 186)
        Me.tbMensajeDeco.TabIndex = 46
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(423, 105)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(70, 15)
        Me.Label85.TabIndex = 45
        Me.Label85.Text = "Mensaje :"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.BackColor = System.Drawing.Color.Black
        Me.Label84.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label84.ForeColor = System.Drawing.Color.White
        Me.Label84.Location = New System.Drawing.Point(492, 17)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(139, 20)
        Me.Label84.TabIndex = 44
        Me.Label84.Text = "Decodificadores"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.BackColor = System.Drawing.Color.Black
        Me.Label83.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label83.ForeColor = System.Drawing.Color.White
        Me.Label83.Location = New System.Drawing.Point(83, 17)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(63, 20)
        Me.Label83.TabIndex = 43
        Me.Label83.Text = "Correo"
        '
        'tbMensaje
        '
        Me.tbMensaje.Location = New System.Drawing.Point(87, 126)
        Me.tbMensaje.MaxLength = 1000
        Me.tbMensaje.Multiline = True
        Me.tbMensaje.Name = "tbMensaje"
        Me.tbMensaje.Size = New System.Drawing.Size(315, 200)
        Me.tbMensaje.TabIndex = 3
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(12, 126)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(70, 15)
        Me.Label82.TabIndex = 2
        Me.Label82.Text = "Mensaje :"
        '
        'tbAsunto
        '
        Me.tbAsunto.Location = New System.Drawing.Point(87, 99)
        Me.tbAsunto.MaxLength = 150
        Me.tbAsunto.Name = "tbAsunto"
        Me.tbAsunto.Size = New System.Drawing.Size(315, 21)
        Me.tbAsunto.TabIndex = 1
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(24, 102)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(58, 15)
        Me.Label81.TabIndex = 0
        Me.Label81.Text = "Asunto :"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(279, 126)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(37, 15)
        Me.Label79.TabIndex = 41
        Me.Label79.Text = "Día :"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(251, 99)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(65, 15)
        Me.Label78.TabIndex = 40
        Me.Label78.Text = "Periodo :"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.BackColor = System.Drawing.Color.Black
        Me.Label80.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.ForeColor = System.Drawing.Color.White
        Me.Label80.Location = New System.Drawing.Point(85, 40)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(320, 20)
        Me.Label80.TabIndex = 39
        Me.Label80.Text = "Días de emisión de Estados de Cuenta"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(713, 521)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 38
        Me.btnGuardar.Text = "&GUARDAR"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'txtDiaEdoCuenta
        '
        Me.txtDiaEdoCuenta.Location = New System.Drawing.Point(322, 120)
        Me.txtDiaEdoCuenta.Name = "txtDiaEdoCuenta"
        Me.txtDiaEdoCuenta.Size = New System.Drawing.Size(100, 21)
        Me.txtDiaEdoCuenta.TabIndex = 37
        '
        'cbPeriodo
        '
        Me.cbPeriodo.DisplayMember = "Descripcion"
        Me.cbPeriodo.FormattingEnabled = True
        Me.cbPeriodo.Location = New System.Drawing.Point(322, 91)
        Me.cbPeriodo.Name = "cbPeriodo"
        Me.cbPeriodo.Size = New System.Drawing.Size(303, 23)
        Me.cbPeriodo.TabIndex = 0
        Me.cbPeriodo.ValueMember = "Clv_Periodo"
        '
        'tbPuntos
        '
        Me.tbPuntos.Controls.Add(Me.Label123)
        Me.tbPuntos.Controls.Add(Me.tcPuntos)
        Me.tbPuntos.Location = New System.Drawing.Point(4, 64)
        Me.tbPuntos.Name = "tbPuntos"
        Me.tbPuntos.Size = New System.Drawing.Size(865, 592)
        Me.tbPuntos.TabIndex = 23
        Me.tbPuntos.Text = "Puntos de Antigüedad"
        Me.tbPuntos.UseVisualStyleBackColor = True
        '
        'Label123
        '
        Me.Label123.AutoSize = True
        Me.Label123.BackColor = System.Drawing.Color.Black
        Me.Label123.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label123.ForeColor = System.Drawing.Color.White
        Me.Label123.Location = New System.Drawing.Point(34, 18)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(187, 20)
        Me.Label123.TabIndex = 67
        Me.Label123.Text = "Puntos de Antigüedad"
        '
        'tcPuntos
        '
        Me.tcPuntos.Controls.Add(Me.tbPuntosRangos)
        Me.tcPuntos.Controls.Add(Me.tbPuntosServicios)
        Me.tcPuntos.Controls.Add(Me.tbPuntosServiciosAdic)
        Me.tcPuntos.Location = New System.Drawing.Point(38, 41)
        Me.tcPuntos.Name = "tcPuntos"
        Me.tcPuntos.SelectedIndex = 0
        Me.tcPuntos.Size = New System.Drawing.Size(769, 517)
        Me.tcPuntos.TabIndex = 0
        '
        'tbPuntosRangos
        '
        Me.tbPuntosRangos.Controls.Add(Me.bnEliminarRango)
        Me.tbPuntosRangos.Controls.Add(Me.bnAgregarRango)
        Me.tbPuntosRangos.Controls.Add(Me.Label109)
        Me.tbPuntosRangos.Controls.Add(Me.tbRangoFin)
        Me.tbPuntosRangos.Controls.Add(Me.dgRangos)
        Me.tbPuntosRangos.Controls.Add(Me.Label108)
        Me.tbPuntosRangos.Controls.Add(Me.tbRangoIni)
        Me.tbPuntosRangos.Location = New System.Drawing.Point(4, 24)
        Me.tbPuntosRangos.Name = "tbPuntosRangos"
        Me.tbPuntosRangos.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPuntosRangos.Size = New System.Drawing.Size(761, 489)
        Me.tbPuntosRangos.TabIndex = 0
        Me.tbPuntosRangos.Text = "Rangos"
        Me.tbPuntosRangos.UseVisualStyleBackColor = True
        '
        'bnEliminarRango
        '
        Me.bnEliminarRango.Location = New System.Drawing.Point(581, 152)
        Me.bnEliminarRango.Name = "bnEliminarRango"
        Me.bnEliminarRango.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminarRango.TabIndex = 6
        Me.bnEliminarRango.Text = "&Eliminar"
        Me.bnEliminarRango.UseVisualStyleBackColor = True
        '
        'bnAgregarRango
        '
        Me.bnAgregarRango.Location = New System.Drawing.Point(581, 108)
        Me.bnAgregarRango.Name = "bnAgregarRango"
        Me.bnAgregarRango.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregarRango.TabIndex = 5
        Me.bnAgregarRango.Text = "&Agregar"
        Me.bnAgregarRango.UseVisualStyleBackColor = True
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Location = New System.Drawing.Point(232, 116)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(54, 15)
        Me.Label109.TabIndex = 4
        Me.Label109.Text = "Al mes:"
        '
        'tbRangoFin
        '
        Me.tbRangoFin.Location = New System.Drawing.Point(292, 110)
        Me.tbRangoFin.Name = "tbRangoFin"
        Me.tbRangoFin.Size = New System.Drawing.Size(268, 21)
        Me.tbRangoFin.TabIndex = 3
        '
        'dgRangos
        '
        Me.dgRangos.AllowUserToAddRows = False
        Me.dgRangos.AllowUserToDeleteRows = False
        Me.dgRangos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgRangos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgRangos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdRango, Me.Rango})
        Me.dgRangos.Location = New System.Drawing.Point(239, 152)
        Me.dgRangos.Name = "dgRangos"
        Me.dgRangos.ReadOnly = True
        Me.dgRangos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgRangos.Size = New System.Drawing.Size(321, 255)
        Me.dgRangos.TabIndex = 2
        '
        'IdRango
        '
        Me.IdRango.DataPropertyName = "IdRango"
        Me.IdRango.HeaderText = "IdRango"
        Me.IdRango.Name = "IdRango"
        Me.IdRango.ReadOnly = True
        Me.IdRango.Visible = False
        '
        'Rango
        '
        Me.Rango.DataPropertyName = "Rango"
        Me.Rango.HeaderText = "Rangos"
        Me.Rango.Name = "Rango"
        Me.Rango.ReadOnly = True
        Me.Rango.Width = 250
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Location = New System.Drawing.Point(222, 87)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(64, 15)
        Me.Label108.TabIndex = 1
        Me.Label108.Text = "Del mes:"
        '
        'tbRangoIni
        '
        Me.tbRangoIni.Location = New System.Drawing.Point(292, 81)
        Me.tbRangoIni.Name = "tbRangoIni"
        Me.tbRangoIni.Size = New System.Drawing.Size(268, 21)
        Me.tbRangoIni.TabIndex = 0
        '
        'tbPuntosServicios
        '
        Me.tbPuntosServicios.Controls.Add(Me.Label110)
        Me.tbPuntosServicios.Controls.Add(Me.Label113)
        Me.tbPuntosServicios.Controls.Add(Me.Label111)
        Me.tbPuntosServicios.Controls.Add(Me.cbTipSerPuntosAnti)
        Me.tbPuntosServicios.Controls.Add(Me.cbServicioPuntosAnti)
        Me.tbPuntosServicios.Controls.Add(Me.bnEliminarPuntosServicio)
        Me.tbPuntosServicios.Controls.Add(Me.bnAgregarPuntosServicio)
        Me.tbPuntosServicios.Controls.Add(Me.Label112)
        Me.tbPuntosServicios.Controls.Add(Me.dgPuntosServicios)
        Me.tbPuntosServicios.Controls.Add(Me.tboxPuntosAnti)
        Me.tbPuntosServicios.Controls.Add(Me.cbRangosPuntosAnti)
        Me.tbPuntosServicios.Location = New System.Drawing.Point(4, 24)
        Me.tbPuntosServicios.Name = "tbPuntosServicios"
        Me.tbPuntosServicios.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPuntosServicios.Size = New System.Drawing.Size(761, 489)
        Me.tbPuntosServicios.TabIndex = 1
        Me.tbPuntosServicios.Text = "Servicios"
        Me.tbPuntosServicios.UseVisualStyleBackColor = True
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(165, 34)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(83, 15)
        Me.Label110.TabIndex = 21
        Me.Label110.Text = "Antigüedad:"
        '
        'Label113
        '
        Me.Label113.AutoSize = True
        Me.Label113.Location = New System.Drawing.Point(186, 92)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(62, 15)
        Me.Label113.TabIndex = 11
        Me.Label113.Text = "Servicio:"
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.Location = New System.Drawing.Point(134, 63)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(114, 15)
        Me.Label111.TabIndex = 10
        Me.Label111.Text = "Tipo de Servicio:"
        '
        'cbTipSerPuntosAnti
        '
        Me.cbTipSerPuntosAnti.DisplayMember = "Concepto"
        Me.cbTipSerPuntosAnti.FormattingEnabled = True
        Me.cbTipSerPuntosAnti.Location = New System.Drawing.Point(254, 55)
        Me.cbTipSerPuntosAnti.Name = "cbTipSerPuntosAnti"
        Me.cbTipSerPuntosAnti.Size = New System.Drawing.Size(310, 23)
        Me.cbTipSerPuntosAnti.TabIndex = 9
        Me.cbTipSerPuntosAnti.ValueMember = "Clv_TipSer"
        '
        'cbServicioPuntosAnti
        '
        Me.cbServicioPuntosAnti.DisplayMember = "Descripcion"
        Me.cbServicioPuntosAnti.FormattingEnabled = True
        Me.cbServicioPuntosAnti.Location = New System.Drawing.Point(254, 84)
        Me.cbServicioPuntosAnti.Name = "cbServicioPuntosAnti"
        Me.cbServicioPuntosAnti.Size = New System.Drawing.Size(310, 23)
        Me.cbServicioPuntosAnti.TabIndex = 8
        Me.cbServicioPuntosAnti.ValueMember = "Clv_Servicio"
        '
        'bnEliminarPuntosServicio
        '
        Me.bnEliminarPuntosServicio.Location = New System.Drawing.Point(592, 157)
        Me.bnEliminarPuntosServicio.Name = "bnEliminarPuntosServicio"
        Me.bnEliminarPuntosServicio.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminarPuntosServicio.TabIndex = 7
        Me.bnEliminarPuntosServicio.Text = "&Eliminar"
        Me.bnEliminarPuntosServicio.UseVisualStyleBackColor = True
        '
        'bnAgregarPuntosServicio
        '
        Me.bnAgregarPuntosServicio.Location = New System.Drawing.Point(592, 115)
        Me.bnAgregarPuntosServicio.Name = "bnAgregarPuntosServicio"
        Me.bnAgregarPuntosServicio.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregarPuntosServicio.TabIndex = 6
        Me.bnAgregarPuntosServicio.Text = "&Agregar"
        Me.bnAgregarPuntosServicio.UseVisualStyleBackColor = True
        '
        'Label112
        '
        Me.Label112.AutoSize = True
        Me.Label112.Location = New System.Drawing.Point(193, 119)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(55, 15)
        Me.Label112.TabIndex = 5
        Me.Label112.Text = "Puntos:"
        '
        'dgPuntosServicios
        '
        Me.dgPuntosServicios.AllowUserToAddRows = False
        Me.dgPuntosServicios.AllowUserToDeleteRows = False
        Me.dgPuntosServicios.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPuntosServicios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPuntosServicios.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdPuntos, Me.Servicio, Me.Puntos})
        Me.dgPuntosServicios.Location = New System.Drawing.Point(137, 157)
        Me.dgPuntosServicios.Name = "dgPuntosServicios"
        Me.dgPuntosServicios.ReadOnly = True
        Me.dgPuntosServicios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPuntosServicios.Size = New System.Drawing.Size(427, 282)
        Me.dgPuntosServicios.TabIndex = 4
        '
        'IdPuntos
        '
        Me.IdPuntos.DataPropertyName = "IdPuntos"
        Me.IdPuntos.HeaderText = "IdPuntos"
        Me.IdPuntos.Name = "IdPuntos"
        Me.IdPuntos.ReadOnly = True
        Me.IdPuntos.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        Me.Servicio.Width = 250
        '
        'Puntos
        '
        Me.Puntos.DataPropertyName = "Puntos"
        Me.Puntos.HeaderText = "Puntos"
        Me.Puntos.Name = "Puntos"
        Me.Puntos.ReadOnly = True
        '
        'tboxPuntosAnti
        '
        Me.tboxPuntosAnti.Location = New System.Drawing.Point(254, 113)
        Me.tboxPuntosAnti.Name = "tboxPuntosAnti"
        Me.tboxPuntosAnti.Size = New System.Drawing.Size(310, 21)
        Me.tboxPuntosAnti.TabIndex = 2
        '
        'cbRangosPuntosAnti
        '
        Me.cbRangosPuntosAnti.DisplayMember = "Rango"
        Me.cbRangosPuntosAnti.FormattingEnabled = True
        Me.cbRangosPuntosAnti.Location = New System.Drawing.Point(254, 26)
        Me.cbRangosPuntosAnti.Name = "cbRangosPuntosAnti"
        Me.cbRangosPuntosAnti.Size = New System.Drawing.Size(310, 23)
        Me.cbRangosPuntosAnti.TabIndex = 0
        Me.cbRangosPuntosAnti.ValueMember = "IdRango"
        '
        'tbPuntosServiciosAdic
        '
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label117)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label118)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.tbPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label114)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label115)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.cbTipSerPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.cbServicioPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.bnEliminarPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.bnAgregarPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label116)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.dgPagosAntiServAdic)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.tbPuntosAntiServiciosPagos)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.cbRangosPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Location = New System.Drawing.Point(4, 24)
        Me.tbPuntosServiciosAdic.Name = "tbPuntosServiciosAdic"
        Me.tbPuntosServiciosAdic.Size = New System.Drawing.Size(761, 489)
        Me.tbPuntosServiciosAdic.TabIndex = 2
        Me.tbPuntosServiciosAdic.Text = "Servicios Adicionales"
        Me.tbPuntosServiciosAdic.UseVisualStyleBackColor = True
        '
        'Label117
        '
        Me.Label117.AutoSize = True
        Me.Label117.Location = New System.Drawing.Point(215, 29)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(83, 15)
        Me.Label117.TabIndex = 25
        Me.Label117.Text = "Antigüedad:"
        '
        'Label118
        '
        Me.Label118.AutoSize = True
        Me.Label118.Location = New System.Drawing.Point(243, 141)
        Me.Label118.Name = "Label118"
        Me.Label118.Size = New System.Drawing.Size(55, 15)
        Me.Label118.TabIndex = 24
        Me.Label118.Text = "Puntos:"
        '
        'tbPuntosAntiServicios
        '
        Me.tbPuntosAntiServicios.Location = New System.Drawing.Point(304, 135)
        Me.tbPuntosAntiServicios.Name = "tbPuntosAntiServicios"
        Me.tbPuntosAntiServicios.Size = New System.Drawing.Size(310, 21)
        Me.tbPuntosAntiServicios.TabIndex = 23
        '
        'Label114
        '
        Me.Label114.AutoSize = True
        Me.Label114.Location = New System.Drawing.Point(236, 87)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(62, 15)
        Me.Label114.TabIndex = 22
        Me.Label114.Text = "Servicio:"
        '
        'Label115
        '
        Me.Label115.AutoSize = True
        Me.Label115.Location = New System.Drawing.Point(184, 58)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(114, 15)
        Me.Label115.TabIndex = 21
        Me.Label115.Text = "Tipo de Servicio:"
        '
        'cbTipSerPuntosAntiServicios
        '
        Me.cbTipSerPuntosAntiServicios.DisplayMember = "Concepto"
        Me.cbTipSerPuntosAntiServicios.FormattingEnabled = True
        Me.cbTipSerPuntosAntiServicios.Location = New System.Drawing.Point(304, 50)
        Me.cbTipSerPuntosAntiServicios.Name = "cbTipSerPuntosAntiServicios"
        Me.cbTipSerPuntosAntiServicios.Size = New System.Drawing.Size(310, 23)
        Me.cbTipSerPuntosAntiServicios.TabIndex = 20
        Me.cbTipSerPuntosAntiServicios.ValueMember = "Clv_TipSer"
        '
        'cbServicioPuntosAntiServicios
        '
        Me.cbServicioPuntosAntiServicios.DisplayMember = "Descripcion"
        Me.cbServicioPuntosAntiServicios.FormattingEnabled = True
        Me.cbServicioPuntosAntiServicios.Location = New System.Drawing.Point(304, 79)
        Me.cbServicioPuntosAntiServicios.Name = "cbServicioPuntosAntiServicios"
        Me.cbServicioPuntosAntiServicios.Size = New System.Drawing.Size(310, 23)
        Me.cbServicioPuntosAntiServicios.TabIndex = 19
        Me.cbServicioPuntosAntiServicios.ValueMember = "Clv_Servicio"
        '
        'bnEliminarPuntosAntiServicios
        '
        Me.bnEliminarPuntosAntiServicios.Location = New System.Drawing.Point(629, 175)
        Me.bnEliminarPuntosAntiServicios.Name = "bnEliminarPuntosAntiServicios"
        Me.bnEliminarPuntosAntiServicios.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminarPuntosAntiServicios.TabIndex = 18
        Me.bnEliminarPuntosAntiServicios.Text = "&Eliminar"
        Me.bnEliminarPuntosAntiServicios.UseVisualStyleBackColor = True
        '
        'bnAgregarPuntosAntiServicios
        '
        Me.bnAgregarPuntosAntiServicios.Location = New System.Drawing.Point(629, 133)
        Me.bnAgregarPuntosAntiServicios.Name = "bnAgregarPuntosAntiServicios"
        Me.bnAgregarPuntosAntiServicios.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregarPuntosAntiServicios.TabIndex = 17
        Me.bnAgregarPuntosAntiServicios.Text = "&Agregar"
        Me.bnAgregarPuntosAntiServicios.UseVisualStyleBackColor = True
        '
        'Label116
        '
        Me.Label116.AutoSize = True
        Me.Label116.Location = New System.Drawing.Point(179, 114)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(119, 15)
        Me.Label116.TabIndex = 16
        Me.Label116.Text = "Pagos Puntuales:"
        '
        'dgPagosAntiServAdic
        '
        Me.dgPagosAntiServAdic.AllowUserToAddRows = False
        Me.dgPagosAntiServAdic.AllowUserToDeleteRows = False
        Me.dgPagosAntiServAdic.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPagosAntiServAdic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPagosAntiServAdic.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.PagosPuntuales, Me.PuntosServicio})
        Me.dgPagosAntiServAdic.Location = New System.Drawing.Point(61, 175)
        Me.dgPagosAntiServAdic.Name = "dgPagosAntiServAdic"
        Me.dgPagosAntiServAdic.ReadOnly = True
        Me.dgPagosAntiServAdic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPagosAntiServAdic.Size = New System.Drawing.Size(553, 260)
        Me.dgPagosAntiServAdic.TabIndex = 15
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "IdPuntos"
        Me.DataGridViewTextBoxColumn4.HeaderText = "IdPuntos"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Servicio"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'PagosPuntuales
        '
        Me.PagosPuntuales.DataPropertyName = "PagosPuntuales"
        Me.PagosPuntuales.HeaderText = "Pagos Puntuales"
        Me.PagosPuntuales.Name = "PagosPuntuales"
        Me.PagosPuntuales.ReadOnly = True
        Me.PagosPuntuales.Width = 150
        '
        'PuntosServicio
        '
        Me.PuntosServicio.DataPropertyName = "Puntos"
        Me.PuntosServicio.HeaderText = "Puntos"
        Me.PuntosServicio.Name = "PuntosServicio"
        Me.PuntosServicio.ReadOnly = True
        '
        'tbPuntosAntiServiciosPagos
        '
        Me.tbPuntosAntiServiciosPagos.Location = New System.Drawing.Point(304, 108)
        Me.tbPuntosAntiServiciosPagos.Name = "tbPuntosAntiServiciosPagos"
        Me.tbPuntosAntiServiciosPagos.Size = New System.Drawing.Size(310, 21)
        Me.tbPuntosAntiServiciosPagos.TabIndex = 14
        '
        'cbRangosPuntosAntiServicios
        '
        Me.cbRangosPuntosAntiServicios.DisplayMember = "Rango"
        Me.cbRangosPuntosAntiServicios.FormattingEnabled = True
        Me.cbRangosPuntosAntiServicios.Location = New System.Drawing.Point(304, 21)
        Me.cbRangosPuntosAntiServicios.Name = "cbRangosPuntosAntiServicios"
        Me.cbRangosPuntosAntiServicios.Size = New System.Drawing.Size(310, 23)
        Me.cbRangosPuntosAntiServicios.TabIndex = 12
        Me.cbRangosPuntosAntiServicios.ValueMember = "IdRango"
        '
        'tpProrroga
        '
        Me.tpProrroga.Controls.Add(Me.Label10)
        Me.tpProrroga.Controls.Add(Me.Label9)
        Me.tpProrroga.Controls.Add(Me.Label8)
        Me.tpProrroga.Controls.Add(Me.bnGuardarProrroga)
        Me.tpProrroga.Controls.Add(Me.dgvUsuarios)
        Me.tpProrroga.Controls.Add(Me.dgvTipoUsuario)
        Me.tpProrroga.Location = New System.Drawing.Point(4, 64)
        Me.tpProrroga.Name = "tpProrroga"
        Me.tpProrroga.Size = New System.Drawing.Size(865, 592)
        Me.tpProrroga.TabIndex = 24
        Me.tpProrroga.Text = "Promesas de Pago"
        Me.tpProrroga.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Black
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(26, 53)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(382, 20)
        Me.Label10.TabIndex = 68
        Me.Label10.Text = "Alerta de Promesas de Pago a Desconectados"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(445, 115)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 15)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Usuarios"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 115)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(116, 15)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Tipo de Usuarios"
        '
        'bnGuardarProrroga
        '
        Me.bnGuardarProrroga.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardarProrroga.Location = New System.Drawing.Point(695, 535)
        Me.bnGuardarProrroga.Name = "bnGuardarProrroga"
        Me.bnGuardarProrroga.Size = New System.Drawing.Size(136, 36)
        Me.bnGuardarProrroga.TabIndex = 2
        Me.bnGuardarProrroga.Text = "&GUARDAR"
        Me.bnGuardarProrroga.UseVisualStyleBackColor = True
        '
        'dgvUsuarios
        '
        Me.dgvUsuarios.AllowUserToAddRows = False
        Me.dgvUsuarios.AllowUserToDeleteRows = False
        Me.dgvUsuarios.BackgroundColor = System.Drawing.Color.White
        Me.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsuarios.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClaveUsuarios, Me.NombreUsuarios, Me.ActivoUsuarios})
        Me.dgvUsuarios.Location = New System.Drawing.Point(448, 133)
        Me.dgvUsuarios.Name = "dgvUsuarios"
        Me.dgvUsuarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUsuarios.Size = New System.Drawing.Size(395, 371)
        Me.dgvUsuarios.TabIndex = 1
        '
        'ClaveUsuarios
        '
        Me.ClaveUsuarios.DataPropertyName = "Clave"
        Me.ClaveUsuarios.HeaderText = "ClaveUsuarios"
        Me.ClaveUsuarios.Name = "ClaveUsuarios"
        Me.ClaveUsuarios.Visible = False
        '
        'NombreUsuarios
        '
        Me.NombreUsuarios.DataPropertyName = "Nombre"
        Me.NombreUsuarios.HeaderText = "Nombre"
        Me.NombreUsuarios.Name = "NombreUsuarios"
        Me.NombreUsuarios.Width = 200
        '
        'ActivoUsuarios
        '
        Me.ActivoUsuarios.DataPropertyName = "Activo"
        Me.ActivoUsuarios.HeaderText = "Activo"
        Me.ActivoUsuarios.Name = "ActivoUsuarios"
        Me.ActivoUsuarios.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ActivoUsuarios.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvTipoUsuario
        '
        Me.dgvTipoUsuario.AllowUserToAddRows = False
        Me.dgvTipoUsuario.AllowUserToDeleteRows = False
        Me.dgvTipoUsuario.BackgroundColor = System.Drawing.Color.White
        Me.dgvTipoUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTipoUsuario.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_TipoUsuario, Me.DescripcionTipoUsuario, Me.ActivoTipoUsuario})
        Me.dgvTipoUsuario.Location = New System.Drawing.Point(18, 133)
        Me.dgvTipoUsuario.Name = "dgvTipoUsuario"
        Me.dgvTipoUsuario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTipoUsuario.Size = New System.Drawing.Size(395, 371)
        Me.dgvTipoUsuario.TabIndex = 0
        '
        'Clv_TipoUsuario
        '
        Me.Clv_TipoUsuario.DataPropertyName = "Clv_TipoUsuario"
        Me.Clv_TipoUsuario.HeaderText = "Clv_TipoUsuario"
        Me.Clv_TipoUsuario.Name = "Clv_TipoUsuario"
        Me.Clv_TipoUsuario.Visible = False
        '
        'DescripcionTipoUsuario
        '
        Me.DescripcionTipoUsuario.DataPropertyName = "Descripcion"
        Me.DescripcionTipoUsuario.HeaderText = "Tipo de Usuario"
        Me.DescripcionTipoUsuario.Name = "DescripcionTipoUsuario"
        Me.DescripcionTipoUsuario.Width = 200
        '
        'ActivoTipoUsuario
        '
        Me.ActivoTipoUsuario.DataPropertyName = "Activo"
        Me.ActivoTipoUsuario.HeaderText = "Activo"
        Me.ActivoTipoUsuario.Name = "ActivoTipoUsuario"
        '
        'TabOxxo
        '
        Me.TabOxxo.Controls.Add(Me.txtPrefijo)
        Me.TabOxxo.Controls.Add(Me.Label62)
        Me.TabOxxo.Controls.Add(Me.Button8)
        Me.TabOxxo.Controls.Add(Me.txtComisionOxxo)
        Me.TabOxxo.Controls.Add(Me.Label76)
        Me.TabOxxo.Location = New System.Drawing.Point(4, 64)
        Me.TabOxxo.Name = "TabOxxo"
        Me.TabOxxo.Padding = New System.Windows.Forms.Padding(3)
        Me.TabOxxo.Size = New System.Drawing.Size(865, 592)
        Me.TabOxxo.TabIndex = 25
        Me.TabOxxo.Text = "OXXO"
        Me.TabOxxo.UseVisualStyleBackColor = True
        '
        'txtPrefijo
        '
        Me.txtPrefijo.Location = New System.Drawing.Point(459, 158)
        Me.txtPrefijo.Name = "txtPrefijo"
        Me.txtPrefijo.Size = New System.Drawing.Size(100, 21)
        Me.txtPrefijo.TabIndex = 26
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(303, 164)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(150, 15)
        Me.Label62.TabIndex = 25
        Me.Label62.Text = "Prefijo del Proveedor :"
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.SystemColors.Control
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(723, 550)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 36)
        Me.Button8.TabIndex = 24
        Me.Button8.Text = "&GUARDAR"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'txtComisionOxxo
        '
        Me.txtComisionOxxo.Location = New System.Drawing.Point(459, 201)
        Me.txtComisionOxxo.Name = "txtComisionOxxo"
        Me.txtComisionOxxo.Size = New System.Drawing.Size(100, 21)
        Me.txtComisionOxxo.TabIndex = 23
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(378, 204)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(75, 15)
        Me.Label76.TabIndex = 22
        Me.Label76.Text = "Comisión :"
        '
        'TpCobroMaterial
        '
        Me.TpCobroMaterial.Controls.Add(Me.Panel14)
        Me.TpCobroMaterial.Controls.Add(Me.btnEliminar)
        Me.TpCobroMaterial.Controls.Add(Me.btnAgregar)
        Me.TpCobroMaterial.Controls.Add(Me.lblRangoFin)
        Me.TpCobroMaterial.Controls.Add(Me.lblRangoIni)
        Me.TpCobroMaterial.Controls.Add(Me.txtRango2)
        Me.TpCobroMaterial.Controls.Add(Me.txtRango1)
        Me.TpCobroMaterial.Controls.Add(Me.nudPagos)
        Me.TpCobroMaterial.Controls.Add(Me.lblPagos)
        Me.TpCobroMaterial.Controls.Add(Me.TextBox17)
        Me.TpCobroMaterial.Controls.Add(Label11)
        Me.TpCobroMaterial.Location = New System.Drawing.Point(4, 64)
        Me.TpCobroMaterial.Name = "TpCobroMaterial"
        Me.TpCobroMaterial.Size = New System.Drawing.Size(865, 592)
        Me.TpCobroMaterial.TabIndex = 26
        Me.TpCobroMaterial.Text = "Cobro de Material"
        Me.TpCobroMaterial.UseVisualStyleBackColor = True
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.dgvRangosMaterial)
        Me.Panel14.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel14.Location = New System.Drawing.Point(122, 217)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(490, 164)
        Me.Panel14.TabIndex = 160
        '
        'dgvRangosMaterial
        '
        Me.dgvRangosMaterial.AllowUserToAddRows = False
        Me.dgvRangosMaterial.AllowUserToDeleteRows = False
        Me.dgvRangosMaterial.AllowUserToOrderColumns = True
        Me.dgvRangosMaterial.AllowUserToResizeColumns = False
        Me.dgvRangosMaterial.AllowUserToResizeRows = False
        Me.dgvRangosMaterial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRangosMaterial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.identity, Me.RangoIni, Me.RangoFinal, Me.NumPagos})
        Me.dgvRangosMaterial.Location = New System.Drawing.Point(7, 7)
        Me.dgvRangosMaterial.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.dgvRangosMaterial.Name = "dgvRangosMaterial"
        Me.dgvRangosMaterial.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRangosMaterial.Size = New System.Drawing.Size(475, 149)
        Me.dgvRangosMaterial.TabIndex = 103
        '
        'identity
        '
        Me.identity.DataPropertyName = "ident"
        Me.identity.HeaderText = "ident"
        Me.identity.Name = "identity"
        Me.identity.ReadOnly = True
        Me.identity.Visible = False
        '
        'RangoIni
        '
        Me.RangoIni.DataPropertyName = "Inicio"
        Me.RangoIni.HeaderText = "Rango Inicial"
        Me.RangoIni.Name = "RangoIni"
        Me.RangoIni.ReadOnly = True
        Me.RangoIni.Width = 120
        '
        'RangoFinal
        '
        Me.RangoFinal.DataPropertyName = "Final"
        Me.RangoFinal.HeaderText = "Rango Final"
        Me.RangoFinal.Name = "RangoFinal"
        Me.RangoFinal.ReadOnly = True
        Me.RangoFinal.Width = 120
        '
        'NumPagos
        '
        Me.NumPagos.DataPropertyName = "NMaximoDiferidos"
        Me.NumPagos.HeaderText = "Número Máximo de Pagos Diferidos"
        Me.NumPagos.Name = "NumPagos"
        Me.NumPagos.ReadOnly = True
        Me.NumPagos.Width = 190
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.SystemColors.Control
        Me.btnEliminar.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.Black
        Me.btnEliminar.Location = New System.Drawing.Point(643, 263)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(99, 30)
        Me.btnEliminar.TabIndex = 159
        Me.btnEliminar.Text = "&Eliminar Rango"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.SystemColors.Control
        Me.btnAgregar.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(643, 221)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(99, 36)
        Me.btnAgregar.TabIndex = 158
        Me.btnAgregar.Text = "&Agregar Rango"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'lblRangoFin
        '
        Me.lblRangoFin.AutoSize = True
        Me.lblRangoFin.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRangoFin.Location = New System.Drawing.Point(283, 165)
        Me.lblRangoFin.Name = "lblRangoFin"
        Me.lblRangoFin.Size = New System.Drawing.Size(77, 18)
        Me.lblRangoFin.TabIndex = 157
        Me.lblRangoFin.Text = "Rango Final:"
        '
        'lblRangoIni
        '
        Me.lblRangoIni.AutoSize = True
        Me.lblRangoIni.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRangoIni.Location = New System.Drawing.Point(134, 165)
        Me.lblRangoIni.Name = "lblRangoIni"
        Me.lblRangoIni.Size = New System.Drawing.Size(83, 18)
        Me.lblRangoIni.TabIndex = 156
        Me.lblRangoIni.Text = "Rango Inicial:"
        '
        'txtRango2
        '
        Me.txtRango2.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRango2.Location = New System.Drawing.Point(265, 186)
        Me.txtRango2.Name = "txtRango2"
        Me.txtRango2.Size = New System.Drawing.Size(118, 26)
        Me.txtRango2.TabIndex = 155
        Me.txtRango2.Text = "0.0"
        Me.txtRango2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtRango1
        '
        Me.txtRango1.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRango1.Location = New System.Drawing.Point(122, 186)
        Me.txtRango1.Name = "txtRango1"
        Me.txtRango1.Size = New System.Drawing.Size(118, 26)
        Me.txtRango1.TabIndex = 154
        Me.txtRango1.Text = "0.0"
        Me.txtRango1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'nudPagos
        '
        Me.nudPagos.BackColor = System.Drawing.Color.White
        Me.nudPagos.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPagos.Location = New System.Drawing.Point(458, 185)
        Me.nudPagos.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.nudPagos.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudPagos.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudPagos.Name = "nudPagos"
        Me.nudPagos.ReadOnly = True
        Me.nudPagos.Size = New System.Drawing.Size(110, 30)
        Me.nudPagos.TabIndex = 153
        Me.nudPagos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudPagos.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblPagos
        '
        Me.lblPagos.AutoSize = True
        Me.lblPagos.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPagos.Location = New System.Drawing.Point(408, 165)
        Me.lblPagos.Name = "lblPagos"
        Me.lblPagos.Size = New System.Drawing.Size(208, 18)
        Me.lblPagos.TabIndex = 152
        Me.lblPagos.Text = "Número Máximo de Pagos Diferidos"
        '
        'TextBox17
        '
        Me.TextBox17.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipServEricBindingSource, "Clv_TipSer", True))
        Me.TextBox17.Location = New System.Drawing.Point(577, 333)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(10, 21)
        Me.TextBox17.TabIndex = 151
        Me.TextBox17.TabStop = False
        '
        'TpBonificaciones
        '
        Me.TpBonificaciones.Controls.Add(Label89)
        Me.TpBonificaciones.Controls.Add(Me.tbBonificacionMax)
        Me.TpBonificaciones.Controls.Add(Me.bnGuardarBonificacionMax)
        Me.TpBonificaciones.Controls.Add(Me.Label77)
        Me.TpBonificaciones.Location = New System.Drawing.Point(4, 64)
        Me.TpBonificaciones.Name = "TpBonificaciones"
        Me.TpBonificaciones.Size = New System.Drawing.Size(865, 592)
        Me.TpBonificaciones.TabIndex = 27
        Me.TpBonificaciones.Text = "Bonificaciones"
        Me.TpBonificaciones.UseVisualStyleBackColor = True
        '
        'tbBonificacionMax
        '
        Me.tbBonificacionMax.Location = New System.Drawing.Point(197, 136)
        Me.tbBonificacionMax.Name = "tbBonificacionMax"
        Me.tbBonificacionMax.Size = New System.Drawing.Size(154, 21)
        Me.tbBonificacionMax.TabIndex = 86
        '
        'bnGuardarBonificacionMax
        '
        Me.bnGuardarBonificacionMax.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardarBonificacionMax.Location = New System.Drawing.Point(691, 519)
        Me.bnGuardarBonificacionMax.Name = "bnGuardarBonificacionMax"
        Me.bnGuardarBonificacionMax.Size = New System.Drawing.Size(136, 36)
        Me.bnGuardarBonificacionMax.TabIndex = 85
        Me.bnGuardarBonificacionMax.Text = "&GUARDAR"
        Me.bnGuardarBonificacionMax.UseVisualStyleBackColor = True
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.BackColor = System.Drawing.Color.Black
        Me.Label77.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label77.ForeColor = System.Drawing.Color.White
        Me.Label77.Location = New System.Drawing.Point(37, 37)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(126, 20)
        Me.Label77.TabIndex = 84
        Me.Label77.Text = "Bonificaciones"
        '
        'TbResumenEjec
        '
        Me.TbResumenEjec.Controls.Add(Me.TextBox19)
        Me.TbResumenEjec.Controls.Add(Me.CheckBox5)
        Me.TbResumenEjec.Controls.Add(Me.Button23)
        Me.TbResumenEjec.Controls.Add(Me.Button22)
        Me.TbResumenEjec.Controls.Add(Me.Button18)
        Me.TbResumenEjec.Controls.Add(Me.DataGridView2)
        Me.TbResumenEjec.Controls.Add(Me.Button17)
        Me.TbResumenEjec.Controls.Add(Me.Label73)
        Me.TbResumenEjec.Controls.Add(Me.TextBox18)
        Me.TbResumenEjec.Controls.Add(Me.Label51)
        Me.TbResumenEjec.Controls.Add(Me.TextBox12)
        Me.TbResumenEjec.Controls.Add(Me.Label50)
        Me.TbResumenEjec.Controls.Add(Me.Label42)
        Me.TbResumenEjec.Controls.Add(Me.Label48)
        Me.TbResumenEjec.Controls.Add(Me.Label13)
        Me.TbResumenEjec.Controls.Add(Me.TextBox9)
        Me.TbResumenEjec.Controls.Add(Me.TextBox10)
        Me.TbResumenEjec.Location = New System.Drawing.Point(4, 64)
        Me.TbResumenEjec.Name = "TbResumenEjec"
        Me.TbResumenEjec.Padding = New System.Windows.Forms.Padding(3)
        Me.TbResumenEjec.Size = New System.Drawing.Size(865, 592)
        Me.TbResumenEjec.TabIndex = 28
        Me.TbResumenEjec.Text = "Resumen Ejecutivo"
        Me.TbResumenEjec.UseVisualStyleBackColor = True
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(128, 69)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(100, 21)
        Me.TextBox19.TabIndex = 60
        Me.TextBox19.Visible = False
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(44, 150)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox5.Size = New System.Drawing.Size(100, 19)
        Me.CheckBox5.TabIndex = 59
        Me.CheckBox5.Text = ":Habilitado "
        Me.CheckBox5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(503, 526)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(75, 23)
        Me.Button23.TabIndex = 58
        Me.Button23.Text = "Guardar"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(610, 214)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(168, 23)
        Me.Button22.TabIndex = 57
        Me.Button22.Text = "Eliminar Usuario"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(245, 176)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(75, 23)
        Me.Button18.TabIndex = 56
        Me.Button18.Text = "Modificar"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'DataGridView2
        '
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Nombre, Me.email, Me.habilitar})
        Me.DataGridView2.Location = New System.Drawing.Point(395, 65)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.Size = New System.Drawing.Size(383, 143)
        Me.DataGridView2.TabIndex = 55
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id Usuario"
        Me.Id.Name = "Id"
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "nombre"
        Me.Nombre.HeaderText = "Usuario"
        Me.Nombre.Name = "Nombre"
        '
        'email
        '
        Me.email.DataPropertyName = "email"
        Me.email.HeaderText = "Email"
        Me.email.Name = "email"
        '
        'habilitar
        '
        Me.habilitar.DataPropertyName = "habilitado"
        Me.habilitar.HeaderText = "habilitado"
        Me.habilitar.Name = "habilitar"
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(164, 176)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(75, 23)
        Me.Button17.TabIndex = 54
        Me.Button17.Text = "Guardar"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(74, 123)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(48, 15)
        Me.Label73.TabIndex = 53
        Me.Label73.Text = "Email:"
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(128, 123)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(192, 21)
        Me.TextBox18.TabIndex = 52
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(60, 99)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(62, 15)
        Me.Label51.TabIndex = 51
        Me.Label51.Text = "Nombre:"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(128, 96)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(192, 21)
        Me.TextBox12.TabIndex = 50
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.BackColor = System.Drawing.Color.Black
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.ForeColor = System.Drawing.Color.White
        Me.Label50.Location = New System.Drawing.Point(245, 18)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(393, 20)
        Me.Label50.TabIndex = 49
        Me.Label50.Text = "Usuarios a quien se envia el Resumen Ejecutivo"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(187, 320)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(70, 15)
        Me.Label42.TabIndex = 48
        Me.Label42.Text = "Mensaje :"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(199, 296)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(58, 15)
        Me.Label48.TabIndex = 47
        Me.Label48.Text = "Asunto :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Black
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(186, 258)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(145, 20)
        Me.Label13.TabIndex = 46
        Me.Label13.Text = "Datos del Correo"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(263, 320)
        Me.TextBox9.MaxLength = 1000
        Me.TextBox9.Multiline = True
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(315, 200)
        Me.TextBox9.TabIndex = 45
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(263, 293)
        Me.TextBox10.MaxLength = 150
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(315, 21)
        Me.TextBox10.TabIndex = 44
        '
        'TbPagosContrataDig
        '
        Me.TbPagosContrataDig.Controls.Add(Me.Button24)
        Me.TbPagosContrataDig.Controls.Add(Me.GroupBox4)
        Me.TbPagosContrataDig.Controls.Add(Me.GroupBox1)
        Me.TbPagosContrataDig.Controls.Add(Me.Label74)
        Me.TbPagosContrataDig.Location = New System.Drawing.Point(4, 64)
        Me.TbPagosContrataDig.Name = "TbPagosContrataDig"
        Me.TbPagosContrataDig.Size = New System.Drawing.Size(865, 592)
        Me.TbPagosContrataDig.TabIndex = 29
        Me.TbPagosContrataDig.Text = "Contratación Digital"
        Me.TbPagosContrataDig.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button24.Location = New System.Drawing.Point(700, 471)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(110, 37)
        Me.Button24.TabIndex = 59
        Me.Button24.Text = "Guardar"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label90)
        Me.GroupBox4.Controls.Add(Me.CheckBox9)
        Me.GroupBox4.Controls.Add(Me.NumericUpDown11)
        Me.GroupBox4.Controls.Add(Me.CheckBox8)
        Me.GroupBox4.Location = New System.Drawing.Point(467, 141)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(343, 249)
        Me.GroupBox4.TabIndex = 52
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Stb´s Adicionales"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(47, 166)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(130, 15)
        Me.Label90.TabIndex = 60
        Me.Label90.Text = "Número de Pagos: "
        '
        'CheckBox9
        '
        Me.CheckBox9.AutoSize = True
        Me.CheckBox9.Location = New System.Drawing.Point(50, 49)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(217, 19)
        Me.CheckBox9.TabIndex = 57
        Me.CheckBox9.Text = "Se Financia Stb´s Adicionales"
        Me.CheckBox9.UseVisualStyleBackColor = True
        '
        'NumericUpDown11
        '
        Me.NumericUpDown11.Location = New System.Drawing.Point(183, 164)
        Me.NumericUpDown11.Name = "NumericUpDown11"
        Me.NumericUpDown11.Size = New System.Drawing.Size(64, 21)
        Me.NumericUpDown11.TabIndex = 59
        '
        'CheckBox8
        '
        Me.CheckBox8.AutoSize = True
        Me.CheckBox8.Location = New System.Drawing.Point(50, 105)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(226, 19)
        Me.CheckBox8.TabIndex = 58
        Me.CheckBox8.Text = "Primer pago en la Contratación"
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label75)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown10)
        Me.GroupBox1.Controls.Add(Me.CheckBox7)
        Me.GroupBox1.Controls.Add(Me.CheckBox6)
        Me.GroupBox1.Location = New System.Drawing.Point(45, 141)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(343, 249)
        Me.GroupBox1.TabIndex = 51
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Stb Principal"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(35, 166)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(130, 15)
        Me.Label75.TabIndex = 56
        Me.Label75.Text = "Número de Pagos: "
        '
        'NumericUpDown10
        '
        Me.NumericUpDown10.Location = New System.Drawing.Point(171, 164)
        Me.NumericUpDown10.Name = "NumericUpDown10"
        Me.NumericUpDown10.Size = New System.Drawing.Size(64, 21)
        Me.NumericUpDown10.TabIndex = 55
        '
        'CheckBox7
        '
        Me.CheckBox7.AutoSize = True
        Me.CheckBox7.Location = New System.Drawing.Point(38, 105)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(226, 19)
        Me.CheckBox7.TabIndex = 54
        Me.CheckBox7.Text = "Primer pago en la Contratación"
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Location = New System.Drawing.Point(38, 49)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(188, 19)
        Me.CheckBox6.TabIndex = 53
        Me.CheckBox6.Text = "Se Financia Stb Principal"
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.BackColor = System.Drawing.Color.Black
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label74.ForeColor = System.Drawing.Color.White
        Me.Label74.Location = New System.Drawing.Point(312, 54)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(262, 20)
        Me.Label74.TabIndex = 50
        Me.Label74.Text = "Contratación de Cajas Digitales"
        '
        'ConsultaRelTecnicosQuejasBindingSource
        '
        Me.ConsultaRelTecnicosQuejasBindingSource.DataMember = "ConsultaRel_Tecnicos_Quejas"
        Me.ConsultaRelTecnicosQuejasBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRelQuejasTecFamiliaBindingSource
        '
        Me.ConsultaRelQuejasTecFamiliaBindingSource.DataMember = "ConsultaRel_Quejas_Tec_Familia"
        Me.ConsultaRelQuejasTecFamiliaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(392, 693)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 74
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ConGeneralTableAdapter1
        '
        Me.ConGeneralTableAdapter1.ClearBeforeFill = True
        '
        'ConGeneralAntiguedadTableAdapter1
        '
        Me.ConGeneralAntiguedadTableAdapter1.ClearBeforeFill = True
        '
        'MUESTRAPERIODOSTableAdapter1
        '
        Me.MUESTRAPERIODOSTableAdapter1.ClearBeforeFill = True
        '
        'CONSULTAGENERALESDESCTableAdapter1
        '
        Me.CONSULTAGENERALESDESCTableAdapter1.ClearBeforeFill = True
        '
        'ConsultaRel_Quejas_Tec_FamiliaBindingSource
        '
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource.DataMember = "ConsultaRel_Quejas_Tec_Familia"
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRel_Quejas_Tec_FamiliaTableAdapter
        '
        Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter.ClearBeforeFill = True
        '
        'ConsultaRel_Tecnicos_QuejasBindingSource
        '
        Me.ConsultaRel_Tecnicos_QuejasBindingSource.DataMember = "ConsultaRel_Tecnicos_Quejas"
        Me.ConsultaRel_Tecnicos_QuejasBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRel_Tecnicos_QuejasTableAdapter
        '
        Me.ConsultaRel_Tecnicos_QuejasTableAdapter.ClearBeforeFill = True
        '
        'Muestra_tecnicosDepartamentos_AlmacenTableAdapter
        '
        Me.Muestra_tecnicosDepartamentos_AlmacenTableAdapter.ClearBeforeFill = True
        '
        'Muestra_TecnicosByFamiliTableAdapter
        '
        Me.Muestra_TecnicosByFamiliTableAdapter.ClearBeforeFill = True
        '
        'NueGeneralMsjTicketsBindingSource
        '
        Me.NueGeneralMsjTicketsBindingSource.DataMember = "NueGeneralMsjTickets"
        '
        'MODIFCAGENERALESDESCBindingSource1
        '
        Me.MODIFCAGENERALESDESCBindingSource1.DataMember = "MODIFCAGENERALESDESC"
        '
        'NueGeneralBindingSource1
        '
        Me.NueGeneralBindingSource1.DataMember = "NueGeneral"
        '
        'NUEGeneralAntiguedadBindingSource1
        '
        Me.NUEGeneralAntiguedadBindingSource1.DataMember = "NUEGeneralAntiguedad"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(388, 372)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ConsecutivoTextBox.TabIndex = 54
        '
        'Consulta_Rel_PaquetesDigTelDataGridView
        '
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToAddRows = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToDeleteRows = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToOrderColumns = True
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AutoGenerateColumns = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consulta_Rel_PaquetesDigTelDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Consulta_Rel_PaquetesDigTelDataGridView.DataSource = Me.Consulta_Rel_PaquetesDigTelBindingSource
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Location = New System.Drawing.Point(300, 233)
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Name = "Consulta_Rel_PaquetesDigTelDataGridView"
        Me.Consulta_Rel_PaquetesDigTelDataGridView.ReadOnly = True
        Me.Consulta_Rel_PaquetesDigTelDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Size = New System.Drawing.Size(254, 220)
        Me.Consulta_Rel_PaquetesDigTelDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "precio"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn3.HeaderText = "Precio"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "nopaquetes"
        Me.DataGridViewTextBoxColumn2.HeaderText = "# Paquetes"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "consecutivo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "consecutivo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'NumericUpDown9
        '
        Me.NumericUpDown9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown9.Location = New System.Drawing.Point(424, 133)
        Me.NumericUpDown9.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDown9.Name = "NumericUpDown9"
        Me.NumericUpDown9.Size = New System.Drawing.Size(59, 22)
        Me.NumericUpDown9.TabIndex = 1
        Me.NumericUpDown9.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'TextBox8
        '
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(424, 160)
        Me.TextBox8.MaxLength = 15
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(109, 22)
        Me.TextBox8.TabIndex = 2
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(291, 134)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(114, 16)
        Me.Label43.TabIndex = 3
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(329, 163)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(79, 16)
        Me.Label44.TabIndex = 4
        '
        'Button25
        '
        Me.Button25.BackColor = System.Drawing.SystemColors.Control
        Me.Button25.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button25.ForeColor = System.Drawing.Color.Black
        Me.Button25.Location = New System.Drawing.Point(560, 141)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(124, 30)
        Me.Button25.TabIndex = 52
        Me.Button25.Text = "&AGREGAR"
        Me.Button25.UseVisualStyleBackColor = False
        '
        'Button26
        '
        Me.Button26.BackColor = System.Drawing.SystemColors.Control
        Me.Button26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button26.ForeColor = System.Drawing.Color.Black
        Me.Button26.Location = New System.Drawing.Point(560, 233)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(124, 30)
        Me.Button26.TabIndex = 53
        Me.Button26.Text = "&BORRAR"
        Me.Button26.UseVisualStyleBackColor = False
        '
        'Label45
        '
        Me.Label45.BackColor = System.Drawing.Color.Black
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.White
        Me.Label45.Location = New System.Drawing.Point(41, 34)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(339, 20)
        Me.Label45.TabIndex = 52
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.Black
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Location = New System.Drawing.Point(43, 27)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(83, 20)
        Me.Label47.TabIndex = 15
        '
        'Button28
        '
        Me.Button28.BackColor = System.Drawing.SystemColors.Control
        Me.Button28.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button28.ForeColor = System.Drawing.Color.Black
        Me.Button28.Location = New System.Drawing.Point(694, 543)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(136, 36)
        Me.Button28.TabIndex = 17
        Me.Button28.Text = "&GUARDAR"
        Me.Button28.UseVisualStyleBackColor = False
        '
        'Clave01900TextBox
        '
        Me.Clave01900TextBox.Location = New System.Drawing.Point(244, 420)
        Me.Clave01900TextBox.MaxLength = 50
        Me.Clave01900TextBox.Name = "Clave01900TextBox"
        Me.Clave01900TextBox.Size = New System.Drawing.Size(143, 20)
        Me.Clave01900TextBox.TabIndex = 70
        '
        'Clave01800TextBox
        '
        Me.Clave01800TextBox.Location = New System.Drawing.Point(244, 394)
        Me.Clave01800TextBox.MaxLength = 50
        Me.Clave01800TextBox.Name = "Clave01800TextBox"
        Me.Clave01800TextBox.Size = New System.Drawing.Size(143, 20)
        Me.Clave01800TextBox.TabIndex = 68
        '
        'Clave045TextBox
        '
        Me.Clave045TextBox.Location = New System.Drawing.Point(244, 368)
        Me.Clave045TextBox.MaxLength = 50
        Me.Clave045TextBox.Name = "Clave045TextBox"
        Me.Clave045TextBox.Size = New System.Drawing.Size(143, 20)
        Me.Clave045TextBox.TabIndex = 66
        '
        'Clave044TextBox
        '
        Me.Clave044TextBox.Location = New System.Drawing.Point(244, 342)
        Me.Clave044TextBox.MaxLength = 50
        Me.Clave044TextBox.Name = "Clave044TextBox"
        Me.Clave044TextBox.Size = New System.Drawing.Size(143, 20)
        Me.Clave044TextBox.TabIndex = 64
        '
        'Pais_LocalTextBox
        '
        Me.Pais_LocalTextBox.Location = New System.Drawing.Point(244, 316)
        Me.Pais_LocalTextBox.MaxLength = 50
        Me.Pais_LocalTextBox.Name = "Pais_LocalTextBox"
        Me.Pais_LocalTextBox.Size = New System.Drawing.Size(143, 20)
        Me.Pais_LocalTextBox.TabIndex = 62
        '
        'Numero_LocalTextBox
        '
        Me.Numero_LocalTextBox.Location = New System.Drawing.Point(244, 290)
        Me.Numero_LocalTextBox.MaxLength = 50
        Me.Numero_LocalTextBox.Name = "Numero_LocalTextBox"
        Me.Numero_LocalTextBox.Size = New System.Drawing.Size(143, 20)
        Me.Numero_LocalTextBox.TabIndex = 60
        '
        'RutaTextBox
        '
        Me.RutaTextBox.Location = New System.Drawing.Point(244, 264)
        Me.RutaTextBox.MaxLength = 250
        Me.RutaTextBox.Name = "RutaTextBox"
        Me.RutaTextBox.Size = New System.Drawing.Size(143, 20)
        Me.RutaTextBox.TabIndex = 58
        '
        'FormatoTextBox
        '
        Me.FormatoTextBox.Location = New System.Drawing.Point(244, 238)
        Me.FormatoTextBox.Name = "FormatoTextBox"
        Me.FormatoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FormatoTextBox.TabIndex = 56
        '
        'Numero_InicioTextBox
        '
        Me.Numero_InicioTextBox.Location = New System.Drawing.Point(244, 212)
        Me.Numero_InicioTextBox.Name = "Numero_InicioTextBox"
        Me.Numero_InicioTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Numero_InicioTextBox.TabIndex = 54
        '
        'Periodo_finalTextBox
        '
        Me.Periodo_finalTextBox.Location = New System.Drawing.Point(338, 185)
        Me.Periodo_finalTextBox.Name = "Periodo_finalTextBox"
        Me.Periodo_finalTextBox.Size = New System.Drawing.Size(49, 20)
        Me.Periodo_finalTextBox.TabIndex = 52
        '
        'Periodo_inicialTextBox
        '
        Me.Periodo_inicialTextBox.Location = New System.Drawing.Point(244, 185)
        Me.Periodo_inicialTextBox.Name = "Periodo_inicialTextBox"
        Me.Periodo_inicialTextBox.Size = New System.Drawing.Size(50, 20)
        Me.Periodo_inicialTextBox.TabIndex = 50
        '
        'Fecha_FacturasDateTimePicker
        '
        Me.Fecha_FacturasDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_FacturasDateTimePicker.Location = New System.Drawing.Point(244, 159)
        Me.Fecha_FacturasDateTimePicker.Name = "Fecha_FacturasDateTimePicker"
        Me.Fecha_FacturasDateTimePicker.Size = New System.Drawing.Size(100, 20)
        Me.Fecha_FacturasDateTimePicker.TabIndex = 48
        '
        'Fecha_InicioDateTimePicker
        '
        Me.Fecha_InicioDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_InicioDateTimePicker.Location = New System.Drawing.Point(244, 133)
        Me.Fecha_InicioDateTimePicker.Name = "Fecha_InicioDateTimePicker"
        Me.Fecha_InicioDateTimePicker.Size = New System.Drawing.Size(100, 20)
        Me.Fecha_InicioDateTimePicker.TabIndex = 46
        '
        'Nom_ArchivoTextBox
        '
        Me.Nom_ArchivoTextBox.Location = New System.Drawing.Point(244, 107)
        Me.Nom_ArchivoTextBox.MaxLength = 250
        Me.Nom_ArchivoTextBox.Name = "Nom_ArchivoTextBox"
        Me.Nom_ArchivoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.Nom_ArchivoTextBox.TabIndex = 44
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.BackColor = System.Drawing.Color.Black
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.White
        Me.Label49.Location = New System.Drawing.Point(498, 188)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(292, 20)
        Me.Label49.TabIndex = 71
        '
        'TextBoxserie
        '
        Me.TextBoxserie.Location = New System.Drawing.Point(723, 229)
        Me.TextBoxserie.MaxLength = 15
        Me.TextBoxserie.Name = "TextBoxserie"
        Me.TextBoxserie.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxserie.TabIndex = 74
        '
        'TextBoxfolio
        '
        Me.TextBoxfolio.Location = New System.Drawing.Point(723, 255)
        Me.TextBoxfolio.MaxLength = 15
        Me.TextBoxfolio.Name = "TextBoxfolio"
        Me.TextBoxfolio.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxfolio.TabIndex = 75
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.BackColor = System.Drawing.Color.Black
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(501, 294)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(131, 20)
        Me.Label54.TabIndex = 76
        '
        'TxtEdoClvAdic
        '
        Me.TxtEdoClvAdic.Location = New System.Drawing.Point(723, 321)
        Me.TxtEdoClvAdic.MaxLength = 2
        Me.TxtEdoClvAdic.Name = "TxtEdoClvAdic"
        Me.TxtEdoClvAdic.Size = New System.Drawing.Size(100, 20)
        Me.TxtEdoClvAdic.TabIndex = 79
        '
        'TxtCdClvAdic
        '
        Me.TxtCdClvAdic.Location = New System.Drawing.Point(723, 348)
        Me.TxtCdClvAdic.MaxLength = 2
        Me.TxtCdClvAdic.Name = "TxtCdClvAdic"
        Me.TxtCdClvAdic.Size = New System.Drawing.Size(100, 20)
        Me.TxtCdClvAdic.TabIndex = 80
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter9
        '
        Me.Muestra_ServiciosDigitalesTableAdapter9.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter10
        '
        Me.Muestra_ServiciosDigitalesTableAdapter10.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter11
        '
        Me.Muestra_ServiciosDigitalesTableAdapter11.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter12
        '
        Me.Muestra_ServiciosDigitalesTableAdapter12.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter13
        '
        Me.Muestra_ServiciosDigitalesTableAdapter13.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter14
        '
        Me.Muestra_ServiciosDigitalesTableAdapter14.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter15
        '
        Me.Muestra_ServiciosDigitalesTableAdapter15.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter16
        '
        Me.Muestra_ServiciosDigitalesTableAdapter16.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter17
        '
        Me.Muestra_ServiciosDigitalesTableAdapter17.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter18
        '
        Me.Muestra_ServiciosDigitalesTableAdapter18.ClearBeforeFill = True
        '
        'FrmGenerales_Sistema
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(901, 741)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TabControl1)
        Me.MaximizeBox = False
        Me.Name = "FrmGenerales_Sistema"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales del Sistema"
        CType(Me.ConsultaCpGeneBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_ImpresorasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMesesCobroAdeudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralCorreoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Mod_Cp_GenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.COnsultaGralfacturaglobalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Gral_factura_globalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.DataGridViewCobro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONSULTAGENERALESDESCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAPERIODOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelBasico.ResumeLayout(False)
        Me.PanelBasico.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        CType(Me.ConGeneralAntiguedadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.Consulta_cobrodepositoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.ConGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTA_General_HsbcBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTA_General_SantanderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTA_General_XmlBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDatos.ResumeLayout(False)
        Me.PanelDatos.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.ConGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_cobrodepositobuenoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_PaquetesDigTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Direcciones_IpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage15.ResumeLayout(False)
        Me.PnQuejas.ResumeLayout(False)
        Me.PnQuejas.PerformLayout()
        CType(Me.MuestraTecnicosByFamiliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestratecnicosDepartamentosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage16.ResumeLayout(False)
        Me.TabPage16.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage17.ResumeLayout(False)
        Me.TabPage17.PerformLayout()
        Me.TabPage18.ResumeLayout(False)
        Me.TabPage18.PerformLayout()
        CType(Me.NumericUpDownIeps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDownIva, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpRecontratacion.ResumeLayout(False)
        Me.tpRecontratacion.PerformLayout()
        Me.tpEstadosCuenta.ResumeLayout(False)
        Me.tpEstadosCuenta.PerformLayout()
        Me.gbMensaje.ResumeLayout(False)
        Me.gbMensaje.PerformLayout()
        Me.tbPuntos.ResumeLayout(False)
        Me.tbPuntos.PerformLayout()
        Me.tcPuntos.ResumeLayout(False)
        Me.tbPuntosRangos.ResumeLayout(False)
        Me.tbPuntosRangos.PerformLayout()
        CType(Me.dgRangos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPuntosServicios.ResumeLayout(False)
        Me.tbPuntosServicios.PerformLayout()
        CType(Me.dgPuntosServicios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPuntosServiciosAdic.ResumeLayout(False)
        Me.tbPuntosServiciosAdic.PerformLayout()
        CType(Me.dgPagosAntiServAdic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpProrroga.ResumeLayout(False)
        Me.tpProrroga.PerformLayout()
        CType(Me.dgvUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTipoUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabOxxo.ResumeLayout(False)
        Me.TabOxxo.PerformLayout()
        Me.TpCobroMaterial.ResumeLayout(False)
        Me.TpCobroMaterial.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        CType(Me.dgvRangosMaterial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPagos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TpBonificaciones.ResumeLayout(False)
        Me.TpBonificaciones.PerformLayout()
        Me.TbResumenEjec.ResumeLayout(False)
        Me.TbResumenEjec.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TbPagosContrataDig.ResumeLayout(False)
        Me.TbPagosContrataDig.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.NumericUpDown11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumericUpDown10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRelTecnicosQuejasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRelQuejasTecFamiliaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRel_Tecnicos_QuejasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MODIFCAGENERALESDESCBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEGeneralAntiguedadBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_PaquetesDigTelDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents ConGeneralBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ConGeneralTableAdapter
    Friend WithEvents NueGeneralBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NueGeneralTableAdapter
    Friend WithEvents ConGeneralAntiguedadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralAntiguedadTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ConGeneralAntiguedadTableAdapter
    Friend WithEvents NUEGeneralAntiguedadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEGeneralAntiguedadTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEGeneralAntiguedadTableAdapter
    Friend WithEvents MUESTRAPERIODOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAPERIODOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAPERIODOSTableAdapter
    Friend WithEvents CONSULTAGENERALESDESCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTAGENERALESDESCTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTAGENERALESDESCTableAdapter
    Friend WithEvents MODIFCAGENERALESDESCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAGENERALESDESCTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MODIFCAGENERALESDESCTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents MODIFCAPeriodosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAPeriodosTableAdapter As New sofTV.DataSetLidiaTableAdapters.MODIFCAPeriodosTableAdapter
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Consulta_ImpresorasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_ImpresorasTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_ImpresorasTableAdapter
    Friend WithEvents Inserta_Mod_Cp_GenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Mod_Cp_GenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Mod_Cp_GenTableAdapter
    Friend WithEvents ConsultaCpGeneBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Cp_GeneTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_Cp_GeneTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConMesesCobroAdeudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMesesCobroAdeudoTableAdapter As sofTV.DataSetEricTableAdapters.ConMesesCobroAdeudoTableAdapter
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConGeneralCorreoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralCorreoTableAdapter As sofTV.DataSetEricTableAdapters.ConGeneralCorreoTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Consulta_Generales_Filtros_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Generales_Filtros_OrdenesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Generales_Filtros_OrdenesTableAdapter
    Friend WithEvents Inserta_Generales_Filtros_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Generales_Filtros_OrdenesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Generales_Filtros_OrdenesTableAdapter
    'Friend WithEvents COnsulta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents COnsulta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter
    Friend WithEvents COnsultaGralfacturaglobalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents COnsulta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter
    Friend WithEvents Inserta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents CuentaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HostTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PortTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MesesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents txtDiaCorte As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents PanelBasico As System.Windows.Forms.Panel
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown7 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown8 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown5 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown6 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Puntos10TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Puntos5TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Puntos2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Puntos1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraOrdenesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CostoExtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeroExtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Carga_trab_insTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CargaDeTrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImportePorExtrasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Si_se_generaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric2 As New sofTV.DataSetEric2
    Friend WithEvents ConGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralMsjTicketsTableAdapter As sofTV.DataSetEric2TableAdapters.ConGeneralMsjTicketsTableAdapter
    Friend WithEvents MensajeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents NueGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralMsjTicketsTableAdapter As sofTV.DataSetEric2TableAdapters.NueGeneralMsjTicketsTableAdapter
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents ConGeneralAlertaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralAlertaTableAdapter As sofTV.DataSetLidia2TableAdapters.ConGeneralAlertaTableAdapter
    Friend WithEvents NueGeneralAlertaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralAlertaTableAdapter As sofTV.DataSetLidia2TableAdapters.NueGeneralAlertaTableAdapter
    Friend WithEvents Dame_Direcciones_IpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Direcciones_IpTableAdapter As sofTV.DataSetLidia2TableAdapters.Dame_Direcciones_IpTableAdapter
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Consulta_cobrodepositoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_cobrodepositoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositoTableAdapter
    Friend WithEvents Consulta_cobrodepositobuenoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_cobrodepositobuenoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositobuenoTableAdapter
    Friend WithEvents Consulta_Rel_PaquetesDigTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Rel_PaquetesDigTelTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_Rel_PaquetesDigTelTableAdapter
    Friend WithEvents PanelDatos As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TELefonosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RfcTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Id_sucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataSetyahve As sofTV.DataSetyahve
    Friend WithEvents CONSULTA_General_XmlBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_XmlTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_XmlTableAdapter
    Friend WithEvents CONSULTA_General_SantanderBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_SantanderTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_SantanderTableAdapter
    Friend WithEvents CONSULTA_General_HsbcBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_HsbcTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_HsbcTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ciudadTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_ciudadTableAdapter
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNumeroExt As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNumeroInt As System.Windows.Forms.TextBox
    Friend WithEvents ConGeneralTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralTableAdapter
    Friend WithEvents ConGeneralAntiguedadTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralAntiguedadTableAdapter
    Friend WithEvents MUESTRAPERIODOSTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.MUESTRAPERIODOSTableAdapter
    Friend WithEvents CONSULTAGENERALESDESCTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.CONSULTAGENERALESDESCTableAdapter
    Friend WithEvents MODIFCAGENERALESDESCBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAGENERALESDESCTableAdapter1 As New sofTV.DataSetEric2TableAdapters.MODIFCAGENERALESDESCTableAdapter
    Friend WithEvents NueGeneralBindingSource1 As New System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralTableAdapter1 As New sofTV.DataSetEric2TableAdapters.NueGeneralTableAdapter
    Friend WithEvents NUEGeneralAntiguedadBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents NUEGeneralAntiguedadTableAdapter1 As sofTV.DataSetEric2TableAdapters.NUEGeneralAntiguedadTableAdapter
    Friend WithEvents TabPage15 As System.Windows.Forms.TabPage
    Friend WithEvents PnQuejas As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents TreViewTecnicos As System.Windows.Forms.TreeView
    Friend WithEvents CmbTecnicos As System.Windows.Forms.ComboBox
    Friend WithEvents CmBDepto As System.Windows.Forms.ComboBox
    Friend WithEvents LblFamilia As System.Windows.Forms.Label
    Friend WithEvents LblTecnico As System.Windows.Forms.Label
    Friend WithEvents LblNotas As System.Windows.Forms.Label
    Friend WithEvents ConsultaRel_Quejas_Tec_FamiliaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRel_Quejas_Tec_FamiliaTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Quejas_Tec_FamiliaTableAdapter
    Friend WithEvents ConsultaRel_Tecnicos_QuejasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRel_Tecnicos_QuejasTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Tecnicos_QuejasTableAdapter
    Friend WithEvents ConsultaRelTecnicosQuejasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRelQuejasTecFamiliaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTecnicosByFamiliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestratecnicosDepartamentosAlmacenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_tecnicosDepartamentos_AlmacenTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_tecnicosDepartamentos_AlmacenTableAdapter
    Friend WithEvents Muestra_TecnicosByFamiliTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_TecnicosByFamiliTableAdapter
    Friend WithEvents TabPage16 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Clv_Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Porcentaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Activo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents LabelNota As System.Windows.Forms.Label
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents TreeViewOrdenes As System.Windows.Forms.TreeView
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewCobro As System.Windows.Forms.DataGridView
    Friend WithEvents ComboBoxCobro As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_TipoCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Meses As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Habilitado As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents TabPage17 As System.Windows.Forms.TabPage
    Friend WithEvents DDescoSusp As System.Windows.Forms.TextBox
    Friend WithEvents DContrata As System.Windows.Forms.TextBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents TabPage18 As System.Windows.Forms.TabPage
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents LblPorIeps As System.Windows.Forms.Label
    Friend WithEvents NumericUpDownIeps As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDownIva As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents ChkCalculo1 As System.Windows.Forms.CheckBox
    Friend WithEvents LabelIEPS As System.Windows.Forms.Label
    Friend WithEvents CtaIepsText As System.Windows.Forms.TextBox
    Friend WithEvents LabelCtaIEPS As System.Windows.Forms.Label
    Friend WithEvents CheckIEPS As System.Windows.Forms.CheckBox
    Friend WithEvents TextIva As System.Windows.Forms.TextBox
    Friend WithEvents TextIEPS As System.Windows.Forms.TextBox
    Friend WithEvents tpRecontratacion As System.Windows.Forms.TabPage
    Friend WithEvents btnAceptarRecon As System.Windows.Forms.Button
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents cbAplica As System.Windows.Forms.CheckBox
    Friend WithEvents txtMeses As System.Windows.Forms.TextBox
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents rb30 As System.Windows.Forms.RadioButton
    Friend WithEvents rb15 As System.Windows.Forms.RadioButton
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents tpEstadosCuenta As System.Windows.Forms.TabPage
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtDiaEdoCuenta As System.Windows.Forms.TextBox
    Friend WithEvents cbPeriodo As System.Windows.Forms.ComboBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents gbMensaje As System.Windows.Forms.GroupBox
    Friend WithEvents tbMensaje As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents tbAsunto As System.Windows.Forms.TextBox
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents tbMensajeDeco As System.Windows.Forms.TextBox
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents dtHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents cbDeco As System.Windows.Forms.CheckBox
    Friend WithEvents cbCorreo As System.Windows.Forms.CheckBox
    Friend WithEvents tbPuntos As System.Windows.Forms.TabPage
    Friend WithEvents tcPuntos As System.Windows.Forms.TabControl
    Friend WithEvents tbPuntosRangos As System.Windows.Forms.TabPage
    Friend WithEvents bnEliminarRango As System.Windows.Forms.Button
    Friend WithEvents bnAgregarRango As System.Windows.Forms.Button
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents tbRangoFin As System.Windows.Forms.TextBox
    Friend WithEvents dgRangos As System.Windows.Forms.DataGridView
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents tbRangoIni As System.Windows.Forms.TextBox
    Friend WithEvents tbPuntosServicios As System.Windows.Forms.TabPage
    Friend WithEvents tbPuntosServiciosAdic As System.Windows.Forms.TabPage
    Friend WithEvents bnEliminarPuntosServicio As System.Windows.Forms.Button
    Friend WithEvents bnAgregarPuntosServicio As System.Windows.Forms.Button
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents dgPuntosServicios As System.Windows.Forms.DataGridView
    Friend WithEvents tboxPuntosAnti As System.Windows.Forms.TextBox
    Friend WithEvents cbRangosPuntosAnti As System.Windows.Forms.ComboBox
    Friend WithEvents cbServicioPuntosAnti As System.Windows.Forms.ComboBox
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents cbTipSerPuntosAnti As System.Windows.Forms.ComboBox
    Friend WithEvents Label118 As System.Windows.Forms.Label
    Friend WithEvents tbPuntosAntiServicios As System.Windows.Forms.TextBox
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents Label115 As System.Windows.Forms.Label
    Friend WithEvents cbTipSerPuntosAntiServicios As System.Windows.Forms.ComboBox
    Friend WithEvents cbServicioPuntosAntiServicios As System.Windows.Forms.ComboBox
    Friend WithEvents bnEliminarPuntosAntiServicios As System.Windows.Forms.Button
    Friend WithEvents bnAgregarPuntosAntiServicios As System.Windows.Forms.Button
    Friend WithEvents Label116 As System.Windows.Forms.Label
    Friend WithEvents dgPagosAntiServAdic As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PagosPuntuales As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tbPuntosAntiServiciosPagos As System.Windows.Forms.TextBox
    Friend WithEvents cbRangosPuntosAntiServicios As System.Windows.Forms.ComboBox
    Friend WithEvents IdRango As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Rango As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdPuntos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Puntos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label123 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents Label117 As System.Windows.Forms.Label
    Friend WithEvents Label124 As System.Windows.Forms.Label
    Friend WithEvents tbPuntosAnti As System.Windows.Forms.TextBox
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Consulta_Rel_PaquetesDigTelDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumericUpDown9 As System.Windows.Forms.NumericUpDown
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Clave01900TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave01800TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave045TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave044TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Pais_LocalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Numero_LocalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RutaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FormatoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Numero_InicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Periodo_finalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Periodo_inicialTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_FacturasDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Fecha_InicioDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Nom_ArchivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents TextBoxserie As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxfolio As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents TxtEdoClvAdic As System.Windows.Forms.TextBox
    Friend WithEvents TxtCdClvAdic As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter9 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter10 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter11 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter12 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter13 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter14 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter15 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents tpProrroga As System.Windows.Forms.TabPage
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents bnGuardarProrroga As System.Windows.Forms.Button
    Friend WithEvents dgvUsuarios As System.Windows.Forms.DataGridView
    Friend WithEvents ClaveUsuarios As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreUsuarios As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActivoUsuarios As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgvTipoUsuario As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_TipoUsuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionTipoUsuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActivoTipoUsuario As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents TabOxxo As System.Windows.Forms.TabPage
    Friend WithEvents txtPrefijo As System.Windows.Forms.TextBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents txtComisionOxxo As System.Windows.Forms.TextBox
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents TpCobroMaterial As System.Windows.Forms.TabPage
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents dgvRangosMaterial As System.Windows.Forms.DataGridView
    Friend WithEvents identity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RangoIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RangoFinal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumPagos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblRangoFin As System.Windows.Forms.Label
    Friend WithEvents lblRangoIni As System.Windows.Forms.Label
    Friend WithEvents txtRango2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRango1 As System.Windows.Forms.TextBox
    Friend WithEvents nudPagos As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPagos As System.Windows.Forms.Label
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter16 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents TpBonificaciones As System.Windows.Forms.TabPage
    Friend WithEvents tbBonificacionMax As System.Windows.Forms.TextBox
    Friend WithEvents bnGuardarBonificacionMax As System.Windows.Forms.Button
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents TbResumenEjec As System.Windows.Forms.TabPage
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents email As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents habilitar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TbPagosContrataDig As System.Windows.Forms.TabPage
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents CheckBox9 As System.Windows.Forms.CheckBox
    Friend WithEvents NumericUpDown11 As System.Windows.Forms.NumericUpDown
    Friend WithEvents CheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown10 As System.Windows.Forms.NumericUpDown
    Friend WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter17 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter18 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    'Friend WithEvents Inserta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents Inserta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter
End Class
