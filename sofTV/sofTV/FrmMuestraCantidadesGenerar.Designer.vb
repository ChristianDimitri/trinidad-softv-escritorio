﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMuestraCantidadesGenerar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxTotales = New System.Windows.Forms.GroupBox()
        Me.txtTotalOrdenes = New System.Windows.Forms.TextBox()
        Me.lblTotalOrdenes = New System.Windows.Forms.Label()
        Me.txtTotalCajas = New System.Windows.Forms.TextBox()
        Me.lblTotalCajas = New System.Windows.Forms.Label()
        Me.txtTotalClientes = New System.Windows.Forms.TextBox()
        Me.lblTotalContratos = New System.Windows.Forms.Label()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxTotales.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxTotales
        '
        Me.gbxTotales.Controls.Add(Me.txtTotalOrdenes)
        Me.gbxTotales.Controls.Add(Me.lblTotalOrdenes)
        Me.gbxTotales.Controls.Add(Me.txtTotalCajas)
        Me.gbxTotales.Controls.Add(Me.lblTotalCajas)
        Me.gbxTotales.Controls.Add(Me.txtTotalClientes)
        Me.gbxTotales.Controls.Add(Me.lblTotalContratos)
        Me.gbxTotales.Location = New System.Drawing.Point(12, 12)
        Me.gbxTotales.Name = "gbxTotales"
        Me.gbxTotales.Size = New System.Drawing.Size(274, 117)
        Me.gbxTotales.TabIndex = 0
        Me.gbxTotales.TabStop = False
        '
        'txtTotalOrdenes
        '
        Me.txtTotalOrdenes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalOrdenes.Location = New System.Drawing.Point(144, 87)
        Me.txtTotalOrdenes.Name = "txtTotalOrdenes"
        Me.txtTotalOrdenes.ReadOnly = True
        Me.txtTotalOrdenes.Size = New System.Drawing.Size(100, 22)
        Me.txtTotalOrdenes.TabIndex = 5
        '
        'lblTotalOrdenes
        '
        Me.lblTotalOrdenes.AutoSize = True
        Me.lblTotalOrdenes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalOrdenes.Location = New System.Drawing.Point(26, 89)
        Me.lblTotalOrdenes.Name = "lblTotalOrdenes"
        Me.lblTotalOrdenes.Size = New System.Drawing.Size(115, 16)
        Me.lblTotalOrdenes.TabIndex = 4
        Me.lblTotalOrdenes.Text = "Total Órdenes :"
        '
        'txtTotalCajas
        '
        Me.txtTotalCajas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCajas.Location = New System.Drawing.Point(144, 53)
        Me.txtTotalCajas.Name = "txtTotalCajas"
        Me.txtTotalCajas.ReadOnly = True
        Me.txtTotalCajas.Size = New System.Drawing.Size(100, 22)
        Me.txtTotalCajas.TabIndex = 3
        '
        'lblTotalCajas
        '
        Me.lblTotalCajas.AutoSize = True
        Me.lblTotalCajas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalCajas.Location = New System.Drawing.Point(45, 55)
        Me.lblTotalCajas.Name = "lblTotalCajas"
        Me.lblTotalCajas.Size = New System.Drawing.Size(96, 16)
        Me.lblTotalCajas.TabIndex = 2
        Me.lblTotalCajas.Text = "Total Cajas :"
        '
        'txtTotalClientes
        '
        Me.txtTotalClientes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalClientes.Location = New System.Drawing.Point(144, 18)
        Me.txtTotalClientes.Name = "txtTotalClientes"
        Me.txtTotalClientes.ReadOnly = True
        Me.txtTotalClientes.Size = New System.Drawing.Size(100, 22)
        Me.txtTotalClientes.TabIndex = 1
        '
        'lblTotalContratos
        '
        Me.lblTotalContratos.AutoSize = True
        Me.lblTotalContratos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalContratos.Location = New System.Drawing.Point(29, 20)
        Me.lblTotalContratos.Name = "lblTotalContratos"
        Me.lblTotalContratos.Size = New System.Drawing.Size(112, 16)
        Me.lblTotalContratos.TabIndex = 0
        Me.lblTotalContratos.Text = "Total Clientes :"
        '
        'btnGenerar
        '
        Me.btnGenerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerar.Location = New System.Drawing.Point(34, 133)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(100, 33)
        Me.btnGenerar.TabIndex = 1
        Me.btnGenerar.Text = "&Generar"
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(167, 133)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(100, 33)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'FrmMuestraCantidadesGenerar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(299, 173)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGenerar)
        Me.Controls.Add(Me.gbxTotales)
        Me.Name = "FrmMuestraCantidadesGenerar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Total Contratos"
        Me.gbxTotales.ResumeLayout(False)
        Me.gbxTotales.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxTotales As System.Windows.Forms.GroupBox
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents txtTotalOrdenes As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalOrdenes As System.Windows.Forms.Label
    Friend WithEvents txtTotalCajas As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalCajas As System.Windows.Forms.Label
    Friend WithEvents txtTotalClientes As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalContratos As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
