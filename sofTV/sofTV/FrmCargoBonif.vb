Public Class FrmCargoBonif

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.TextBox1.Text.Length = 0 Then
            MsgBox("Selecciona una opci�n.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.ListBox1.SelectedIndex = 0 Then
            eBndCargoEsp = True
            eBndBonifEsp = False
        Else
            eBndCargoEsp = False
            eBndBonifEsp = True
        End If

        My.Forms.BrwCargosEspeciales.Show()
        Me.Close()
    End Sub

    Private Sub FrmCargoBonif_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.ListBox1.Items.Insert(0, "Cargos Especiales")
        Me.ListBox1.Items.Insert(1, "Bonificaciones Especiales")
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        Me.TextBox1.Text = Me.ListBox1.SelectedItem.ToString
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        eBndCargoEsp = False
        eBndBonifEsp = False
        Me.Close()
    End Sub
End Class