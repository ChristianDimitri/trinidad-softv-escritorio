﻿Public Class FrmCiudadTv



    Private Sub FrmCiudadTv_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        colorea(Me, 1)
        MuestraCVECIU()
        ConsultaTvsCiudad()
        ConsultaTvsCiudades()


    End Sub

    Private Sub cBoxCiudades_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cBoxCiudades.SelectedIndexChanged
        ConsultaTvsCiudad()
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(tBoxTvAdic.Text) Then
            ModTbl_rel_ciudades_tvAdic()
            ConsultaTvsCiudades()
        Else
            MsgBox("Favor de insertar un numero de Tv´s correcto", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub MuestraCVECIU()
        BaseII.limpiaParametros()
        cBoxCiudades.DataSource = BaseII.ConsultaDT("MuestraCVECIU")

        cBoxCiudades.ValueMember = "Clv_Ciudad"
        cBoxCiudades.DisplayMember = "Nombre"
    End Sub

    Private Sub ConsultaTvsCiudad()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, cBoxCiudades.SelectedValue)
        BaseII.CreateMyParameter("@TV_Adiciona", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ConsultaTvsCiudad")
        tBoxTvAdic.Text = ""
        tBoxTvAdic.Text = BaseII.dicoPar("@TV_Adiciona").ToString
    End Sub

    Private Sub ConsultaTvsCiudades()
        BaseII.limpiaParametros()
        dGridTvsAdic.DataSource = BaseII.ConsultaDT("ConsultaTvsCiudades")
    End Sub

    Private Sub ModTbl_rel_ciudades_tvAdic()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, cBoxCiudades.SelectedValue)
        BaseII.CreateMyParameter("@Tvs_Adic", SqlDbType.Int, CInt(tBoxTvAdic.Text))
        BaseII.Inserta("ModTbl_rel_ciudades_tvAdic")
    End Sub

    
End Class