﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambiaClientePlaca
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxDatosClientePlaca = New System.Windows.Forms.GroupBox()
        Me.txtPlacaNueva = New System.Windows.Forms.TextBox()
        Me.lblPlacaActual = New System.Windows.Forms.Label()
        Me.txtPlacaActual = New System.Windows.Forms.TextBox()
        Me.lblNuevaPlaca = New System.Windows.Forms.Label()
        Me.txtPeriodoPagado = New System.Windows.Forms.TextBox()
        Me.txtStatus = New System.Windows.Forms.TextBox()
        Me.txtColonia = New System.Windows.Forms.TextBox()
        Me.txtNumero = New System.Windows.Forms.TextBox()
        Me.txtCalle = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.lblUltMes = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblColonia = New System.Windows.Forms.Label()
        Me.lblNumero = New System.Windows.Forms.Label()
        Me.lblCalle = New System.Windows.Forms.Label()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.lblContrato = New System.Windows.Forms.Label()
        Me.btnCambiar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxDatosClientePlaca.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxDatosClientePlaca
        '
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtPlacaNueva)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblPlacaActual)
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtPlacaActual)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblNuevaPlaca)
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtPeriodoPagado)
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtStatus)
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtColonia)
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtNumero)
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtCalle)
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtNombre)
        Me.gbxDatosClientePlaca.Controls.Add(Me.txtContrato)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblUltMes)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblStatus)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblColonia)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblNumero)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblCalle)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblNombre)
        Me.gbxDatosClientePlaca.Controls.Add(Me.lblContrato)
        Me.gbxDatosClientePlaca.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxDatosClientePlaca.Location = New System.Drawing.Point(9, 4)
        Me.gbxDatosClientePlaca.Name = "gbxDatosClientePlaca"
        Me.gbxDatosClientePlaca.Size = New System.Drawing.Size(486, 223)
        Me.gbxDatosClientePlaca.TabIndex = 0
        Me.gbxDatosClientePlaca.TabStop = False
        '
        'txtPlacaNueva
        '
        Me.txtPlacaNueva.Location = New System.Drawing.Point(330, 189)
        Me.txtPlacaNueva.Name = "txtPlacaNueva"
        Me.txtPlacaNueva.Size = New System.Drawing.Size(150, 22)
        Me.txtPlacaNueva.TabIndex = 18
        '
        'lblPlacaActual
        '
        Me.lblPlacaActual.AutoSize = True
        Me.lblPlacaActual.Location = New System.Drawing.Point(176, 170)
        Me.lblPlacaActual.Name = "lblPlacaActual"
        Me.lblPlacaActual.Size = New System.Drawing.Size(141, 16)
        Me.lblPlacaActual.TabIndex = 17
        Me.lblPlacaActual.Text = "Identificador Actual"
        '
        'txtPlacaActual
        '
        Me.txtPlacaActual.Location = New System.Drawing.Point(172, 189)
        Me.txtPlacaActual.Name = "txtPlacaActual"
        Me.txtPlacaActual.ReadOnly = True
        Me.txtPlacaActual.Size = New System.Drawing.Size(150, 22)
        Me.txtPlacaActual.TabIndex = 16
        '
        'lblNuevaPlaca
        '
        Me.lblNuevaPlaca.AutoSize = True
        Me.lblNuevaPlaca.Location = New System.Drawing.Point(333, 170)
        Me.lblNuevaPlaca.Name = "lblNuevaPlaca"
        Me.lblNuevaPlaca.Size = New System.Drawing.Size(143, 16)
        Me.lblNuevaPlaca.TabIndex = 8
        Me.lblNuevaPlaca.Text = "Identificador Nuevo"
        '
        'txtPeriodoPagado
        '
        Me.txtPeriodoPagado.Location = New System.Drawing.Point(8, 189)
        Me.txtPeriodoPagado.Name = "txtPeriodoPagado"
        Me.txtPeriodoPagado.ReadOnly = True
        Me.txtPeriodoPagado.Size = New System.Drawing.Size(155, 22)
        Me.txtPeriodoPagado.TabIndex = 15
        '
        'txtStatus
        '
        Me.txtStatus.Location = New System.Drawing.Point(402, 142)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(78, 22)
        Me.txtStatus.TabIndex = 14
        '
        'txtColonia
        '
        Me.txtColonia.Location = New System.Drawing.Point(8, 142)
        Me.txtColonia.Name = "txtColonia"
        Me.txtColonia.ReadOnly = True
        Me.txtColonia.Size = New System.Drawing.Size(388, 22)
        Me.txtColonia.TabIndex = 13
        '
        'txtNumero
        '
        Me.txtNumero.Location = New System.Drawing.Point(402, 93)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.ReadOnly = True
        Me.txtNumero.Size = New System.Drawing.Size(78, 22)
        Me.txtNumero.TabIndex = 12
        '
        'txtCalle
        '
        Me.txtCalle.Location = New System.Drawing.Point(8, 93)
        Me.txtCalle.Name = "txtCalle"
        Me.txtCalle.ReadOnly = True
        Me.txtCalle.Size = New System.Drawing.Size(388, 22)
        Me.txtCalle.TabIndex = 11
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(96, 44)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.ReadOnly = True
        Me.txtNombre.Size = New System.Drawing.Size(384, 22)
        Me.txtNombre.TabIndex = 10
        '
        'txtContrato
        '
        Me.txtContrato.Location = New System.Drawing.Point(8, 44)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.ReadOnly = True
        Me.txtContrato.Size = New System.Drawing.Size(78, 22)
        Me.txtContrato.TabIndex = 9
        '
        'lblUltMes
        '
        Me.lblUltMes.AutoSize = True
        Me.lblUltMes.Location = New System.Drawing.Point(11, 170)
        Me.lblUltMes.Name = "lblUltMes"
        Me.lblUltMes.Size = New System.Drawing.Size(122, 16)
        Me.lblUltMes.TabIndex = 6
        Me.lblUltMes.Text = "Periodo Pagado"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(406, 123)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(51, 16)
        Me.lblStatus.TabIndex = 5
        Me.lblStatus.Text = "Status"
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Location = New System.Drawing.Point(11, 123)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(50, 16)
        Me.lblColonia.TabIndex = 4
        Me.lblColonia.Text = "Barrio"
        '
        'lblNumero
        '
        Me.lblNumero.AutoSize = True
        Me.lblNumero.Location = New System.Drawing.Point(406, 74)
        Me.lblNumero.Name = "lblNumero"
        Me.lblNumero.Size = New System.Drawing.Size(32, 16)
        Me.lblNumero.TabIndex = 3
        Me.lblNumero.Text = "No."
        '
        'lblCalle
        '
        Me.lblCalle.AutoSize = True
        Me.lblCalle.Location = New System.Drawing.Point(11, 74)
        Me.lblCalle.Name = "lblCalle"
        Me.lblCalle.Size = New System.Drawing.Size(44, 16)
        Me.lblCalle.TabIndex = 2
        Me.lblCalle.Text = "Calle"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(100, 25)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(63, 16)
        Me.lblNombre.TabIndex = 1
        Me.lblNombre.Text = "Nombre"
        '
        'lblContrato
        '
        Me.lblContrato.AutoSize = True
        Me.lblContrato.Location = New System.Drawing.Point(11, 25)
        Me.lblContrato.Name = "lblContrato"
        Me.lblContrato.Size = New System.Drawing.Size(66, 16)
        Me.lblContrato.TabIndex = 0
        Me.lblContrato.Text = "Contrato"
        '
        'btnCambiar
        '
        Me.btnCambiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCambiar.Location = New System.Drawing.Point(121, 233)
        Me.btnCambiar.Name = "btnCambiar"
        Me.btnCambiar.Size = New System.Drawing.Size(117, 30)
        Me.btnCambiar.TabIndex = 1
        Me.btnCambiar.Text = "&Cambiar"
        Me.btnCambiar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(265, 233)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(117, 30)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'FrmCambiaClientePlaca
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(502, 270)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnCambiar)
        Me.Controls.Add(Me.gbxDatosClientePlaca)
        Me.Name = "FrmCambiaClientePlaca"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Cliente"
        Me.gbxDatosClientePlaca.ResumeLayout(False)
        Me.gbxDatosClientePlaca.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxDatosClientePlaca As System.Windows.Forms.GroupBox
    Friend WithEvents txtPlacaActual As System.Windows.Forms.TextBox
    Friend WithEvents txtPeriodoPagado As System.Windows.Forms.TextBox
    Friend WithEvents txtStatus As System.Windows.Forms.TextBox
    Friend WithEvents txtColonia As System.Windows.Forms.TextBox
    Friend WithEvents txtNumero As System.Windows.Forms.TextBox
    Friend WithEvents txtCalle As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents lblNuevaPlaca As System.Windows.Forms.Label
    Friend WithEvents lblUltMes As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents lblNumero As System.Windows.Forms.Label
    Friend WithEvents lblCalle As System.Windows.Forms.Label
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents lblContrato As System.Windows.Forms.Label
    Friend WithEvents btnCambiar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtPlacaNueva As System.Windows.Forms.TextBox
    Friend WithEvents lblPlacaActual As System.Windows.Forms.Label
End Class
