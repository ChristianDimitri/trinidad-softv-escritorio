﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambioDeTipoDeServicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bnBuscar = New System.Windows.Forms.Button()
        Me.cbDeTvADig = New System.Windows.Forms.RadioButton()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.gbServicioTv = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbServicioTv = New System.Windows.Forms.ComboBox()
        Me.nudTvConPago = New System.Windows.Forms.NumericUpDown()
        Me.nudTvSinPago = New System.Windows.Forms.NumericUpDown()
        Me.cbDeDigATv = New System.Windows.Forms.RadioButton()
        Me.bnGuardar = New System.Windows.Forms.Button()
        Me.gbServicioDig = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbAnalogas = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.TextBox35 = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.TextBox31 = New System.Windows.Forms.TextBox()
        Me.nudExtensiones = New System.Windows.Forms.NumericUpDown()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.cbServicioDig = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.nudCajas = New System.Windows.Forms.NumericUpDown()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbServicioTv.SuspendLayout()
        CType(Me.nudTvConPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudTvSinPago, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbServicioDig.SuspendLayout()
        CType(Me.nudExtensiones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCajas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnBuscar
        '
        Me.bnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscar.Location = New System.Drawing.Point(180, 80)
        Me.bnBuscar.Name = "bnBuscar"
        Me.bnBuscar.Size = New System.Drawing.Size(35, 23)
        Me.bnBuscar.TabIndex = 3
        Me.bnBuscar.Text = "..."
        Me.bnBuscar.UseVisualStyleBackColor = True
        '
        'cbDeTvADig
        '
        Me.cbDeTvADig.AutoSize = True
        Me.cbDeTvADig.Checked = True
        Me.cbDeTvADig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDeTvADig.Location = New System.Drawing.Point(32, 29)
        Me.cbDeTvADig.Name = "cbDeTvADig"
        Me.cbDeTvADig.Size = New System.Drawing.Size(101, 19)
        Me.cbDeTvADig.TabIndex = 0
        Me.cbDeTvADig.TabStop = True
        Me.cbDeTvADig.Text = "De TV a Dig"
        Me.cbDeTvADig.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(29, 62)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(65, 15)
        Me.CMBLabel1.TabIndex = 2
        Me.CMBLabel1.Text = "Contrato:"
        '
        'tbContrato
        '
        Me.tbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContrato.Location = New System.Drawing.Point(32, 80)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(142, 21)
        Me.tbContrato.TabIndex = 2
        '
        'gbServicioTv
        '
        Me.gbServicioTv.Controls.Add(Me.Label4)
        Me.gbServicioTv.Controls.Add(Me.Label3)
        Me.gbServicioTv.Controls.Add(Me.Label2)
        Me.gbServicioTv.Controls.Add(Me.cbServicioTv)
        Me.gbServicioTv.Controls.Add(Me.nudTvConPago)
        Me.gbServicioTv.Controls.Add(Me.nudTvSinPago)
        Me.gbServicioTv.Enabled = False
        Me.gbServicioTv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbServicioTv.Location = New System.Drawing.Point(413, 118)
        Me.gbServicioTv.Name = "gbServicioTv"
        Me.gbServicioTv.Size = New System.Drawing.Size(364, 183)
        Me.gbServicioTv.TabIndex = 5
        Me.gbServicioTv.TabStop = False
        Me.gbServicioTv.Text = "Servicio de TV"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 15)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Servicio:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 121)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "TV con Pago:"
        Me.Label3.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "TV Adic:"
        '
        'cbServicioTv
        '
        Me.cbServicioTv.DisplayMember = "Descripcion"
        Me.cbServicioTv.FormattingEnabled = True
        Me.cbServicioTv.Location = New System.Drawing.Point(80, 35)
        Me.cbServicioTv.Name = "cbServicioTv"
        Me.cbServicioTv.Size = New System.Drawing.Size(278, 23)
        Me.cbServicioTv.TabIndex = 0
        Me.cbServicioTv.ValueMember = "Clv_Servicio"
        '
        'nudTvConPago
        '
        Me.nudTvConPago.Location = New System.Drawing.Point(108, 119)
        Me.nudTvConPago.Name = "nudTvConPago"
        Me.nudTvConPago.Size = New System.Drawing.Size(36, 21)
        Me.nudTvConPago.TabIndex = 2
        Me.nudTvConPago.Visible = False
        '
        'nudTvSinPago
        '
        Me.nudTvSinPago.Location = New System.Drawing.Point(80, 80)
        Me.nudTvSinPago.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudTvSinPago.Name = "nudTvSinPago"
        Me.nudTvSinPago.Size = New System.Drawing.Size(36, 21)
        Me.nudTvSinPago.TabIndex = 1
        '
        'cbDeDigATv
        '
        Me.cbDeDigATv.AutoSize = True
        Me.cbDeDigATv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDeDigATv.Location = New System.Drawing.Point(139, 29)
        Me.cbDeDigATv.Name = "cbDeDigATv"
        Me.cbDeDigATv.Size = New System.Drawing.Size(101, 19)
        Me.cbDeDigATv.TabIndex = 1
        Me.cbDeDigATv.Text = "De Dig a TV"
        Me.cbDeDigATv.UseVisualStyleBackColor = True
        '
        'bnGuardar
        '
        Me.bnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardar.Location = New System.Drawing.Point(669, 12)
        Me.bnGuardar.Name = "bnGuardar"
        Me.bnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.bnGuardar.TabIndex = 6
        Me.bnGuardar.Text = "&GUARDAR"
        Me.bnGuardar.UseVisualStyleBackColor = True
        '
        'gbServicioDig
        '
        Me.gbServicioDig.Controls.Add(Me.Label11)
        Me.gbServicioDig.Controls.Add(Me.tbAnalogas)
        Me.gbServicioDig.Controls.Add(Me.Label10)
        Me.gbServicioDig.Controls.Add(Me.Button1)
        Me.gbServicioDig.Controls.Add(Me.CheckBox4)
        Me.gbServicioDig.Controls.Add(Me.CheckBox3)
        Me.gbServicioDig.Controls.Add(Me.Label59)
        Me.gbServicioDig.Controls.Add(Me.TextBox35)
        Me.gbServicioDig.Controls.Add(Me.Label58)
        Me.gbServicioDig.Controls.Add(Me.TextBox31)
        Me.gbServicioDig.Controls.Add(Me.nudExtensiones)
        Me.gbServicioDig.Controls.Add(Me.Label57)
        Me.gbServicioDig.Controls.Add(Me.Label6)
        Me.gbServicioDig.Controls.Add(Me.ComboBox1)
        Me.gbServicioDig.Controls.Add(Me.cbServicioDig)
        Me.gbServicioDig.Controls.Add(Me.Label5)
        Me.gbServicioDig.Controls.Add(Me.CheckBox1)
        Me.gbServicioDig.Controls.Add(Me.Label9)
        Me.gbServicioDig.Controls.Add(Me.Label1)
        Me.gbServicioDig.Controls.Add(Me.ComboBox2)
        Me.gbServicioDig.Controls.Add(Me.Label8)
        Me.gbServicioDig.Controls.Add(Me.Label7)
        Me.gbServicioDig.Controls.Add(Me.nudCajas)
        Me.gbServicioDig.Enabled = False
        Me.gbServicioDig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbServicioDig.Location = New System.Drawing.Point(32, 118)
        Me.gbServicioDig.Name = "gbServicioDig"
        Me.gbServicioDig.Size = New System.Drawing.Size(364, 225)
        Me.gbServicioDig.TabIndex = 4
        Me.gbServicioDig.TabStop = False
        Me.gbServicioDig.Text = "Servicio Digital"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(230, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 15)
        Me.Label11.TabIndex = 108
        Me.Label11.Text = "Tv´s Análogas"
        '
        'tbAnalogas
        '
        Me.tbAnalogas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbAnalogas.Location = New System.Drawing.Point(173, 25)
        Me.tbAnalogas.Name = "tbAnalogas"
        Me.tbAnalogas.Size = New System.Drawing.Size(51, 21)
        Me.tbAnalogas.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(170, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(166, 15)
        Me.Label10.TabIndex = 107
        Me.Label10.Text = "Actualmente Cuenta con:"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(233, 150)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(111, 23)
        Me.Button1.TabIndex = 106
        Me.Button1.Text = "Guardar STB"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CheckBox4.Location = New System.Drawing.Point(4, 401)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(79, 19)
        Me.CheckBox4.TabIndex = 105
        Me.CheckBox4.Text = ": Tarjeta"
        Me.CheckBox4.UseVisualStyleBackColor = True
        Me.CheckBox4.Visible = False
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CheckBox3.Location = New System.Drawing.Point(8, 354)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(60, 19)
        Me.CheckBox3.TabIndex = 104
        Me.CheckBox3.Text = ": STB"
        Me.CheckBox3.UseVisualStyleBackColor = True
        Me.CheckBox3.Visible = False
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label59.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label59.Location = New System.Drawing.Point(4, 324)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(83, 15)
        Me.Label59.TabIndex = 103
        Me.Label59.Text = "Cuenta con:"
        Me.Label59.Visible = False
        '
        'TextBox35
        '
        Me.TextBox35.Enabled = False
        Me.TextBox35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox35.Location = New System.Drawing.Point(90, 401)
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.Size = New System.Drawing.Size(268, 21)
        Me.TextBox35.TabIndex = 102
        Me.TextBox35.Visible = False
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label58.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label58.Location = New System.Drawing.Point(88, 381)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(274, 15)
        Me.Label58.TabIndex = 101
        Me.Label58.Text = "Capture el número de serie de su Tarjeta:"
        Me.Label58.Visible = False
        '
        'TextBox31
        '
        Me.TextBox31.Enabled = False
        Me.TextBox31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox31.Location = New System.Drawing.Point(90, 352)
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.Size = New System.Drawing.Size(268, 21)
        Me.TextBox31.TabIndex = 100
        Me.TextBox31.Visible = False
        '
        'nudExtensiones
        '
        Me.nudExtensiones.Location = New System.Drawing.Point(173, 194)
        Me.nudExtensiones.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudExtensiones.Name = "nudExtensiones"
        Me.nudExtensiones.Size = New System.Drawing.Size(36, 21)
        Me.nudExtensiones.TabIndex = 2
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label57.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label57.Location = New System.Drawing.Point(88, 332)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(255, 15)
        Me.Label57.TabIndex = 99
        Me.Label57.Text = "Capture el número de serie de su STB:"
        Me.Label57.Visible = False
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(7, 194)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(166, 20)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Extensiones Análogas:"
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(9, 160)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(200, 23)
        Me.ComboBox1.TabIndex = 13
        '
        'cbServicioDig
        '
        Me.cbServicioDig.DisplayMember = "Descripcion"
        Me.cbServicioDig.FormattingEnabled = True
        Me.cbServicioDig.Location = New System.Drawing.Point(74, 111)
        Me.cbServicioDig.Name = "cbServicioDig"
        Me.cbServicioDig.Size = New System.Drawing.Size(284, 23)
        Me.cbServicioDig.TabIndex = 0
        Me.cbServicioDig.ValueMember = "Clv_Servicio"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(1, 114)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 15)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Servicio:"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(13, 251)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(98, 19)
        Me.CheckBox1.TabIndex = 19
        Me.CheckBox1.Text = "STB Propia"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(35, 76)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 15)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "STB:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(9, 141)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 15)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Modelo de STB: "
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(81, 73)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(235, 23)
        Me.ComboBox2.TabIndex = 17
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(1, 54)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(167, 15)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Configura STB a instalar:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(1, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 15)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "No. STB:"
        '
        'nudCajas
        '
        Me.nudCajas.Location = New System.Drawing.Point(74, 23)
        Me.nudCajas.Maximum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudCajas.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudCajas.Name = "nudCajas"
        Me.nudCajas.Size = New System.Drawing.Size(36, 21)
        Me.nudCajas.TabIndex = 1
        Me.nudCajas.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(669, 54)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 7
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'FrmCambioDeTipoDeServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(817, 350)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.gbServicioDig)
        Me.Controls.Add(Me.bnGuardar)
        Me.Controls.Add(Me.cbDeDigATv)
        Me.Controls.Add(Me.gbServicioTv)
        Me.Controls.Add(Me.tbContrato)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.cbDeTvADig)
        Me.Controls.Add(Me.bnBuscar)
        Me.Name = "FrmCambioDeTipoDeServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Tipo de Servicio"
        Me.gbServicioTv.ResumeLayout(False)
        Me.gbServicioTv.PerformLayout()
        CType(Me.nudTvConPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudTvSinPago, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbServicioDig.ResumeLayout(False)
        Me.gbServicioDig.PerformLayout()
        CType(Me.nudExtensiones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCajas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnBuscar As System.Windows.Forms.Button
    Friend WithEvents cbDeTvADig As System.Windows.Forms.RadioButton
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents gbServicioTv As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbServicioTv As System.Windows.Forms.ComboBox
    Friend WithEvents nudTvConPago As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudTvSinPago As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbDeDigATv As System.Windows.Forms.RadioButton
    Friend WithEvents bnGuardar As System.Windows.Forms.Button
    Friend WithEvents gbServicioDig As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbServicioDig As System.Windows.Forms.ComboBox
    Friend WithEvents nudExtensiones As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudCajas As System.Windows.Forms.NumericUpDown
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents TextBox35 As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents TextBox31 As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbAnalogas As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
