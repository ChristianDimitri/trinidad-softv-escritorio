﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCambiaClientePlaca
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.lblPlaca = New System.Windows.Forms.Label()
        Me.txtPlaca = New System.Windows.Forms.TextBox()
        Me.btnBuscarPlaca = New System.Windows.Forms.Button()
        Me.dgvContratosPlacas = New System.Windows.Forms.DataGridView()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxIngresaPlaca = New System.Windows.Forms.GroupBox()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.placa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.modificar = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.dgvContratosPlacas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxIngresaPlaca.SuspendLayout()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'lblPlaca
        '
        Me.lblPlaca.AutoSize = True
        Me.lblPlaca.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlaca.Location = New System.Drawing.Point(16, 33)
        Me.lblPlaca.Name = "lblPlaca"
        Me.lblPlaca.Size = New System.Drawing.Size(102, 16)
        Me.lblPlaca.TabIndex = 0
        Me.lblPlaca.Text = "Identificador :"
        '
        'txtPlaca
        '
        Me.txtPlaca.Location = New System.Drawing.Point(120, 33)
        Me.txtPlaca.Name = "txtPlaca"
        Me.txtPlaca.Size = New System.Drawing.Size(174, 20)
        Me.txtPlaca.TabIndex = 1
        '
        'btnBuscarPlaca
        '
        Me.btnBuscarPlaca.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarPlaca.Location = New System.Drawing.Point(300, 30)
        Me.btnBuscarPlaca.Name = "btnBuscarPlaca"
        Me.btnBuscarPlaca.Size = New System.Drawing.Size(100, 23)
        Me.btnBuscarPlaca.TabIndex = 2
        Me.btnBuscarPlaca.Text = "&Buscar"
        Me.btnBuscarPlaca.UseVisualStyleBackColor = True
        '
        'dgvContratosPlacas
        '
        Me.dgvContratosPlacas.AllowUserToAddRows = False
        Me.dgvContratosPlacas.AllowUserToDeleteRows = False
        Me.dgvContratosPlacas.BackgroundColor = System.Drawing.Color.White
        Me.dgvContratosPlacas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvContratosPlacas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.contrato, Me.nombre, Me.placa, Me.modificar})
        Me.dgvContratosPlacas.Location = New System.Drawing.Point(12, 99)
        Me.dgvContratosPlacas.Name = "dgvContratosPlacas"
        Me.dgvContratosPlacas.ReadOnly = True
        Me.dgvContratosPlacas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvContratosPlacas.Size = New System.Drawing.Size(592, 264)
        Me.dgvContratosPlacas.TabIndex = 3
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'gbxIngresaPlaca
        '
        Me.gbxIngresaPlaca.Controls.Add(Me.btnBuscarPlaca)
        Me.gbxIngresaPlaca.Controls.Add(Me.lblPlaca)
        Me.gbxIngresaPlaca.Controls.Add(Me.txtPlaca)
        Me.gbxIngresaPlaca.Location = New System.Drawing.Point(12, 12)
        Me.gbxIngresaPlaca.Name = "gbxIngresaPlaca"
        Me.gbxIngresaPlaca.Size = New System.Drawing.Size(592, 81)
        Me.gbxIngresaPlaca.TabIndex = 4
        Me.gbxIngresaPlaca.TabStop = False
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'contrato
        '
        Me.contrato.DataPropertyName = "CONTRATO"
        Me.contrato.HeaderText = "Contrato"
        Me.contrato.Name = "contrato"
        Me.contrato.ReadOnly = True
        '
        'nombre
        '
        Me.nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nombre.DataPropertyName = "NOMBRE"
        Me.nombre.HeaderText = "Nombre"
        Me.nombre.Name = "nombre"
        Me.nombre.ReadOnly = True
        '
        'placa
        '
        Me.placa.DataPropertyName = "PLACA"
        Me.placa.HeaderText = "Identificador"
        Me.placa.Name = "placa"
        Me.placa.ReadOnly = True
        '
        'modificar
        '
        Me.modificar.DataPropertyName = "ACCION"
        Me.modificar.HeaderText = "Modificar"
        Me.modificar.Name = "modificar"
        Me.modificar.ReadOnly = True
        Me.modificar.Text = "Modificar"
        '
        'BrwCambiaClientePlaca
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(616, 387)
        Me.Controls.Add(Me.gbxIngresaPlaca)
        Me.Controls.Add(Me.dgvContratosPlacas)
        Me.Name = "BrwCambiaClientePlaca"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Busca Cliente"
        CType(Me.dgvContratosPlacas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxIngresaPlaca.ResumeLayout(False)
        Me.gbxIngresaPlaca.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents lblPlaca As System.Windows.Forms.Label
    Friend WithEvents txtPlaca As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscarPlaca As System.Windows.Forms.Button
    Friend WithEvents dgvContratosPlacas As System.Windows.Forms.DataGridView
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents gbxIngresaPlaca As System.Windows.Forms.GroupBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents placa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents modificar As System.Windows.Forms.DataGridViewButtonColumn
End Class
