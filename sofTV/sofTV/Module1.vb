Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data

Module Module1
    Public GlobndImprimirQueja As Boolean = False
    Public GlobndImprimirRobo As Boolean = False
    'Servivios Habilitados
    Public GloHabilitadosTV As Boolean = False
    Public GloHabilitadosDIG As Boolean = False
    Public GloHabilitadosNET As Boolean = False
    Public GloHabilitadosTEL As Boolean = False

    Public gloColoniaCliente As Integer
    Public gloCiudadCliente As Integer

    Public GloTipoCablemodemR As Integer = 0

    Public GloEsHotelR As Boolean = False

    Public GloComboSinDigital As Integer = 0
    Public GloPreguntaBonContProp As Integer = 0
    Public GloImprimirOrdenesPant As Integer = 0

    Public GloTvSinPago As Integer = 0
    Public GloTvConpago As Integer = 0
    Public GloImprimeTicket As Boolean = False
    Public GloExportaTicket As Boolean = False

    Public GloInstalaRouter As Boolean = False

    Public GloTarjetaPropia As Integer = 0

    Public GloConPlaca As Boolean = False
    Public GloActivarCFD As Integer = 0
    Public GloActivarFibra As Boolean = 0

    Public GloCajaPropia As Integer = 0
    Public GloCajaenBaja As Integer = 0
    Public GloCajaOTarjeta As Integer = 0
    Public GloClvTarjDig As String = 0

    Public gloClvTipSerReportes As Integer = 0
    Public LocMasivaCapar As Boolean = False

    Public BndCambioServicio As String = "R"
    Public LocNoArticuloCajas As Integer = 0
    Public BndValRecon As Integer = 0
    Public BndRecibiRecontratacion As Long = 0
    Public op As String = Nothing
    Public IMPRESORA_TARJETAS As String = ""
    Public IMPRESORA_CONTRATOS As String = ""
    Public eClv_Sector As Integer = 0
    Public eIdPoste As Integer = 0
    Public eIdTap As Integer = 0
    Public OpProrroga As Integer = 0
    Public Bnd_Orden As Boolean = False
    Public VISIBLE_IEPS As Boolean = False
    Public sms As Boolean = False
    Public ComentarioAgenda As String = Nothing
    Public Bnprocesa As Boolean = False
    Public MiConexion As String = Nothing
    Public MiConexionProspectos As String = Nothing
    Public LocOpTel As String = Nothing
    Public LocDescrip As String = Nothing
    Public LocClv_Tel As Long = Nothing
    Public LocBndTel As Boolean = False
    Public LocTipoPaquerebnd As Boolean = False
    Public LocGloTipoPaquete As Long = 0
    Public locbndrepcancelaciones As Boolean = False
    Public LocOpTipoPaqAdic As String = Nothing
    Public Locbndrepcontspago As Boolean = False
    Public LOCBNDREPDATOSBANC As Boolean = False
    Public ImpresoraContatos As String = Nothing
    Public LocMontoEsp As String = Nothing
    Public LocFechaesp As String = Nothing
    Public LocContratoesp As String = Nothing
    Public Locclv_cobro As Integer = 0
    Public Locbndactualiza As Boolean = False
    Public LocOpCargosEsp As String = Nothing
    Public Locbndcontrato3 As Boolean = False
    Public locpagosespeciales As Boolean = False
    Public locbndreportbit As Boolean = False
    Public loc1op As Integer = 0
    Public Loc1contrato As Integer = 0
    Public Loc1clv_sistema As String = Nothing
    Public Loc1Fechaini As String = Nothing
    Public Loc1Fechafin As String = Nothing
    Public Loc1clv_usuario As String = Nothing
    Public Loc1clv_pantalla As String = Nothing
    Public Loc1clv_control As String = Nothing
    Public Loc1horaini As String = Nothing
    Public Loc1horafin As String = Nothing
    Public LocbndProceso1 As Boolean = False
    Public Locbndvalcliente As Boolean = False
    Public Locbndcontrato1 As Boolean = False
    Public Locbndcontrato2 As Boolean = False
    Public Glocontratoini As Integer = 0
    Public Glocontratofin As Integer = 0
    Public Locformulario As Integer = 0
    Public LocopPoliza As String = Nothing
    Public LocbndPolizaCiudad As Boolean = False
    Public LocbndPolizaCiudad2 As Boolean = False
    Public Locbndactualizapoiza As Boolean = False
    Public LocGloClv_poliza As Integer = 0
    Public LocFecha1Agnd As String = Nothing
    Public LocFecha2Agnd As String = Nothing
    Public LocClv_tecnico As Integer = 0
    Public LocopAgnd As Integer = 0
    Public Locbndagnd As Boolean = False
    Public LocbndProceso As Boolean = False
    Public LocProceso As Integer = 0
    Public LocBndFiltro1 As Integer = 0
    Public LocBndFiltro2 As Integer = 0
    Public GloTarjVerde As Boolean = False
    Public GloTarjRoja As Boolean = False

    Public LocOpVisita As String = Nothing
    Public Locclv_visita As Integer = 0
    Public LocBndVisitas As Boolean = False
    Public RutaReportes As String
    Public glotiposervicioppal As Integer
    Public GloTipoCablemodem As Integer
    Public GloClvUsuario As Integer
    Public GloTickets As Boolean = False
    Public IdSistema As String = Nothing
    Public eGuardarCliente As Boolean = False
    Public LocFechaTec1 As Date = DateValue("01/01/1900")
    Public LocFechaTec2 As Date = DateValue("01/01/1900")
    Public LocTipRepTec As Integer = 0
    Public bndreportetec As Boolean = False
    Public eImprimeContrato As Boolean
    Public eErrorContrato As Integer
    Public eContrato As Long
    Public rNombre As String = ""
    Public eClvMotivo As Long
    Public eReImpresion As Integer = 0
    Public eClv_Servicio As Integer = 0
    Public eCveRango As Integer = 0
    Public bndCCABM As Boolean = False
    Public eCveComision As Integer = 0
    Public eRangoInferior As Integer = 0
    Public eRangoSuperior As Integer = 0
    Public ePrecio As Single = 0
    Public eOpcion As Char = Nothing
    Public eValidaRango As Integer = 0
    Public eValidaRangoComision As Integer = 0
    Public eFechaIni As Date = Today
    Public eFechaFin As Date = Today
    Public eClv_Vendedor As Long = 0
    Public eOp As Integer = 0
    Public eRInferior As Integer = 0
    Public eRSuperior As Integer = 0
    Public eTituloComision As String = Nothing
    Public eOpVentas As Integer = 0
    Public eClv_Session As Long = 0
    Public ePrimero As Boolean = False
    Public eSegundo As Boolean = False
    Public eCont As Integer = 0
    Public eInst As Integer = 0
    Public eDesc As Integer = 0
    Public eSusp As Integer = 0
    Public eBaja As Integer = 0
    Public eFuera As Integer = 0
    Public eTempo As Integer = 0
    Public eTipSer As Integer = 0
    Public eServicio As String = Nothing
    Public eClv_Unica As Integer = 0
    Public eConsecutivo As Integer = 0
    Public eClv_Encargado As Integer = 0
    Public eStatusOrdSer As Char = Nothing
    Public eActTecnico As Boolean = True
    Public eEntraUM As Boolean = False
    Public eEntraUMB As Boolean = False
    Public eRespuesta As Integer = 0
    Public eCabModPropio As Boolean = False
    Public eGloContrato As Long = 0
    Public eGloContratoAux As Long = 0
    Public eGloSubContrato As Long = 0
    Public eGloSubContratoAux As Long = 0
    Public eGloClvSession As Long = 0
    Public eResAco As Integer = 0
    Public eBndReportePPE As Boolean = False
    Public eRobo As Boolean = False
    Public eEntraMaestro As Boolean = False
    Public eAccesoAdmin As Boolean = False
    Public eClv_Usuario As Integer = 0
    Public eOpPPE As Integer = 0

    Public eConsec As Long = 0
    Public eContratoRec As Long = 0
    Public eClv_UnicaNetDig As Long = 0
    Public eClv_UnicaNet As Long = 0
    Public eGloDescuento As Integer = 0
    Public eGloTipSerDesc As Integer = 0
    Public eBndDesc As Boolean = False
    Public GloCiudad As String = Nothing
    Public GloClv_MotCan As Integer = 0
    Public eBndMenIns As Boolean = False
    Public eBndGraf As Boolean = False
    Public eClv_Sucursal As Integer = 0
    Public eNombre As String = Nothing
    Public eBndVen As Boolean = False
    Public eClv_MotAtenTel As Integer = 0
    Public eClave As Long = 0
    Public eMesIni As Integer = 0
    Public eMesFin As Integer = 0
    Public eAnioIni As Integer = 0
    Public eAnioFin As Integer = 0
    Public eStrMesIni As String = Nothing
    Public eStrMesFin As String = Nothing
    Public eBndOpVentas As Boolean = False
    Public eBndContratoF As Boolean = False
    Public eTrabajo As String = Nothing
    Public eClv_Grupo As Integer = 0
    Public eGrupo As String = Nothing
    Public eBndCombo As Boolean = False
    Public eClv_Descuento As Integer = 0
    Public eNombreCombo As String = Nothing
    Public eClv_TipoCliente As Integer = 0
    Public eClv_PaqAdi As Integer = 0
    Public eBndPaqAdi As Boolean = False
    Public eBndAtenTelGraf As Boolean = False
    Public eOP1 As Integer = 0
    Public eOP2 As Integer = 0
    Public eOP3 As Integer = 0
    Public eOpIrdeto As Integer = 0
    Public eBndIrdeto As Boolean = False
    Public eOpContratoF As Integer = 0
    Public eClaveCorreo As Long = 0
    Public eOpCorreo As Integer = 0
    Public eClv_Tipo_Paquete_Adicional As Long = 0
    Public eOpCNRCNRDIG As Integer = 0
    Public eBndClv_CablemodemSelPropio As Boolean = False
    Public eBndMismoNombre As Boolean = False
    Public eBndEntraDire As Boolean = False
    Public eClavePorRecuperar As Long = 0
    Public eBndPorRecuperar As Boolean = False
    Public eBndCargoEsp As Boolean = False
    Public eBndBonifEsp As Boolean = False
    Public eClv_TipSer As Integer = 0
    Public eRealizado As String = Nothing
    Public eTipo As String = String.Empty
    Public eBndComisiones As Boolean = False
    Public eClv_Rango As Integer = 0
    Public eTitulo As String = String.Empty
    Public eSeVende As Boolean = False
    Public ePropio As Boolean = False
    Public eEjecutivo As Boolean = False
    Public eC As Boolean = False
    Public eI As Boolean = False
    Public eD As Boolean = False
    Public eS As Boolean = False
    Public eB As Boolean = False
    Public eF As Boolean = False
    Public eAntIni As Integer = 0
    Public eAntFin As Integer = 0
    Public eRepAnt As Char = String.Empty
    Public eTipoPen As Integer = 0
    Public eIDPregunta As Integer = 0
    Public eBnd As Boolean = False
    Public eBndRespuesta As Boolean = False
    Public eIDPreguntaAux As Integer = 0
    Public eIDEncuesta As Integer = 0
    Public eEncuestaNombre As String = String.Empty
    Public eBndEncuesta As Boolean = False
    Public eBndRepEncuesta As Boolean = False
    Public eBndNet As Boolean = False
    Public eBndDig As Boolean = False
    Public eBndEntregaAparato As Boolean = False
    Public eIdPromocion As Integer = 0
    Public eBndPromocion As Boolean = 0
    Public eBndDeco As Boolean = False
    Public eBndPenetracion As Boolean = False
    Public RTapPenetracion As Boolean = False
    Public eId As Integer = 0
    Public eRefrescar As Boolean = False
    '-----------------------------------------
    Public LocGloOpRep As Integer = 0
    Public LocTarjNo_Contrato As Integer = 0
    Public GloTipo As String = Nothing
    Public GloContrato As Long = 0
    Public GloOpFacturas As Integer = 0
    Public eRes As Integer = 0
    Public eMsj As String = String.Empty
    Public TvsMsj As String = String.Empty

    Public Pasafibra As Integer = 0
    'Public eBnd As Boolean = False
    Public eResString As Char
    Public opcFrm As Integer
    Public GloOpEtiqueta As String = "0"
    Public GloBndEtiqueta As Boolean = False
    Public GloClv_tipser2 As Integer
    Public Locorden As Integer = 0
    Public LocServicios As Boolean = False
    Public LocBndC As Boolean = False
    Public LocBndB As Boolean = False
    Public LocBndI As Boolean = False
    Public LocBndD As Boolean = False
    Public LocBndS As Boolean = False
    Public LocBndF As Boolean = False
    Public LocBndDT As Boolean = False
    Public LocBndRepMix As Boolean = False
    Public oprepetiq As Integer = 0
    Public LocClv_session As Integer
    Public LocTipoClientes As Boolean = False
    Public LocOp As Integer
    Public Recordatorio As Integer = 0
    Public GloPermisoCortesia As Integer = 0
    Public GloOpPermiso As Integer = 0
    Public GloCmdBanco As Integer = 0
    Public Locclv_tec As Integer = 0
    Public clv_sessionTecnico As Integer = 0
    Public LocValida1 As Boolean = False
    Public Locclv_folio As Integer = 0
    Public LocNo_Bitacora As Integer = 0
    Public GloContratoNet2 As Long = 0
    Public GloClvUnicaNet As Long = 0
    Public gLOVERgUARDA As Integer = 0
    '
    Public Locclv_txt As String = Nothing
    Public LocDescr As String = Nothing
    '
    Public GloOpRep As String
    Public GloBndSelBanco As Boolean = False
    Public GloSelBanco As Integer

    Public glolec As Short
    Public gloescr As Short
    Public gloctr As Short
    Public GloTipoUsuario As Integer
    Public oOpcionMenu As ToolStripMenuItem
    '
    Public GloControlaReloj As Integer = 0
    Public GloBloqueaDetalle As Boolean = False
    Public GloSucursal As String = Nothing ' Sucursal : Aguascalientes,Ags. "
    '
    Public GloClv_UnicaDig_Nuevo As Long = 0
    Public GloContratoDig_Nuevo As Long = 0
    Public GloGuardarDig As Boolean = False
    '
    Public GloClv_UnicaNet_Nuevo As Long = 0
    Public GloContratonet_Nuevo As Long = 0
    Public GloGuardarNet As Boolean = False
    Public GloTrabajo_OrdSer As String
    Public Bloquea As Boolean = False
    Public GloBndTrabajo As Boolean = False
    Public GloObs_OrdSer As String
    Public GloSeRealiza_OrdSer As Boolean = True
    Public GloClv_Trabajo_OrdSer As Integer
    Public GloBndClv_CablemodemSel As Boolean = False
    Public GloClv_CablemodemSel As Long = 0
    Public GloMacCablemodemSel As String = Nothing
    Public GLOTRABAJO As String = Nothing
    Public gloClv_Orden As Long = 0
    Public GloDetClave As Long = 0
    Public GloBndTipSer As Boolean = False
    Public GloContratoVer As Long = 0
    Public GloGuardo As Boolean = False
    Public Clv_HoraAgenda As Long
    Public FechaAgenda As String = Nothing
    Public HoraAgenda As String = Nothing
    Public Contrato As Long = 0
    Public GLOCONTRATOSEL As Long = 0
    Public GLOCONTRATOSEL_agenda As Long = 0
    Public GloClv_Cablemodem As Integer = 0
    Public OpcionCli As Char = Nothing
    Public GloClv_TipSer As Integer = 0
    Public GloNom_TipSer As String = Nothing
    Public GloClv_Servicio As Integer = 0
    Public GloServicior As String = Nothing
    Public GloContratonet As Long = 0
    Public Pantalla As String = Nothing
    Public LoContratonet As Long = 0
    Public LoClv_Unicanet As Long = 0
    Public GLOMOVNET As Integer = 0
    Public GLOMOVDIG As Integer = 0
    Public opcion As Char = Nothing
    Public GloClv_Calle As Integer = 0
    Public GloClv_COLONIA As Integer = 0
    Public GloClv_tecnico As Integer = 0
    Public GLONOM_TECNICO As String = Nothing
    Public GloBnd As Boolean
    Public opcionAgenda As Char = "N"
    Public GloClv_TipoServicio As Integer = 0
    Public GloGClv_Servicio As Integer = 0
    Public GloTipoAparato As Char = Nothing
    Public GloGClv_Cablemodem As Integer = 0
    Public gloClave As Integer = 0
    Public GlovienedeCartera As Integer = 0
    Public GloPeriodoCartera As Integer = 0

    '--Variable para Control de los Periodos en las Carteras
    Public gloPorClv_Periodo As Integer = 0
    '--Variables de Telefonia
    Public GlobndGuardaTelefonia As Boolean = False
    Public gLONET_1 As Integer = 0
    Public gLOTEL_1 As Integer = 0
    '--Variable de Combos
    Public Glo_Combo_Tipo As Integer = 0
    Public Servicio_Principal As Integer = 0
    Public HAB_TV As Integer = 1
    Public HAB_TVDIG As Integer = 1
    Public HAB_INTERNET As Integer = 1
    Public HAB_Telefonia As Integer = 1

    '--Que Servicios contratto en combo
    Public Tiene_Internet As Integer = 0
    Public Tiene_Tv As Integer = 0
    Public Tiene_Dig As Integer = 0
    Public Tiene_Tel As Integer = 0
    '
    Public CliTiene_Internet As Integer = 0
    Public CliTiene_Tv As Integer = 0
    Public CliTiene_Dig As Integer = 0
    Public CliTiene_Tel As Integer = 0
    '
    Public CLITVSINPAGO As Integer = 0
    Public CLITVCONPAGO As Integer = 0
    '
    Public eResValida As Integer = 0
    Public eMsgValida As String = Nothing
    Public rvalida As Boolean = False
    Public rdesconexiones As Boolean = False
    Public rquitaServ As Boolean = False

    Public bnd_prefijos As String = Nothing
    Public clv_prefijos As Integer = 0

    Public EsqoPer As String = Nothing
    Public IDCamesquema As Integer = 0

    Public GloCI As String = Nothing

    Public PreguntaDatosFiscales As Boolean = False
    Public DimeSiDatosFiscales As Boolean = False

    Public Rstatus As Integer = 0
    Public Rorden As Boolean = 0
    Public Rqueja As Boolean = 0

    'Public eClv_Servicio As Integer = 0
    Public eClv_PPE As Integer = 0
    Public eClv_Progra As Long = 0
    Public bytesImg() As Byte
    Public byte3() As Byte
    'Mensajes Personalizados 
    Public rMensajes As Boolean = False
    Public rSession As Long = 0
    Public rOrdenQueja As Boolean = False

    'JUANJO COSTOS PAGARE�S
    Public OpPagare As String = Nothing
    Public IdPagare As Integer = 0
    Public BndActualizaPagare As Boolean = False
    Public OpICAJA As String = Nothing
    Public ContratoCajasDig As Integer = Nothing
    Public GloClvCajaDig As String = 0
    Public ContratoNetCajasDig As Integer = 0
    Public ContratoNetDetCajas As Integer = 0
    Public BNDCAJAS As Boolean = False
    Public BndGuardaCajas As Boolean = False
    Public BndEliminar As Boolean = False

    Public OpPaqAdic As String = ""
    'JUANJO COSTOS PAGARE�S (FIN)

    'SAUL AREA TECNICA QUEJAS
    Public BanderaAreaQuejas As Boolean = False
    Public BanderaAreaTecnica As Boolean = False
    Public opcionarea As Integer = 0
    Public locMorososConSaldo As Integer = Nothing
    'SAUL AREA TECNICA QUEJAS (FIN)

    'SAUL VENTAS SERIE
    Public GloSeries As String = Nothing
    'SAUL VENTAS SERIES (FIN)

    'PARA QUEJAS (INICIO) *JUANJO*
    Public TurnoAgenda As String = Nothing
    'PARA QUEJAS (INICIO) *JUANJO*

    'JUANJO FECHAS Y HORAS VSITAS ORDENES Y QUEJAS <INICIO>
    Public ordenGloFechaVisita1 As String = Nothing
    Public ordenGloFechaVisita2 As String = Nothing
    Public ordenGloFechaVisita3 As String = Nothing
    Public ordenGloHoraVisita1 As String = Nothing
    Public ordenGloHoraVisita2 As String = Nothing
    Public ordenGloHoraVisita3 As String = Nothing

    Public quejaGloFechaVisita1 As String = Nothing
    Public quejaGloFechaVisita2 As String = Nothing
    Public quejaGloFechaVisita3 As String = Nothing
    Public quejaGloHoraVisita1 As String = Nothing
    Public quejaGloHoraVisita2 As String = Nothing
    Public quejaGloHoraVisita3 As String = Nothing

    Public quejaGloFechaEnProceso As String = Nothing
    Public quejaGloHoraEnProceso As String = Nothing
    'JUANJO FECHAS Y HORAS VSITAS ORDENES Y QUEJAS <FIN>

    'JUANJO PARA ELIMINAR SERVICIO TV <INICIO>
    Public MsjBorraTV As Integer = Nothing
    'JUANJO PARA ELIMINAR SERVICIO TV <FIN>

    'AGENDA DE T�CNICOS (INICIO) <ANTONIO>
    Public opcionprimera As Integer = 0
    Public opcionprimeraTurno As Integer = 0
    Public OpcionQuejaOrden As String
    'AGENDA DE T�CNICOS (FIN) <ANTONIO>

    'SAUL ReporteFolios
    Public GloReportFolios As Integer = 0
    'SAUL ReporteFolios (FIN)

    'Clasificaci�n Llamadas
    Public ID_ClasificacionLlamadas As Integer = 0
    Public OpcionClasificacionLlamadas As String = ""
    Public NombreClasificacion As String = ""
    Public DescClasificacion As String = ""
    Public es_LD As Boolean = False
    Public Clv_Clasificacion As Integer = 0

    'Catalogo IP
    Public BanderaCatalogoIP As String = ""
    Public IdIP As Integer = 0
    Public Function imageToByteArrayF(ByVal imageIn As System.Drawing.Image, ByVal pformato As System.Drawing.Imaging.ImageFormat) As Byte()

        Dim ms As New IO.MemoryStream

        Try

            imageIn.Save(ms, pformato)

        Catch ex As Exception

            MessageBox.Show("Ocurri� un error " & ex.Message)

        End Try

        Return ms.ToArray()

    End Function

    Public Function byteArrayToImageF(ByVal byteArrayIn As Byte()) As Image

        Dim returnImage As Image = Nothing

        Try

            Dim ms As New IO.MemoryStream(byteArrayIn)

            returnImage = Image.FromStream(ms)

        Catch ex As Exception

            MessageBox.Show("Ocurri� un error " & ex.Message)

        End Try

        Return returnImage

    End Function




    Public Sub DimeSiAplicaIEPS(ByVal OP As Integer, ByVal CLV_TIPSER As Integer, ByVal CLV_SERVICIO As Integer, ByVal CLAVE As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("DimeSiAplicaIEPS", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@OP", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = OP
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@CLV_TIPSER", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = CLV_TIPSER
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@CLV_SERVICIO", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = CLV_SERVICIO
        command.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@CLAVE", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = CLAVE
        command.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@APLICA", SqlDbType.Bit)
        parametro5.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@VISIBLE", SqlDbType.Bit)
        parametro6.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro6)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
            VISIBLE_IEPS = parametro6.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Public Function Image2Bytes(ByVal img As Image) As Byte()
        Dim sTemp As String = Path.GetTempFileName()
        Dim fs As New FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite)

        If img Is Nothing Then Return Nothing
        img.Save(fs, System.Drawing.Imaging.ImageFormat.Png)
        fs.Position = 0
        '
        Dim imgLength As Integer = CInt(fs.Length)
        If img Is My.Forms.FrmServicioPPE2.ImagenPictureBox.Image Then
            Dim bytes(0 To imgLength - 1) As Byte
            fs.Read(bytes, 0, imgLength)
            fs.Close()
            bytesImg = bytes
            byte3 = bytes
            Return bytes
            Exit Function
        End If

    End Function

    Public Sub Dime_Checa_Servicios_Cliente(ByVal XContrato As Long)
        Dim CON90 As New SqlClient.SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand()
        Try
            CliTiene_Internet = 0
            CliTiene_Tv = 0
            CliTiene_Dig = 0
            CliTiene_Tel = 0
            CON90.Open()
            With Cmd
                .CommandText = "Dime_Checa_Servicios_Cliente"
                .Connection = CON90
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                '@clv_combo bigint,@error int output

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = XContrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@TV", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@INT", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@DIG", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@TEL", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)
                Dim ia As Integer = .ExecuteNonQuery()
                CliTiene_Tv = prm1.Value
                CliTiene_Internet = prm2.Value
                CliTiene_Dig = prm3.Value
                CliTiene_Tel = prm4.Value
            End With
            CON90.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub ChecaTelefonosDisponibles()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaTelefonosDisponibles", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Res", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            eResValida = 0
            eMsgValida = String.Empty
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eResValida = CInt(parametro.Value)
            eMsgValida = parametro2.Value.ToString
        Catch ex As Exception
            conexion.Close()
        End Try
    End Sub



    Public Sub NUEMOVREL_CITAS(ByVal opClv_Cita As Long, ByVal opComentario As String)
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "NUEMOVREL_CITAS"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = opClv_Cita
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Comentario", SqlDbType.VarChar, 250)
                prm1.Direction = ParameterDirection.Input
                'If IsNumeric(Me.TextCosto.Text) = True Then
                prm1.Value = opComentario
                'Else
                'prm1.Value = 0
                'End If

                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

                'Me.TextComentario.Text = prm1.Value

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub DIME_SITELYNET(ByVal optcontrato As Integer)
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            gLONET_1 = 0
            gLOTEL_1 = 0
            CliTiene_Tv = 0
            CliTiene_Dig = 0

            Dim cmd As New SqlCommand

            cON_x.Open()
            With cmd
                .CommandText = "DIME_SITELYNET"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x

                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@NUMNET", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@NUMTEL", SqlDbType.Int)
                Dim prm4 As New SqlParameter("@TV", SqlDbType.Int)
                Dim prm5 As New SqlParameter("@Dig", SqlDbType.Int)

                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Output
                prm4.Direction = ParameterDirection.Output
                prm5.Direction = ParameterDirection.Output

                prm.Value = optcontrato
                prm2.Value = 0
                prm3.Value = 0
                prm4.Value = 0
                prm5.Value = 0

                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)

                Dim i As Integer = .ExecuteNonQuery
                gLONET_1 = prm2.Value
                gLOTEL_1 = prm3.Value
                CliTiene_Tv = prm4.Value
                CliTiene_Dig = prm5.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
            gLONET_1 = 0
            gLOTEL_1 = 0
        End Try
    End Sub

    Public Function TEL_TIENE_ADEUDOS(ByVal optcontrato As Integer) As Integer
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            TEL_TIENE_ADEUDOS = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "TEL_TIENE_ADEUDOS"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@BND", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm.Value = optcontrato
                prm2.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                TEL_TIENE_ADEUDOS = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function

    Public Function DameCantidadALetra(ByVal LImporte As Decimal) As String
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
            DameCantidadALetra = ""
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "DameCantidadALetra"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm As New SqlParameter("@Numero", SqlDbType.Money)
                Dim prm2 As New SqlParameter("@moneda", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@letra", SqlDbType.VarChar, 250)
                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Output
                prm.Value = LImporte
                prm2.Value = 0
                prm3.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                Dim i As Integer = .ExecuteNonQuery
                DameCantidadALetra = prm3.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function

    Public Function DimeTipSer_CualEsPrincipal() As Integer
        Dim con As New SqlConnection(MiConexion)
        Try
            DimeTipSer_CualEsPrincipal = 0

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("DimeTipSer_CualEsPrincipal", con)
            cmd.CommandType = CommandType.StoredProcedure
            con.Open()
            reader = cmd.ExecuteReader()
            Using reader
                While reader.Read
                    DimeTipSer_CualEsPrincipal = reader.GetValue(0)
                End While
            End Using

            con.Close()
        Catch ex As Exception
            DimeTipSer_CualEsPrincipal = 1
            MsgBox(ex.Message)
            con.Close()
        End Try
    End Function



    Public Function DAMESTATUSHAB(ByVal CLV_TIPSER As Integer) As Integer
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            DAMESTATUSHAB = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "DAMESTATUSHAB"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm As New SqlParameter("@CLV_TIPSER", SqlDbType.Int)
                Dim prm2 As New SqlParameter("@HABILITAR", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm.Value = CLV_TIPSER
                prm2.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                DAMESTATUSHAB = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            DAMESTATUSHAB = 1
            'MsgBox(ex.Message)
            cON_x.Close()
        End Try
    End Function


    Public Sub bitsist(ByVal usuario As String, ByVal contrato As Long, ByVal sistema As String, ByVal pantalla As String, ByVal control As String, ByVal valorant As String, ByVal valornuevo As String, ByVal clv_ciudad As String)
        Dim COn85 As New SqlConnection(MiConexion)
        COn85.Open()
        guardabitacora = New sofTV.ProcedimientosArnoldo2.Inserta_MovSistDataTable
        guardabitacorabuena = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_MovSistTableAdapter
       
        If valorant <> valornuevo Then
            guardabitacorabuena.Connection = COn85
            guardabitacorabuena.Fill(guardabitacora, usuario, contrato, sistema, pantalla, control, valorant, valornuevo, clv_ciudad)
        End If
        COn85.Close()

    End Sub


    Public Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
        Dim menu As ToolStripMenuItem
        For Each oOpcionMenu As ToolStripMenuItem In oMenu.Items
            menu = New ToolStripMenuItem
            menu = oOpcionMenu
            menu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
            menu.Visible = TipoAccesoMenusPerfiles(GloTipoUsuario, oOpcionMenu.Name)
            menu = Nothing
            If oOpcionMenu.DropDownItems.Count > 0 Then
                RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
            End If
        Next
    End Sub

    Public Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
        Dim submenu As ToolStripItem
        For Each oSubitem As ToolStripItem In oSubmenuItems
            If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
                submenu = New ToolStripMenuItem
                submenu = oSubitem
                submenu.Visible = TipoAccesoMenusPerfiles(GloTipoUsuario, oSubitem.Name)
                submenu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
                submenu = Nothing
                If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
                    RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
                End If
            End If
        Next
    End Sub
    Public Sub bwrpanel(ByVal panel2 As Panel, ByVal formulario As Form) 'BUSCA PANEL----------------------------------
        Dim data As DataGridView
        Dim label As Label
        Dim boton As Button
        Dim split As SplitContainer
        Dim panel3 As Panel
        Dim text As TextBox
        Dim GROUP As GroupBox
        Dim var As String
        Dim TAB As TabControl
        Dim radio As RadioButton
        Dim CHECK As CheckBox
        Dim BN As BindingNavigator
        'Dim CON27 As New SqlConnection(MiConexion)
        'CON27.Open()
        'busca = New sofTV.DataSetLidia.BUSCAControlDataTable
        'buscaCtr = New sofTV.DataSetLidiaTableAdapters.BUSCAControlTableAdapter
        'borra = New sofTV.DataSetLidia.BORRARControlDataTable
        'borraCtr = New sofTV.DataSetLidiaTableAdapters.BORRARControlTableAdapter
        'nuevo = New sofTV.DataSetLidia.NUEVOControlDataTable
        'NuevoCtr = New sofTV.DataSetLidiaTableAdapters.NUEVOControlTableAdapter

        If panel2.BackColor <> Color.WhiteSmoke Then
            panel2.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
            'panel2.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
        End If

        For Each ctr As Control In panel2.Controls
            'SPLIT -----------------------------------------------------
            If ctr.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctr
                bwrpanel(split.Panel1, formulario)
                bwrpanel(split.Panel2, formulario)
                split = Nothing
                'CRYSTAL REPORTS ----------------------------
            ElseIf ctr.GetType Is GetType(CrystalDecisions.Windows.Forms.CrystalReportViewer) Then
                ctr.BackColor = Color.WhiteSmoke


                'DATA GRID----------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                data = New DataGridView
                data = ctr
                data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                ' data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                'buscaCtr.Connection = CON27
                'buscaCtr.Fill(busca, 1, formulario.Name, data.Name, data.Name, salida)
                'If salida = 0 Then
                '    'NuevoCtr.Fill(nuevo, 1, formulario.Name, data.Name, data.Name, GloTipoUsuario, data.Location.Y, data.Location.X)
                '    'ElseIf salida > 0 Then
                '    '    borraCtr.Fill(borra, data.Name, data.Name, formulario.Name, 1)
                'End If
                data = Nothing
            End If
            var = Mid(ctr.Name, 1, 3)
            'LABEL----------------------------------------
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctr.BackColor <> Color.WhiteSmoke Then
                label = New Label
                label = ctr
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" Then
                label = New Label
                label = ctr
                label.ForeColor = Color.Black
                label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                label = Nothing

                'TEXT BOX --------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.TextBox) And var = "CMB" Then
                text = New TextBox
                text = ctr
                text.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                text.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                text = Nothing
                'BOTONES -------------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctr
                boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                'If boton.Text <> "&Buscar" Then
                '    buscaCtr.Connection = CON27
                '    buscaCtr.Fill(busca, 1, formulario.Name, boton.Text, boton.Name, salida)
                '    If salida = 0 Then
                '        NuevoCtr.Connection = CON27
                '        NuevoCtr.Fill(nuevo, 1, formulario.Name, boton.Text, boton.Name, GloTipoUsuario, boton.Location.Y, boton.Location.X)
                '        'ElseIf salida > 0 Then
                '        '    borraCtr.Fill(borra, boton.Text, boton.Name, formulario.Name, 1)
                '    End If
                'End If
                boton = Nothing

                'PANEL ---------------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
                panel3 = New Panel
                panel3 = ctr
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3, formulario)
                panel3 = Nothing

                'GROUP BOX -----------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GROUP = New GroupBox
                GROUP = ctr
                GROUP.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                GROUP.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                bwrgroup(GROUP, formulario)
                GROUP = Nothing

                'TAB CONTROL ------------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.TabControl) Then
                TAB = New TabControl
                TAB = ctr
                TAB.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                tabbwr(TAB, formulario)
                TAB = Nothing

                'RADIOBUTON--------------------------------------------------------
                'ElseIf ctr.GetType Is GetType(System.Windows.Forms.RadioButton) Then
                '    radio = New RadioButton
                '    radio = ctr
                '    radio.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                '    radio.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                '    radio = Nothing
            End If
            'PARA LLENAR NOMBRES DE LOS CONTROLES---------------------------------------------
            'If ctr.GetType Is GetType(System.Windows.Forms.TextBox) Then
            '    text = New TextBox
            '    text = ctr
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON27
            '    buscaCtr.Fill(busca, 1, formulario.Name, text.Name, text.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON27
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, text.Name, text.Name, GloTipoUsuario, text.Location.Y, text.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, text.Text, text.Name, formulario.Name, 1)
            '    End If
            '    text = Nothing
            'End If
            'If ctr.GetType Is GetType(System.Windows.Forms.CheckBox) Then
            '    CHECK = New CheckBox
            '    CHECK = ctr
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON27
            '    buscaCtr.Fill(busca, 1, formulario.Name, CHECK.Name, CHECK.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON27
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, CHECK.Name, CHECK.Name, GloTipoUsuario, CHECK.Location.Y, CHECK.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, CHECK.Name, CHECK.Name, formulario.Name, 1)
            '    End If
            '    CHECK = Nothing
            'End If

            'If ctr.GetType Is GetType(System.Windows.Forms.RadioButton) Then
            '    radio = New RadioButton
            '    radio = ctr
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON27
            '    buscaCtr.Fill(busca, 1, formulario.Name, radio.Name, radio.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON27
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, radio.Name, radio.Name, GloTipoUsuario, radio.Location.Y, radio.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, radio.Text, radio.Name, formulario.Name, 1)
            '    End If

            '    radio = Nothing
            'End If

            'If ctr.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
            '    BN = New BindingNavigator
            '    BN = ctr
            '    For Each BIN As ToolStripItem In BN.Items
            '        If BIN.GetType Is GetType(System.Windows.Forms.ToolStripButton) Then
            '            'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '            buscaCtr.Connection = CON27
            '            buscaCtr.Fill(busca, 1, formulario.Name, BIN.Text, BIN.Name, salida)
            '            If salida = 0 Then
            '                NuevoCtr.Connection = CON27
            '                NuevoCtr.Fill(nuevo, 1, formulario.Name, BIN.Text, BIN.Name, GloTipoUsuario, 0, 0)
            '                'ElseIf salida > 0 Then
            '                '    borraCtr.Fill(borra, BIN.Text, BIN.Name, formulario.Name, 1)
            '            End If
            '        End If
            '    Next
            '    BN = Nothing
            'End If
            '-----------------------------------------------------------------------------------

            'CON27.Close()
            If ctr.GetType Is GetType(System.Windows.Forms.Panel) And var <> "CMB" Then
                bwrpanel(ctr, formulario)
            End If
        Next
    End Sub
    Public Sub bwrgroup(ByVal grup As GroupBox, ByVal formulario As Form) 'GROUP BOX--------------------------------
        Dim panel3 As Panel
        Dim label As Label
        Dim var As String
        Dim TEXT As TextBox
        Dim CHECK As CheckBox
        Dim RADIO As RadioButton
        Dim boton As Button

        'nuevo = New sofTV.DataSetLidia.NUEVOControlDataTable
        'NuevoCtr = New sofTV.DataSetLidiaTableAdapters.NUEVOControlTableAdapter
        'borra = New sofTV.DataSetLidia.BORRARControlDataTable
        'borraCtr = New sofTV.DataSetLidiaTableAdapters.BORRARControlTableAdapter
        'busca = New sofTV.DataSetLidia.BUSCAControlDataTable
        'buscaCtr = New sofTV.DataSetLidiaTableAdapters.BUSCAControlTableAdapter
        'Dim CON13 As New SqlConnection(MiConexion)
        'CON13.Open()
        For Each ctm As Control In grup.Controls
            var = Mid(ctm.Name, 1, 3)
            'PANEL------------------------------------------------------------- 
            If ctm.GetType Is GetType(System.Windows.Forms.Panel) And ctm.BackColor <> Color.WhiteSmoke Then
                panel3 = New Panel
                panel3 = ctm
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3, formulario)
                panel3 = Nothing

                'LABEL--------------------------------------------------------------
            ElseIf ctm.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctm.BackColor <> Color.WhiteSmoke Then
                label = New Label
                label = ctm
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            End If
            ''PARA LLENAR NOMBRES DE LOS CONTROLES
            'If ctm.GetType Is GetType(System.Windows.Forms.TextBox) Then
            '    TEXT = New TextBox
            '    TEXT = ctm
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON13
            '    buscaCtr.Fill(busca, 1, formulario.Name, TEXT.Name, TEXT.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON13
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, TEXT.Name, TEXT.Name, GloTipoUsuario, TEXT.Location.Y, TEXT.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, TEXT.Text, TEXT.Name, formulario.Name, 1)
            '    End If
            '    TEXT = Nothing
            'End If

            'If ctm.GetType Is GetType(System.Windows.Forms.CheckBox) Then
            '    CHECK = New CheckBox
            '    CHECK = ctm
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON13
            '    buscaCtr.Fill(busca, 1, formulario.Name, CHECK.Name, CHECK.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON13
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, CHECK.Name, CHECK.Name, GloTipoUsuario, CHECK.Location.Y, CHECK.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, CHECK.Name, CHECK.Name, formulario.Name, 1)
            '    End If
            '    CHECK = Nothing
            'End If
            'If ctm.GetType Is GetType(System.Windows.Forms.RadioButton) Then
            '    RADIO = New RadioButton
            '    RADIO = ctm
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON13
            '    buscaCtr.Fill(busca, 1, formulario.Name, RADIO.Name, RADIO.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON13
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, RADIO.Name, RADIO.Name, GloTipoUsuario, RADIO.Location.Y, RADIO.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, RADIO.Text, RADIO.Name, formulario.Name, 1)
            '    End If
            '    RADIO = Nothing
            'End If
            '-----------------------------------------------------------------------------------
            'If ctm.GetType Is GetType(System.Windows.Forms.Button) Then
            '    boton = New Button
            '    boton = ctm
            '    boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
            '    boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
            '    If boton.Text <> "&Buscar" Then
            '        'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '        buscaCtr.Connection = CON13
            '        buscaCtr.Fill(busca, 1, formulario.Name, boton.Text, boton.Name, salida)
            '        If salida = 0 Then
            '            NuevoCtr.Connection = CON13
            '            NuevoCtr.Fill(nuevo, 1, formulario.Name, boton.Text, boton.Name, GloTipoUsuario, boton.Location.Y, boton.Location.X)
            '            'ElseIf salida > 0 Then
            '            '    borraCtr.Fill(borra, boton.Text, boton.Name, formulario.Name, 1)
            '        End If
            '    End If
            '    boton = Nothing
            'End If

        Next
        'CON13.Close()
    End Sub

    Public Sub tabbwr(ByVal tab As TabControl, ByVal formulario As Form) 'BUSCA EN UN TAB CONTROL
        Dim page As TabPage
        For Each ctn As Control In tab.Controls
            If ctn.GetType Is GetType(System.Windows.Forms.TabPage) Then
                page = New TabPage
                page = ctn
                page.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                pagebwr(page, formulario)
            End If
        Next

    End Sub
    Public Sub pagebwr(ByVal page As TabPage, ByVal formulario As Form) 'BUSCA EN UN TAB PAGE
        Dim panel As Panel
        Dim split As SplitContainer
        For Each ctn As Control In page.Controls
            'PANEL---------------------------------------------------------------
            If ctn.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctn
                panel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(page, formulario)
            End If
            'SPLIT-----------------------------------------------------------------
            If ctn.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctn
                bwrpanel(split.Panel1, formulario)
                bwrpanel(split.Panel2, formulario)
            End If
        Next

    End Sub

    Public Sub clvSessionMensajes()
        rMensajes = True
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)
        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            rSession = CLng(parametro.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub



    Public Sub SetDBReport(ByVal ds As DataSet, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                myTable.SetDataSource(ds)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Function ObtenerTurnoCita(ByVal prmClaveCita As Integer) As String
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()

        If Contrato = Nothing Then Contrato = 0
        Try
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "CONSULTARREL_CITAS_Turno"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = prmClaveCita
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Turno", SqlDbType.NVarChar, 50)
                prm1.Direction = ParameterDirection.Output
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

                Return prm1.Value

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function

    Public Function spConsultaTurnos(Optional ByVal prmIncluyeTodos As Boolean = False) As DataTable
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spConsultaTurnos", CON)
        CMD.CommandType = CommandType.StoredProcedure
        'para darle chekin
        If prmIncluyeTodos Then
            Dim prmIncluTodos As New SqlParameter("@IncluyeTodos", SqlDbType.Bit)
            prmIncluTodos.Value = prmIncluyeTodos
            CMD.Parameters.Add(prmIncluTodos)
        End If

        Dim DA As New SqlDataAdapter(CMD)
        Dim DT As New DataTable

        Try
            CON.Open()
            DA.Fill(DT)
            spConsultaTurnos = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function

    'SAUL AREA TECNICA QUEJAS
    Public Function DAMESclv_Sessionporfavor() As Long
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            DAMESclv_Sessionporfavor = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "DameClv_Session_Servicios"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                DAMESclv_Sessionporfavor = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function
    'SAUL AREA TECNICA QUEJAS (FIN)

    Public Sub AgregaImgCodeBarEstadoDeCuentaTV(ByVal oClv_Recibo As Integer, ByVal strBarCode As String, ByVal prmop As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()

        Dim comando As New SqlClient.SqlCommand("AgregaImgCodeBarEstadoDeCuentaTV", conexion)

        Try

            'GENERAMOS EL C�DIGO DE BARRAS DE ESTE ESTADO DE CUENTA
            Dim imgBarCode As Byte()
            imgBarCode = GeneraCodeBar128Nuevo(strBarCode)

            'GUADAMOS EL C�DIGO DE BARRAS GENERADO
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Contrato", SqlDbType.BigInt)
            comando.Parameters(0).Direction = ParameterDirection.Input
            comando.Parameters(0).Value = oClv_Recibo

            comando.Parameters.Add("@imgBarCode", SqlDbType.Image)
            comando.Parameters(1).Direction = ParameterDirection.Input
            comando.Parameters(1).Value = imgBarCode

            comando.Parameters.Add("@op", SqlDbType.Int)
            comando.Parameters(2).Direction = ParameterDirection.Input
            comando.Parameters(2).Value = prmop

            comando.ExecuteNonQuery()

            conexion.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally

        End Try

    End Sub
    Dim b As New BarcodeLib.Barcode()
    Public Function UspValidarClienteConCortesia(ByVal PRMCONTRATO As Long) As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, PRMCONTRATO)
        UspValidarClienteConCortesia = BaseII.ConsultaDT("UspValidarClienteConCortesia")
    End Function
    Public Function GeneraCodeBar128Nuevo(ByVal Data As String) As Byte()
        Dim imgBarCode As Byte()
        Dim barcode As New PictureBox()
        Dim W As Integer = Convert.ToInt32(250)
        Dim H As Integer = Convert.ToInt32(100)
        'Dim Data As [String] = "31000016024201101070637005"
        Dim Align As BarcodeLib.AlignmentPositions = BarcodeLib.AlignmentPositions.CENTER
        Align = BarcodeLib.AlignmentPositions.CENTER
        Dim type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
        type = BarcodeLib.TYPE.CODE128
        Try
            If type <> BarcodeLib.TYPE.UNSPECIFIED Then
                b.IncludeLabel = True
                b.Alignment = Align
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone)
                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
                barcode.Image = b.Encode(type, Data, Color.Black, Color.White, W, H)
            End If
            barcode.Width = barcode.Image.Width
            barcode.Height = barcode.Image.Height
            'File.WriteAllBytes(@"C:\XSD\1.jpg", imgbites);                
            Dim imgbites As Byte() = ImageToByte2(barcode.Image)
            imgBarCode = imgbites
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return imgBarCode
    End Function

    Public Function ImageToByte2(ByVal img As Image) As Byte()
        Dim byteArray As Byte()
        Using stream As New MemoryStream()
            img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg)
            stream.Close()
            byteArray = stream.ToArray()
        End Using
        Return byteArray
    End Function


    Public Function UspValidaSiActivos(ByVal optcontrato As Integer) As Integer
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            UspValidaSiActivos = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "UspValidaSiActivos"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@BND", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm.Value = optcontrato
                prm2.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                UspValidaSiActivos = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function

    Public Function SP_VALIDA_APARATOS_RECON(ByVal oClv_Session As Long, ByVal oOP As String) As String
        '@Clv_Session Bigint,@BND INT,@MSJ VARCHAR(800) OUTPUT
        SP_VALIDA_APARATOS_RECON = ""
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_VALIDA_APARATOS_RECON", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Session
        comando.Parameters.Add(parametro)

        Dim parametro3 As New SqlParameter("@BND", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@MSJ", SqlDbType.VarChar, 800)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)


        Dim parametro5 As New SqlParameter("@OP", SqlDbType.VarChar, 10)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = oOP
        comando.Parameters.Add(parametro5)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            SP_VALIDA_APARATOS_RECON = parametro4.Value
        Catch ex As Exception
            SP_VALIDA_APARATOS_RECON = 0
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Function

    Public Sub DameClv_Session()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameClv_Session", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = parametro.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Public Sub SP_Dame_General_sistema_II()
        GloTvSinPago = 0
        GloTvConpago = 0
        GloImprimeTicket = False
        GloExportaTicket = False

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_Dame_General_sistema_II", conexion)
        comando.CommandType = CommandType.StoredProcedure
        '@Tv bit OUTPUT,@Dig bit OUTPUT,@Net bit OUTPUT,@Tel bit OUTPUT

        Dim parametro As New SqlParameter("@TvSinPago", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@TvConPago", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@ComboSinDigital", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@PreguntaBonContProp", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@ImprimirOrdenesPant", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@ImprimeTicket", SqlDbType.Bit)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@ExportaTicket", SqlDbType.Bit)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@InstalaRouter", SqlDbType.Bit)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@ConPlaca", SqlDbType.Bit)
        parametro8.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@ActivarCFD", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@TieneFibra", SqlDbType.Int)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            GloTvSinPago = parametro.Value
            GloTvConpago = parametro1.Value
            GloComboSinDigital = parametro2.Value
            GloPreguntaBonContProp = parametro3.Value
            GloImprimirOrdenesPant = parametro4.Value
            GloImprimeTicket = parametro5.Value
            GloExportaTicket = parametro6.Value
            GloInstalaRouter = parametro7.Value
            GloConPlaca = parametro8.Value
            GloActivarCFD = parametro9.Value
            GloActivarFibra = parametro10.Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Public Sub SP_SERVICIOS_HABILITADOS()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_SERVICIOS_HABILITADOS", conexion)
        comando.CommandType = CommandType.StoredProcedure
        '@Tv bit OUTPUT,@Dig bit OUTPUT,@Net bit OUTPUT,@Tel bit OUTPUT

        Dim parametro As New SqlParameter("@Tv", SqlDbType.Bit)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Dig", SqlDbType.Bit)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Net", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Tel", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            GloHabilitadosTV = parametro.Value
            GloHabilitadosDIG = parametro1.Value
            GloHabilitadosNET = parametro2.Value
            GloHabilitadosTEL = parametro3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

End Module
