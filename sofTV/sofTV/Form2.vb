Imports System.Data.SqlClient

Public Class Form2

    Private Sub CONCAPARBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAPARBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONCAPARBindingSource.EndEdit()
        Me.CONCAPARTableAdapter.Connection = CON
        Me.CONCAPARTableAdapter.Update(Me.NewSofTvDataSet.CONCAPAR)
        CON.Close()

    End Sub

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FillToolStripButton.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCAPARTableAdapter.Connection = CON
            Me.CONCAPARTableAdapter.Fill(Me.NewSofTvDataSet.CONCAPAR, New System.Nullable(Of Long)(CType(ClaveToolStripTextBox.Text, Long)), New System.Nullable(Of Long)(CType(Clv_OrdenToolStripTextBox.Text, Long)), New System.Nullable(Of Long)(CType(ContratonetToolStripTextBox.Text, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub
End Class