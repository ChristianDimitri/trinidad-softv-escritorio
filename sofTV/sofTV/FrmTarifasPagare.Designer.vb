﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTarifasPagare
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TipServCombo = New System.Windows.Forms.ComboBox()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.HDRadioBut = New System.Windows.Forms.RadioButton()
        Me.NormalRadioBut = New System.Windows.Forms.RadioButton()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.AparatoCombo = New System.Windows.Forms.ComboBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CostoText = New System.Windows.Forms.TextBox()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel6 = New System.Windows.Forms.Label()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.RentaPrincipalText = New System.Windows.Forms.TextBox()
        Me.RentaAdicionalText = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CostoAdicText = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(49, 12)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(73, 16)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Servicio :"
        '
        'TipServCombo
        '
        Me.TipServCombo.DisplayMember = "CONCEPTO"
        Me.TipServCombo.FormattingEnabled = True
        Me.TipServCombo.Location = New System.Drawing.Point(128, 12)
        Me.TipServCombo.Name = "TipServCombo"
        Me.TipServCombo.Size = New System.Drawing.Size(295, 21)
        Me.TipServCombo.TabIndex = 0
        Me.TipServCombo.ValueMember = "CLV_TIPSER"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(12, 45)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(110, 16)
        Me.CMBLabel3.TabIndex = 3
        Me.CMBLabel3.Text = "Tipo Paquete :"
        '
        'HDRadioBut
        '
        Me.HDRadioBut.AutoSize = True
        Me.HDRadioBut.Location = New System.Drawing.Point(222, 45)
        Me.HDRadioBut.Name = "HDRadioBut"
        Me.HDRadioBut.Size = New System.Drawing.Size(41, 17)
        Me.HDRadioBut.TabIndex = 2
        Me.HDRadioBut.Text = "HD"
        Me.HDRadioBut.UseVisualStyleBackColor = True
        '
        'NormalRadioBut
        '
        Me.NormalRadioBut.AutoSize = True
        Me.NormalRadioBut.Checked = True
        Me.NormalRadioBut.Location = New System.Drawing.Point(128, 45)
        Me.NormalRadioBut.Name = "NormalRadioBut"
        Me.NormalRadioBut.Size = New System.Drawing.Size(58, 17)
        Me.NormalRadioBut.TabIndex = 1
        Me.NormalRadioBut.TabStop = True
        Me.NormalRadioBut.Text = "Normal"
        Me.NormalRadioBut.UseVisualStyleBackColor = True
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(51, 79)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(71, 16)
        Me.CMBLabel4.TabIndex = 5
        Me.CMBLabel4.Text = "Aparato :"
        '
        'AparatoCombo
        '
        Me.AparatoCombo.DisplayMember = "Descripcion"
        Me.AparatoCombo.FormattingEnabled = True
        Me.AparatoCombo.Location = New System.Drawing.Point(128, 78)
        Me.AparatoCombo.Name = "AparatoCombo"
        Me.AparatoCombo.Size = New System.Drawing.Size(295, 21)
        Me.AparatoCombo.TabIndex = 3
        Me.AparatoCombo.ValueMember = "NoArticulo"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(33, 228)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(193, 16)
        Me.CMBLabel5.TabIndex = 8
        Me.CMBLabel5.Text = "    Costo del Equipo :      Bs"
        Me.CMBLabel5.Visible = False
        '
        'CostoText
        '
        Me.CostoText.Location = New System.Drawing.Point(222, 228)
        Me.CostoText.Name = "CostoText"
        Me.CostoText.Size = New System.Drawing.Size(154, 20)
        Me.CostoText.TabIndex = 4
        Me.CostoText.Visible = False
        '
        'SaveButton
        '
        Me.SaveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.Location = New System.Drawing.Point(45, 177)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(142, 35)
        Me.SaveButton.TabIndex = 7
        Me.SaveButton.Text = "&Guardar"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'ExitButton
        '
        Me.ExitButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitButton.Location = New System.Drawing.Point(251, 177)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(140, 35)
        Me.ExitButton.TabIndex = 8
        Me.ExitButton.Text = "&Cancelar"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'CMBLabel6
        '
        Me.CMBLabel6.AutoSize = True
        Me.CMBLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel6.Location = New System.Drawing.Point(61, 117)
        Me.CMBLabel6.Name = "CMBLabel6"
        Me.CMBLabel6.Size = New System.Drawing.Size(144, 16)
        Me.CMBLabel6.TabIndex = 12
        Me.CMBLabel6.Text = "Renta Principal : Bs"
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.Location = New System.Drawing.Point(57, 139)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(148, 16)
        Me.CMBLabel7.TabIndex = 13
        Me.CMBLabel7.Text = "Renta Adicional : Bs"
        '
        'RentaPrincipalText
        '
        Me.RentaPrincipalText.Location = New System.Drawing.Point(211, 117)
        Me.RentaPrincipalText.Name = "RentaPrincipalText"
        Me.RentaPrincipalText.Size = New System.Drawing.Size(154, 20)
        Me.RentaPrincipalText.TabIndex = 5
        '
        'RentaAdicionalText
        '
        Me.RentaAdicionalText.Location = New System.Drawing.Point(211, 139)
        Me.RentaAdicionalText.Name = "RentaAdicionalText"
        Me.RentaAdicionalText.Size = New System.Drawing.Size(154, 20)
        Me.RentaAdicionalText.TabIndex = 6
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'CostoAdicText
        '
        Me.CostoAdicText.Location = New System.Drawing.Point(221, 254)
        Me.CostoAdicText.Name = "CostoAdicText"
        Me.CostoAdicText.Size = New System.Drawing.Size(154, 20)
        Me.CostoAdicText.TabIndex = 14
        Me.CostoAdicText.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(32, 254)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 16)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Costo del Equipo Adic : Bs"
        Me.Label1.Visible = False
        '
        'FrmTarifasPagare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(460, 226)
        Me.Controls.Add(Me.CostoAdicText)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RentaAdicionalText)
        Me.Controls.Add(Me.RentaPrincipalText)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Controls.Add(Me.CMBLabel6)
        Me.Controls.Add(Me.HDRadioBut)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.TipServCombo)
        Me.Controls.Add(Me.AparatoCombo)
        Me.Controls.Add(Me.NormalRadioBut)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.CostoText)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Name = "FrmTarifasPagare"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarifas Pagaré"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TipServCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents HDRadioBut As System.Windows.Forms.RadioButton
    Friend WithEvents NormalRadioBut As System.Windows.Forms.RadioButton
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents AparatoCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CostoText As System.Windows.Forms.TextBox
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel6 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents RentaPrincipalText As System.Windows.Forms.TextBox
    Friend WithEvents RentaAdicionalText As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CostoAdicText As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
