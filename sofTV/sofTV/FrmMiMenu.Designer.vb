<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMiMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBNombreLabel As System.Windows.Forms.Label
        Dim LabelUsuario As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMiMenu))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CatálogosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatálogosDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatalogoÁreaTécnicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SectoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PostesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ÁreasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosAlClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClasificaciónTécnicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TecnicosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClasificaciónProblemasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HUBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OLTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NAPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatálogoDeGeneralesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CableModemsYAparatosDigitalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ColoniasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeColoniasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CallesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CiudadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SucursalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BancosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeCancelaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeCancelaciónFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeReImpresiónFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentosComboToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MensajesPrefijosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrefijosDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoDeMensajesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatalogoDeMensajesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RentaACajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoDeGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TvsAdicionalesPorCiudadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IndicadoresPagosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VelocidadesDeInternetFTTHToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DireccionesIPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatálogoDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PromotoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RangosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrecioDeComisionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GrupoDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstablecerComisionesCobroADomicilioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PolizaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MedidoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IndividualesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CarteraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesosDeServicioPremiumToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrimerPeriodoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SegundoPeriodoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesconexionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServicioBasicoYCanalesPremiumToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InternetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenesDeServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuejasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuejasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgendaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AtenciónTelefónicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgendaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActivaciónPaqueteDePruebaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepuraciónDeÓrdenesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambioDeServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MesajesInstantaneosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesVariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PruebaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MsjsPersonalizadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevaProgramaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CtgMsjPerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CorreoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResetearAparatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReseteoMasivoDeAparatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambioDeClienteASoloInternetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambioDeClienteAClienteNormalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BajaDeServiciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecontrataciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegresaAparatosAlAlmacenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrórrogasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PruebaDeInternetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DevoluciónDeAparatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenerarÓrdenesDeDesconexiónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CostoPorAparatoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambioDePeriodoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistorialDeCambiosDePeriodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DdToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesoMasivoCambioDeAparatoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesosComcastToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadosDeCarteraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesVariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteClientesConComboToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HotelesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDePermanenciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlazoForzosoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AreaTécnicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenesDeServicioToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuejasToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LlamadasTelefónicasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgendaDeActividadesDelTécnicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ÓrdenesClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClaveTécnicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DevoluciónDeAparatosAlAlmacénToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenOrdenesQuejasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnálisisDePenetraciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalcularComisionesPorVendedorToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenVendedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GráficasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobrosADomicilioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MedidoresToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BitácoraDePruebasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BitácoraDeCorreosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesConAdeudoDeMaterialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesVariosMezcladosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContratoForzosoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoDeCuentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CarteraEjecutivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfazCablemodemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfazDigitalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecontratacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DecodificadoresToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeCumpleañosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PromesasDePagoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PruebasDeInternetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecordatoriosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MensualidadesTotalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RoboDeSeñalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteSTBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenEjecutivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDelSistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfazCablemodemsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfasDecodificadoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDeBancosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BancosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesProsaBancomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDeInterfacesDigitalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDeOXXOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionDelSistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BitacoraDelSistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerfilesSISTEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerfilesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfazDigitalesXcriptToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfazFtthToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.CMBLabelSistema = New System.Windows.Forms.Label()
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.DameClv_Session_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_ServiciosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter()
        Me.Valida_periodo_reportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_periodo_reportesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Valida_periodo_reportesTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Borrar_Tablas_Reporte_nuevoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borrar_Tablas_Reporte_nuevoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borrar_Tablas_Reporte_nuevoTableAdapter()
        Me.Borra_Separacion_ClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Separacion_ClientesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Separacion_ClientesTableAdapter()
        Me.Borra_temporales_trabajosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_temporales_trabajosTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borra_temporales_trabajosTableAdapter()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.DataSetarnoldo1 = New sofTV.DataSetarnoldo()
        Me.Selecciona_Impresora_SucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_Impresora_SucursalTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Selecciona_Impresora_SucursalTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter9 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter10 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter11 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter12 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter13 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter14 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter15 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter16 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter17 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter18 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter19 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter20 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter21 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter22 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter23 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter24 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter25 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter26 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabelSistema2 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter27 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter28 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter29 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.LabelNombreUsuario = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter30 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter31 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter32 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter33 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter34 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter35 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter36 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter37 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter38 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.DameTipoUsusarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameEspecifBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameTipoUsusarioBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamePermisosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ALTASMENUSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ALTASformsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CMBNombreLabel = New System.Windows.Forms.Label()
        LabelUsuario = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_periodo_reportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borrar_Tablas_Reporte_nuevoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Separacion_ClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_temporales_trabajosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameTipoUsusarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameEspecifBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameTipoUsusarioBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamePermisosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALTASMENUSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALTASformsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBNombreLabel
        '
        CMBNombreLabel.AutoSize = True
        CMBNombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBNombreLabel.ForeColor = System.Drawing.Color.Gray
        CMBNombreLabel.Location = New System.Drawing.Point(737, 685)
        CMBNombreLabel.Name = "CMBNombreLabel"
        CMBNombreLabel.Size = New System.Drawing.Size(75, 20)
        CMBNombreLabel.TabIndex = 7
        CMBNombreLabel.Text = "Ciudad :"
        CMBNombreLabel.Visible = False
        '
        'LabelUsuario
        '
        LabelUsuario.AutoSize = True
        LabelUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LabelUsuario.ForeColor = System.Drawing.Color.Gray
        LabelUsuario.Location = New System.Drawing.Point(44, 618)
        LabelUsuario.Name = "LabelUsuario"
        LabelUsuario.Size = New System.Drawing.Size(81, 20)
        LabelUsuario.TabIndex = 21
        LabelUsuario.Text = "Usuario :"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Orange
        Me.MenuStrip1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CatálogosToolStripMenuItem, Me.ProcesosToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.GeneralesToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1036, 26)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.TabStop = True
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CatálogosToolStripMenuItem
        '
        Me.CatálogosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CatálogosDeClientesToolStripMenuItem, Me.CatalogoÁreaTécnicaToolStripMenuItem, Me.CatálogoDeGeneralesToolStripMenuItem, Me.CatálogoDeVentasToolStripMenuItem, Me.PolizaToolStripMenuItem, Me.MedidoresToolStripMenuItem})
        Me.CatálogosToolStripMenuItem.Name = "CatálogosToolStripMenuItem"
        Me.CatálogosToolStripMenuItem.Size = New System.Drawing.Size(94, 22)
        Me.CatálogosToolStripMenuItem.Text = "&Catálogos"
        '
        'CatálogosDeClientesToolStripMenuItem
        '
        Me.CatálogosDeClientesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesToolStripMenuItem1})
        Me.CatálogosDeClientesToolStripMenuItem.Name = "CatálogosDeClientesToolStripMenuItem"
        Me.CatálogosDeClientesToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CatálogosDeClientesToolStripMenuItem.Text = "Clientes"
        '
        'ClientesToolStripMenuItem1
        '
        Me.ClientesToolStripMenuItem1.Name = "ClientesToolStripMenuItem1"
        Me.ClientesToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.ClientesToolStripMenuItem1.Text = "Clientes"
        '
        'CatalogoÁreaTécnicaToolStripMenuItem
        '
        Me.CatalogoÁreaTécnicaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SectoresToolStripMenuItem, Me.PostesToolStripMenuItem, Me.ÁreasToolStripMenuItem, Me.ServiciosAlClienteToolStripMenuItem, Me.ClasificaciónTécnicaToolStripMenuItem, Me.TecnicosToolStripMenuItem, Me.ClasificaciónProblemasToolStripMenuItem, Me.HUBToolStripMenuItem, Me.OLTToolStripMenuItem, Me.NAPToolStripMenuItem})
        Me.CatalogoÁreaTécnicaToolStripMenuItem.Name = "CatalogoÁreaTécnicaToolStripMenuItem"
        Me.CatalogoÁreaTécnicaToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CatalogoÁreaTécnicaToolStripMenuItem.Text = "Área Técnica"
        '
        'SectoresToolStripMenuItem
        '
        Me.SectoresToolStripMenuItem.Name = "SectoresToolStripMenuItem"
        Me.SectoresToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.SectoresToolStripMenuItem.Text = "Cuadrantes"
        '
        'PostesToolStripMenuItem
        '
        Me.PostesToolStripMenuItem.Name = "PostesToolStripMenuItem"
        Me.PostesToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.PostesToolStripMenuItem.Text = "Taps"
        '
        'ÁreasToolStripMenuItem
        '
        Me.ÁreasToolStripMenuItem.Name = "ÁreasToolStripMenuItem"
        Me.ÁreasToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.ÁreasToolStripMenuItem.Text = "Clave Técnica"
        '
        'ServiciosAlClienteToolStripMenuItem
        '
        Me.ServiciosAlClienteToolStripMenuItem.Name = "ServiciosAlClienteToolStripMenuItem"
        Me.ServiciosAlClienteToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.ServiciosAlClienteToolStripMenuItem.Text = "Servicios al Cliente"
        '
        'ClasificaciónTécnicaToolStripMenuItem
        '
        Me.ClasificaciónTécnicaToolStripMenuItem.Name = "ClasificaciónTécnicaToolStripMenuItem"
        Me.ClasificaciónTécnicaToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.ClasificaciónTécnicaToolStripMenuItem.Text = "Clasificación Técnica"
        '
        'TecnicosToolStripMenuItem
        '
        Me.TecnicosToolStripMenuItem.Name = "TecnicosToolStripMenuItem"
        Me.TecnicosToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.TecnicosToolStripMenuItem.Text = "Tecnicos"
        Me.TecnicosToolStripMenuItem.Visible = False
        '
        'ClasificaciónProblemasToolStripMenuItem
        '
        Me.ClasificaciónProblemasToolStripMenuItem.Name = "ClasificaciónProblemasToolStripMenuItem"
        Me.ClasificaciónProblemasToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.ClasificaciónProblemasToolStripMenuItem.Text = "Clasificación Problemas"
        '
        'HUBToolStripMenuItem
        '
        Me.HUBToolStripMenuItem.Name = "HUBToolStripMenuItem"
        Me.HUBToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.HUBToolStripMenuItem.Text = "HUB"
        '
        'OLTToolStripMenuItem
        '
        Me.OLTToolStripMenuItem.Name = "OLTToolStripMenuItem"
        Me.OLTToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.OLTToolStripMenuItem.Text = "OLT"
        '
        'NAPToolStripMenuItem
        '
        Me.NAPToolStripMenuItem.Name = "NAPToolStripMenuItem"
        Me.NAPToolStripMenuItem.Size = New System.Drawing.Size(255, 22)
        Me.NAPToolStripMenuItem.Text = "NAP"
        '
        'CatálogoDeGeneralesToolStripMenuItem
        '
        Me.CatálogoDeGeneralesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TiposDeServicioToolStripMenuItem, Me.ServiciosToolStripMenuItem, Me.CableModemsYAparatosDigitalesToolStripMenuItem, Me.ColoniasToolStripMenuItem, Me.TiposDeColoniasToolStripMenuItem, Me.CallesToolStripMenuItem, Me.CiudadesToolStripMenuItem, Me.SucursalesToolStripMenuItem, Me.BancosToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.CajasToolStripMenuItem, Me.MotivosDeCancelaciónToolStripMenuItem, Me.MotivosDeCancelaciónFacturasToolStripMenuItem, Me.MotivosDeReImpresiónFacturasToolStripMenuItem, Me.DescuentosComboToolStripMenuItem, Me.MensajesPrefijosToolStripMenuItem, Me.RentaACajasToolStripMenuItem, Me.ToolStripMenuItem2, Me.TipoDeGastosToolStripMenuItem, Me.TipoUsuarioToolStripMenuItem, Me.TvsAdicionalesPorCiudadToolStripMenuItem, Me.IndicadoresPagosToolStripMenuItem, Me.VelocidadesDeInternetFTTHToolStripMenuItem, Me.DireccionesIPToolStripMenuItem})
        Me.CatálogoDeGeneralesToolStripMenuItem.Name = "CatálogoDeGeneralesToolStripMenuItem"
        Me.CatálogoDeGeneralesToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CatálogoDeGeneralesToolStripMenuItem.Text = "Catálogo de Generales"
        '
        'TiposDeServicioToolStripMenuItem
        '
        Me.TiposDeServicioToolStripMenuItem.Name = "TiposDeServicioToolStripMenuItem"
        Me.TiposDeServicioToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.TiposDeServicioToolStripMenuItem.Text = "Tipos de Servicio"
        '
        'ServiciosToolStripMenuItem
        '
        Me.ServiciosToolStripMenuItem.Name = "ServiciosToolStripMenuItem"
        Me.ServiciosToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.ServiciosToolStripMenuItem.Text = "Servicios"
        '
        'CableModemsYAparatosDigitalesToolStripMenuItem
        '
        Me.CableModemsYAparatosDigitalesToolStripMenuItem.Name = "CableModemsYAparatosDigitalesToolStripMenuItem"
        Me.CableModemsYAparatosDigitalesToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.CableModemsYAparatosDigitalesToolStripMenuItem.Text = "Aparatos Digitales"
        '
        'ColoniasToolStripMenuItem
        '
        Me.ColoniasToolStripMenuItem.Name = "ColoniasToolStripMenuItem"
        Me.ColoniasToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.ColoniasToolStripMenuItem.Text = "Barrios"
        '
        'TiposDeColoniasToolStripMenuItem
        '
        Me.TiposDeColoniasToolStripMenuItem.Name = "TiposDeColoniasToolStripMenuItem"
        Me.TiposDeColoniasToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.TiposDeColoniasToolStripMenuItem.Text = "Tipos de Barrio"
        '
        'CallesToolStripMenuItem
        '
        Me.CallesToolStripMenuItem.Name = "CallesToolStripMenuItem"
        Me.CallesToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.CallesToolStripMenuItem.Text = "Calles"
        '
        'CiudadesToolStripMenuItem
        '
        Me.CiudadesToolStripMenuItem.Name = "CiudadesToolStripMenuItem"
        Me.CiudadesToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.CiudadesToolStripMenuItem.Text = "Ciudades"
        '
        'SucursalesToolStripMenuItem
        '
        Me.SucursalesToolStripMenuItem.Name = "SucursalesToolStripMenuItem"
        Me.SucursalesToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.SucursalesToolStripMenuItem.Text = "Sucursales"
        '
        'BancosToolStripMenuItem
        '
        Me.BancosToolStripMenuItem.Name = "BancosToolStripMenuItem"
        Me.BancosToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.BancosToolStripMenuItem.Text = "Bancos"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'CajasToolStripMenuItem
        '
        Me.CajasToolStripMenuItem.Name = "CajasToolStripMenuItem"
        Me.CajasToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.CajasToolStripMenuItem.Text = "Cajas"
        '
        'MotivosDeCancelaciónToolStripMenuItem
        '
        Me.MotivosDeCancelaciónToolStripMenuItem.Name = "MotivosDeCancelaciónToolStripMenuItem"
        Me.MotivosDeCancelaciónToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.MotivosDeCancelaciónToolStripMenuItem.Text = "Motivos de Cancelación"
        '
        'MotivosDeCancelaciónFacturasToolStripMenuItem
        '
        Me.MotivosDeCancelaciónFacturasToolStripMenuItem.Name = "MotivosDeCancelaciónFacturasToolStripMenuItem"
        Me.MotivosDeCancelaciónFacturasToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.MotivosDeCancelaciónFacturasToolStripMenuItem.Text = "Motivos de Cancelación Pagos"
        '
        'MotivosDeReImpresiónFacturasToolStripMenuItem
        '
        Me.MotivosDeReImpresiónFacturasToolStripMenuItem.Name = "MotivosDeReImpresiónFacturasToolStripMenuItem"
        Me.MotivosDeReImpresiónFacturasToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.MotivosDeReImpresiónFacturasToolStripMenuItem.Text = "Motivos de ReImpresión Pagos"
        '
        'DescuentosComboToolStripMenuItem
        '
        Me.DescuentosComboToolStripMenuItem.Name = "DescuentosComboToolStripMenuItem"
        Me.DescuentosComboToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.DescuentosComboToolStripMenuItem.Text = "Descuentos Combo"
        '
        'MensajesPrefijosToolStripMenuItem
        '
        Me.MensajesPrefijosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrefijosDeClientesToolStripMenuItem, Me.TipoDeMensajesToolStripMenuItem, Me.CatalogoDeMensajesToolStripMenuItem})
        Me.MensajesPrefijosToolStripMenuItem.Name = "MensajesPrefijosToolStripMenuItem"
        Me.MensajesPrefijosToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.MensajesPrefijosToolStripMenuItem.Text = "Mensajes Personalizados"
        '
        'PrefijosDeClientesToolStripMenuItem
        '
        Me.PrefijosDeClientesToolStripMenuItem.Name = "PrefijosDeClientesToolStripMenuItem"
        Me.PrefijosDeClientesToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.PrefijosDeClientesToolStripMenuItem.Text = "Prefijos de Clientes"
        '
        'TipoDeMensajesToolStripMenuItem
        '
        Me.TipoDeMensajesToolStripMenuItem.Name = "TipoDeMensajesToolStripMenuItem"
        Me.TipoDeMensajesToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.TipoDeMensajesToolStripMenuItem.Text = "Tipo de Mensajes"
        '
        'CatalogoDeMensajesToolStripMenuItem
        '
        Me.CatalogoDeMensajesToolStripMenuItem.Name = "CatalogoDeMensajesToolStripMenuItem"
        Me.CatalogoDeMensajesToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.CatalogoDeMensajesToolStripMenuItem.Text = "Catalogo de Mensajes"
        '
        'RentaACajasToolStripMenuItem
        '
        Me.RentaACajasToolStripMenuItem.Name = "RentaACajasToolStripMenuItem"
        Me.RentaACajasToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.RentaACajasToolStripMenuItem.Text = "Costo de STB"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(344, 22)
        Me.ToolStripMenuItem2.Text = "Tarifas Fijas de Contratación"
        '
        'TipoDeGastosToolStripMenuItem
        '
        Me.TipoDeGastosToolStripMenuItem.Name = "TipoDeGastosToolStripMenuItem"
        Me.TipoDeGastosToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.TipoDeGastosToolStripMenuItem.Text = "Tipo de Gastos"
        '
        'TipoUsuarioToolStripMenuItem
        '
        Me.TipoUsuarioToolStripMenuItem.Name = "TipoUsuarioToolStripMenuItem"
        Me.TipoUsuarioToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.TipoUsuarioToolStripMenuItem.Text = "Tipo Usuario"
        '
        'TvsAdicionalesPorCiudadToolStripMenuItem
        '
        Me.TvsAdicionalesPorCiudadToolStripMenuItem.Name = "TvsAdicionalesPorCiudadToolStripMenuItem"
        Me.TvsAdicionalesPorCiudadToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.TvsAdicionalesPorCiudadToolStripMenuItem.Text = "TV incluidas en el costo del contrato"
        '
        'IndicadoresPagosToolStripMenuItem
        '
        Me.IndicadoresPagosToolStripMenuItem.Name = "IndicadoresPagosToolStripMenuItem"
        Me.IndicadoresPagosToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.IndicadoresPagosToolStripMenuItem.Text = "Indicadores Pagos Adeudo"
        '
        'VelocidadesDeInternetFTTHToolStripMenuItem
        '
        Me.VelocidadesDeInternetFTTHToolStripMenuItem.Name = "VelocidadesDeInternetFTTHToolStripMenuItem"
        Me.VelocidadesDeInternetFTTHToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.VelocidadesDeInternetFTTHToolStripMenuItem.Text = "Velocidades de Internet FTTH"
        '
        'DireccionesIPToolStripMenuItem
        '
        Me.DireccionesIPToolStripMenuItem.Name = "DireccionesIPToolStripMenuItem"
        Me.DireccionesIPToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.DireccionesIPToolStripMenuItem.Text = "Direcciones IP"
        '
        'CatálogoDeVentasToolStripMenuItem
        '
        Me.CatálogoDeVentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PromotoresToolStripMenuItem, Me.SeriesToolStripMenuItem, Me.RangosToolStripMenuItem, Me.PrecioDeComisionesToolStripMenuItem, Me.GrupoDeVentasToolStripMenuItem, Me.EstablecerComisionesCobroADomicilioToolStripMenuItem})
        Me.CatálogoDeVentasToolStripMenuItem.Name = "CatálogoDeVentasToolStripMenuItem"
        Me.CatálogoDeVentasToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CatálogoDeVentasToolStripMenuItem.Text = "Ventas"
        '
        'PromotoresToolStripMenuItem
        '
        Me.PromotoresToolStripMenuItem.Name = "PromotoresToolStripMenuItem"
        Me.PromotoresToolStripMenuItem.Size = New System.Drawing.Size(379, 22)
        Me.PromotoresToolStripMenuItem.Text = "Vendedores"
        '
        'SeriesToolStripMenuItem
        '
        Me.SeriesToolStripMenuItem.Name = "SeriesToolStripMenuItem"
        Me.SeriesToolStripMenuItem.Size = New System.Drawing.Size(379, 22)
        Me.SeriesToolStripMenuItem.Text = "Dosificación"
        '
        'RangosToolStripMenuItem
        '
        Me.RangosToolStripMenuItem.Name = "RangosToolStripMenuItem"
        Me.RangosToolStripMenuItem.Size = New System.Drawing.Size(379, 22)
        Me.RangosToolStripMenuItem.Text = "Rangos"
        '
        'PrecioDeComisionesToolStripMenuItem
        '
        Me.PrecioDeComisionesToolStripMenuItem.Name = "PrecioDeComisionesToolStripMenuItem"
        Me.PrecioDeComisionesToolStripMenuItem.Size = New System.Drawing.Size(379, 22)
        Me.PrecioDeComisionesToolStripMenuItem.Text = "Establecer Comisiones por Servicio"
        '
        'GrupoDeVentasToolStripMenuItem
        '
        Me.GrupoDeVentasToolStripMenuItem.Name = "GrupoDeVentasToolStripMenuItem"
        Me.GrupoDeVentasToolStripMenuItem.Size = New System.Drawing.Size(379, 22)
        Me.GrupoDeVentasToolStripMenuItem.Text = "Grupo de Ventas"
        '
        'EstablecerComisionesCobroADomicilioToolStripMenuItem
        '
        Me.EstablecerComisionesCobroADomicilioToolStripMenuItem.Name = "EstablecerComisionesCobroADomicilioToolStripMenuItem"
        Me.EstablecerComisionesCobroADomicilioToolStripMenuItem.Size = New System.Drawing.Size(379, 22)
        Me.EstablecerComisionesCobroADomicilioToolStripMenuItem.Text = "Establecer Comisiones Cobro a Domicilio"
        '
        'PolizaToolStripMenuItem
        '
        Me.PolizaToolStripMenuItem.Name = "PolizaToolStripMenuItem"
        Me.PolizaToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.PolizaToolStripMenuItem.Text = "Poliza"
        Me.PolizaToolStripMenuItem.Visible = False
        '
        'MedidoresToolStripMenuItem
        '
        Me.MedidoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ServiciosDeVentasToolStripMenuItem, Me.IndividualesToolStripMenuItem, Me.IngresosToolStripMenuItem, Me.CarteraToolStripMenuItem})
        Me.MedidoresToolStripMenuItem.Name = "MedidoresToolStripMenuItem"
        Me.MedidoresToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.MedidoresToolStripMenuItem.Text = "Medidores"
        '
        'ServiciosDeVentasToolStripMenuItem
        '
        Me.ServiciosDeVentasToolStripMenuItem.Name = "ServiciosDeVentasToolStripMenuItem"
        Me.ServiciosDeVentasToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.ServiciosDeVentasToolStripMenuItem.Text = "Globales"
        '
        'IndividualesToolStripMenuItem
        '
        Me.IndividualesToolStripMenuItem.Name = "IndividualesToolStripMenuItem"
        Me.IndividualesToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.IndividualesToolStripMenuItem.Text = "Individuales"
        '
        'IngresosToolStripMenuItem
        '
        Me.IngresosToolStripMenuItem.Name = "IngresosToolStripMenuItem"
        Me.IngresosToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.IngresosToolStripMenuItem.Text = "Ingresos"
        '
        'CarteraToolStripMenuItem
        '
        Me.CarteraToolStripMenuItem.Name = "CarteraToolStripMenuItem"
        Me.CarteraToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.CarteraToolStripMenuItem.Text = "Cartera"
        '
        'ProcesosToolStripMenuItem
        '
        Me.ProcesosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProcesosDeServicioPremiumToolStripMenuItem, Me.DesconexionToolStripMenuItem, Me.OrdenesDeServicioToolStripMenuItem, Me.QuejasToolStripMenuItem, Me.AgendaToolStripMenuItem1, Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem, Me.ActivaciónPaqueteDePruebaToolStripMenuItem, Me.DepuraciónDeÓrdenesToolStripMenuItem, Me.CambioDeServicioToolStripMenuItem, Me.MesajesInstantaneosToolStripMenuItem, Me.MsjsPersonalizadosToolStripMenuItem, Me.CorreoToolStripMenuItem, Me.ResetearAparatosToolStripMenuItem, Me.ReseteoMasivoDeAparatosToolStripMenuItem, Me.CambioDeClienteASoloInternetToolStripMenuItem, Me.CambioDeClienteAClienteNormalToolStripMenuItem, Me.CortesToolStripMenuItem, Me.BajaDeServiciosToolStripMenuItem, Me.RecontrataciónToolStripMenuItem, Me.RegresaAparatosAlAlmacenToolStripMenuItem, Me.PrórrogasToolStripMenuItem, Me.PruebaDeInternetToolStripMenuItem, Me.DevoluciónDeAparatosToolStripMenuItem, Me.GenerarÓrdenesDeDesconexiónToolStripMenuItem, Me.ToolStripMenuItem1, Me.CostoPorAparatoToolStripMenuItem, Me.CambioDePeriodoToolStripMenuItem1, Me.HistorialDeCambiosDePeriodoToolStripMenuItem, Me.DdToolStripMenuItem, Me.ProcesoMasivoCambioDeAparatoToolStripMenuItem, Me.ProcesosComcastToolStripMenuItem})
        Me.ProcesosToolStripMenuItem.Name = "ProcesosToolStripMenuItem"
        Me.ProcesosToolStripMenuItem.Size = New System.Drawing.Size(88, 22)
        Me.ProcesosToolStripMenuItem.Text = "Procesos"
        '
        'ProcesosDeServicioPremiumToolStripMenuItem
        '
        Me.ProcesosDeServicioPremiumToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrimerPeriodoToolStripMenuItem1, Me.SegundoPeriodoToolStripMenuItem1})
        Me.ProcesosDeServicioPremiumToolStripMenuItem.Name = "ProcesosDeServicioPremiumToolStripMenuItem"
        Me.ProcesosDeServicioPremiumToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ProcesosDeServicioPremiumToolStripMenuItem.Text = "Desconexión"
        '
        'PrimerPeriodoToolStripMenuItem1
        '
        Me.PrimerPeriodoToolStripMenuItem1.Name = "PrimerPeriodoToolStripMenuItem1"
        Me.PrimerPeriodoToolStripMenuItem1.Size = New System.Drawing.Size(202, 22)
        Me.PrimerPeriodoToolStripMenuItem1.Text = "Primer Periodo"
        '
        'SegundoPeriodoToolStripMenuItem1
        '
        Me.SegundoPeriodoToolStripMenuItem1.Name = "SegundoPeriodoToolStripMenuItem1"
        Me.SegundoPeriodoToolStripMenuItem1.Size = New System.Drawing.Size(202, 22)
        Me.SegundoPeriodoToolStripMenuItem1.Text = "Segundo Periodo"
        '
        'DesconexionToolStripMenuItem
        '
        Me.DesconexionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ServicioBasicoYCanalesPremiumToolStripMenuItem, Me.InternetToolStripMenuItem})
        Me.DesconexionToolStripMenuItem.Name = "DesconexionToolStripMenuItem"
        Me.DesconexionToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.DesconexionToolStripMenuItem.Text = "Desconexión"
        Me.DesconexionToolStripMenuItem.Visible = False
        '
        'ServicioBasicoYCanalesPremiumToolStripMenuItem
        '
        Me.ServicioBasicoYCanalesPremiumToolStripMenuItem.Name = "ServicioBasicoYCanalesPremiumToolStripMenuItem"
        Me.ServicioBasicoYCanalesPremiumToolStripMenuItem.Size = New System.Drawing.Size(339, 22)
        Me.ServicioBasicoYCanalesPremiumToolStripMenuItem.Text = "Servicio Basico Y Canales Premium"
        '
        'InternetToolStripMenuItem
        '
        Me.InternetToolStripMenuItem.Name = "InternetToolStripMenuItem"
        Me.InternetToolStripMenuItem.Size = New System.Drawing.Size(339, 22)
        Me.InternetToolStripMenuItem.Text = "Internet"
        '
        'OrdenesDeServicioToolStripMenuItem
        '
        Me.OrdenesDeServicioToolStripMenuItem.Name = "OrdenesDeServicioToolStripMenuItem"
        Me.OrdenesDeServicioToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.OrdenesDeServicioToolStripMenuItem.Text = "Ordenes de Servicio"
        '
        'QuejasToolStripMenuItem
        '
        Me.QuejasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.QuejasToolStripMenuItem1, Me.AgendaToolStripMenuItem, Me.AtenciónTelefónicaToolStripMenuItem})
        Me.QuejasToolStripMenuItem.Name = "QuejasToolStripMenuItem"
        Me.QuejasToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.QuejasToolStripMenuItem.Text = "Quejas"
        '
        'QuejasToolStripMenuItem1
        '
        Me.QuejasToolStripMenuItem1.Name = "QuejasToolStripMenuItem1"
        Me.QuejasToolStripMenuItem1.Size = New System.Drawing.Size(191, 22)
        Me.QuejasToolStripMenuItem1.Text = "Quejas"
        '
        'AgendaToolStripMenuItem
        '
        Me.AgendaToolStripMenuItem.Name = "AgendaToolStripMenuItem"
        Me.AgendaToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.AgendaToolStripMenuItem.Text = "Agenda"
        Me.AgendaToolStripMenuItem.Visible = False
        '
        'AtenciónTelefónicaToolStripMenuItem
        '
        Me.AtenciónTelefónicaToolStripMenuItem.Name = "AtenciónTelefónicaToolStripMenuItem"
        Me.AtenciónTelefónicaToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.AtenciónTelefónicaToolStripMenuItem.Text = "Levantar Queja"
        '
        'AgendaToolStripMenuItem1
        '
        Me.AgendaToolStripMenuItem1.Name = "AgendaToolStripMenuItem1"
        Me.AgendaToolStripMenuItem1.Size = New System.Drawing.Size(473, 22)
        Me.AgendaToolStripMenuItem1.Text = "Agenda"
        '
        'ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem
        '
        Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Name = "ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem"
        Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Text = "Desconexión Temporal por Contrato"
        '
        'ActivaciónPaqueteDePruebaToolStripMenuItem
        '
        Me.ActivaciónPaqueteDePruebaToolStripMenuItem.Name = "ActivaciónPaqueteDePruebaToolStripMenuItem"
        Me.ActivaciónPaqueteDePruebaToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ActivaciónPaqueteDePruebaToolStripMenuItem.Text = "Activación Paquete de Prueba"
        '
        'DepuraciónDeÓrdenesToolStripMenuItem
        '
        Me.DepuraciónDeÓrdenesToolStripMenuItem.Name = "DepuraciónDeÓrdenesToolStripMenuItem"
        Me.DepuraciónDeÓrdenesToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.DepuraciónDeÓrdenesToolStripMenuItem.Text = "Depuración de Órdenes"
        Me.DepuraciónDeÓrdenesToolStripMenuItem.Visible = False
        '
        'CambioDeServicioToolStripMenuItem
        '
        Me.CambioDeServicioToolStripMenuItem.Name = "CambioDeServicioToolStripMenuItem"
        Me.CambioDeServicioToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CambioDeServicioToolStripMenuItem.Text = "Cambio de Paquete"
        '
        'MesajesInstantaneosToolStripMenuItem
        '
        Me.MesajesInstantaneosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesVariosToolStripMenuItem, Me.ClienteToolStripMenuItem, Me.PruebaToolStripMenuItem})
        Me.MesajesInstantaneosToolStripMenuItem.Name = "MesajesInstantaneosToolStripMenuItem"
        Me.MesajesInstantaneosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.MesajesInstantaneosToolStripMenuItem.Text = "Mensajes Instantáneos"
        '
        'ClientesVariosToolStripMenuItem
        '
        Me.ClientesVariosToolStripMenuItem.Name = "ClientesVariosToolStripMenuItem"
        Me.ClientesVariosToolStripMenuItem.Size = New System.Drawing.Size(276, 22)
        Me.ClientesVariosToolStripMenuItem.Text = "Clientes Varios"
        '
        'ClienteToolStripMenuItem
        '
        Me.ClienteToolStripMenuItem.Name = "ClienteToolStripMenuItem"
        Me.ClienteToolStripMenuItem.Size = New System.Drawing.Size(276, 22)
        Me.ClienteToolStripMenuItem.Text = "Por Cliente"
        '
        'PruebaToolStripMenuItem
        '
        Me.PruebaToolStripMenuItem.Name = "PruebaToolStripMenuItem"
        Me.PruebaToolStripMenuItem.Size = New System.Drawing.Size(276, 22)
        Me.PruebaToolStripMenuItem.Text = "Programación de Mensajes"
        '
        'MsjsPersonalizadosToolStripMenuItem
        '
        Me.MsjsPersonalizadosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevaProgramaciónToolStripMenuItem, Me.CtgMsjPerToolStripMenuItem})
        Me.MsjsPersonalizadosToolStripMenuItem.Name = "MsjsPersonalizadosToolStripMenuItem"
        Me.MsjsPersonalizadosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.MsjsPersonalizadosToolStripMenuItem.Text = "Mensajes Instantáneos Personalizados"
        '
        'NuevaProgramaciónToolStripMenuItem
        '
        Me.NuevaProgramaciónToolStripMenuItem.Name = "NuevaProgramaciónToolStripMenuItem"
        Me.NuevaProgramaciónToolStripMenuItem.Size = New System.Drawing.Size(276, 22)
        Me.NuevaProgramaciónToolStripMenuItem.Text = "Nueva Programación"
        '
        'CtgMsjPerToolStripMenuItem
        '
        Me.CtgMsjPerToolStripMenuItem.Name = "CtgMsjPerToolStripMenuItem"
        Me.CtgMsjPerToolStripMenuItem.Size = New System.Drawing.Size(276, 22)
        Me.CtgMsjPerToolStripMenuItem.Text = "Programación de Mensajes"
        '
        'CorreoToolStripMenuItem
        '
        Me.CorreoToolStripMenuItem.Name = "CorreoToolStripMenuItem"
        Me.CorreoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CorreoToolStripMenuItem.Text = "Correo"
        '
        'ResetearAparatosToolStripMenuItem
        '
        Me.ResetearAparatosToolStripMenuItem.Name = "ResetearAparatosToolStripMenuItem"
        Me.ResetearAparatosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ResetearAparatosToolStripMenuItem.Text = "Resetear Aparatos"
        '
        'ReseteoMasivoDeAparatosToolStripMenuItem
        '
        Me.ReseteoMasivoDeAparatosToolStripMenuItem.Name = "ReseteoMasivoDeAparatosToolStripMenuItem"
        Me.ReseteoMasivoDeAparatosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ReseteoMasivoDeAparatosToolStripMenuItem.Text = "Reseteo Masivo de Aparatos"
        '
        'CambioDeClienteASoloInternetToolStripMenuItem
        '
        Me.CambioDeClienteASoloInternetToolStripMenuItem.Name = "CambioDeClienteASoloInternetToolStripMenuItem"
        Me.CambioDeClienteASoloInternetToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CambioDeClienteASoloInternetToolStripMenuItem.Text = "Cambio de Cliente Normal a Solo Internet"
        '
        'CambioDeClienteAClienteNormalToolStripMenuItem
        '
        Me.CambioDeClienteAClienteNormalToolStripMenuItem.Name = "CambioDeClienteAClienteNormalToolStripMenuItem"
        Me.CambioDeClienteAClienteNormalToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CambioDeClienteAClienteNormalToolStripMenuItem.Text = "Cambio de Solo Internet a Cliente Normal"
        '
        'CortesToolStripMenuItem
        '
        Me.CortesToolStripMenuItem.Name = "CortesToolStripMenuItem"
        Me.CortesToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CortesToolStripMenuItem.Text = "Cortes"
        Me.CortesToolStripMenuItem.Visible = False
        '
        'BajaDeServiciosToolStripMenuItem
        '
        Me.BajaDeServiciosToolStripMenuItem.Name = "BajaDeServiciosToolStripMenuItem"
        Me.BajaDeServiciosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.BajaDeServiciosToolStripMenuItem.Text = "Cancelación de Servicios"
        '
        'RecontrataciónToolStripMenuItem
        '
        Me.RecontrataciónToolStripMenuItem.Name = "RecontrataciónToolStripMenuItem"
        Me.RecontrataciónToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.RecontrataciónToolStripMenuItem.Text = "Recontratación"
        '
        'RegresaAparatosAlAlmacenToolStripMenuItem
        '
        Me.RegresaAparatosAlAlmacenToolStripMenuItem.Name = "RegresaAparatosAlAlmacenToolStripMenuItem"
        Me.RegresaAparatosAlAlmacenToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.RegresaAparatosAlAlmacenToolStripMenuItem.Text = "Regresa Aparatos al Almacen"
        '
        'PrórrogasToolStripMenuItem
        '
        Me.PrórrogasToolStripMenuItem.Name = "PrórrogasToolStripMenuItem"
        Me.PrórrogasToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.PrórrogasToolStripMenuItem.Text = "Promesas de Pago"
        '
        'PruebaDeInternetToolStripMenuItem
        '
        Me.PruebaDeInternetToolStripMenuItem.Name = "PruebaDeInternetToolStripMenuItem"
        Me.PruebaDeInternetToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.PruebaDeInternetToolStripMenuItem.Text = "Prueba de Internet"
        '
        'DevoluciónDeAparatosToolStripMenuItem
        '
        Me.DevoluciónDeAparatosToolStripMenuItem.Name = "DevoluciónDeAparatosToolStripMenuItem"
        Me.DevoluciónDeAparatosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.DevoluciónDeAparatosToolStripMenuItem.Text = "Devolución de Aparatos al Almacén"
        '
        'GenerarÓrdenesDeDesconexiónToolStripMenuItem
        '
        Me.GenerarÓrdenesDeDesconexiónToolStripMenuItem.Name = "GenerarÓrdenesDeDesconexiónToolStripMenuItem"
        Me.GenerarÓrdenesDeDesconexiónToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.GenerarÓrdenesDeDesconexiónToolStripMenuItem.Text = "Generar Órdenes de Desconexión"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(473, 22)
        Me.ToolStripMenuItem1.Text = "Cambio de Tipo de Servicio"
        '
        'CostoPorAparatoToolStripMenuItem
        '
        Me.CostoPorAparatoToolStripMenuItem.Name = "CostoPorAparatoToolStripMenuItem"
        Me.CostoPorAparatoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CostoPorAparatoToolStripMenuItem.Text = "Costo por Aparato"
        '
        'CambioDePeriodoToolStripMenuItem1
        '
        Me.CambioDePeriodoToolStripMenuItem1.Name = "CambioDePeriodoToolStripMenuItem1"
        Me.CambioDePeriodoToolStripMenuItem1.Size = New System.Drawing.Size(473, 22)
        Me.CambioDePeriodoToolStripMenuItem1.Text = "Cambio de Periodo"
        '
        'HistorialDeCambiosDePeriodoToolStripMenuItem
        '
        Me.HistorialDeCambiosDePeriodoToolStripMenuItem.Name = "HistorialDeCambiosDePeriodoToolStripMenuItem"
        Me.HistorialDeCambiosDePeriodoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.HistorialDeCambiosDePeriodoToolStripMenuItem.Text = "Historial de Cambios de Periodo"
        '
        'DdToolStripMenuItem
        '
        Me.DdToolStripMenuItem.Name = "DdToolStripMenuItem"
        Me.DdToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.DdToolStripMenuItem.Text = "Cambio de Identificador"
        '
        'ProcesoMasivoCambioDeAparatoToolStripMenuItem
        '
        Me.ProcesoMasivoCambioDeAparatoToolStripMenuItem.Name = "ProcesoMasivoCambioDeAparatoToolStripMenuItem"
        Me.ProcesoMasivoCambioDeAparatoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ProcesoMasivoCambioDeAparatoToolStripMenuItem.Text = "Generación Masiva de Órdenes de Cambio de Aparato"
        '
        'ProcesosComcastToolStripMenuItem
        '
        Me.ProcesosComcastToolStripMenuItem.Name = "ProcesosComcastToolStripMenuItem"
        Me.ProcesosComcastToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ProcesosComcastToolStripMenuItem.Text = "Procesos Comcast"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EstadosDeCarteraToolStripMenuItem, Me.ClientesToolStripMenuItem2, Me.AreaTécnicaToolStripMenuItem, Me.AnálisisDePenetraciónToolStripMenuItem, Me.VentasToolStripMenuItem, Me.MedidoresToolStripMenuItem1, Me.BitácoraDePruebasToolStripMenuItem, Me.BitácoraDeCorreosToolStripMenuItem, Me.ClientesConAdeudoDeMaterialToolStripMenuItem, Me.ClientesVariosMezcladosToolStripMenuItem, Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem, Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem, Me.ContratoForzosoToolStripMenuItem, Me.EstadoDeCuentaToolStripMenuItem, Me.CarteraEjecutivaToolStripMenuItem, Me.InterfazCablemodemsToolStripMenuItem, Me.InterfazDigitalesToolStripMenuItem, Me.RecontratacionesToolStripMenuItem, Me.DecodificadoresToolStripMenuItem1, Me.ListaDeCumpleañosToolStripMenuItem, Me.PromesasDePagoToolStripMenuItem, Me.PruebasDeInternetToolStripMenuItem, Me.RecordatoriosToolStripMenuItem, Me.MensualidadesTotalesToolStripMenuItem, Me.RoboDeSeñalToolStripMenuItem, Me.ReporteSTBToolStripMenuItem, Me.ResumenEjecutivoToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(88, 22)
        Me.ReportesToolStripMenuItem.Text = "&Reportes"
        '
        'EstadosDeCarteraToolStripMenuItem
        '
        Me.EstadosDeCarteraToolStripMenuItem.Name = "EstadosDeCarteraToolStripMenuItem"
        Me.EstadosDeCarteraToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.EstadosDeCarteraToolStripMenuItem.Text = "Estados de Cartera"
        '
        'ClientesToolStripMenuItem2
        '
        Me.ClientesToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReportesVariosToolStripMenuItem, Me.ReporteClientesConComboToolStripMenuItem, Me.HotelesToolStripMenuItem, Me.OrdenesToolStripMenuItem, Me.ReporteDePermanenciaToolStripMenuItem, Me.PlazoForzosoToolStripMenuItem})
        Me.ClientesToolStripMenuItem2.Name = "ClientesToolStripMenuItem2"
        Me.ClientesToolStripMenuItem2.Size = New System.Drawing.Size(377, 22)
        Me.ClientesToolStripMenuItem2.Text = "Clientes"
        '
        'ReportesVariosToolStripMenuItem
        '
        Me.ReportesVariosToolStripMenuItem.Name = "ReportesVariosToolStripMenuItem"
        Me.ReportesVariosToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.ReportesVariosToolStripMenuItem.Text = "Reportes Varios"
        '
        'ReporteClientesConComboToolStripMenuItem
        '
        Me.ReporteClientesConComboToolStripMenuItem.Name = "ReporteClientesConComboToolStripMenuItem"
        Me.ReporteClientesConComboToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.ReporteClientesConComboToolStripMenuItem.Text = "Reporte Clientes Con Combo"
        '
        'HotelesToolStripMenuItem
        '
        Me.HotelesToolStripMenuItem.Name = "HotelesToolStripMenuItem"
        Me.HotelesToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.HotelesToolStripMenuItem.Text = "Hoteles"
        '
        'OrdenesToolStripMenuItem
        '
        Me.OrdenesToolStripMenuItem.Name = "OrdenesToolStripMenuItem"
        Me.OrdenesToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.OrdenesToolStripMenuItem.Text = "Ordenes"
        '
        'ReporteDePermanenciaToolStripMenuItem
        '
        Me.ReporteDePermanenciaToolStripMenuItem.Name = "ReporteDePermanenciaToolStripMenuItem"
        Me.ReporteDePermanenciaToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.ReporteDePermanenciaToolStripMenuItem.Text = "Reporte de Permanencia"
        '
        'PlazoForzosoToolStripMenuItem
        '
        Me.PlazoForzosoToolStripMenuItem.Name = "PlazoForzosoToolStripMenuItem"
        Me.PlazoForzosoToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.PlazoForzosoToolStripMenuItem.Text = "Plazo Forzoso"
        '
        'AreaTécnicaToolStripMenuItem
        '
        Me.AreaTécnicaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OrdenesDeServicioToolStripMenuItem1, Me.QuejasToolStripMenuItem2, Me.LlamadasTelefónicasToolStripMenuItem, Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1, Me.AgendaDeActividadesDelTécnicoToolStripMenuItem, Me.ÓrdenesClientesToolStripMenuItem, Me.ClaveTécnicaToolStripMenuItem, Me.DevoluciónDeAparatosAlAlmacénToolStripMenuItem, Me.ResumenOrdenesQuejasToolStripMenuItem})
        Me.AreaTécnicaToolStripMenuItem.Name = "AreaTécnicaToolStripMenuItem"
        Me.AreaTécnicaToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.AreaTécnicaToolStripMenuItem.Text = "Area Técnica"
        '
        'OrdenesDeServicioToolStripMenuItem1
        '
        Me.OrdenesDeServicioToolStripMenuItem1.Name = "OrdenesDeServicioToolStripMenuItem1"
        Me.OrdenesDeServicioToolStripMenuItem1.Size = New System.Drawing.Size(337, 22)
        Me.OrdenesDeServicioToolStripMenuItem1.Text = "Ordenes de Servicio"
        '
        'QuejasToolStripMenuItem2
        '
        Me.QuejasToolStripMenuItem2.Name = "QuejasToolStripMenuItem2"
        Me.QuejasToolStripMenuItem2.Size = New System.Drawing.Size(337, 22)
        Me.QuejasToolStripMenuItem2.Text = "Quejas"
        '
        'LlamadasTelefónicasToolStripMenuItem
        '
        Me.LlamadasTelefónicasToolStripMenuItem.Name = "LlamadasTelefónicasToolStripMenuItem"
        Me.LlamadasTelefónicasToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.LlamadasTelefónicasToolStripMenuItem.Text = "Atención Telefónica"
        '
        'ListadoDeActividadesDelTécnicoToolStripMenuItem1
        '
        Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1.Name = "ListadoDeActividadesDelTécnicoToolStripMenuItem1"
        Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1.Size = New System.Drawing.Size(337, 22)
        Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1.Text = "Listado de Actividades del Técnico"
        '
        'AgendaDeActividadesDelTécnicoToolStripMenuItem
        '
        Me.AgendaDeActividadesDelTécnicoToolStripMenuItem.Name = "AgendaDeActividadesDelTécnicoToolStripMenuItem"
        Me.AgendaDeActividadesDelTécnicoToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.AgendaDeActividadesDelTécnicoToolStripMenuItem.Text = "Agenda de Actividades del Técnico"
        '
        'ÓrdenesClientesToolStripMenuItem
        '
        Me.ÓrdenesClientesToolStripMenuItem.Name = "ÓrdenesClientesToolStripMenuItem"
        Me.ÓrdenesClientesToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.ÓrdenesClientesToolStripMenuItem.Text = "Órdenes Clientes"
        '
        'ClaveTécnicaToolStripMenuItem
        '
        Me.ClaveTécnicaToolStripMenuItem.Name = "ClaveTécnicaToolStripMenuItem"
        Me.ClaveTécnicaToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.ClaveTécnicaToolStripMenuItem.Text = "Clave Técnica"
        '
        'DevoluciónDeAparatosAlAlmacénToolStripMenuItem
        '
        Me.DevoluciónDeAparatosAlAlmacénToolStripMenuItem.Name = "DevoluciónDeAparatosAlAlmacénToolStripMenuItem"
        Me.DevoluciónDeAparatosAlAlmacénToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.DevoluciónDeAparatosAlAlmacénToolStripMenuItem.Text = "Devolución de Aparatos al Almacén"
        '
        'ResumenOrdenesQuejasToolStripMenuItem
        '
        Me.ResumenOrdenesQuejasToolStripMenuItem.Name = "ResumenOrdenesQuejasToolStripMenuItem"
        Me.ResumenOrdenesQuejasToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.ResumenOrdenesQuejasToolStripMenuItem.Text = "Resumen Ordenes/Quejas"
        '
        'AnálisisDePenetraciónToolStripMenuItem
        '
        Me.AnálisisDePenetraciónToolStripMenuItem.Name = "AnálisisDePenetraciónToolStripMenuItem"
        Me.AnálisisDePenetraciónToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.AnálisisDePenetraciónToolStripMenuItem.Text = "Análisis de Penetración"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalcularComisionesPorVendedorToolStripMenuItem1, Me.ResumenDeVentasToolStripMenuItem, Me.ResumenVendedoresToolStripMenuItem, Me.GráficasToolStripMenuItem, Me.CobrosADomicilioToolStripMenuItem})
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.VentasToolStripMenuItem.Text = "Ventas"
        '
        'CalcularComisionesPorVendedorToolStripMenuItem1
        '
        Me.CalcularComisionesPorVendedorToolStripMenuItem1.Name = "CalcularComisionesPorVendedorToolStripMenuItem1"
        Me.CalcularComisionesPorVendedorToolStripMenuItem1.Size = New System.Drawing.Size(236, 22)
        Me.CalcularComisionesPorVendedorToolStripMenuItem1.Text = "Reportes Generales"
        '
        'ResumenDeVentasToolStripMenuItem
        '
        Me.ResumenDeVentasToolStripMenuItem.Name = "ResumenDeVentasToolStripMenuItem"
        Me.ResumenDeVentasToolStripMenuItem.Size = New System.Drawing.Size(236, 22)
        Me.ResumenDeVentasToolStripMenuItem.Text = "Resumen Sucursal"
        '
        'ResumenVendedoresToolStripMenuItem
        '
        Me.ResumenVendedoresToolStripMenuItem.Name = "ResumenVendedoresToolStripMenuItem"
        Me.ResumenVendedoresToolStripMenuItem.Size = New System.Drawing.Size(236, 22)
        Me.ResumenVendedoresToolStripMenuItem.Text = "Resumen Vendedores"
        '
        'GráficasToolStripMenuItem
        '
        Me.GráficasToolStripMenuItem.Name = "GráficasToolStripMenuItem"
        Me.GráficasToolStripMenuItem.Size = New System.Drawing.Size(236, 22)
        Me.GráficasToolStripMenuItem.Text = "Gráficas"
        '
        'CobrosADomicilioToolStripMenuItem
        '
        Me.CobrosADomicilioToolStripMenuItem.Name = "CobrosADomicilioToolStripMenuItem"
        Me.CobrosADomicilioToolStripMenuItem.Size = New System.Drawing.Size(236, 22)
        Me.CobrosADomicilioToolStripMenuItem.Text = "Cobros a Domicilio"
        '
        'MedidoresToolStripMenuItem1
        '
        Me.MedidoresToolStripMenuItem1.Name = "MedidoresToolStripMenuItem1"
        Me.MedidoresToolStripMenuItem1.Size = New System.Drawing.Size(377, 22)
        Me.MedidoresToolStripMenuItem1.Text = "Medidores"
        Me.MedidoresToolStripMenuItem1.Visible = False
        '
        'BitácoraDePruebasToolStripMenuItem
        '
        Me.BitácoraDePruebasToolStripMenuItem.Name = "BitácoraDePruebasToolStripMenuItem"
        Me.BitácoraDePruebasToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.BitácoraDePruebasToolStripMenuItem.Text = "Bitácora de Pruebas"
        '
        'BitácoraDeCorreosToolStripMenuItem
        '
        Me.BitácoraDeCorreosToolStripMenuItem.Name = "BitácoraDeCorreosToolStripMenuItem"
        Me.BitácoraDeCorreosToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.BitácoraDeCorreosToolStripMenuItem.Text = "Bitácora de Correos"
        '
        'ClientesConAdeudoDeMaterialToolStripMenuItem
        '
        Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Name = "ClientesConAdeudoDeMaterialToolStripMenuItem"
        Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Text = "Clientes con Adeudo de Material"
        '
        'ClientesVariosMezcladosToolStripMenuItem
        '
        Me.ClientesVariosMezcladosToolStripMenuItem.Name = "ClientesVariosMezcladosToolStripMenuItem"
        Me.ClientesVariosMezcladosToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.ClientesVariosMezcladosToolStripMenuItem.Text = "Clientes Varios Mezclados"
        Me.ClientesVariosMezcladosToolStripMenuItem.Visible = False
        '
        'DesgloceDeMensualidadesAdelantadasToolStripMenuItem
        '
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Name = "DesgloceDeMensualidadesAdelantadasToolStripMenuItem"
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Text = "Desgloce de Mensualidades Adelantadas"
        '
        'ImporteDeMensualidadesAdelantadasToolStripMenuItem
        '
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Name = "ImporteDeMensualidadesAdelantadasToolStripMenuItem"
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Text = "Importe De Mensualidades Adelantadas"
        '
        'ContratoForzosoToolStripMenuItem
        '
        Me.ContratoForzosoToolStripMenuItem.Name = "ContratoForzosoToolStripMenuItem"
        Me.ContratoForzosoToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.ContratoForzosoToolStripMenuItem.Text = "Contratos a Plazo Forzoso"
        '
        'EstadoDeCuentaToolStripMenuItem
        '
        Me.EstadoDeCuentaToolStripMenuItem.Name = "EstadoDeCuentaToolStripMenuItem"
        Me.EstadoDeCuentaToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.EstadoDeCuentaToolStripMenuItem.Text = "Estado de Cuenta"
        '
        'CarteraEjecutivaToolStripMenuItem
        '
        Me.CarteraEjecutivaToolStripMenuItem.Name = "CarteraEjecutivaToolStripMenuItem"
        Me.CarteraEjecutivaToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.CarteraEjecutivaToolStripMenuItem.Text = "Cartera Ejecutiva"
        '
        'InterfazCablemodemsToolStripMenuItem
        '
        Me.InterfazCablemodemsToolStripMenuItem.Name = "InterfazCablemodemsToolStripMenuItem"
        Me.InterfazCablemodemsToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.InterfazCablemodemsToolStripMenuItem.Text = "Interfaz Cablemodems"
        '
        'InterfazDigitalesToolStripMenuItem
        '
        Me.InterfazDigitalesToolStripMenuItem.Name = "InterfazDigitalesToolStripMenuItem"
        Me.InterfazDigitalesToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.InterfazDigitalesToolStripMenuItem.Text = "Interfaz Digitales"
        '
        'RecontratacionesToolStripMenuItem
        '
        Me.RecontratacionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoToolStripMenuItem})
        Me.RecontratacionesToolStripMenuItem.Name = "RecontratacionesToolStripMenuItem"
        Me.RecontratacionesToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.RecontratacionesToolStripMenuItem.Text = "Recontratación"
        '
        'ListadoToolStripMenuItem
        '
        Me.ListadoToolStripMenuItem.Name = "ListadoToolStripMenuItem"
        Me.ListadoToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.ListadoToolStripMenuItem.Text = "Listado"
        '
        'DecodificadoresToolStripMenuItem1
        '
        Me.DecodificadoresToolStripMenuItem1.Name = "DecodificadoresToolStripMenuItem1"
        Me.DecodificadoresToolStripMenuItem1.Size = New System.Drawing.Size(377, 22)
        Me.DecodificadoresToolStripMenuItem1.Text = "Decodificadores"
        '
        'ListaDeCumpleañosToolStripMenuItem
        '
        Me.ListaDeCumpleañosToolStripMenuItem.Name = "ListaDeCumpleañosToolStripMenuItem"
        Me.ListaDeCumpleañosToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.ListaDeCumpleañosToolStripMenuItem.Text = "Lista de Cumpleaños"
        '
        'PromesasDePagoToolStripMenuItem
        '
        Me.PromesasDePagoToolStripMenuItem.Name = "PromesasDePagoToolStripMenuItem"
        Me.PromesasDePagoToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.PromesasDePagoToolStripMenuItem.Text = "Promesas de Pago"
        '
        'PruebasDeInternetToolStripMenuItem
        '
        Me.PruebasDeInternetToolStripMenuItem.Name = "PruebasDeInternetToolStripMenuItem"
        Me.PruebasDeInternetToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.PruebasDeInternetToolStripMenuItem.Text = "Pruebas de Internet"
        '
        'RecordatoriosToolStripMenuItem
        '
        Me.RecordatoriosToolStripMenuItem.Name = "RecordatoriosToolStripMenuItem"
        Me.RecordatoriosToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.RecordatoriosToolStripMenuItem.Text = "Recordatorios"
        '
        'MensualidadesTotalesToolStripMenuItem
        '
        Me.MensualidadesTotalesToolStripMenuItem.Name = "MensualidadesTotalesToolStripMenuItem"
        Me.MensualidadesTotalesToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.MensualidadesTotalesToolStripMenuItem.Text = "Mensualidades Totales "
        '
        'RoboDeSeñalToolStripMenuItem
        '
        Me.RoboDeSeñalToolStripMenuItem.Name = "RoboDeSeñalToolStripMenuItem"
        Me.RoboDeSeñalToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.RoboDeSeñalToolStripMenuItem.Text = "Robo de Señal"
        '
        'ReporteSTBToolStripMenuItem
        '
        Me.ReporteSTBToolStripMenuItem.Name = "ReporteSTBToolStripMenuItem"
        Me.ReporteSTBToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.ReporteSTBToolStripMenuItem.Text = "Ubicación de Aparatos"
        '
        'ResumenEjecutivoToolStripMenuItem
        '
        Me.ResumenEjecutivoToolStripMenuItem.Name = "ResumenEjecutivoToolStripMenuItem"
        Me.ResumenEjecutivoToolStripMenuItem.Size = New System.Drawing.Size(377, 22)
        Me.ResumenEjecutivoToolStripMenuItem.Text = "Resumen Ejecutivo"
        '
        'GeneralesToolStripMenuItem
        '
        Me.GeneralesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GeneralesDelSistemaToolStripMenuItem, Me.InterfazCablemodemsToolStripMenuItem1, Me.InterfasDecodificadoresToolStripMenuItem, Me.GeneralesDeBancosToolStripMenuItem, Me.GeneralesDeInterfacesDigitalesToolStripMenuItem, Me.GeneralesDeOXXOToolStripMenuItem, Me.ConfiguracionDelSistemaToolStripMenuItem, Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem, Me.BitacoraDelSistemaToolStripMenuItem, Me.ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem, Me.PerfilesSISTEToolStripMenuItem, Me.PerfilesToolStripMenuItem, Me.InterfazDigitalesXcriptToolStripMenuItem, Me.InterfazFtthToolStripMenuItem})
        Me.GeneralesToolStripMenuItem.Name = "GeneralesToolStripMenuItem"
        Me.GeneralesToolStripMenuItem.Size = New System.Drawing.Size(96, 22)
        Me.GeneralesToolStripMenuItem.Text = "&Generales"
        '
        'GeneralesDelSistemaToolStripMenuItem
        '
        Me.GeneralesDelSistemaToolStripMenuItem.Name = "GeneralesDelSistemaToolStripMenuItem"
        Me.GeneralesDelSistemaToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.GeneralesDelSistemaToolStripMenuItem.Text = "Sistema"
        '
        'InterfazCablemodemsToolStripMenuItem1
        '
        Me.InterfazCablemodemsToolStripMenuItem1.Name = "InterfazCablemodemsToolStripMenuItem1"
        Me.InterfazCablemodemsToolStripMenuItem1.Size = New System.Drawing.Size(415, 22)
        Me.InterfazCablemodemsToolStripMenuItem1.Text = "Interfaz Cablemodems"
        '
        'InterfasDecodificadoresToolStripMenuItem
        '
        Me.InterfasDecodificadoresToolStripMenuItem.Name = "InterfasDecodificadoresToolStripMenuItem"
        Me.InterfasDecodificadoresToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.InterfasDecodificadoresToolStripMenuItem.Text = "Interfaz Digitales"
        '
        'GeneralesDeBancosToolStripMenuItem
        '
        Me.GeneralesDeBancosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BancosToolStripMenuItem1, Me.GeneralesProsaBancomerToolStripMenuItem})
        Me.GeneralesDeBancosToolStripMenuItem.Name = "GeneralesDeBancosToolStripMenuItem"
        Me.GeneralesDeBancosToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.GeneralesDeBancosToolStripMenuItem.Text = "Bancos"
        '
        'BancosToolStripMenuItem1
        '
        Me.BancosToolStripMenuItem1.Name = "BancosToolStripMenuItem1"
        Me.BancosToolStripMenuItem1.Size = New System.Drawing.Size(277, 22)
        Me.BancosToolStripMenuItem1.Text = "Bancos"
        '
        'GeneralesProsaBancomerToolStripMenuItem
        '
        Me.GeneralesProsaBancomerToolStripMenuItem.Name = "GeneralesProsaBancomerToolStripMenuItem"
        Me.GeneralesProsaBancomerToolStripMenuItem.Size = New System.Drawing.Size(277, 22)
        Me.GeneralesProsaBancomerToolStripMenuItem.Text = "Generales Prosa Bancomer"
        '
        'GeneralesDeInterfacesDigitalesToolStripMenuItem
        '
        Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Name = "GeneralesDeInterfacesDigitalesToolStripMenuItem"
        Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Text = "Interfaces Digitales"
        '
        'GeneralesDeOXXOToolStripMenuItem
        '
        Me.GeneralesDeOXXOToolStripMenuItem.Name = "GeneralesDeOXXOToolStripMenuItem"
        Me.GeneralesDeOXXOToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.GeneralesDeOXXOToolStripMenuItem.Text = "Cobros en el OXXO"
        '
        'ConfiguracionDelSistemaToolStripMenuItem
        '
        Me.ConfiguracionDelSistemaToolStripMenuItem.Name = "ConfiguracionDelSistemaToolStripMenuItem"
        Me.ConfiguracionDelSistemaToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.ConfiguracionDelSistemaToolStripMenuItem.Text = "Configuracion del Sistema"
        Me.ConfiguracionDelSistemaToolStripMenuItem.Visible = False
        '
        'PreciosDeArticulosDeInstalaciónToolStripMenuItem
        '
        Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Name = "PreciosDeArticulosDeInstalaciónToolStripMenuItem"
        Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Text = "Precios de Articulos de Instalación"
        '
        'BitacoraDelSistemaToolStripMenuItem
        '
        Me.BitacoraDelSistemaToolStripMenuItem.Name = "BitacoraDelSistemaToolStripMenuItem"
        Me.BitacoraDelSistemaToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.BitacoraDelSistemaToolStripMenuItem.Text = "Bitacora del Sistema"
        '
        'ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem
        '
        Me.ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem.Name = "ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem"
        Me.ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem.Text = "Configuración de Cambios de Tipo de Servicio"
        '
        'PerfilesSISTEToolStripMenuItem
        '
        Me.PerfilesSISTEToolStripMenuItem.Name = "PerfilesSISTEToolStripMenuItem"
        Me.PerfilesSISTEToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.PerfilesSISTEToolStripMenuItem.Text = "Perfiles (SISTE)"
        '
        'PerfilesToolStripMenuItem
        '
        Me.PerfilesToolStripMenuItem.Name = "PerfilesToolStripMenuItem"
        Me.PerfilesToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.PerfilesToolStripMenuItem.Text = "Perfiles"
        '
        'InterfazDigitalesXcriptToolStripMenuItem
        '
        Me.InterfazDigitalesXcriptToolStripMenuItem.Name = "InterfazDigitalesXcriptToolStripMenuItem"
        Me.InterfazDigitalesXcriptToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.InterfazDigitalesXcriptToolStripMenuItem.Text = "Interfaz Digitales Xcript"
        '
        'InterfazFtthToolStripMenuItem
        '
        Me.InterfazFtthToolStripMenuItem.Name = "InterfazFtthToolStripMenuItem"
        Me.InterfazFtthToolStripMenuItem.Size = New System.Drawing.Size(415, 22)
        Me.InterfazFtthToolStripMenuItem.Text = "Interfaz Ftth"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(56, 22)
        Me.SalirToolStripMenuItem.Text = "&Salir"
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(48, 137)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(881, 466)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(924, 151)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 53)
        Me.Button1.TabIndex = 4
        Me.Button1.TabStop = False
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(420, 651)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabel1.Location = New System.Drawing.Point(48, 94)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(881, 37)
        Me.CMBLabel1.TabIndex = 6
        Me.CMBLabel1.Text = "Label1"
        Me.CMBLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(160, 163)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(600, 122)
        Me.Panel1.TabIndex = 8
        Me.Panel1.Visible = False
        '
        'BackgroundWorker2
        '
        Me.BackgroundWorker2.WorkerReportsProgress = True
        '
        'CMBLabelSistema
        '
        Me.CMBLabelSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelSistema.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabelSistema.Location = New System.Drawing.Point(46, 60)
        Me.CMBLabelSistema.Name = "CMBLabelSistema"
        Me.CMBLabelSistema.Size = New System.Drawing.Size(881, 37)
        Me.CMBLabelSistema.TabIndex = 8
        Me.CMBLabelSistema.Text = "Softv"
        Me.CMBLabelSistema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameClv_Session_ServiciosBindingSource
        '
        Me.DameClv_Session_ServiciosBindingSource.DataMember = "DameClv_Session_Servicios"
        Me.DameClv_Session_ServiciosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DameClv_Session_ServiciosTableAdapter
        '
        Me.DameClv_Session_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Valida_periodo_reportesBindingSource
        '
        Me.Valida_periodo_reportesBindingSource.DataMember = "Valida_periodo_reportes"
        Me.Valida_periodo_reportesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Valida_periodo_reportesTableAdapter
        '
        Me.Valida_periodo_reportesTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Borrar_Tablas_Reporte_nuevoBindingSource
        '
        Me.Borrar_Tablas_Reporte_nuevoBindingSource.DataMember = "Borrar_Tablas_Reporte_nuevo"
        Me.Borrar_Tablas_Reporte_nuevoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borrar_Tablas_Reporte_nuevoTableAdapter
        '
        Me.Borrar_Tablas_Reporte_nuevoTableAdapter.ClearBeforeFill = True
        '
        'Borra_Separacion_ClientesBindingSource
        '
        Me.Borra_Separacion_ClientesBindingSource.DataMember = "Borra_Separacion_Clientes"
        Me.Borra_Separacion_ClientesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borra_Separacion_ClientesTableAdapter
        '
        Me.Borra_Separacion_ClientesTableAdapter.ClearBeforeFill = True
        '
        'Borra_temporales_trabajosBindingSource
        '
        Me.Borra_temporales_trabajosBindingSource.DataMember = "Borra_temporales_trabajos"
        Me.Borra_temporales_trabajosBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borra_temporales_trabajosTableAdapter
        '
        Me.Borra_temporales_trabajosTableAdapter.ClearBeforeFill = True
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'DataSetarnoldo1
        '
        Me.DataSetarnoldo1.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Selecciona_Impresora_SucursalBindingSource
        '
        Me.Selecciona_Impresora_SucursalBindingSource.DataMember = "Selecciona_Impresora_Sucursal"
        Me.Selecciona_Impresora_SucursalBindingSource.DataSource = Me.DataSetarnoldo1
        '
        'Selecciona_Impresora_SucursalTableAdapter
        '
        Me.Selecciona_Impresora_SucursalTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter9
        '
        Me.Muestra_ServiciosDigitalesTableAdapter9.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter10
        '
        Me.Muestra_ServiciosDigitalesTableAdapter10.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter11
        '
        Me.Muestra_ServiciosDigitalesTableAdapter11.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter12
        '
        Me.Muestra_ServiciosDigitalesTableAdapter12.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter13
        '
        Me.Muestra_ServiciosDigitalesTableAdapter13.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter14
        '
        Me.Muestra_ServiciosDigitalesTableAdapter14.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter15
        '
        Me.Muestra_ServiciosDigitalesTableAdapter15.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter16
        '
        Me.Muestra_ServiciosDigitalesTableAdapter16.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter17
        '
        Me.Muestra_ServiciosDigitalesTableAdapter17.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter18
        '
        Me.Muestra_ServiciosDigitalesTableAdapter18.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter19
        '
        Me.Muestra_ServiciosDigitalesTableAdapter19.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter20
        '
        Me.Muestra_ServiciosDigitalesTableAdapter20.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter21
        '
        Me.Muestra_ServiciosDigitalesTableAdapter21.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter22
        '
        Me.Muestra_ServiciosDigitalesTableAdapter22.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter23
        '
        Me.Muestra_ServiciosDigitalesTableAdapter23.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter24
        '
        Me.Muestra_ServiciosDigitalesTableAdapter24.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(45, 651)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(250, 16)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Versión: 1.0.0.80 Fecha: 11/08/2020"
        '
        'Muestra_ServiciosDigitalesTableAdapter25
        '
        Me.Muestra_ServiciosDigitalesTableAdapter25.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter26
        '
        Me.Muestra_ServiciosDigitalesTableAdapter26.ClearBeforeFill = True
        '
        'CMBLabelSistema2
        '
        Me.CMBLabelSistema2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelSistema2.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabelSistema2.Location = New System.Drawing.Point(48, 29)
        Me.CMBLabelSistema2.Name = "CMBLabelSistema2"
        Me.CMBLabelSistema2.Size = New System.Drawing.Size(881, 37)
        Me.CMBLabelSistema2.TabIndex = 16
        Me.CMBLabelSistema2.Text = "Softv"
        Me.CMBLabelSistema2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Muestra_ServiciosDigitalesTableAdapter27
        '
        Me.Muestra_ServiciosDigitalesTableAdapter27.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter28
        '
        Me.Muestra_ServiciosDigitalesTableAdapter28.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter29
        '
        Me.Muestra_ServiciosDigitalesTableAdapter29.ClearBeforeFill = True
        '
        'LabelNombreUsuario
        '
        Me.LabelNombreUsuario.AutoSize = True
        Me.LabelNombreUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelNombreUsuario.ForeColor = System.Drawing.Color.Gray
        Me.LabelNombreUsuario.Location = New System.Drawing.Point(131, 618)
        Me.LabelNombreUsuario.Name = "LabelNombreUsuario"
        Me.LabelNombreUsuario.Size = New System.Drawing.Size(177, 20)
        Me.LabelNombreUsuario.TabIndex = 22
        Me.LabelNombreUsuario.Text = "LabelNombreUsuario"
        '
        'Muestra_ServiciosDigitalesTableAdapter30
        '
        Me.Muestra_ServiciosDigitalesTableAdapter30.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter31
        '
        Me.Muestra_ServiciosDigitalesTableAdapter31.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter32
        '
        Me.Muestra_ServiciosDigitalesTableAdapter32.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter33
        '
        Me.Muestra_ServiciosDigitalesTableAdapter33.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter34
        '
        Me.Muestra_ServiciosDigitalesTableAdapter34.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter35
        '
        Me.Muestra_ServiciosDigitalesTableAdapter35.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter36
        '
        Me.Muestra_ServiciosDigitalesTableAdapter36.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter37
        '
        Me.Muestra_ServiciosDigitalesTableAdapter37.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter38
        '
        Me.Muestra_ServiciosDigitalesTableAdapter38.ClearBeforeFill = True
        '
        'DameTipoUsusarioBindingSource
        '
        Me.DameTipoUsusarioBindingSource.DataMember = "DameTipoUsusario"
        '
        'DameEspecifBindingSource
        '
        Me.DameEspecifBindingSource.DataMember = "DameEspecif"
        '
        'DameTipoUsusarioBindingSource1
        '
        Me.DameTipoUsusarioBindingSource1.DataMember = "DameTipoUsusario"
        '
        'DamePermisosBindingSource
        '
        Me.DamePermisosBindingSource.DataMember = "DamePermisos"
        '
        'ALTASMENUSBindingSource
        '
        Me.ALTASMENUSBindingSource.DataMember = "ALTASMENUS"
        '
        'ALTASformsBindingSource
        '
        Me.ALTASformsBindingSource.DataMember = "ALTASforms"
        '
        'FrmMiMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1036, 741)
        Me.Controls.Add(Me.LabelNombreUsuario)
        Me.Controls.Add(LabelUsuario)
        Me.Controls.Add(Me.CMBLabelSistema2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBLabelSistema)
        Me.Controls.Add(CMBNombreLabel)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMiMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Softv Menú Principal"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_periodo_reportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borrar_Tablas_Reporte_nuevoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Separacion_ClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_temporales_trabajosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameTipoUsusarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameEspecifBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameTipoUsusarioBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamePermisosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALTASMENUSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALTASformsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents CatálogosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatálogosDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatalogoÁreaTécnicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ÁreasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClasificaciónTécnicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosAlClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TecnicosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatálogoDeGeneralesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDeServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CableModemsYAparatosDigitalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColoniasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDeColoniasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CallesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CiudadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SucursalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BancosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosDeCancelaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatálogoDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PromotoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SeriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesosDeServicioPremiumToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenesDeServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuejasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuejasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgendaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AtenciónTelefónicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadosDeCarteraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesVariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AreaTécnicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnálisisDePenetraciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesDelSistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfasDecodificadoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesDeBancosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents BancosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenesDeServicioToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuejasToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SectoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LlamadasTelefónicasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesDeInterfacesDigitalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GeneralesDeOXXOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguracionDelSistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosDeCancelaciónFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosDeReImpresiónFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RangosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrecioDeComisionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalcularComisionesPorVendedorToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrimerPeriodoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SegundoPeriodoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActivaciónPaqueteDePruebaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PreciosDeArticulosDeInstalaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesConAdeudoDeMaterialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BitácoraDePruebasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepuraciónDeÓrdenesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambioDeServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesVariosMezcladosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents DameClv_Session_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_ServiciosTableAdapter As sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Borrar_Tablas_Reporte_nuevoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borrar_Tablas_Reporte_nuevoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borrar_Tablas_Reporte_nuevoTableAdapter
    Friend WithEvents Valida_periodo_reportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_periodo_reportesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Valida_periodo_reportesTableAdapter
    Friend WithEvents Borra_Separacion_ClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Separacion_ClientesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Separacion_ClientesTableAdapter
    Friend WithEvents Borra_temporales_trabajosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_temporales_trabajosTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borra_temporales_trabajosTableAdapter
    Friend WithEvents ListadoDeActividadesDelTécnicoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MesajesInstantaneosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CorreoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResetearAparatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenVendedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GráficasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesVariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PruebaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambioDeClienteASoloInternetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambioDeClienteAClienteNormalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents AgendaDeActividadesDelTécnicoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PolizaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProgramacionesDeMesnajesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloceDeMensualidadesAdelantadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratoForzosoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MedidoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CarteraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MedidoresToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GrupoDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BitacoraDelSistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgendaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IndividualesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescuentosComboToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CMBLabelSistema As System.Windows.Forms.Label
    Friend WithEvents ImporteDeMensualidadesAdelantadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents EstadoDeCuentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteClientesConComboToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Private WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents CarteraEjecutivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BitácoraDeCorreosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesconexionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServicioBasicoYCanalesPremiumToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InternetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfazCablemodemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfazDigitalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CortesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesProsaBancomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HotelesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DameTipoUsusarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameEspecifBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameTipoUsusarioBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DamePermisosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ALTASMENUSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ALTASformsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ReporteDePermanenciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReseteoMasivoDeAparatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlazoForzosoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MensajesPrefijosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BajaDeServiciosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatalogoDeMensajesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoDeMensajesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrefijosDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MsjsPersonalizadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevaProgramaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CtgMsjPerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecontrataciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecontratacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DecodificadoresToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RentaACajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents DataSetarnoldo1 As sofTV.DataSetarnoldo
    Friend WithEvents Selecciona_Impresora_SucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_Impresora_SucursalTableAdapter As sofTV.DataSetarnoldoTableAdapters.Selecciona_Impresora_SucursalTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter9 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ListaDeCumpleañosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter10 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter11 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter12 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ÓrdenesClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter13 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter14 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents PostesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter15 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ClasificaciónProblemasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegresaAparatosAlAlmacenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrórrogasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PromesasDePagoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstablecerComisionesCobroADomicilioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobrosADomicilioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PruebaDeInternetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PruebasDeInternetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter16 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter17 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents InterfazCablemodemsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClaveTécnicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DevoluciónDeAparatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DevoluciónDeAparatosAlAlmacénToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter18 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents GenerarÓrdenesDeDesconexiónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter19 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter20 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter21 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter22 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter23 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter24 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents PerfilesSISTEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerfilesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter25 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter26 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents RecordatoriosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoDeGastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MensualidadesTotalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CostoPorAparatoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CMBLabelSistema2 As System.Windows.Forms.Label
    Friend WithEvents TipoUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter27 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter28 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents TvsAdicionalesPorCiudadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IndicadoresPagosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter29 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents LabelNombreUsuario As System.Windows.Forms.Label
    Friend WithEvents RoboDeSeñalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteSTBToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambioDePeriodoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistorialDeCambiosDePeriodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DdToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfazDigitalesXcriptToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenEjecutivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesoMasivoCambioDeAparatoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter30 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter31 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter32 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter33 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter34 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter35 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter36 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter37 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter38 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents HUBToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OLTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NAPToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VelocidadesDeInternetFTTHToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfazFtthToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenOrdenesQuejasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesosComcastToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DireccionesIPToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    'Friend WithEvents MensajesInstantáneosPersonalizadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    'Friend WithEvents NuevoMensajeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    'Friend WithEvents ProgramacionDeMensajesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    'Friend WithEvents CuestionarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    'Friend WithEvents CatálogoTelefoníaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    ' Friend WithEvents CatalogoPaisesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
