﻿Public Class FrmBuscarOnu
    Public Consecutivo As Integer = 0
    Public Encontrada As Integer
    Dim cont As Integer = 0
    Dim contBtnSalir As Integer = 0

    Private Sub FrmBuscarOnu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Timer1.Start()
        Timer2.Start()

        BuscarOnu()
    End Sub

    Private Sub BuscarOnu()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@consecutivo", SqlDbType.BigInt, Consecutivo)
        BaseII.CreateMyParameter("@Encontrada", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("BuscarOnu")

        Encontrada = CInt(BaseII.dicoPar("@Encontrada").ToString)
        If Encontrada > 0 Then
            Timer1.Stop()
            Timer2.Stop()
            ProgressBar1.Value = 100
            If Encontrada = 1 Then
                MsgBox("Aparato encontrado")
            Else
                MsgBox("Aparato no encontrado")
            End If
            Me.Close()
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        cont = 0
        BuscarOnu()
        If btnSalir.Visible = False Then
            contBtnSalir = contBtnSalir + 10
            If contBtnSalir >= 60 Then
                btnSalir.Visible = True
            End If
        End If

    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        cont = cont + 10
        ProgressBar1.Value = cont
    End Sub
End Class