﻿Public Class FrmRelClientesExtensiones

    Dim Instalado As Boolean = False

    Private Sub CONRelClientesExtensiones(ByVal Contrato As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        dTable = BaseII.ConsultaDT("CONRelClientesExtensiones")
        nudExt.Value = 0
        If dTable.Rows.Count = 0 Then Exit Sub
        nudExt.Value = dTable.Rows(0)(0).ToString
    End Sub

    Private Sub NUERelClientesExtensiones(ByVal Contrato As Integer, ByVal Extensiones As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Extensiones", SqlDbType.Int, Extensiones)
        BaseII.Inserta("NUERelClientesExtensiones")
    End Sub

    Private Sub BORRelClientesExtensiones(ByVal Contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.Inserta("BORRelClientesExtensiones")
    End Sub

    Private Sub FrmRelClientesExtensiones_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        FrmClientes.nudExtensioesCableadas.Text = nudExt.Value
    End Sub

    Private Sub FrmRelClientesExtensiones_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If GloEsHotelR = True Then
            nudExt.Maximum = 99999
        ElseIf GloEsHotelR = False Then
            nudExt.Maximum = 3
        End If

        CONRelClientesExtensiones(Contrato)
        ValidaSiEstaInstalado(Contrato)
        If Instalado = True Then
            tsbGuardar.Enabled = False
            tsbEliminar.Enabled = False
            nudExt.Enabled = False
        Else
            tsbGuardar.Enabled = True
            tsbEliminar.Enabled = True
            nudExt.Enabled = True
        End If
        
    End Sub

    Private Sub tsbGuardar_Click(sender As System.Object, e As System.EventArgs) Handles tsbGuardar.Click
        ValidaCuantasTvsTiene(GloContrato)

        If GloEsHotelR = False Then
            ValidaCuantasTvsTiene(GloContrato)
        ElseIf GloEsHotelR = True Then
            TvsMsj = ""
        End If

        If Len(TvsMsj) = 0 Then
            NUERelClientesExtensiones(Contrato, nudExt.Value)
            MessageBox.Show("Se guardó con éxito.")
            Me.Close()
        Else
            MsgBox(TvsMsj, MsgBoxStyle.Exclamation)
        End If
        

    End Sub

    Private Sub ValidaCuantasTvsTiene(ByVal Contrato)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@TvsNewAna", SqlDbType.Int, nudExt.Value)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("ValidaCuantasTvsTiene")
        TvsMsj = ""
        TvsMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub

    Private Sub tsbEliminar_Click(sender As System.Object, e As System.EventArgs) Handles tsbEliminar.Click
        BORRelClientesExtensiones(Contrato)
        MessageBox.Show("Se eliminó con éxito.")
        CONRelClientesExtensiones(Contrato)
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
    Private Sub ValidaSiEstaInstalado(ByVal Contrato)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Instalado", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("ValidaSiEstaInstalado")
        Instalado = BaseII.dicoPar("@Instalado").ToString
    End Sub
End Class