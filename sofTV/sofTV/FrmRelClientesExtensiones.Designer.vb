﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelClientesExtensiones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRelClientesExtensiones))
        Me.bnRelClientesExtensiones = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.CMBNumExt = New System.Windows.Forms.Label()
        Me.nudExt = New System.Windows.Forms.NumericUpDown()
        Me.bnSalir = New System.Windows.Forms.Button()
        CType(Me.bnRelClientesExtensiones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnRelClientesExtensiones.SuspendLayout()
        CType(Me.nudExt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnRelClientesExtensiones
        '
        Me.bnRelClientesExtensiones.AddNewItem = Nothing
        Me.bnRelClientesExtensiones.CountItem = Nothing
        Me.bnRelClientesExtensiones.DeleteItem = Me.tsbEliminar
        Me.bnRelClientesExtensiones.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnRelClientesExtensiones.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminar, Me.tsbGuardar})
        Me.bnRelClientesExtensiones.Location = New System.Drawing.Point(0, 0)
        Me.bnRelClientesExtensiones.MoveFirstItem = Nothing
        Me.bnRelClientesExtensiones.MoveLastItem = Nothing
        Me.bnRelClientesExtensiones.MoveNextItem = Nothing
        Me.bnRelClientesExtensiones.MovePreviousItem = Nothing
        Me.bnRelClientesExtensiones.Name = "bnRelClientesExtensiones"
        Me.bnRelClientesExtensiones.PositionItem = Nothing
        Me.bnRelClientesExtensiones.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnRelClientesExtensiones.Size = New System.Drawing.Size(396, 25)
        Me.bnRelClientesExtensiones.TabIndex = 5
        Me.bnRelClientesExtensiones.Text = "BindingNavigator1"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.Image = CType(resources.GetObject("tsbEliminar.Image"), System.Drawing.Image)
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbEliminar.Size = New System.Drawing.Size(90, 22)
        Me.tsbEliminar.Text = "&ELIMINAR"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(91, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'CMBNumExt
        '
        Me.CMBNumExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CMBNumExt.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBNumExt.Location = New System.Drawing.Point(64, 53)
        Me.CMBNumExt.Name = "CMBNumExt"
        Me.CMBNumExt.Size = New System.Drawing.Size(144, 30)
        Me.CMBNumExt.TabIndex = 82
        Me.CMBNumExt.Text = "Número de Extensiones Analógicas:"
        Me.CMBNumExt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'nudExt
        '
        Me.nudExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.nudExt.Location = New System.Drawing.Point(214, 61)
        Me.nudExt.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudExt.Name = "nudExt"
        Me.nudExt.Size = New System.Drawing.Size(49, 22)
        Me.nudExt.TabIndex = 83
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(248, 122)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 84
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'FrmRelClientesExtensiones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(396, 170)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.CMBNumExt)
        Me.Controls.Add(Me.nudExt)
        Me.Controls.Add(Me.bnRelClientesExtensiones)
        Me.Name = "FrmRelClientesExtensiones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Extensiones Analógicas"
        CType(Me.bnRelClientesExtensiones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnRelClientesExtensiones.ResumeLayout(False)
        Me.bnRelClientesExtensiones.PerformLayout()
        CType(Me.nudExt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnRelClientesExtensiones As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents CMBNumExt As System.Windows.Forms.Label
    Friend WithEvents nudExt As System.Windows.Forms.NumericUpDown
    Friend WithEvents bnSalir As System.Windows.Forms.Button
End Class
