<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Acceso_TipoServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Acceso_TipoServicios))
        Me.VerAcceso_ChecaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2
        Me.VerAcceso_ChecaTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.VerAcceso_ChecaTableAdapter
        Me.CMBClv_UsuarioLabel = New System.Windows.Forms.Label
        Me.CMBPasswordLabel = New System.Windows.Forms.Label
        Me.Cancel = New System.Windows.Forms.Button
        Me.OK = New System.Windows.Forms.Button
        Me.PasaporteTextBox = New System.Windows.Forms.TextBox
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox
        Me.redLabel1 = New System.Windows.Forms.Label
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2
        Me.Dame_clvtipusuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clvtipusuarioTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Dame_clvtipusuarioTableAdapter
        CType(Me.VerAcceso_ChecaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clvtipusuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'VerAcceso_ChecaBindingSource
        '
        Me.VerAcceso_ChecaBindingSource.DataMember = "VerAcceso_Checa"
        Me.VerAcceso_ChecaBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VerAcceso_ChecaTableAdapter
        '
        Me.VerAcceso_ChecaTableAdapter.ClearBeforeFill = True
        '
        'CMBClv_UsuarioLabel
        '
        Me.CMBClv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBClv_UsuarioLabel.Location = New System.Drawing.Point(295, 42)
        Me.CMBClv_UsuarioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBClv_UsuarioLabel.Name = "CMBClv_UsuarioLabel"
        Me.CMBClv_UsuarioLabel.Size = New System.Drawing.Size(149, 38)
        Me.CMBClv_UsuarioLabel.TabIndex = 27
        Me.CMBClv_UsuarioLabel.Text = "Login del Administrador :"
        Me.CMBClv_UsuarioLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CMBPasswordLabel
        '
        Me.CMBPasswordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBPasswordLabel.Location = New System.Drawing.Point(295, 110)
        Me.CMBPasswordLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBPasswordLabel.Name = "CMBPasswordLabel"
        Me.CMBPasswordLabel.Size = New System.Drawing.Size(149, 38)
        Me.CMBPasswordLabel.TabIndex = 26
        Me.CMBPasswordLabel.Text = "Contraseña del Administrador :"
        Me.CMBPasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.Orange
        Me.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel.Location = New System.Drawing.Point(421, 209)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(152, 35)
        Me.Cancel.TabIndex = 3
        Me.Cancel.Text = "&CANCELAR"
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.Orange
        Me.OK.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.Location = New System.Drawing.Point(258, 209)
        Me.OK.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(155, 35)
        Me.OK.TabIndex = 2
        Me.OK.Text = "&ACEPTAR"
        Me.OK.UseVisualStyleBackColor = False
        '
        'PasaporteTextBox
        '
        Me.PasaporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PasaporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasaporteTextBox.Location = New System.Drawing.Point(298, 151)
        Me.PasaporteTextBox.MaxLength = 10
        Me.PasaporteTextBox.Name = "PasaporteTextBox"
        Me.PasaporteTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasaporteTextBox.Size = New System.Drawing.Size(97, 24)
        Me.PasaporteTextBox.TabIndex = 1
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_UsuarioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(298, 83)
        Me.Clv_UsuarioTextBox.MaxLength = 15
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(116, 24)
        Me.Clv_UsuarioTextBox.TabIndex = 0
        '
        'redLabel1
        '
        Me.redLabel1.BackColor = System.Drawing.Color.Transparent
        Me.redLabel1.CausesValidation = False
        Me.redLabel1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.redLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.redLabel1.ForeColor = System.Drawing.Color.Red
        Me.redLabel1.Location = New System.Drawing.Point(22, 11)
        Me.redLabel1.Name = "redLabel1"
        Me.redLabel1.Size = New System.Drawing.Size(220, 35)
        Me.redLabel1.TabIndex = 25
        Me.redLabel1.Text = "Acceso Restringido"
        Me.redLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(22, 49)
        Me.LogoPictureBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(220, 195)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LogoPictureBox.TabIndex = 24
        Me.LogoPictureBox.TabStop = False
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_clvtipusuarioBindingSource
        '
        Me.Dame_clvtipusuarioBindingSource.DataMember = "Dame_clvtipusuario"
        Me.Dame_clvtipusuarioBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Dame_clvtipusuarioTableAdapter
        '
        Me.Dame_clvtipusuarioTableAdapter.ClearBeforeFill = True
        '
        'Acceso_TipoServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(601, 272)
        Me.Controls.Add(Me.CMBClv_UsuarioLabel)
        Me.Controls.Add(Me.CMBPasswordLabel)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.PasaporteTextBox)
        Me.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Controls.Add(Me.redLabel1)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Acceso_TipoServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso Tipo de Servicios"
        CType(Me.VerAcceso_ChecaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clvtipusuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents VerAcceso_ChecaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents VerAcceso_ChecaTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.VerAcceso_ChecaTableAdapter
    Friend WithEvents CMBClv_UsuarioLabel As System.Windows.Forms.Label
    Friend WithEvents CMBPasswordLabel As System.Windows.Forms.Label
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents PasaporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents redLabel1 As System.Windows.Forms.Label
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Dame_clvtipusuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clvtipusuarioTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Dame_clvtipusuarioTableAdapter
End Class
