<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BitacoraDelSistema
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Consulta_dinamica_movsistDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Consulta_dinamica_movsistBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2
        Me.MuestracontrolbitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraPantallabitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraHorafinbitBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraHorainibitBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestrausuariosbitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSistemabitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button
        Me.Muestra_Sistema_bitacoraTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Sistema_bitacoraTableAdapter
        Me.Muestra_usuarios_bitacoraTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_usuarios_bitacoraTableAdapter
        Me.MuestraHoraini_bitTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraHoraini_bitTableAdapter
        Me.MuestraHorafin_bitTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraHorafin_bitTableAdapter
        Me.Muestra_Pantalla_bitacoraTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Pantalla_bitacoraTableAdapter
        Me.Muestra_control_bitacoraTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_control_bitacoraTableAdapter
        Me.Button2 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.CheckBox6 = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.CheckBox5 = New System.Windows.Forms.CheckBox
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.CheckBox4 = New System.Windows.Forms.CheckBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label202 = New System.Windows.Forms.Label
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.ComboBox5 = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        Me.ComboBox6 = New System.Windows.Forms.ComboBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Consulta_dinamica_movsistTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.consulta_dinamica_movsistTableAdapter
        Me.Panel2.SuspendLayout()
        CType(Me.Consulta_dinamica_movsistDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_dinamica_movsistBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestracontrolbitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraPantallabitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraHorafinbitBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraHorainibitBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestrausuariosbitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSistemabitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Consulta_dinamica_movsistDataGridView)
        Me.Panel2.Location = New System.Drawing.Point(348, 12)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(656, 617)
        Me.Panel2.TabIndex = 1
        '
        'Consulta_dinamica_movsistDataGridView
        '
        Me.Consulta_dinamica_movsistDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkGoldenrod
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consulta_dinamica_movsistDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.Consulta_dinamica_movsistDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10})
        Me.Consulta_dinamica_movsistDataGridView.DataSource = Me.Consulta_dinamica_movsistBindingSource
        Me.Consulta_dinamica_movsistDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_dinamica_movsistDataGridView.Name = "Consulta_dinamica_movsistDataGridView"
        Me.Consulta_dinamica_movsistDataGridView.ReadOnly = True
        Me.Consulta_dinamica_movsistDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Consulta_dinamica_movsistDataGridView.Size = New System.Drawing.Size(653, 617)
        Me.Consulta_dinamica_movsistDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "control"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Control"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "consecutivo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "consecutivo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "contrato"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Contrato"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Fecha"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "usuario"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Usuario"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Sistema"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Sistema"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Pantalla"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Pantalla"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "valorant"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Valor Anterior"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "valornuevo"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Valor Nuevo"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "clv_ciudad"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Ciudad"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'Consulta_dinamica_movsistBindingSource
        '
        Me.Consulta_dinamica_movsistBindingSource.DataMember = "consulta_dinamica_movsist"
        Me.Consulta_dinamica_movsistBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestracontrolbitacoraBindingSource
        '
        Me.MuestracontrolbitacoraBindingSource.DataMember = "Muestra_control_bitacora"
        Me.MuestracontrolbitacoraBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraPantallabitacoraBindingSource
        '
        Me.MuestraPantallabitacoraBindingSource.DataMember = "Muestra_Pantalla_bitacora"
        Me.MuestraPantallabitacoraBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraHorafinbitBindingSource
        '
        Me.MuestraHorafinbitBindingSource.DataMember = "MuestraHorafin_bit"
        Me.MuestraHorafinbitBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraHorainibitBindingSource
        '
        Me.MuestraHorainibitBindingSource.DataMember = "MuestraHoraini_bit"
        Me.MuestraHorainibitBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestrausuariosbitacoraBindingSource
        '
        Me.MuestrausuariosbitacoraBindingSource.DataMember = "Muestra_usuarios_bitacora"
        Me.MuestrausuariosbitacoraBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSistemabitacoraBindingSource
        '
        Me.MuestraSistemabitacoraBindingSource.DataMember = "Muestra_Sistema_bitacora"
        Me.MuestraSistemabitacoraBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Olive
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(839, 662)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(165, 33)
        Me.Button5.TabIndex = 15
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Muestra_Sistema_bitacoraTableAdapter
        '
        Me.Muestra_Sistema_bitacoraTableAdapter.ClearBeforeFill = True
        '
        'Muestra_usuarios_bitacoraTableAdapter
        '
        Me.Muestra_usuarios_bitacoraTableAdapter.ClearBeforeFill = True
        '
        'MuestraHoraini_bitTableAdapter
        '
        Me.MuestraHoraini_bitTableAdapter.ClearBeforeFill = True
        '
        'MuestraHorafin_bitTableAdapter
        '
        Me.MuestraHorafin_bitTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Pantalla_bitacoraTableAdapter
        '
        Me.Muestra_Pantalla_bitacoraTableAdapter.ClearBeforeFill = True
        '
        'Muestra_control_bitacoraTableAdapter
        '
        Me.Muestra_control_bitacoraTableAdapter.ClearBeforeFill = True
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Olive
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(348, 662)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(371, 33)
        Me.Button2.TabIndex = 42
        Me.Button2.Text = "&IMPRIMIR REPORTE"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.CheckBox6)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.CheckBox5)
        Me.Panel1.Controls.Add(Me.ComboBox4)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.CheckBox4)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.ComboBox5)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.CheckBox3)
        Me.Panel1.Controls.Add(Me.ComboBox6)
        Me.Panel1.Controls.Add(Me.CheckBox2)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(330, 617)
        Me.Panel1.TabIndex = 43
        '
        'TextBox1
        '
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(14, 205)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(172, 20)
        Me.TextBox1.TabIndex = 72
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(90, 183)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 71
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(14, 179)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(79, 18)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Contrato:"
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Location = New System.Drawing.Point(90, 529)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox6.TabIndex = 69
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(327, 26)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "Opciones De Busqueda Por :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(92, 462)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox5.TabIndex = 68
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MuestraSistemabitacoraBindingSource
        Me.ComboBox4.DisplayMember = "Sistema"
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.ForeColor = System.Drawing.Color.Black
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(14, 60)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(286, 24)
        Me.ComboBox4.TabIndex = 52
        Me.ComboBox4.ValueMember = "Sistema"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.ComboBox3)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.ComboBox2)
        Me.Panel4.Enabled = False
        Me.Panel4.Location = New System.Drawing.Point(7, 376)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(312, 63)
        Me.Panel4.TabIndex = 67
        '
        'ComboBox3
        '
        Me.ComboBox3.DataSource = Me.MuestraHorainibitBindingSource
        Me.ComboBox3.DisplayMember = "mensaje"
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.ForeColor = System.Drawing.Color.Black
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(12, 36)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(137, 24)
        Me.ComboBox3.TabIndex = 5
        Me.ComboBox3.ValueMember = "clv_hora"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(14, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 18)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Hora Inicial:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(172, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 18)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Hora Final:"
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MuestraHorafinbitBindingSource
        Me.ComboBox2.DisplayMember = "mensaje"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.ForeColor = System.Drawing.Color.Black
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(175, 34)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(134, 24)
        Me.ComboBox2.TabIndex = 6
        Me.ComboBox2.ValueMember = "clv_hora"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(14, 37)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 18)
        Me.Label7.TabIndex = 57
        Me.Label7.Text = "Sistema:"
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Location = New System.Drawing.Point(153, 356)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox4.TabIndex = 66
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestrausuariosbitacoraBindingSource
        Me.ComboBox1.DisplayMember = "Nombre_usuario"
        Me.ComboBox1.Enabled = False
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.ForeColor = System.Drawing.Color.Black
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(14, 132)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(286, 24)
        Me.ComboBox1.TabIndex = 53
        Me.ComboBox1.ValueMember = "clv_usuario"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(16, 352)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(131, 18)
        Me.Label2.TabIndex = 65
        Me.Label2.Text = "Rango de Horas"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(14, 102)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 18)
        Me.Label10.TabIndex = 58
        Me.Label10.Text = "Usuario:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.DateTimePicker1)
        Me.Panel3.Controls.Add(Me.Label33)
        Me.Panel3.Controls.Add(Me.Label202)
        Me.Panel3.Controls.Add(Me.DateTimePicker2)
        Me.Panel3.Enabled = False
        Me.Panel3.Location = New System.Drawing.Point(7, 273)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(312, 67)
        Me.Panel3.TabIndex = 64
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(14, 32)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DateTimePicker1.Size = New System.Drawing.Size(137, 22)
        Me.DateTimePicker1.TabIndex = 3
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.Black
        Me.Label33.Location = New System.Drawing.Point(14, 11)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(107, 18)
        Me.Label33.TabIndex = 30
        Me.Label33.Text = "Fecha Inicial:"
        '
        'Label202
        '
        Me.Label202.AutoSize = True
        Me.Label202.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label202.ForeColor = System.Drawing.Color.Black
        Me.Label202.Location = New System.Drawing.Point(172, 11)
        Me.Label202.Name = "Label202"
        Me.Label202.Size = New System.Drawing.Size(100, 18)
        Me.Label202.TabIndex = 31
        Me.Label202.Text = "Fecha Final:"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(175, 32)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(134, 22)
        Me.DateTimePicker2.TabIndex = 4
        '
        'ComboBox5
        '
        Me.ComboBox5.DataSource = Me.MuestraPantallabitacoraBindingSource
        Me.ComboBox5.DisplayMember = "Pantalla"
        Me.ComboBox5.Enabled = False
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.ForeColor = System.Drawing.Color.Black
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(21, 479)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(286, 24)
        Me.ComboBox5.TabIndex = 55
        Me.ComboBox5.ValueMember = "Pantalla"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(19, 252)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(140, 18)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "Rango de Fechas"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(18, 458)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 18)
        Me.Label6.TabIndex = 59
        Me.Label6.Text = "Pantalla:"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(171, 256)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox3.TabIndex = 62
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'ComboBox6
        '
        Me.ComboBox6.DataSource = Me.MuestracontrolbitacoraBindingSource
        Me.ComboBox6.DisplayMember = "control"
        Me.ComboBox6.Enabled = False
        Me.ComboBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox6.ForeColor = System.Drawing.Color.Black
        Me.ComboBox6.FormattingEnabled = True
        Me.ComboBox6.Location = New System.Drawing.Point(21, 546)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(286, 24)
        Me.ComboBox6.TabIndex = 56
        Me.ComboBox6.ValueMember = "control"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(85, 106)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox2.TabIndex = 61
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(18, 525)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(69, 18)
        Me.Label8.TabIndex = 60
        Me.Label8.Text = "Control:"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(63, 662)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(165, 33)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "&BUSCAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Consulta_dinamica_movsistTableAdapter
        '
        Me.Consulta_dinamica_movsistTableAdapter.ClearBeforeFill = True
        '
        'BitacoraDelSistema
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 710)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "BitacoraDelSistema"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bitacora Del Sistema"
        Me.Panel2.ResumeLayout(False)
        CType(Me.Consulta_dinamica_movsistDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_dinamica_movsistBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestracontrolbitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraPantallabitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraHorafinbitBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraHorainibitBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestrausuariosbitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSistemabitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents MuestracontrolbitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents MuestraPantallabitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraHorafinbitBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraHorainibitBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestrausuariosbitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSistemabitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Sistema_bitacoraTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Sistema_bitacoraTableAdapter
    Friend WithEvents Muestra_usuarios_bitacoraTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_usuarios_bitacoraTableAdapter
    Friend WithEvents MuestraHoraini_bitTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraHoraini_bitTableAdapter
    Friend WithEvents MuestraHorafin_bitTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraHorafin_bitTableAdapter
    Friend WithEvents Muestra_Pantalla_bitacoraTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Pantalla_bitacoraTableAdapter
    Friend WithEvents Muestra_control_bitacoraTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_control_bitacoraTableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Consulta_dinamica_movsistBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_dinamica_movsistTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.consulta_dinamica_movsistTableAdapter
    Friend WithEvents Consulta_dinamica_movsistDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label202 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
End Class
