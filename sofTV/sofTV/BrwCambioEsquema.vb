﻿Imports System.Data
Imports System.Data.SqlClient

Public Class BrwCambioEsquema

    Dim Contrato As Integer = 0

    Private Sub BrwCambioEsquema_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim cnn As New SqlConnection(MiConexion)

        If EsqoPer = "E" Then
            Dim da As New SqlDataAdapter("SELECT id as ID, Contrato, clv_Usuario as Usuario, Fecha, Status FROM Tbl_RegCambioFormaPago", cnn)
            Dim ds As New DataSet

            da.Fill(ds)

            DataGridCambioEsquema.DataSource = ds.Tables(0)
        ElseIf EsqoPer = "P" Then
            Dim da As New SqlDataAdapter("select clv_id as Id, Contrato, Usuario, Fecha from tbl_regCambioPeriodo", cnn)
            Dim ds As New DataSet

            da.Fill(ds)

            DataGridCambioEsquema.DataSource = ds.Tables(0)
        End If

    End Sub
    Private Sub BrwCambioEsquema_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        colorea(Me, Me.Name)

        Dim cnn As New SqlConnection(MiConexion)

        If EsqoPer = "E" Then
            Dim da As New SqlDataAdapter("SELECT id as ID, Contrato, clv_Usuario as Usuario, Fecha, Status FROM Tbl_RegCambioFormaPago", cnn)
            Dim ds As New DataSet

            da.Fill(ds)

            DataGridCambioEsquema.DataSource = ds.Tables(0)
        ElseIf EsqoPer = "P" Then

            Me.Text = "Cambios de Periodo"

            Dim da As New SqlDataAdapter("select clv_id as Id, Contrato, Usuario, Fecha from tbl_regCambioPeriodo", cnn)
            Dim ds As New DataSet

            da.Fill(ds)

            DataGridCambioEsquema.DataSource = ds.Tables(0)
        End If
        
    End Sub

    Private Sub BtnBuscarcontrato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBuscarcontrato.Click

        Contrato = TBoxContrato.Text

        Dim cnn As New SqlConnection(MiConexion)

        If EsqoPer = "E" Then
            Dim da As New SqlDataAdapter("SELECT id as ID, Contrato, clv_Usuario as Usuario, Fecha, Status FROM Tbl_RegCambioFormaPago where contrato =" & Contrato, cnn)
            Dim ds As New DataSet

            da.Fill(ds)

            DataGridCambioEsquema.DataSource = ds.Tables(0)
        ElseIf EsqoPer = "P" Then
            Dim da As New SqlDataAdapter("select clv_id as Id, Contrato, Usuario, Fecha from tbl_regCambioPeriodo where contrato =" & Contrato, cnn)
            Dim ds As New DataSet

            da.Fill(ds)

            DataGridCambioEsquema.DataSource = ds.Tables(0)
        End If
    End Sub

    Private Sub BtnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnConsultar.Click
        GloContrato = DataGridCambioEsquema.Item(1, DataGridCambioEsquema.CurrentRow.Index).Value
        IDCamesquema = DataGridCambioEsquema.Item(0, DataGridCambioEsquema.CurrentRow.Index).Value
        FrmConsultaCambiosEsq.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class