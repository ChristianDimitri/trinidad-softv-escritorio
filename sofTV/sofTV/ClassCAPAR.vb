﻿Public Class ClassCAPAR
    Dim CAPAR As New BaseIII
    Public noClientes As String
    Public noCajas As String

    Public Sub uspMandaOrdenesCambioAparato(ByVal prmClvSession As Long, ByVal prmClvTipSer As Integer)
        CAPAR.limpiaParametros()
        CAPAR.CreateMyParameter("@clvSession", SqlDbType.BigInt, prmClvSession)
        CAPAR.CreateMyParameter("@clvTipSer", SqlDbType.Int, prmClvTipSer)

        CAPAR.Inserta("uspMandaOrdenesCambioAparato")
    End Sub

    Public Sub uspCalculaClientesOrdenesCapar(ByVal prmClvSession As Long, ByVal prmClvTipSer As Integer)
        CAPAR.limpiaParametros()
        CAPAR.CreateMyParameter("@clvSession", SqlDbType.BigInt, prmClvSession)
        CAPAR.CreateMyParameter("@clvTipSer", SqlDbType.Int, prmClvTipSer)
        CAPAR.CreateMyParameter("@noContratos", ParameterDirection.Output, 0)
        CAPAR.CreateMyParameter("@noCajas", ParameterDirection.Output, 0)
        CAPAR.ProcedimientoOutPut("uspCalculaClientesOrdenesCapar")

        noClientes = CAPAR.dicoPar("@noContratos").ToString()
        noCajas = CAPAR.dicoPar("@noCajas").ToString()
    End Sub

    Public Sub uspEliminaSessionCambioAparato(ByVal prmClvSession As Long)
        CAPAR.limpiaParametros()
        CAPAR.CreateMyParameter("@clvSession", SqlDbType.BigInt, prmClvSession)
        CAPAR.Inserta("uspEliminaSessionCambioAparato")
    End Sub
End Class
