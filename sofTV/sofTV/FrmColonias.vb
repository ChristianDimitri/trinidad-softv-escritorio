Imports System.Data.SqlClient
Public Class FrmColonias
    Private locclv_Ciudad1 As Integer = 0
    Private ciudadtxt As String = Nothing
    Private locclv_TipSer As Integer = 0
    Private locTiene_serv As Integer = 0
    Private tipservtxt As String = Nothing
    Private Nombrecol As String = Nothing
    Private CP As String = Nothing
    Private Tipocolo As String = Nothing
    Private tieneserv As String = Nothing
    Private fechaentrega As String = Nothing

    Private Sub Guardar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.ConColoniasBindingSource.EndEdit()
        Me.ConColoniasTableAdapter.Connection = CON
        Me.ConColoniasTableAdapter.Update(Me.NewSofTvDataSet.ConColonias)
        CON.Close()
    End Sub
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                Nombrecol = Me.NombreTextBox.Text
                CP = Me.CPTextBox.Text
                Tipocolo = Me.ComboBox1.Text
                If Me.CheckBox1.CheckState = CheckState.Checked Then
                    tieneserv = "True"
                Else
                    tieneserv = "False "
                End If
                fechaentrega = Me.FechaEntregaTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Dim validacion1 As String = Nothing
        Select Case op
            Case 0
                If opcion = "N" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Capturo Un Nuevo Barrio", "", "Se Capturo Un Nuevo Barrio: " + Me.NombreTextBox.Text, LocClv_Ciudad)
                ElseIf opcion = "M" Then

                    'Nombrecol = Me.NombreTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NombreTextBox.Name + " Barrio: " + Me.NombreTextBox.Text, Nombrecol, Me.NombreTextBox.Text, LocClv_Ciudad)
                    'CP = Me.CPTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.CPTextBox.Name + " Barrio: " + Me.NombreTextBox.Text, CP, Me.CPTextBox.Text, LocClv_Ciudad)
                    'Tipocolo = Me.ComboBox1.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "TipodeBarrio," + " Barrio: " + Me.NombreTextBox.Text, Tipocolo, Me.ComboBox1.Text, LocClv_Ciudad)
                    'Tiene servicio
                    If Me.CheckBox1.CheckState = CheckState.Checked Then
                        'tieneserv = "True"
                        validacion1 = "True"
                    Else
                        'tieneserv = "False "
                        validacion1 = "False"
                    End If
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Tieneservicio," + " Col: " + Me.NombreTextBox.Text, tieneserv, validacion1, LocClv_Ciudad)

                    'fechaentrega = Me.FechaEntregaTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.FechaEntregaTextBox.Name + " Col: " + Me.NombreTextBox.Text, fechaentrega, Me.FechaEntregaTextBox.Text, LocClv_Ciudad)
                    damedatosbitacora()
                End If
            Case 1
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Nueva Ciudad Para el Barrio", " Ciudad: " + Me.ComboBox3.Text, "Nueva Ciudad Para el Barrio: " + Me.NombreTextBox.Text, LocClv_Ciudad)
            Case 2
                'Me.locclv_Ciudad1
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Una Ciudad Para el Barrio", " Ciudad :" + ciudadtxt, "Se Elimino Una Ciudad Para el Barrio: " + Me.NombreTextBox.Text, LocClv_Ciudad)
            Case 3
                'Agregar tipo de servicio
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Agrego Un Tipo De Servicio Para El Barrio ", " Tipo de Servicio: " + Me.ComboBox4.Text, "Nuevo Tipo De Servicio Para El Barrio: " + Me.NombreTextBox.Text, LocClv_Ciudad)
            Case 4
                'Eliminar tipo de servicio
                'Me.locclv_TipSer
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Un Tipo De Servicio Para El Barrio", " Tipo de Servicio :" + tipservtxt, "Se Elimino Un Tipo de Serrvicio Para El Barrio: " + Me.NombreTextBox.Text, LocClv_Ciudad)
        End Select

    End Sub

    Public Sub CREAARBOLCiudades()
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Try
        '    Dim I As Integer = 0
        '    Dim X As Integer = 0
        '    Me.CONCVECOLCIUTableAdapter.Connection = CON
        '    Me.CONCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.CONCVECOLCIU, New System.Nullable(Of Integer)(CType(GloClv_COLONIA, Integer)))
        '    Dim FilaRow As DataRow
        '    Me.TreeView1.Nodes.Clear()
        '    For Each FilaRow In Me.NewSofTvDataSet.CONCVECOLCIU.Rows
        '        'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
        '        X = 0
        '        If IsNumeric(Trim(FilaRow("Clv_Ciudad").ToString())) = True Then
        '            Me.TreeView1.Nodes.Add(Trim(FilaRow("Clv_Ciudad").ToString()), Trim(FilaRow("Nombre").ToString()))
        '            Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("Clv_Ciudad").ToString())
        '            I += 1
        '        End If
        '    Next
        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        'CON.Close()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.BigInt, GloClv_COLONIA)
            DataGridView1.DataSource = BaseII.ConsultaDT("ConsultaCVECOLCIU")
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CREAARBOLRELCOLONIASSER()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.CONRelColoniasSerTableAdapter.Connection = CON
            Me.CONRelColoniasSerTableAdapter.Fill(Me.NewSofTvDataSet.CONRelColoniasSer, New System.Nullable(Of Integer)(CType(GloClv_COLONIA, Integer)))
            Dim FilaRow As DataRow
            Me.TreeView2.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.CONRelColoniasSer.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                If IsNumeric(Trim(FilaRow("Clv_TIPSER").ToString())) = True Then
                    Me.TreeView2.Nodes.Add(Trim(FilaRow("Clv_TIPSER").ToString()), Trim(FilaRow("CONCEPTO").ToString()))
                    Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("Clv_TIPSER").ToString())
                    I += 1
                End If
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub BuscaClientes(ByVal Clv_coloniA As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim valida_servicio As Integer = 0
        CON.Open()
        Try
            Me.ConColoniasTableAdapter.Connection = CON
            Me.ConColoniasTableAdapter.Fill(Me.NewSofTvDataSet.ConColonias, New System.Nullable(Of Integer)(CType(Clv_coloniA, Integer)))
            Me.Consulta_Rel_Colonia_ServicioTableAdapter.Connection = CON
            Me.Consulta_Rel_Colonia_ServicioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Consulta_Rel_Colonia_Servicio, CType(Clv_coloniA, Integer), valida_servicio)
            If valida_servicio = 0 Then
                Me.CheckBox1.Checked = False
            ElseIf valida_servicio = 1 Then
                Me.CheckBox1.Checked = True
            End If
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub ConColoniasBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Guardar()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmColonias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraCiudades' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraCiudadesTableAdapter.Connection = CON
        Me.MuestraCiudadesTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCiudades)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTiposServicioTv' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTiposServicioTvTableAdapter.Connection = CON
        Me.MuestraTiposServicioTvTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTiposServicioTv)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.Tipo_Colonias' Puede moverla o quitarla seg�n sea necesario.
        Me.Tipo_ColoniasTableAdapter.Connection = CON
        Me.Tipo_ColoniasTableAdapter.Fill(Me.NewSofTvDataSet.Tipo_Colonias)
        If opcion = "N" Then
            Me.ConColoniasBindingSource.AddNew()

        ElseIf opcion = "C" Then
            Me.BuscaClientes(GloClv_COLONIA)
            CREAARBOLCiudades()
            CREAARBOLRELCOLONIASSER()
            Me.Panel1.Enabled = False
            Me.Panel2.Enabled = False
            Me.Panel3.Enabled = False
        ElseIf opcion = "M" Then
            Me.BuscaClientes(GloClv_COLONIA)
            CREAARBOLCiudades()
            CREAARBOLRELCOLONIASSER()
        End If
        CON.Close()
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)


    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.Clv_ServicioTipoTextBox.Text = Me.ComboBox2.SelectedValue
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.Clv_TipoTextBox.Text = Me.ComboBox1.SelectedValue
    End Sub

    Private Sub ConColoniasBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConColoniasBindingNavigatorSaveItem.Click

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.ConColoniasBindingSource.EndEdit()
        Me.ConColoniasTableAdapter.Connection = CON
        Me.ConColoniasTableAdapter.Update(Me.NewSofTvDataSet.ConColonias)
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            locTiene_serv = 1
        ElseIf Me.CheckBox1.CheckState = CheckState.Unchecked Then
            locTiene_serv = 0
        End If
        If IsNumeric(Me.Clv_ColoniaTextBox.Text) = True Then
            Me.Inserta_Rel_Colonia_ServicioTableAdapter.Connection = CON
            Me.Inserta_Rel_Colonia_ServicioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Rel_Colonia_Servicio, CLng(Me.Clv_ColoniaTextBox.Text), locTiene_serv)
        End If
        guardabitacora(0)
        MsgBox("Se Guardo con Ex�to")
        GloBnd = True
        CON.Close()
        'Me.Close()
    End Sub

    Private Sub Clv_ColoniaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaTextBox.TextChanged
        If IsNumeric(Me.Clv_ColoniaTextBox.Text) = True Then
            GloClv_COLONIA = Me.Clv_ColoniaTextBox.Text
            activa()
        End If
    End Sub

    Private Sub activa()
        If GloClv_COLONIA > 0 Then
            Me.Panel2.Enabled = True
            Me.Panel3.Enabled = True
        Else
            Me.Panel2.Enabled = False
            Me.Panel3.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim bnd As Boolean = True
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.Validate()
            Me.CONCVECOLCIUBindingSource.EndEdit()
            Me.CONCVECOLCIUTableAdapter.Connection = CON
            Me.CONCVECOLCIUTableAdapter.Update(Me.NewSofTvDataSet.CONCVECOLCIU)
            '--MsgBox("Se Guardo con Ex�to", MsgBoxStyle.Information)
            Me.CONCVECOLCIUTableAdapter.Connection = CON
            Me.CONCVECOLCIUTableAdapter.Insert(GloClv_COLONIA, Me.ComboBox3.SelectedValue)
            Me.Validate()
            Me.CONCVECOLCIUBindingSource.EndEdit()
            Me.CONCVECOLCIUTableAdapter.Connection = CON
            Me.CONCVECOLCIUTableAdapter.Update(Me.NewSofTvDataSet.CONCVECOLCIU)
            If bnd = True Then
                CREAARBOLCiudades()
            End If
            guardabitacora(1)
        Catch ex As System.Exception

            bnd = False
            Me.CONCVECOLCIUBindingSource.CancelEdit()
            MsgBox("La Ciudad Ya Ex�ste en la Lista")
            '--System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If locclv_Ciudad1 > 0 Then
            Me.CONCVECOLCIUTableAdapter.Connection = CON
            Me.CONCVECOLCIUTableAdapter.Delete(GloClv_COLONIA, Me.locclv_Ciudad1)
            Me.Validate()
            Me.CONCVECOLCIUBindingSource.EndEdit()
            Me.CONCVECOLCIUTableAdapter.Connection = CON
            Me.CONCVECOLCIUTableAdapter.Update(Me.NewSofTvDataSet.CONCVECOLCIU)
            Me.CREAARBOLCiudades()
            guardabitacora(2)
            locclv_Ciudad1 = 0
        Else
            MsgBox("Seleccione lo que desea Quitar de la Lista", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        locclv_Ciudad1 = e.Node.Tag
        ciudadtxt = e.Node.Text
    End Sub


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim bnd As Boolean = True
        Try
            Me.Validate()
            Me.CONRelColoniasSerBindingSource.EndEdit()
            Me.CONRelColoniasSerTableAdapter.Connection = CON
            Me.CONRelColoniasSerTableAdapter.Update(Me.NewSofTvDataSet.CONRelColoniasSer)
            '--MsgBox("Se Guardo con Ex�to", MsgBoxStyle.Information)
            Me.CONRelColoniasSerTableAdapter.Connection = CON
            Me.CONRelColoniasSerTableAdapter.Insert(GloClv_COLONIA, Me.ComboBox4.SelectedValue)
            Me.Validate()
            Me.CONRelColoniasSerBindingSource.EndEdit()
            Me.CONRelColoniasSerTableAdapter.Connection = CON
            Me.CONRelColoniasSerTableAdapter.Update(Me.NewSofTvDataSet.CONRelColoniasSer)
            If bnd = True Then
                CREAARBOLRELCOLONIASSER()
            End If
            guardabitacora(3)

        Catch ex As System.Exception

            bnd = False
            Me.CONRelColoniasSerBindingSource.CancelEdit()
            MsgBox("El Servicio Ya Ex�ste en la Lista")
            '--System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        locclv_TipSer = e.Node.Tag
        tipservtxt = e.Node.Text
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If locclv_TipSer > 0 Then
            Me.CONRelColoniasSerTableAdapter.Connection = CON
            Me.CONRelColoniasSerTableAdapter.Delete(GloClv_COLONIA, Me.locclv_TipSer)
            Me.Validate()
            Me.CONRelColoniasSerBindingSource.EndEdit()
            Me.CONRelColoniasSerTableAdapter.Connection = CON
            Me.CONRelColoniasSerTableAdapter.Update(Me.NewSofTvDataSet.CONRelColoniasSer)
            Me.CREAARBOLRELCOLONIASSER()
            guardabitacora(4)
            locclv_TipSer = 0
        Else
            MsgBox("Seleccione lo que desea Quitar de la Lista", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripLabel1.Click
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

   
    Private Sub FillToolStripButton_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        locclv_Ciudad1 = DataGridView1.Rows(e.RowIndex).Cells(1).Value
        ciudadtxt = DataGridView1.Rows(e.RowIndex).Cells(2).Value
    End Sub

    Private Sub DataGridView1_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.BigInt, GloClv_COLONIA)
            BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, DataGridView1.Rows(e.RowIndex).Cells(1).Value)
            BaseII.CreateMyParameter("@TieneFibra", SqlDbType.Bit, CInt(DataGridView1.Rows(e.RowIndex).Cells(3).Value))
            BaseII.CreateMyParameter("@TieneCoaxial", SqlDbType.Bit, CInt(DataGridView1.Rows(e.RowIndex).Cells(4).Value))
            BaseII.Inserta("UpdateCVECOLCIU")
        Catch ex As System.Exception
        End Try
    End Sub
End Class