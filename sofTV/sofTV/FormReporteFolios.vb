﻿Imports System.Text
Imports System.Data.SqlClient
Imports sofTV.Base
Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engine

Public Class FormReporteFolios

    Dim pSerie As String
    Dim pVendedor As Integer
    Dim pInicio As Integer
    Dim pFin As Integer
    Dim pExistentes As Integer
    Dim minimo As Integer

    Dim Reporte As New Reporte
    Dim consulta As New CBase
    Private customersByCityReport As ReportDocument

    Private Sub Vendedor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Vendedor.TextChanged
        LlenaSerie()
    End Sub

    Private Sub LlenaSerie()
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Dim strSQL As StringBuilder
        Dim conexion As New SqlConnection(MiConexion)
        If IsNumeric(Me.Vendedor.SelectedValue) = True Then
            'And Len(Trim(Me.Vendedor.SelectedText)) > 0 
            strSQL = New StringBuilder("EXEC Ultimo_SERIEYFOLIO " & CStr(Me.Vendedor.SelectedValue))
        Else
            strSQL = New StringBuilder("EXEC Ultimo_SERIEYFOLIO " & CStr(0))
        End If
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.Serie.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        Me.Serie.Text = ""
    End Sub

    Private Sub FormReporteFolios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LenaVendedor()
        If GloReportFolios = 5 Then
            CMBLabel3.Visible = False
            Txt_inicio.Visible = False
            Txt_fin.Visible = False
        Else
            CMBLabel3.Visible = True
            Txt_inicio.Visible = True
            Txt_fin.Visible = True
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub LenaVendedor()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MUESTRAVENDEDORES_2 ")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.Vendedor.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        Me.Vendedor.Text = ""
    End Sub

    Private Sub Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        Me.Close()
    End Sub

    Private Sub Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Function FoliosExistentes(ByVal vserie As String) As Integer

        Dim parametro(1) As SqlParameter

        parametro(0) = New SqlParameter("@serie", SqlDbType.NVarChar, 150)
        parametro(0).Direction = ParameterDirection.Input
        parametro(0).Value = vserie

        Dim registro As DataTable = consulta.consultarDT("SP_ReimpresionFoliosExistentes", parametro)

        If registro.Rows.Count > 0 Then
            Return Convert.ToInt32(registro.Rows(0)("Existentes").ToString())
        Else
            MsgBox("No hay folios existentes", MsgBoxStyle.Information)
            Return 0
        End If
    End Function

    Private Sub ResumenFolios()
        customersByCityReport = New ReportDocument
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReportFoliosS2.rpt"
        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Vendedor", SqlDbType.Int, CInt(pVendedor))
        BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, CStr(pSerie))
        BaseII.CreateMyParameter("@Fini", SqlDbType.Int, CInt(pInicio))
        BaseII.CreateMyParameter("@Ffin", SqlDbType.Int, CInt(pFin))

        Dim listatablas As New List(Of String)
        listatablas.Add("Disponibles")
        'listatablas.Add("Cancelados")
        listatablas.Add("TotalFactura")
        'listatablas.Add("Vendedore")
        DS = BaseII.ConsultaDS("Reporte_Folios", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        eTituloComision = "Del " & pInicio & " al " & pFin
        customersByCityReport.DataDefinition.FormulaFields("Folios").Text = "'" & eTituloComision & "'"
        'CrystalReportViewer1.ReportSource = customersByCityReport
        'Reporte.Show()
        Reporte.CrystalReportViewer1.ReportSource = customersByCityReport
        'Reporte.CrystalReportViewer1.Show()
        Reporte.Show()
    End Sub

    Private Sub ResumenFolios2()
        pVendedor = CInt(Me.Vendedor.SelectedValue)
        pSerie = CStr(Me.Serie.SelectedValue)
        customersByCityReport = New ReportDocument
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReportFoliosS5.rpt"
        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Vendedor", SqlDbType.Int, CInt(pVendedor))
        BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, CStr(pSerie))

        Dim listatablas As New List(Of String)
        listatablas.Add("Disponibles")
        listatablas.Add("TotalFactura")
        DS = BaseII.ConsultaDS("Reporte_Folios5", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        eTituloComision = "Del " & pInicio & " al " & pFin
        customersByCityReport.DataDefinition.FormulaFields("Folios").Text = "'" & eTituloComision & "'"
        Reporte.CrystalReportViewer1.ReportSource = customersByCityReport
        Reporte.Show()
    End Sub

    Private Function FoliosMinimo(ByVal vserie As String) As Integer

        Dim parametro(1) As SqlParameter

        parametro(0) = New SqlParameter("@serie", SqlDbType.NVarChar, 150)
        parametro(0).Direction = ParameterDirection.Input
        parametro(0).Value = vserie

        Dim registro As DataTable = consulta.consultarDT("SP_ReimpresionFoliosExistentesMin", parametro)

        If registro.Rows.Count > 0 Then
            Return Convert.ToInt32(registro.Rows(0)("Minimo").ToString())
        Else
            MsgBox("No hay folios existentes", MsgBoxStyle.Information)
            Return 0
        End If

    End Function

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        'Dim conexion As New SqlConnection(MiConexion)
        'Dim comando As New SqlCommand("Cancela_Folios", conexion)
        'comando.CommandType = CommandType.StoredProcedure
        If IsNumeric(Me.Vendedor.SelectedValue) = True Then
            If Len(Me.Serie.SelectedValue) > 0 Then
                If GloReportFolios = 5 Then
                    ResumenFolios2()
                    GloReportFolios = 0
                    Me.Close()
                Else
                    If (Not IsNumeric(Txt_inicio.Text.ToString()) Or Not IsNumeric(Txt_fin.Text.ToString())) Then
                        MsgBox("Inserta datos enteros y selecciona una serie", MsgBoxStyle.Information)
                    Else
                        pVendedor = CInt(Me.Vendedor.SelectedValue)
                        pSerie = CStr(Me.Serie.SelectedValue)
                        pInicio = Convert.ToInt32(Txt_inicio.Text.ToString())
                        pFin = Convert.ToInt32(Txt_fin.Text.ToString())
                        pExistentes = FoliosExistentes(pSerie)
                        minimo = FoliosMinimo(pSerie)
                        If pInicio < minimo Or pInicio > pFin Then
                            'MsgBox("El folio de inicio debe ser mayor a cero y menor o igual al folio final", MsgBoxStyle.Information)
                            MsgBox("El folio de inicio debe ser mayor a " & minimo.ToString() & " y menor o igual al folio final", MsgBoxStyle.Information)
                        Else
                            If pFin < pInicio Or pFin > pExistentes Then
                                MsgBox("El folio final debe ser mayor o igual al folio de inicio y menor a " & pExistentes.ToString(), MsgBoxStyle.Information)
                            Else
                                ResumenFolios()
                                Me.Close()
                            End If
                        End If
                    End If
                End If
            Else
                MsgBox("Elija Una Serie")
            End If
        Else
            MsgBox("Elija Al Vendedor")
        End If
    End Sub
End Class