﻿Public Class FrmNombresSeparadosBuro

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        PreguntaDatosFiscales = False
        DimeSiDatosFiscales = False

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, CInt(FrmClientes.CONTRATOTextBox.Text))
        BaseII.CreateMyParameter("@DatosFiscales", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@DimeSiDatosFiscales", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("DimeSiPreguntaDatosFiscales")
        PreguntaDatosFiscales = BaseII.dicoPar("@DatosFiscales").ToString
        DimeSiDatosFiscales = BaseII.dicoPar("@DimeSiDatosFiscales").ToString

        If Len(Trim(Me.txtNombre.Text)) = 0 Then
            MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.txtApellidoPaterno.Text)) = 0 Then
            MsgBox("Se Requiere el Apellido Paterno ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.txtRFC.Text)) = 0 And PreguntaDatosFiscales = False Then
            MsgBox("Se Requiere el NIT ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TxtCI.Text)) = 0 Then
            MsgBox("Se Requiere la CI ", MsgBoxStyle.Information)
            Exit Sub
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmNombresSeparadosBuro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        TxtCI.Text = GloCI
    End Sub
End Class