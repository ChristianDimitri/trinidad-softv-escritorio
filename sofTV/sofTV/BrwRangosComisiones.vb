Imports System.Data.SqlClient
Imports System.Text
Public Class BrwRangosComisiones
    Private conexion As New SqlConnection(MiConexion)

    Private Sub ConComision(ByVal Clv_Servicio As Integer)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC CATALOGOCOMISIONES ")
        strSQL.Append(CStr(Clv_Servicio))

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            Me.DataGridView1.DataSource = bindingSource

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub MuestraTipServEric()
        Dim strSQl As New StringBuilder
        strSQl.Append("EXEC MuestraTipServEric 0,0")

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQl.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            Me.ComboBox1.DataSource = bindingSource
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer)
        Dim strSQl As New StringBuilder
        strSQl.Append("EXEC MuestraServiciosEric ")
        strSQl.Append(CStr(Clv_TipSer) & ",0,2")

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQl.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            Me.ComboBox2.DataSource = bindingSource
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BrwRangosComisiones_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBndComisiones = True Then
            eBndComisiones = False
            ConComision(CInt(Me.ComboBox2.SelectedValue))
        End If
    End Sub

    Private Sub BrwRangosComisiones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraTipServEric()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = False Or IsNumeric(Me.ComboBox2.SelectedValue) = False Then
            Exit Sub
        End If
        eTipo = Me.ComboBox1.Text
        eClv_TipSer = Me.ComboBox1.SelectedValue
        eServicio = Me.ComboBox2.Text
        eClv_Servicio = Me.ComboBox2.SelectedValue
        eOpcion = "N"
        FrmRangosComisiones.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If IsNumeric(Me.ComboBox1.SelectedValue) = False Or IsNumeric(Me.ComboBox2.SelectedValue) = False Then
            Exit Sub
        End If
        If Me.DataGridView1.RowCount = 0 Then
            MsgBox("No existen registros a modificar.", MsgBoxStyle.Information)
            Exit Sub
        End If

        eTipo = Me.ComboBox1.Text
        eClv_TipSer = Me.ComboBox1.SelectedValue
        eServicio = Me.ComboBox2.Text
        eClv_Servicio = Me.ComboBox2.SelectedValue
        eClv_Rango = Me.DataGridView1.SelectedCells.Item(3).Value
        eOpcion = "M"
        FrmRangosComisiones.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedValueChanged
        MuestraServiciosEric(CInt(Me.ComboBox1.SelectedValue))
        ConComision(CInt(Me.ComboBox2.SelectedValue))
    End Sub

    Private Sub ComboBox2_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedValueChanged
        ConComision(CInt(Me.ComboBox2.SelectedValue))
    End Sub

End Class