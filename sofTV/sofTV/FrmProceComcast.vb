Imports System.Data.SqlClient
Public Class FrmProceComcast
    Dim Clv_TipSer As Integer
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Private TipSer As String = Nothing
    Private Mac As String = Nothing
    Private yaesta As Integer = 0

    Private Sub GuardaDatosBitacora()
        Try
            Dim Cont As Integer = 0
            'If Me.RadioButton3.Checked = True Then
            '    TipSer = Me.RadioButton3.Text
            'Else
            '    TipSer = Me.RadioButton4.Text
            'End If

            Mac = Me.Clv_CableModemTextBox.Text
            bitsist(GloUsuario, CLng(Me.TextBox1.Text), LocGloSistema, Me.Text, "Se Mando Comando Onu", "", "Comando: " + TipSer + " / Mac Onu: " + Mac, LocClv_Ciudad)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eContrato = 0
        BrwSelContrato.Show()
    End Sub

    Private Sub FrmProceComcast_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eContrato > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            Me.TextBox1.Text = eContrato
            CON.Open()
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            CON.Close()

            If yaesta = 0 Then
                BuscarCliente()
            End If
            yaesta = 0

        End If

    End Sub

    Private Sub FrmProceComcast_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        'If GloHabilitadosNET = False Then
        '    Me.RadioButton4.Visible = False
        'Else
        '    RadioButton4.Visible = True
        '    RadioButton4.Checked = True
        'End If


        'If GloHabilitadosDIG = False Then
        '    RadioButton3.Visible = False
        'Else
        '    RadioButton3.Visible = True
        '    If RadioButton4.Checked = False Then
        '        RadioButton3.Checked = True
        '    Else
        '        RadioButton3.Checked = False
        '    End If

        'End If
        eContrato = 0
        'If Me.RadioButton3.Checked = True Then
        '    Clv_TipSer = 3
        'End If
        'If Me.RadioButton4.Checked = True Then
        '    Clv_TipSer = 2
        'End If
    End Sub
    Private Sub BuscarCliente()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameClientesActivosTableAdapter.Connection = CON
        Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
        CON.Close()
        BuscarServ()
    End Sub
    Private Sub BuscarServ()
        Dim CON As New SqlConnection(MiConexion)
        eRes = 0
        eMsg = ""
        CON.Open()
        Me.MuestraServCteResetTableAdapter.Connection = CON
        Me.MuestraServCteResetTableAdapter.Fill(Me.DataSetEric.MuestraServCteReset, Me.TextBox1.Text, 7, eRes, eMsg)
        CON.Close()
        If eRes = 1 Then
            yaesta = 1
            MsgBox(eMsg)
        End If


    End Sub

    'Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Button4.Visible = False
    '    If Me.RadioButton1.Checked = True Then
    '        Clv_TipSer = 3
    '        If IsNumeric(Me.TextBox1.Text) = True Then
    '            BuscarCliente()
    '        End If

    '    End If
    'End Sub

    'Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Button4.Visible = False
    '    If Me.RadioButton2.Checked = True Then
    '        Clv_TipSer = 2
    '        If IsNumeric(Me.TextBox1.Text) = True Then
    '            BuscarCliente()
    '        End If

    '    End If
    'End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.ResetServCteTableAdapter.Connection = CON
            Me.ResetServCteTableAdapter.Fill(Me.DataSetEric.ResetServCte, Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, 6, eRes, eMsg)
            CON.Close()
            TipSer = "Activar"
            GuardaDatosBitacora()
            MsgBox(eMsg)


        Else
            MsgBox("Selecciona Un Aparato a Activar", , "Atenci�n")
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                BuscarCliente()
            End If
        End If
    End Sub


    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub ResetPrimer_Modelo(ByVal mcontrato As Long, ByVal mClv_Cablemodem As Long, ByVal MClv_Tipser As Integer)
        Dim Con45 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim Res As Integer = 0
        Dim Msg As String = ""
        Try
            cmd = New SqlClient.SqlCommand()
            Con45.Open()
            With cmd
                .CommandText = "ResetServCtePrimer_Modelo"
                .CommandTimeout = 0
                .Connection = Con45
                .CommandType = CommandType.StoredProcedure

                '@Contrato Bigint,@Clv_CableModem Bigint,@Clv_TipSer int,@Res int output,@Msg varchar(150) output

                Dim prm1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = mcontrato
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Clv_CableModem", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = mClv_Cablemodem
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = MClv_Tipser
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Res", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@Msg", SqlDbType.VarChar, 50)
                prm5.Direction = ParameterDirection.Output
                prm5.Value = ""
                .Parameters.Add(prm5)

                Dim i As Integer = cmd.ExecuteNonQuery()
                Res = prm4.Value
                Msg = prm5.Value
            End With
            Con45.Close()
        Catch ex As Exception
            If Con45.State <> ConnectionState.Closed Then Con45.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            ResetPrimer_Modelo(Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, Clv_TipSer)
            GuardaDatosBitacora()
            MsgBox("Se realizo con Exito")
        Else
            MsgBox("Selecciona Un Aparato a Resetear", , "Atenci�n")
        End If
    End Sub

    'Private Sub RadioButton4_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    Me.Button4.Visible = False
    '    If Me.RadioButton4.Checked = True Then
    '        Clv_TipSer = 2
    '        If IsNumeric(Me.TextBox1.Text) = True Then
    '            BuscarCliente()
    '        End If

    '    End If
    'End Sub

    'Private Sub RadioButton3_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    Me.Button4.Visible = False
    '    If Me.RadioButton3.Checked = True Then
    '        Clv_TipSer = 3
    '        If IsNumeric(Me.TextBox1.Text) = True Then
    '            BuscarCliente()
    '        End If

    '    End If
    'End Sub

    Private Sub Button4_Click_1(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.ResetServCteTableAdapter.Connection = CON
            Me.ResetServCteTableAdapter.Fill(Me.DataSetEric.ResetServCte, Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, 7, eRes, eMsg)
            CON.Close()
            TipSer = "Borrar"
            GuardaDatosBitacora()
            MsgBox(eMsg)


        Else
            MsgBox("Selecciona Un Aparato a Borrar", , "Atenci�n")
        End If
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.ResetServCteTableAdapter.Connection = CON
            Me.ResetServCteTableAdapter.Fill(Me.DataSetEric.ResetServCte, Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, 8, eRes, eMsg)
            CON.Close()
            TipSer = "Reset"
            GuardaDatosBitacora()
            MsgBox(eMsg)


        Else
            MsgBox("Selecciona Un Aparato a Resetear", , "Atenci�n")
        End If
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.ResetServCteTableAdapter.Connection = CON
            Me.ResetServCteTableAdapter.Fill(Me.DataSetEric.ResetServCte, Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, 9, eRes, eMsg)
            CON.Close()
            TipSer = "ResetTv"
            GuardaDatosBitacora()
            MsgBox(eMsg)


        Else
            MsgBox("Selecciona Un Aparato a Resetear", , "Atenci�n")
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.ResetServCteTableAdapter.Connection = CON
            Me.ResetServCteTableAdapter.Fill(Me.DataSetEric.ResetServCte, Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, 10, eRes, eMsg)
            CON.Close()
            TipSer = "Borrar"
            GuardaDatosBitacora()
            MsgBox(eMsg)


        Else
            MsgBox("Selecciona Un Aparato a Borrar", , "Atenci�n")
        End If
    End Sub

    Private Sub BtnBuscar_Click(sender As Object, e As EventArgs) Handles BtnBuscar.Click
        If IsNumeric(Me.Clv_CableModemTextBox.Text) Then


            TipSer = "Buscar"
            GuardaDatosBitacora()

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Me.ContratoTextBox.Text)
            BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, Me.Clv_CableModemTextBox.Text)
            BaseII.CreateMyParameter("@consecutivo", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("insertaComandoBuscarProceComcast")

            Dim frm1 As New FrmBuscarOnu
            frm1.Consecutivo = CInt(BaseII.dicoPar("@consecutivo").ToString)


            frm1.ShowDialog()




        Else
            MsgBox("Selecciona Un Aparato a Borrar", , "Atenci�n")
        End If
    End Sub
End Class