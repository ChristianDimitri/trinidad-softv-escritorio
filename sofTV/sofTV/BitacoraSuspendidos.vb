Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Text


Public Class BitacoraSuspendidos
    Private tipo As String = 0
    Private contrato As Int64 = Convert.ToInt64(FrmClientes.CONTRATOTextBox.Text.ToString)
    Private TipoServ As Integer = 0
    Private Inicial As DateTime = "01-01-1900"
    Private Final As DateTime = "12-12-9999"
    Private Con As New SqlConnection(MiConexion)

    Private Sub BitacoraSuspendidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Con As New SqlConnection(MiConexion)
        MuestraTipSer(0, 0)
        Try
            Con.Open()
            tipo = 2
            TipoServ = cb_tipoServicio.SelectedValue
            Me.HistorialDesconexionesTableAdapter.Connection = Con
            Me.HistorialDesconexionesTableAdapter.Fill(Me.Ds.historialDesconexiones, contrato, TipoServ, Inicial, Final, tipo)
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.Message, "Error")
        End Try
        Con.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub btnFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
        Me.Refresh()
        TipoServ = cb_tipoServicio.SelectedValue
        If CheckBox1.Checked = True Then
            Inicial = Convert.ToDateTime(dtpInicial.Value.ToString)
            Final = Convert.ToDateTime(dtpFinal.Value.ToString)
            If Inicial <= Final Or Inicial = "01-01-1900" Or Final = "12-12-9999" Then
                Try
                    Con.Open()
                    tipo = 1
                    Me.HistorialDesconexionesTableAdapter.Connection = Con
                    Me.HistorialDesconexionesTableAdapter.Fill(Me.Ds.historialDesconexiones, contrato, TipoServ, Inicial, Final, tipo)
                Catch ex As Exception
                    Windows.Forms.MessageBox.Show(ex.Message, "Error")
                End Try
                Con.Close()
            Else
                Windows.Forms.MessageBox.Show("El rango de fechas no es correcto.", "Error")
            End If
        Else
            Try
                Con.Open()
                tipo = 2
                Me.HistorialDesconexionesTableAdapter.Connection = Con
                Me.HistorialDesconexionesTableAdapter.Fill(Me.Ds.historialDesconexiones, contrato, TipoServ, Inicial, Final, tipo)
            Catch ex As Exception
                Windows.Forms.MessageBox.Show(ex.Message, "Error")
            End Try
            Con.Close()
        End If
        Me.Refresh()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim nuevo As ImprimirHistorialDesconexiones = New ImprimirHistorialDesconexiones
        nuevo.Reportes(tipo, contrato, Me.cb_tipoServicio.SelectedValue, Inicial, Final)
        nuevo.Show()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        lbl_fechaInicial.Visible = True
        lblFinal.Visible = True
        dtpInicial.Visible = True
        dtpFinal.Visible = True
    End Sub

    Private Sub cb_tipoServicio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_tipoServicio.SelectedIndexChanged
        Dim Con As New SqlConnection(MiConexion)
        Try
            Con.Open()
            tipo = 2
            TipoServ = cb_tipoServicio.SelectedValue
            Me.HistorialDesconexionesTableAdapter.Connection = Con
            Me.HistorialDesconexionesTableAdapter.Fill(Me.Ds.historialDesconexiones, contrato, TipoServ, Inicial, Final, tipo)
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.Message, "Error")
        End Try
        Con.Close()
    End Sub

    Private Sub MuestraTipSer(ByVal Clv_TipSer As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipServEric ")
        strSQL.Append(CStr(Clv_TipSer) & ", ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As SqlDataAdapter = New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As DataTable = New DataTable
        Dim bindingSource As BindingSource = New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cb_tipoServicio.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
End Class



