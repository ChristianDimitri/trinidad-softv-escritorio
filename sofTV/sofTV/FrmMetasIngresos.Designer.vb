<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMetasIngresos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim AnioLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMetasIngresos))
        Me.ConMetasIngresosDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ConMetasIngresosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2
        Me.ConMetasIngresosTableAdapter = New sofTV.DataSetEric2TableAdapters.ConMetasIngresosTableAdapter
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox
        Me.AnioTextBox = New System.Windows.Forms.TextBox
        Me.MuestraAniosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraAniosTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraAniosTableAdapter
        Me.AnioComboBox = New System.Windows.Forms.ComboBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ConMetasIngresosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ConMetasIngresosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Button1 = New System.Windows.Forms.Button
        AnioLabel = New System.Windows.Forms.Label
        CType(Me.ConMetasIngresosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMetasIngresosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraAniosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ConMetasIngresosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMetasIngresosBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'AnioLabel
        '
        AnioLabel.AutoSize = True
        AnioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        AnioLabel.Location = New System.Drawing.Point(391, 31)
        AnioLabel.Name = "AnioLabel"
        AnioLabel.Size = New System.Drawing.Size(35, 15)
        AnioLabel.TabIndex = 6
        AnioLabel.Text = "Año:"
        '
        'ConMetasIngresosDataGridView
        '
        Me.ConMetasIngresosDataGridView.AllowUserToResizeColumns = False
        Me.ConMetasIngresosDataGridView.AllowUserToResizeRows = False
        Me.ConMetasIngresosDataGridView.AutoGenerateColumns = False
        Me.ConMetasIngresosDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConMetasIngresosDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConMetasIngresosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15})
        Me.ConMetasIngresosDataGridView.DataSource = Me.ConMetasIngresosBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConMetasIngresosDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConMetasIngresosDataGridView.Location = New System.Drawing.Point(3, 28)
        Me.ConMetasIngresosDataGridView.Name = "ConMetasIngresosDataGridView"
        Me.ConMetasIngresosDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.ConMetasIngresosDataGridView.Size = New System.Drawing.Size(1005, 185)
        Me.ConMetasIngresosDataGridView.TabIndex = 2
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Concepto"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Concepto"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Ene"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Ene"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 90
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Feb"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Feb"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 90
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Mar"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Mar"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 90
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Abr"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Abr"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 90
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "May"
        Me.DataGridViewTextBoxColumn7.HeaderText = "May"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 90
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Jun"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Jun"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 90
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Jul"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Jul"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 90
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Ago"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Ago"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 90
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Sep"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Sep"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Width = 90
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Oct"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Oct"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Width = 90
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Nov"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Nov"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Width = 90
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Dic"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Dic"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Width = 90
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "Anio"
        Me.DataGridViewTextBoxColumn15.HeaderText = "Anio"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'ConMetasIngresosBindingSource
        '
        Me.ConMetasIngresosBindingSource.DataMember = "ConMetasIngresos"
        Me.ConMetasIngresosBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConMetasIngresosTableAdapter
        '
        Me.ConMetasIngresosTableAdapter.ClearBeforeFill = True
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasIngresosBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(920, 329)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipSerTextBox.TabIndex = 4
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'AnioTextBox
        '
        Me.AnioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasIngresosBindingSource, "Anio", True))
        Me.AnioTextBox.Location = New System.Drawing.Point(936, 329)
        Me.AnioTextBox.Name = "AnioTextBox"
        Me.AnioTextBox.ReadOnly = True
        Me.AnioTextBox.Size = New System.Drawing.Size(10, 20)
        Me.AnioTextBox.TabIndex = 6
        Me.AnioTextBox.TabStop = False
        '
        'MuestraAniosBindingSource
        '
        Me.MuestraAniosBindingSource.DataMember = "MuestraAnios"
        Me.MuestraAniosBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraAniosTableAdapter
        '
        Me.MuestraAniosTableAdapter.ClearBeforeFill = True
        '
        'AnioComboBox
        '
        Me.AnioComboBox.DataSource = Me.MuestraAniosBindingSource
        Me.AnioComboBox.DisplayMember = "Anio"
        Me.AnioComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnioComboBox.FormattingEnabled = True
        Me.AnioComboBox.Location = New System.Drawing.Point(428, 28)
        Me.AnioComboBox.Name = "AnioComboBox"
        Me.AnioComboBox.Size = New System.Drawing.Size(121, 23)
        Me.AnioComboBox.TabIndex = 7
        Me.AnioComboBox.ValueMember = "Anio"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ConMetasIngresosBindingNavigator)
        Me.Panel1.Controls.Add(Me.ConMetasIngresosDataGridView)
        Me.Panel1.Location = New System.Drawing.Point(3, 80)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1011, 216)
        Me.Panel1.TabIndex = 8
        '
        'ConMetasIngresosBindingNavigator
        '
        Me.ConMetasIngresosBindingNavigator.AddNewItem = Nothing
        Me.ConMetasIngresosBindingNavigator.BindingSource = Me.ConMetasIngresosBindingSource
        Me.ConMetasIngresosBindingNavigator.CountItem = Nothing
        Me.ConMetasIngresosBindingNavigator.DeleteItem = Nothing
        Me.ConMetasIngresosBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConMetasIngresosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConMetasIngresosBindingNavigatorSaveItem})
        Me.ConMetasIngresosBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConMetasIngresosBindingNavigator.MoveFirstItem = Nothing
        Me.ConMetasIngresosBindingNavigator.MoveLastItem = Nothing
        Me.ConMetasIngresosBindingNavigator.MoveNextItem = Nothing
        Me.ConMetasIngresosBindingNavigator.MovePreviousItem = Nothing
        Me.ConMetasIngresosBindingNavigator.Name = "ConMetasIngresosBindingNavigator"
        Me.ConMetasIngresosBindingNavigator.PositionItem = Nothing
        Me.ConMetasIngresosBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConMetasIngresosBindingNavigator.Size = New System.Drawing.Size(1011, 25)
        Me.ConMetasIngresosBindingNavigator.TabIndex = 3
        Me.ConMetasIngresosBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(88, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConMetasIngresosBindingNavigatorSaveItem
        '
        Me.ConMetasIngresosBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConMetasIngresosBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConMetasIngresosBindingNavigatorSaveItem.Name = "ConMetasIngresosBindingNavigatorSaveItem"
        Me.ConMetasIngresosBindingNavigatorSaveItem.Size = New System.Drawing.Size(88, 22)
        Me.ConMetasIngresosBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(868, 318)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmMetasIngresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 366)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(AnioLabel)
        Me.Controls.Add(Me.AnioComboBox)
        Me.Controls.Add(Me.AnioTextBox)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Name = "FrmMetasIngresos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Metas de Ingresos"
        CType(Me.ConMetasIngresosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMetasIngresosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraAniosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConMetasIngresosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMetasIngresosBindingNavigator.ResumeLayout(False)
        Me.ConMetasIngresosBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConMetasIngresosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMetasIngresosTableAdapter As sofTV.DataSetEric2TableAdapters.ConMetasIngresosTableAdapter
    Friend WithEvents ConMetasIngresosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AnioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MuestraAniosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraAniosTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraAniosTableAdapter
    Friend WithEvents AnioComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConMetasIngresosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMetasIngresosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
