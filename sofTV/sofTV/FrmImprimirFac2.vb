Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class FrmImprimirFac2
    Private customersByCityReport As ReportDocument

    Private op As String = Nothing
    Private Titulo As String = Nothing
    'Private Const PARAMETER_FIELD_NAME As String = "Op"

    'Direccion Sucursal
    Dim RCalleSucur As String = Nothing
    Dim RNumSucur As String = Nothing
    Dim RColSucur As String = Nothing
    Dim RMuniSucur As String = Nothing
    Dim RCiudadSucur As String = Nothing
    Dim RCPSucur As String = Nothing
    Dim RTelSucur As String = Nothing

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("ReportesFacturas", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim reportPath As String = Nothing


        If IdSistema = "TO" Then
            reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        ElseIf IdSistema = "VA" Then
            reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        Else
            reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        End If

        'customersByCityReport.Load(reportPath)
        ''If IdSistema <> "TO" Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")


        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = GloClv_Factura
        cmd.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_Factura_Ini", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = 0
        cmd.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Clv_Factura_Fin", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = 0
        cmd.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = "01/01/1900"
        cmd.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = "01/01/1900"
        cmd.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@op", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = 0
        cmd.Parameters.Add(parametro5)

        Dim da As New SqlDataAdapter(cmd)

        Dim ds As New DataSet()


        da.Fill(ds)
        ds.Tables(0).TableName = "ReportesFacturas"
        ds.Tables(1).TableName = "CALLES"
        ds.Tables(2).TableName = "CatalogoCajas"
        ds.Tables(3).TableName = "CIUDADES"
        ds.Tables(4).TableName = "CLIENTES"
        ds.Tables(5).TableName = "COLONIAS"
        ds.Tables(6).TableName = "DatosFiscales"
        ds.Tables(7).TableName = "DetFacturas"
        ds.Tables(8).TableName = "DetFacturasImpuestos"
        ds.Tables(9).TableName = "Facturas"
        ds.Tables(10).TableName = "GeneralDesconexion"
        ds.Tables(11).TableName = "SUCURSALES"
        ds.Tables(12).TableName = "Usuarios"

        DamePerido(Contrato)
        If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
            GloFechaPeridoPagado = "5"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
            GloFechaPeridoPagado = "10"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
            GloFechaPeridoPagado = "15"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
            GloFechaPeridoPagado = "20"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
            GloFechaPeridoPagado = "25"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
            GloFechaPeridoPagado = "1"
        ElseIf GloFechaPeridoPagado = "Periodo : " Then
            GloFechaPeridoPagado = " "
        End If

        consultaDatosGeneralesSucursal(0, GloClv_Factura)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"

        customersByCityReport.DataDefinition.FormulaFields("DireccionSucursal").Text = "'" & RCalleSucur & " - #" & RNumSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpSucursal").Text = "'" & RColSucur & ", C.P." & RCPSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadSucursal").Text = "'" & RCiudadSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoSucursal").Text = "'" & RTelSucur & "'"

        customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
        'customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
        'customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
        customersByCityReport.DataDefinition.FormulaFields("PagoProximo").Text = "'" & GloFechaProximoPago & "'"


        CrystalReportViewer1.ReportSource = customersByCityReport

        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub
    Private Sub DamePerido(ByVal contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Sp_InformacionTicket", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = GloClv_Factura
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                GloFechaPeridoPagado = reader(0).ToString()
                GloFechaPeriodoPagadoMes = reader(1).ToString()
                GloFechaPeriodoFinal = reader(2).ToString()
                GloFechaProximoPago = reader(3).ToString()
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub OpenSubreport(ByVal reportObjectName As String)

        ' Preview the subreport.

    End Sub

    Private Sub ConfigureCrystalReports1(ByVal No_Contrato As Integer)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Tarjetas.rpt"
        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@No_Contrato 
        customersByCityReport.SetParameterValue(0, No_Contrato)
        mySelectFormula = "*" & No_Contrato & "*"
        customersByCityReport.DataDefinition.FormulaFields("codigo").Text = "'" & mySelectFormula & "'"
        customersByCityReport.PrintOptions.PrinterName = "Datacard Printer"
        customersByCityReport.PrintToPrinter(1, True, 1, 1)

        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.PrintReport()
        customersByCityReport = Nothing
    End Sub

    'Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
    '    'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '    For Each myTable In myTables
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '        myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '    Next
    'End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsPaquetes()
        Dim CON As New SqlConnection(MiConexion)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim Servicios As String = Nothing
        Dim Titulo As String = Nothing
        Dim Subtitulo As String = Nothing
        Dim Paquetes As String = Nothing
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReportePaquetes.rpt"
        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@conectado", SqlDbType.Bit, LocBndC)
        BaseII.CreateMyParameter("@baja", SqlDbType.Bit, LocBndB)
        BaseII.CreateMyParameter("@Insta", SqlDbType.Bit, LocBndI)
        BaseII.CreateMyParameter("@Desconect", SqlDbType.Bit, LocBndD)
        BaseII.CreateMyParameter("@Susp", SqlDbType.Bit, LocBndS)
        BaseII.CreateMyParameter("@Fuera", SqlDbType.Bit, LocBndF)
        BaseII.CreateMyParameter("@tempo", SqlDbType.Bit, LocBndDT)
        BaseII.CreateMyParameter("@habilita", SqlDbType.Int, LocValidaHab)
        BaseII.CreateMyParameter("@periodo1", SqlDbType.Bit, LocPeriodo1)
        BaseII.CreateMyParameter("@periodo2", SqlDbType.Bit, LocPeriodo2)

        Dim listatablas As New List(Of String)

        listatablas.Add("Reporte_Mezcla1")
        listatablas.Add("CALLES")
        listatablas.Add("COLONIAS")

        DS = BaseII.ConsultaDS("Reporte_Mezcla1", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)
        'customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@clv_session,, , , ,, ,, , , 
        'customersByCityReport.SetParameterValue(0, LocClv_session)
        '',@contratado bit
        'customersByCityReport.SetParameterValue(1, LocBndC)
        '',@Baja bit
        'customersByCityReport.SetParameterValue(2, LocBndB)
        '',@Insta bit
        'customersByCityReport.SetParameterValue(3, LocBndI)
        ''@Desconect bit
        'customersByCityReport.SetParameterValue(4, LocBndD)
        ''@Suspendido bit
        'customersByCityReport.SetParameterValue(5, LocBndS)
        ''@Fuera bit
        'customersByCityReport.SetParameterValue(6, LocBndF)
        '' @tempo
        'customersByCityReport.SetParameterValue(7, LocBndDT)
        ''@habilita
        'customersByCityReport.SetParameterValue(8, LocValidaHab)
        ''@periodo1
        'customersByCityReport.SetParameterValue(9, LocPeriodo1)
        ''@periodo2
        'customersByCityReport.SetParameterValue(10, LocPeriodo2)
        CON.Open()
        Me.Dame_Servicios_SeleccionadosTableAdapter.Connection = CON
        Me.Dame_Servicios_SeleccionadosTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_Servicios_Seleccionados, LocClv_session, Servicios, Paquetes)
        CON.Close()



        Titulo = "Resumen de Clientes con Servicio(s): " + Servicios
        Subtitulo = " Y con Paquetes: " + Paquetes


        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Subtitulo & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport

        customersByCityReport = Nothing
    End Sub

    Private Sub FrmImprimirFac_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If LocGloOpRep = 29 Then
            Me.Show()
            Exit Sub
        End If
    End Sub

    Private Sub FrmImprimirFac_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If LocGloOpRep = 0 Then
            If GloClv_Factura = 0 Then Me.Opacity = 0 Else Me.Opacity = 1
            ConfigureCrystalReports(GloClv_Factura)
        ElseIf LocGloOpRep = 1 Then
            'ConfigureCrystalReports1(LocTarjNo_Contrato)
        ElseIf LocGloOpRep = 5 Then
            ConfigureCrystalReportsDesPagAde()
        ElseIf LocGloOpRep = 2 Then
            Me.Text = "Reporte de Paquetes"
            ConfigureCrystalReportsPaquetes()
        ElseIf LocGloOpRep = 3 Then
            Me.Text = "Nota de Cr�dito"
            ConfigureCrystalReportsNota(gloClvNota)
        ElseIf LocGloOpRep = 6 Then
            ConfigureCrystalReports_tickets2(GloClv_Factura)
        ElseIf LocGloOpRep = 20 Then

            ConfigureCrystalReportsSeriesFioliosXml(GloSeries)
        ElseIf LocGloOpRep = 21 Then
            ConfigureCrystalReportsReimpresionFoliosXml(GloSeries)
        ElseIf LocGloOpRep = 27 Then
            ReporteClientesCablemodem()
        ElseIf LocGloOpRep = 28 Then
            ResumenEjecutivo()
        ElseIf LocGloOpRep = 29 Then
            ResumenEjecutivoCartera()
        End If

    End Sub

    Private Sub ResumenEjecutivo()
        customersByCityReport = New ReportDocument
        Dim reportPath As String = Nothing
        'Titulo = "Tipo Cablemodems " '& eServicio
        'Me.Text = "Tipo Cablemodems"

        Titulo = "Resumen Ejecutivo" '& eServicio
        Me.Text = "Resumen Ejecutivo"
        reportPath = RutaReportes + "\ReporteEjecutivoNuevo.rpt"

        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@fecha", SqlDbType.Date, eFechaIni)

        Dim listatablas As New List(Of String)

        'listatablas.Add("Tbl_DatosCartera")
        'listatablas.Add("Tbl_Ingresos")
        'listatablas.Add("Tbl_Ingresos_Dig")
        'listatablas.Add("Tbl_OrdenesQuejas")
        'listatablas.Add("Tbl_totalIngresoEje")

        listatablas.Add("Tbl_DatosCarteraNuevo")
        listatablas.Add("Tbl_DatosCarteraNuevoDesco")
        listatablas.Add("Tbl_DatosCarteraNuevoRecon")
        listatablas.Add("Tbl_DatosCarteraNuevoBajas")
        listatablas.Add("Tbl_IngresosNuevo")
        listatablas.Add("Tbl_Ingresos_DigNuevo")
        listatablas.Add("Tbl_OrdenesQuejasNuevo")
        listatablas.Add("Tbl_totalIngresoEjeNuevo")
        listatablas.Add("Tbl_ContratacionesNuevo")
        listatablas.Add("tbl_AbonadosStb")


        DS = BaseII.ConsultaDS("Usp_ResumenEjecutivoReporteNuevo", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaIni & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)

        customersByCityReport = Nothing



    End Sub

    Private Sub ResumenEjecutivoCartera()
        customersByCityReport = New ReportDocument
        Dim reportPath As String = Nothing
        'Titulo = "Tipo Cablemodems " '& eServicio
        'Me.Text = "Tipo Cablemodems"

        Titulo = "Resumen Ejecutivo" '& eServicio
        Me.Text = "Resumen Ejecutivo"
        reportPath = RutaReportes + "\ReporteEjecutivoNuevoCartera.rpt"

        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@fecha", SqlDbType.Date, eFechaIni)

        Dim listatablas As New List(Of String)

        'listatablas.Add("Tbl_DatosCartera")
        'listatablas.Add("Tbl_Ingresos")
        'listatablas.Add("Tbl_Ingresos_Dig")
        'listatablas.Add("Tbl_OrdenesQuejas")
        'listatablas.Add("Tbl_totalIngresoEje")

        listatablas.Add("Tbl_DatosCarteraNuevo")
        listatablas.Add("Tbl_DatosCarteraNuevoDesco")
        listatablas.Add("Tbl_DatosCarteraNuevoRecon")
        listatablas.Add("Tbl_DatosCarteraNuevoBajas")
        listatablas.Add("Tbl_IngresosNuevo")
        listatablas.Add("Tbl_Ingresos_DigNuevo")
        listatablas.Add("Tbl_Ingresos_IntNuevo")
        listatablas.Add("Tbl_OrdenesQuejasNuevo")
        listatablas.Add("Tbl_totalIngresoEjeNuevo")
        listatablas.Add("Tbl_ContratacionesNuevo")
        listatablas.Add("tbl_AbonadosStb")


        DS = BaseII.ConsultaDS("Usp_ResumenEjecutivoReporteNuevo", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaIni & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)

        customersByCityReport = Nothing



    End Sub

    Private Sub ReporteClientesCablemodem()
        customersByCityReport = New ReportDocument
        Dim reportPath As String = Nothing
        'Titulo = "Tipo Cablemodems " '& eServicio
        'Me.Text = "Tipo Cablemodems"

        If GloTipoCablemodemR = 1 Then
            Titulo = "Stb Asignados" '& eServicio
            Me.Text = "Stb Asignados"
            reportPath = RutaReportes + "\ReporteClientesStbAsignados.rpt"

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, CShort(GloTipoCablemodemR))

            Dim listatablas As New List(Of String)

            listatablas.Add("ReporteClientesStbTarjetas")

            DS = BaseII.ConsultaDS("ReporteClientesStbTarjetas", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        ElseIf GloTipoCablemodemR = 2 Or GloTipoCablemodemR = 3 Then
            Titulo = "Stb Asignados" '& eServicio
            Me.Text = "Stb Asignados"
            reportPath = RutaReportes + "\ReporteClientesStbSinAsignar.rpt"

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, CShort(GloTipoCablemodemR))

            Dim listatablas As New List(Of String)

            listatablas.Add("ReporteClientesStbTarjetas")

            DS = BaseII.ConsultaDS("ReporteClientesStbTarjetas", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing
        End If

    End Sub

    Private Sub ConfigureCrystalReportsDesPagAde()
        customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing
        Ciudades = " Ciudad(es): " + LocCiudades
        reportPath = RutaReportes + "\ClientesconPagosAdelantados.rpt"
        Titulo = "Relaci�n de Clientes con Pagos Adelantados"

        Sucursal = " Sucursal: " + GloSucursal
        'customersByCityReport.Load(reportPath)

        ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        ''SetDBLogonForReport(connectionInfo, customersByCityReport)
        ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)

        ''@FECHA_INI
        'customersByCityReport.SetParameterValue(0, GloFecha_Ini)
        ''@FECHA_FIN
        'customersByCityReport.SetParameterValue(1, GloFecha_Ini)
        ''@TIPO
        'customersByCityReport.SetParameterValue(2, "")
        ''@SUCURSAL
        'customersByCityReport.SetParameterValue(3, "0")
        ''@CAJA
        'customersByCityReport.SetParameterValue(4, "0")
        ''@CAJERA
        'customersByCityReport.SetParameterValue(5, "")
        ''@OP
        'customersByCityReport.SetParameterValue(6, "1")
        ''Clv_Session
        'customersByCityReport.SetParameterValue(7, glosessioncar)

        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, CObj(GloFecha_Ini))
        BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, CObj(GloFecha_Ini))
        BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@sucursal", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Caja", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Cajera", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(glosessioncar))

        Dim listatablas As New List(Of String)
        listatablas.Add("DesglocepagosAdelantados")
        listatablas.Add("Servicios")
        DS = BaseII.ConsultaDS("DesglocepagosAdelantados", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)


        Dim eFechaTitulo As String = Nothing
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        eFechaTitulo = "de la Fecha " & GloFecha_Ini
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)

        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing

    End Sub

    Private Sub ConfigureCrystalReportsNota(ByVal nota As Long)
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        Dim ba As Boolean

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"

        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True

        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport1(connectionInfo, customersByCityReport)
        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, nota)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        End If

        'If (IdSistema = "TO" Or IdSistema = "SA") Then 'And facnormal = True And identi > 0 
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else
        CrystalReportViewer1.ReportSource = customersByCityReport
        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        ' End If

        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub
    Private Sub SetDBLogonForReport1(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub ConfigureCrystalReports_tickets2(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        Select Case IdSistema
            Case "LO"
                customersByCityReport = New ReporteCajasTickets_2Log
            Case "YU"
                customersByCityReport = New ReporteCajasTickets_2Log
        End Select
        Dim connectionInfo As New ConnectionInfo
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        customersByCityReport.SetParameterValue(0, Clv_Factura)
        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowExportButton = True
        CrystalReportViewer1.ShowPrintButton = True
        CrystalReportViewer1.ShowRefreshButton = True

    End Sub



    Private Sub ConfigureCrystalReportsSeriesFioliosXml(ByVal eSerie As String)
        Try
            customersByCityReport = New ReportDocument

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\Reporte_Series_Folios.rpt"

            Dim cnn As New SqlConnection(MiConexion)

            Dim cmd As New SqlCommand("Reporte_Series_Folios", cnn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim parametro As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = eSerie
            cmd.Parameters.Add(parametro)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            Dim data1 As New DataTable()

            da.Fill(data1)

            data1.TableName = "Tbl_SerieFolios"

            ds.Tables.Add(data1)

            customersByCityReport.Load(reportPath)
            SetDBReport(ds, customersByCityReport)


            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Serie", System.Data.SqlDbType.VarChar, eSerie, 5)
            BaseII.CreateMyParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Output, "")
            BaseII.CreateMyParameter("@GrupoVenta", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Output, "")
            Dim diccionario As New Dictionary(Of String, Object)
            diccionario = BaseII.ProcedimientoOutPut("uspObtenNombreVendedorGrupoVenta")

            customersByCityReport.SetParameterValue("Nombre", diccionario("@Nombre").ToString())
            customersByCityReport.SetParameterValue("Grupo", diccionario("@GrupoVenta").ToString())
            CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.Zoom(75)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsReimpresionFoliosXml(ByVal eSerie As String)
        Try
            customersByCityReport = New ReportDocument

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\Reporte_Reimpresion_Folios.rpt"

            Dim cnn As New SqlConnection(MiConexion)

            Dim cmd As New SqlCommand("Reporte_Reimpresion_Folios", cnn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim parametro As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = eSerie
            cmd.Parameters.Add(parametro)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            Dim data1 As New DataTable()

            da.Fill(data1)

            data1.TableName = "Tbl_ReimpresionFolio"

            ds.Tables.Add(data1)

            customersByCityReport.Load(reportPath)
            SetDBReport(ds, customersByCityReport)
            CrystalReportViewer1.ReportSource = customersByCityReport

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Serie", System.Data.SqlDbType.VarChar, eSerie, 5)
            BaseII.CreateMyParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Output, "")
            BaseII.CreateMyParameter("@GrupoVenta", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Output, "")
            Dim diccionario As New Dictionary(Of String, Object)
            diccionario = BaseII.ProcedimientoOutPut("uspObtenNombreVendedorGrupoVenta")

            customersByCityReport.SetParameterValue("Nombre", diccionario("@Nombre").ToString())
            customersByCityReport.SetParameterValue("Grupo", diccionario("@GrupoVenta").ToString())
            CrystalReportViewer1.ReportSource = customersByCityReport


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.Zoom(75)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            Me.RCalleSucur = dtDatosGenerales.Rows(0)("calle").ToString
            Me.RNumSucur = dtDatosGenerales.Rows(0)("numero").ToString
            Me.RColSucur = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.RCPSucur = CInt(dtDatosGenerales.Rows(0)("cp").ToString)
            Me.RMuniSucur = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.RCiudadSucur = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.RTelSucur = dtDatosGenerales.Rows(0)("telefono").ToString
        End If
    End Sub


End Class