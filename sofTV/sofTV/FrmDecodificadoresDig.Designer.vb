﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDecodificadoresDig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel74 = New System.Windows.Forms.Label()
        Me.CMBLabel73 = New System.Windows.Forms.Label()
        Me.txtDescripcionDec = New System.Windows.Forms.TextBox()
        Me.comboServicioDec = New System.Windows.Forms.ComboBox()
        Me.dgvDec = New System.Windows.Forms.DataGridView()
        Me.IdDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_ServicioDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServicioDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.txtEliminarDec = New System.Windows.Forms.Button()
        Me.txtAgregarDec = New System.Windows.Forms.Button()
        CType(Me.dgvDec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel74
        '
        Me.CMBLabel74.AutoSize = True
        Me.CMBLabel74.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel74.Location = New System.Drawing.Point(91, 68)
        Me.CMBLabel74.Name = "CMBLabel74"
        Me.CMBLabel74.Size = New System.Drawing.Size(62, 15)
        Me.CMBLabel74.TabIndex = 31
        Me.CMBLabel74.Text = "Servicio:"
        '
        'CMBLabel73
        '
        Me.CMBLabel73.AutoSize = True
        Me.CMBLabel73.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel73.Location = New System.Drawing.Point(53, 39)
        Me.CMBLabel73.Name = "CMBLabel73"
        Me.CMBLabel73.Size = New System.Drawing.Size(100, 15)
        Me.CMBLabel73.TabIndex = 30
        Me.CMBLabel73.Text = "Decodificador:"
        '
        'txtDescripcionDec
        '
        Me.txtDescripcionDec.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcionDec.Location = New System.Drawing.Point(159, 33)
        Me.txtDescripcionDec.Name = "txtDescripcionDec"
        Me.txtDescripcionDec.Size = New System.Drawing.Size(319, 21)
        Me.txtDescripcionDec.TabIndex = 29
        '
        'comboServicioDec
        '
        Me.comboServicioDec.DisplayMember = "Descripcion"
        Me.comboServicioDec.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboServicioDec.FormattingEnabled = True
        Me.comboServicioDec.Location = New System.Drawing.Point(159, 60)
        Me.comboServicioDec.Name = "comboServicioDec"
        Me.comboServicioDec.Size = New System.Drawing.Size(319, 23)
        Me.comboServicioDec.TabIndex = 28
        Me.comboServicioDec.ValueMember = "Clv_Servicio"
        '
        'dgvDec
        '
        Me.dgvDec.AllowUserToAddRows = False
        Me.dgvDec.AllowUserToDeleteRows = False
        Me.dgvDec.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDec.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDec.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDec, Me.DescripcionDec, Me.Clv_ServicioDec, Me.ServicioDec})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDec.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvDec.Location = New System.Drawing.Point(56, 118)
        Me.dgvDec.Name = "dgvDec"
        Me.dgvDec.ReadOnly = True
        Me.dgvDec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDec.Size = New System.Drawing.Size(448, 320)
        Me.dgvDec.TabIndex = 32
        '
        'IdDec
        '
        Me.IdDec.DataPropertyName = "Id"
        Me.IdDec.HeaderText = "Id"
        Me.IdDec.Name = "IdDec"
        Me.IdDec.ReadOnly = True
        Me.IdDec.Visible = False
        '
        'DescripcionDec
        '
        Me.DescripcionDec.DataPropertyName = "Descripcion"
        Me.DescripcionDec.HeaderText = "Decodificador"
        Me.DescripcionDec.Name = "DescripcionDec"
        Me.DescripcionDec.ReadOnly = True
        Me.DescripcionDec.Width = 200
        '
        'Clv_ServicioDec
        '
        Me.Clv_ServicioDec.DataPropertyName = "Clv_Servicio"
        Me.Clv_ServicioDec.HeaderText = "Clv_Servicio"
        Me.Clv_ServicioDec.Name = "Clv_ServicioDec"
        Me.Clv_ServicioDec.ReadOnly = True
        Me.Clv_ServicioDec.Visible = False
        '
        'ServicioDec
        '
        Me.ServicioDec.DataPropertyName = "Servicio"
        Me.ServicioDec.HeaderText = "Servicio"
        Me.ServicioDec.Name = "ServicioDec"
        Me.ServicioDec.ReadOnly = True
        Me.ServicioDec.Width = 200
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(467, 465)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 33
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'txtEliminarDec
        '
        Me.txtEliminarDec.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEliminarDec.Location = New System.Drawing.Point(527, 118)
        Me.txtEliminarDec.Name = "txtEliminarDec"
        Me.txtEliminarDec.Size = New System.Drawing.Size(75, 23)
        Me.txtEliminarDec.TabIndex = 35
        Me.txtEliminarDec.Text = "&Eliminar"
        Me.txtEliminarDec.UseVisualStyleBackColor = True
        '
        'txtAgregarDec
        '
        Me.txtAgregarDec.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgregarDec.Location = New System.Drawing.Point(527, 60)
        Me.txtAgregarDec.Name = "txtAgregarDec"
        Me.txtAgregarDec.Size = New System.Drawing.Size(75, 23)
        Me.txtAgregarDec.TabIndex = 34
        Me.txtAgregarDec.Text = "&Agregar"
        Me.txtAgregarDec.UseVisualStyleBackColor = True
        '
        'FrmDecodificadoresDig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(615, 513)
        Me.Controls.Add(Me.txtEliminarDec)
        Me.Controls.Add(Me.txtAgregarDec)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.dgvDec)
        Me.Controls.Add(Me.CMBLabel74)
        Me.Controls.Add(Me.CMBLabel73)
        Me.Controls.Add(Me.txtDescripcionDec)
        Me.Controls.Add(Me.comboServicioDec)
        Me.Name = "FrmDecodificadoresDig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Decodificadores"
        CType(Me.dgvDec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel74 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel73 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcionDec As System.Windows.Forms.TextBox
    Friend WithEvents comboServicioDec As System.Windows.Forms.ComboBox
    Friend WithEvents dgvDec As System.Windows.Forms.DataGridView
    Friend WithEvents IdDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_ServicioDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ServicioDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtEliminarDec As System.Windows.Forms.Button
    Friend WithEvents txtAgregarDec As System.Windows.Forms.Button
End Class
