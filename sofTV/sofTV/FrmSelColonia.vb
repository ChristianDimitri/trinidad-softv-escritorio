Imports System.Data.SqlClient
Public Class FrmSelColonia

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            GlobndClv_Colonia = True
            GloNumClv_Colonia = Me.ComboBox1.SelectedValue
            GLONOMCOLONIA = Me.ComboBox1.Text
            Me.Close()
        Else
            MsgBox("Seleccione el Barrio")
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmSelColonia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.MUESTRACOLONIASTableAdapter.Connection = CON
        Me.MUESTRACOLONIASTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRACOLONIAS, 0)
        CON.Close()
    End Sub
End Class