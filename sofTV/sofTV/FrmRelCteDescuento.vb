Imports System.Data.SqlClient
Public Class FrmRelCteDescuento

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmRelCteDescuento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        BuscarRelCteDescuento()
        If OpcionCli = "N" Then
            Me.BindingNavigatorDeleteItem.Enabled = False
        End If
        If OpcionCli = "C" Then
            Me.ConRelCteDescuentoBindingNavigator.Enabled = False
            Me.DescuentoNumericUpDown.Enabled = False
        End If
        If OpcionCli = "N" Or OpcionCli = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub ConRelCteDescuentoBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConRelCteDescuentoBindingNavigatorSaveItem.Click
        Try
            If Me.DescuentoNumericUpDown.Value > 0 Then
                GuardarRelCteDescuento()
                MsgBox(mensaje5)
                eBndDesc = True
                Me.Close()
            Else
                MsgBox("Esa cifra es Incorrecta.", , "Error")
            End If
        Catch
            MsgBox("Se ha Producido un Error.", , "Atenci�n")
        End Try
    End Sub

    Private Sub BuscarRelCteDescuento()
        Dim CON As New SqlConnection(MiConexion)

        If eTipSer = 1 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, eContrato, 1)
            CON.Close()
        End If

        If eTipSer = 2 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, eClv_UnicaNet, 2)
            CON.Close()
        End If

        If eTipSer = 3 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, eClv_UnicaNetDig, 3)
            CON.Close()
        End If
        If eTipSer = 5 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, Clv_UnicaTel, 5)
            CON.Close()
        End If
    End Sub

    Private Sub GuardarRelCteDescuento()
        Dim CON As New SqlConnection(MiConexion)
        If eTipSer = 1 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Insert(eContrato, 1, Me.DescuentoNumericUpDown.Value)
            CON.Close()
        End If

        If eTipSer = 2 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Insert(eClv_UnicaNet, 2, Me.DescuentoNumericUpDown.Value)
            CON.Close()
        End If

        If eTipSer = 3 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Insert(eClv_UnicaNetDig, 3, Me.DescuentoNumericUpDown.Value)
            CON.Close()
        End If

        If eTipSer = 5 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Insert(Clv_UnicaTel, 5, Me.DescuentoNumericUpDown.Value)
            CON.Close()
        End If
    End Sub

    Private Sub EliminarRelCteDescuento()
        Dim CON As New SqlConnection(MiConexion)
        If eTipSer = 1 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Delete(eContrato, 1)
            CON.Close()
        End If

        If eTipSer = 2 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Delete(eClv_UnicaNet, 2)
            CON.Close()
        End If

        If eTipSer = 3 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Delete(eClv_UnicaNetDig, 3)
            CON.Close()
        End If

        If eTipSer = 5 Then
            CON.Open()
            Me.ConRelCteDescuentoTableAdapter.Connection = CON
            Me.ConRelCteDescuentoTableAdapter.Delete(Clv_UnicaTel, 5)
            CON.Close()
        End If
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            EliminarRelCteDescuento()
            MsgBox(mensaje6)
            eBndDesc = True
            Me.Close()
        Catch
            MsgBox("Se ha Producido un Error.", , "Atenci�n")
        End Try
    End Sub

    
End Class