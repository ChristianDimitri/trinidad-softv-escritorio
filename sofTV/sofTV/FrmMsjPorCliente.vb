Imports System.Data.SqlClient
Public Class FrmMsjPorCliente
    Dim mensaje As String
    Dim respuesta As Integer = 0
    Private Sub FrmMsjPorCliente_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eContrato > 0 Then
            Dim CONE As New SqlConnection(MiConexion)
            Me.TextBox1.Text = eContrato
            CONE.Open()
            Me.DameClientesActivosTableAdapter.Connection = CONE
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetLidia.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            Me.MuestraServCteResetTableAdapter.Connection = CONE
            Me.MuestraServCteResetTableAdapter.Fill(Me.DataSetLidia.MuestraServCteReset, eContrato, 3, respuesta, mensaje)
            CONE.Close()
            eContrato = 0
            If respuesta = 1 Then
                MsgBox(mensaje)
            End If
        End If
    End Sub

    Private Sub FrmMsjPorCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eContrato = 0
        BrwSelContrato.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim cone1 As New SqlClient.SqlConnection(MiConexion)
        If Me.TextBox2.Text.Trim.Length = 0 Then
            MsgBox("Capture un Mensaje", MsgBoxStyle.Information, "Atenci�n")
            Exit Sub
        ElseIf Me.TextBox1.Text.Length = 0 Then
            MsgBox("Favor de Escoger un Cliente", MsgBoxStyle.Information, "Atenci�n")
        Else
            'cone1.Open()
            'Me.Mensaje_Por_ClienteTableAdapter.Connection = cone1
            'Me.Mensaje_Por_ClienteTableAdapter.Fill(Me.DataSetLidia.Mensaje_Por_Cliente, Me.TextBox1.Text, Me.TextBox2.Text)
            'cone1.Close()

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Me.TextBox1.Text)
            BaseII.CreateMyParameter("@mensaje", SqlDbType.VarChar, Me.TextBox2.Text)
            BaseII.CreateMyParameter("@MacSelec", SqlDbType.VarChar, Convert.ToString(MuestraServCteResetDataGridView.CurrentRow.Cells(1).Value))
            BaseII.Inserta("Mensaje_Por_Cliente")

            MsgBox("El Mensaje Ha Sido Enviado con �xito", MsgBoxStyle.Information, "Proceso Terminado con �xito")
            bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, Me.Name, "Se Mando Un Mensaje A Un Cliente", "", Me.TextBox2.Text, LocClv_Ciudad)
            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyValue = Keys.Enter Then
            If TextBox1.TextLength > 0 Then
                Dim CONE As New SqlConnection(MiConexion)
                CONE.Open()
                Me.DameClientesActivosTableAdapter.Connection = CONE
                Me.DameClientesActivosTableAdapter.Fill(Me.DataSetLidia.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
                Me.MuestraServCteResetTableAdapter.Connection = CONE
                Me.MuestraServCteResetTableAdapter.Fill(Me.DataSetLidia.MuestraServCteReset, CInt(Me.TextBox1.Text), 3, respuesta, mensaje)
                CONE.Close()
                If respuesta = 1 Then
                    MsgBox(mensaje)
                End If
            End If
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class