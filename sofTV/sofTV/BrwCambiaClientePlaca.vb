﻿Public Class BrwCambiaClientePlaca

    Dim ClientePlaca As New ClassCambioClientePlaca

    Private Sub btnBuscarPlaca_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarPlaca.Click
        If Me.txtPlaca.Text.Length = 0 Then
            MsgBox("¡Capture un Identificador Válido!")
            Exit Sub
        End If

        llenaGridView(Me.txtPlaca.Text)
    End Sub

    Private Sub llenaGridView(ByVal prmPlaca As String)
        Me.dgvContratosPlacas.DataSource = ClientePlaca.uspBuscaClientePlaca(prmPlaca)
    End Sub

    Private Sub dgvContratosPlacas_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvContratosPlacas.CellClick
        If IsNumeric(dgvContratosPlacas.SelectedCells(0).Value) = True Then
            If dgvContratosPlacas.SelectedCells(3).Value = "Modificar" Then
                Dim frm As New FrmCambiaClientePlaca
                frm.contratoPlaca = CLng(Me.dgvContratosPlacas.SelectedCells(0).Value)
                frm.ShowDialog()
            End If
        End If
    End Sub

    Private Sub BrwCambiaClientePlaca_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub
End Class