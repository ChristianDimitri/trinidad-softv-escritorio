﻿Imports System.Data.SqlClient
Imports System.Text
Public Class FrmRepPagosDif

    Private Sub MuestraTipServEric(ByVal Clv_TipSer As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraTipServEric " & CStr(Clv_TipSer) & ", " & CStr(Op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbTipSer.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub FrmRepPagosDif_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraTipServEric(0, 0)
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        eTipSer = 0
        eInst = 0
        eTempo = 0
        ePrimero = False
        eSegundo = False

        If IsNumeric(cbTipSer.SelectedValue) = True Then eTipSer = cbTipSer.SelectedValue
        If cbPrimero.Checked = True Then ePrimero = True
        If cbSegundo.Checked = True Then eSegundo = True
        If cbStatusI.Checked = True Then eInst = 1
        If cbStatusT.Checked = True Then eTempo = 1

        If eTipSer <= 0 Then
            MsgBox("Selecciona un Tipo de Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ePrimero = False And eSegundo = False Then
            MsgBox("Marca al menos un Periodo.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If eInst = 0 And eTempo = 0 Then
            MsgBox("Marca al menos un Status.", MsgBoxStyle.Information)
            Exit Sub
        End If

        eTitulo = String.Empty
        eTitulo = "Reporte de Clientes con Pagos Diferidos de Servicio " & cbTipSer.Text & ", del "

        If ePrimero = True Then eTitulo = eTitulo & "Primero "
        If eSegundo = True Then eTitulo = eTitulo & "Segundo "
        eTitulo = eTitulo & "periodo con el status "
        If eInst = True Then eTitulo = eTitulo & "Instalado "
        If eTempo = True Then eTitulo = eTitulo & "Suspensión Temporal"

        eOpVentas = 78
        FrmImprimirComision.Show()


    End Sub


    Private Sub btnSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class