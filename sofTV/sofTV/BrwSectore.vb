Imports System.Data.SqlClient
Public Class BrwSectore

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSectorTableAdapter.Connection = CON
        Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, Me.TextBox1.Text, "", 1)
        CON.Close()

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            Me.ConSectorTableAdapter.Connection = CON
            Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, Me.TextBox1.Text, "", 1)
        End If
        CON.Close()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSectorTableAdapter.Connection = CON
        Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", Me.TextBox2.Text, 2)
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            Me.ConSectorTableAdapter.Connection = CON
            Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", Me.TextBox2.Text, 2)
        End If
        CON.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmSectore.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConSectorDataGridView.RowCount > 0 Then
            eOpcion = "C"
            eClv_Sector = Me.Clv_SectorLabel1.Text
            FrmSectore.Show()
        Else
            MsgBox("Selecciona un Registro a Consultar.", , "Atenci�n")
        End If


    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConSectorDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClv_Sector = Me.Clv_SectorLabel1.Text
            FrmSectore.Show()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub BrwSectore_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSectorTableAdapter.Connection = CON
        Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", "", 0)
        CON.Close()
    End Sub

    Private Sub BrwSectore_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSectorTableAdapter.Connection = CON
        Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", "", 0)
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub
End Class