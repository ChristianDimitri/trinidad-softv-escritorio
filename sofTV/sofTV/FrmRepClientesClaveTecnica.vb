﻿Public Class FrmRepClientesClaveTecnica

    Private Sub MUESTRASectorReporte()
        BaseII.limpiaParametros()
        cbSector.DataSource = BaseII.ConsultaDT("MUESTRASectorReporte")
    End Sub

    Private Sub MUESTRAPosteReporte()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_sector", SqlDbType.Int, CLng(cbSector.SelectedValue))
        cbPoste.DataSource = BaseII.ConsultaDT("MUESTRAPosteReportePorSector")
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If cbSector.Text.Length = 0 Then
            MessageBox.Show("Selecciona Sector.")
            Exit Sub
        End If
        If cbPoste.Text.Length = 0 Then
            MessageBox.Show("Selecciona Tap.")
            Exit Sub
        End If
        eOpVentas = 96
        eClv_Sector = cbSector.SelectedValue
        eIdPoste = cbPoste.SelectedValue
        Dim FrmImpComi As New FrmImprimirComision
        FrmImpComi.ShowDialog()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        eOpVentas = 0
        eClv_Sector = 0
        eIdPoste = 0
        Me.Close()
    End Sub

    Private Sub FrmRepClientesClaveTecnica_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        eOpVentas = 0
        eClv_Sector = 0
        eIdPoste = 0
    End Sub

    Private Sub FrmRepClientesClaveTecnica_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        eClv_Sector = 0
        eIdPoste = 0
        MUESTRASectorReporte()
        MUESTRAPosteReporte()

    End Sub

    Private Sub cbSector_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSector.SelectedIndexChanged
        MUESTRAPosteReporte()
    End Sub
End Class