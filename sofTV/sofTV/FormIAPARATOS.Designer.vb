﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormIAPARATOS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.BtnAceptar = New System.Windows.Forms.Button()
        Me.ComboBoxPorAsignar = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBoxAparatosDisponibles = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblEstadoAparato = New System.Windows.Forms.Label()
        Me.CMBoxEstadoAparato = New System.Windows.Forms.ComboBox()
        Me.ComboBoxTipoAparato = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxWan = New System.Windows.Forms.TextBox()
        Me.LabelWan = New System.Windows.Forms.Label()
        Me.BtnBuscar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(308, 247)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 32
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'BtnAceptar
        '
        Me.BtnAceptar.BackColor = System.Drawing.SystemColors.Control
        Me.BtnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAceptar.ForeColor = System.Drawing.Color.Black
        Me.BtnAceptar.Location = New System.Drawing.Point(166, 247)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(136, 33)
        Me.BtnAceptar.TabIndex = 41
        Me.BtnAceptar.Text = "&ACEPTAR"
        Me.BtnAceptar.UseVisualStyleBackColor = False
        '
        'ComboBoxPorAsignar
        '
        Me.ComboBoxPorAsignar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxPorAsignar.FormattingEnabled = True
        Me.ComboBoxPorAsignar.Location = New System.Drawing.Point(14, 52)
        Me.ComboBoxPorAsignar.Name = "ComboBoxPorAsignar"
        Me.ComboBoxPorAsignar.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxPorAsignar.TabIndex = 40
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(11, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(166, 16)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Aparatos por asignar  :"
        '
        'ComboBoxAparatosDisponibles
        '
        Me.ComboBoxAparatosDisponibles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxAparatosDisponibles.FormattingEnabled = True
        Me.ComboBoxAparatosDisponibles.Location = New System.Drawing.Point(14, 172)
        Me.ComboBoxAparatosDisponibles.Name = "ComboBoxAparatosDisponibles"
        Me.ComboBoxAparatosDisponibles.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxAparatosDisponibles.TabIndex = 43
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(13, 153)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(178, 16)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Seleccione el Aparato   :"
        '
        'LblEstadoAparato
        '
        Me.LblEstadoAparato.AutoSize = True
        Me.LblEstadoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEstadoAparato.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LblEstadoAparato.Location = New System.Drawing.Point(11, 90)
        Me.LblEstadoAparato.Name = "LblEstadoAparato"
        Me.LblEstadoAparato.Size = New System.Drawing.Size(257, 16)
        Me.LblEstadoAparato.TabIndex = 44
        Me.LblEstadoAparato.Text = "Seleccione el Estado del Aparato   :"
        Me.LblEstadoAparato.Visible = False
        '
        'CMBoxEstadoAparato
        '
        Me.CMBoxEstadoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBoxEstadoAparato.FormattingEnabled = True
        Me.CMBoxEstadoAparato.Location = New System.Drawing.Point(274, 87)
        Me.CMBoxEstadoAparato.Name = "CMBoxEstadoAparato"
        Me.CMBoxEstadoAparato.Size = New System.Drawing.Size(170, 24)
        Me.CMBoxEstadoAparato.TabIndex = 45
        '
        'ComboBoxTipoAparato
        '
        Me.ComboBoxTipoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipoAparato.FormattingEnabled = True
        Me.ComboBoxTipoAparato.Location = New System.Drawing.Point(274, 121)
        Me.ComboBoxTipoAparato.Name = "ComboBoxTipoAparato"
        Me.ComboBoxTipoAparato.Size = New System.Drawing.Size(170, 24)
        Me.ComboBoxTipoAparato.TabIndex = 48
        Me.ComboBoxTipoAparato.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(11, 124)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(213, 16)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "Seleccione el tipo de aparato"
        Me.Label2.Visible = False
        '
        'TextBoxWan
        '
        Me.TextBoxWan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxWan.Location = New System.Drawing.Point(275, 205)
        Me.TextBoxWan.Name = "TextBoxWan"
        Me.TextBoxWan.Size = New System.Drawing.Size(170, 22)
        Me.TextBoxWan.TabIndex = 50
        Me.TextBoxWan.Visible = False
        '
        'LabelWan
        '
        Me.LabelWan.AutoSize = True
        Me.LabelWan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelWan.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelWan.Location = New System.Drawing.Point(17, 211)
        Me.LabelWan.Name = "LabelWan"
        Me.LabelWan.Size = New System.Drawing.Size(153, 16)
        Me.LabelWan.TabIndex = 49
        Me.LabelWan.Text = "Dirección MAC WAN:"
        Me.LabelWan.Visible = False
        '
        'BtnBuscar
        '
        Me.BtnBuscar.BackColor = System.Drawing.SystemColors.Control
        Me.BtnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBuscar.ForeColor = System.Drawing.Color.Black
        Me.BtnBuscar.Location = New System.Drawing.Point(24, 247)
        Me.BtnBuscar.Name = "BtnBuscar"
        Me.BtnBuscar.Size = New System.Drawing.Size(136, 33)
        Me.BtnBuscar.TabIndex = 51
        Me.BtnBuscar.Text = "&BUSCAR"
        Me.BtnBuscar.UseVisualStyleBackColor = False
        Me.BtnBuscar.Visible = False
        '
        'FormIAPARATOS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(461, 291)
        Me.Controls.Add(Me.BtnBuscar)
        Me.Controls.Add(Me.TextBoxWan)
        Me.Controls.Add(Me.LabelWan)
        Me.Controls.Add(Me.ComboBoxTipoAparato)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CMBoxEstadoAparato)
        Me.Controls.Add(Me.LblEstadoAparato)
        Me.Controls.Add(Me.ComboBoxAparatosDisponibles)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.ComboBoxPorAsignar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormIAPARATOS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BtnAceptar As System.Windows.Forms.Button
    Friend WithEvents ComboBoxPorAsignar As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxAparatosDisponibles As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LblEstadoAparato As System.Windows.Forms.Label
    Friend WithEvents CMBoxEstadoAparato As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxTipoAparato As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBoxWan As System.Windows.Forms.TextBox
    Friend WithEvents LabelWan As System.Windows.Forms.Label
    Friend WithEvents BtnBuscar As System.Windows.Forms.Button
End Class
