Imports System.Data.SqlClient
Imports System.Text
Public Class BrwQuiz

    Private Sub MuestraClientesPreguntas(ByVal Contrato As Long, ByVal Nombre As String, ByVal IDEncuesta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraClientesPreguntas ")
        strSQL.Append(CStr(Contrato) & ", ")
        strSQL.Append("'" & Nombre & "', ")
        strSQL.Append(CStr(IDEncuesta) & ", ")
        strSQL.Append(CStr(Op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            DataGridView1.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub MuestraEncuestas(ByVal Op As Integer, ByVal Nombre As String, ByVal Descripcion As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraEncuestas ")
        strSQL.Append(CStr(Op) & ", ")
        strSQL.Append("'" & Nombre & "', ")
        strSQL.Append("'" & Descripcion & "'")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ComboBoxEncuesta.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub


    Private Sub TextBoxBusContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusContrato.KeyPress
        If Asc(e.KeyChar) <> 13 And TextBoxBusContrato.Text.Length = 0 Then
            MuestraClientesPreguntas(0, String.Empty, 0, 0)
            Exit Sub
        End If
        MuestraClientesPreguntas(CLng(TextBoxBusContrato.Text), String.Empty, 0, 1)
    End Sub

    Private Sub ButtonBusDescripcion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusContrato.Click
        If TextBoxBusContrato.Text.Length = 0 Then
            MuestraClientesPreguntas(0, String.Empty, 0, 0)
            Exit Sub
        End If
        MuestraClientesPreguntas(CLng(TextBoxBusContrato.Text), String.Empty, 0, 1)
    End Sub

    Private Sub TextBoxBusNombre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusNombre.KeyPress
        If Asc(e.KeyChar) <> 13 And TextBoxBusNombre.Text.Length = 0 Then
            MuestraClientesPreguntas(0, String.Empty, 0, 0)
            Exit Sub
        End If
        MuestraClientesPreguntas(0, TextBoxBusNombre.Text, 0, 2)
    End Sub

    Private Sub ButtonBusNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusNombre.Click
        If TextBoxBusNombre.Text.Length = 0 Then
            MuestraClientesPreguntas(0, String.Empty, 0, 0)
            Exit Sub
        End If
        MuestraClientesPreguntas(0, TextBoxBusNombre.Text, 0, 2)
    End Sub

    Private Sub ButtonBusEncuesta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusEncuesta.Click
        If ComboBoxEncuesta.Text.Length = 0 Then
            MuestraClientesPreguntas(0, String.Empty, 0, 0)
            Exit Sub
        End If
        MuestraClientesPreguntas(0, String.Empty, ComboBoxEncuesta.SelectedValue, 3)
    End Sub

    Private Sub BrwQuiz_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBnd = True Then
            eBnd = False
            MuestraClientesPreguntas(0, String.Empty, 0, 0)
        End If
    End Sub

    Private Sub BrwQuiz_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        MuestraClientesPreguntas(0, String.Empty, 0, 0)
        MuestraEncuestas(0, "", "")
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ButtonNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNuevo.Click
        eOpcion = "N"
        FrmQuiz.Show()
    End Sub

    Private Sub ButtonConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonConsultar.Click
        If DataGridView1.RowCount = 0 Then
            MsgBox("Selecciona un registro a Consultar.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eOpcion = "C"
        eIDEncuesta = DataGridView1.SelectedCells.Item(0).Value
        eContrato = DataGridView1.SelectedCells.Item(1).Value
        eEncuestaNombre = DataGridView1.SelectedCells.Item(3).Value
        FrmQuiz.Show()
    End Sub

    Private Sub ButtonModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonModificar.Click
        If DataGridView1.RowCount = 0 Then
            MsgBox("Selecciona un registro a Modificar.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eOpcion = "M"
        eIDEncuesta = DataGridView1.SelectedCells.Item(0).Value
        FrmQuiz.Show()
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub
End Class