'Imports sofTV.Org.Mentalis.Multimedia
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic
Public Class FrmMiMenu

    'Dim v As New VideoFile(RutaReportes + "\Read.avi", Me.Panel1)
    ' Puede cambiar más opciones de impresión mediante la propiedad PrintOptions de ReportDocument
    Dim j As Integer = 0
    Dim Proceso As New System.Diagnostics.Process
    Dim proceso2 As New System.Diagnostics.Process

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub SP_DamedatosUsuario(ByVal oClv_Usuario As String) '''''VERIFICAMOS SI EL CAJERO YA TIENE DESGLOSE GENERADO
        '''''LIMPIAMOS LOS PARÁMETROS (INICIO)
        BaseII.limpiaParametros()
        '''''LIMPIAMOS LOS PARÁMETROS (FIN)

        '''''MANDAMOS LOS PARÁMETROS DEL PROCEDIMIENTO (INICIO) 
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, oClv_Usuario, 50)
        BaseII.CreateMyParameter("@Nombre", ParameterDirection.Output, SqlDbType.VarChar, 250)
        '''''DECLARAMOS EL DICCIONARIO DONDE VAMOS A LLENAR LOS PARÁMETROS DE SALIDA (INICIO)
        Dim DicoParametros As New Dictionary(Of String, Object)
        '''''DECLARAMOS EL DICCIONARIO DONDE VAMOS A LLENAR LOS PARÁMETROS DE SALIDA (FIN)

        '''''LLENAMOS EL DICCIONARIO MANDANDO EJECUTAR EL PROCEDIMIENTO (INICIO)
        DicoParametros = BaseII.ProcedimientoOutPut("SP_DamedatosUsuario")
        '''''LLENAMOS EL DICCIONARIO MANDANDO EJECUTAR EL PROCEDIMIENTO (FIN)

        '''''LE ASIGNAMOS EL VALOR A LA VARIABLE QUE DEFINIRÁ SI TIENE O NO CORTE GENERADO (INICIO)
        LabelNombreUsuario.Text = DicoParametros("@Nombre").ToString
        '''''LE ASIGNAMOS EL VALOR A LA VARIABLE QUE DEFINIRÁ SI TIENE O NO CORTE GENERADO (FIN)
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'FrmClientes.Show()
        BrwClientes.Show()
    End Sub

    Private Sub MenuItem19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '     BrwCalles.Button1.Visible = False
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCalles.Show()
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If

        BrwColonias.Show()
    End Sub

    Private Sub MenuItem20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCiudades.Show()
    End Sub

    Private Sub MenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwTiposServicios.Show()
    End Sub

    Private Sub MenuItem15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwServicios.Show()
    End Sub

    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCablemodems.Show()
    End Sub

    Private Sub MenuItem21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPromociones.Show()
    End Sub

    Private Sub MenuItem22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwSucursales.Show()
    End Sub

    Private Sub MenuItem23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwBancos.Show()
    End Sub

    Private Sub MenuItem24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwUsuarios.Show()
    End Sub
    Private Sub MenuItem26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCajas.Show()
    End Sub

    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPuestoTec.Show()
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)

        End If
        BrwTrabajos.Show()
    End Sub

    Private Sub MenuItem12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWTECNICOS.Show()
    End Sub

    Private Sub MenuItem58_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWORDSER.Show()
    End Sub

    Private Sub MenuItem66_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWQUEJAS.Show()
    End Sub

    Private Sub MenuItem68_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwAnttelQuejas.Show()
    End Sub

    Private Sub MenuItem7_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwTiposColonia.Show()
    End Sub

    Private Sub MenuItem73_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub MenuItem67_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwAgenda.Show()
    End Sub

    Private Sub MenuItem74_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub MenuItem17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWMotivoCancelacion.Show()
    End Sub


    Private Sub MenuItem126_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWBUSCACNR.Show()
    End Sub

    Private Sub MenuItem83_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmImprimir.Show()
    End Sub

    Private Sub MenuItem124_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmGenerales_Sistema.Show()
    End Sub

    Private Sub MenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ClientesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If IdSistema = "VA" Or IdSistema = "LO" Or IdSistema = "YU" Then
            BrwClientes2.Show()
        Else
            BrwClientes.Show()
        End If
    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        bitsist(GloUsuario, 0, LocGloSistema, "Salida Del Sistema", "", "Salida Del Sistema", GloSucursal, LocClv_Ciudad)
        End
    End Sub

    Private Sub OrdenesDeServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdenesDeServicioToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWORDSER.Show()
    End Sub

    Private Sub AgendaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgendaToolStripMenuItem.Click

    End Sub

    Private Sub AtenciónTelefónicaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AtenciónTelefónicaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwAnttelQuejas.Show()
    End Sub

    Private Sub ReportesVariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportesVariosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmImprimir.Show()
    End Sub

    Private Sub GeneralesDelSistemaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDelSistemaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 2
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub InterfasCablemodemsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 3
        'BRWBUSCACNR.Show()
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub ClasificaciónTécnicaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClasificaciónTécnicaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPuestoTec.Show()
    End Sub

    Private Sub ServiciosAlClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosAlClienteToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvNew.Module1.GloTipoUsuario = GloTipoUsuario
        SoftvNew.Module1.GloClaveMenus = GloClaveMenus
        Dim frm As New SoftvNew.BrwTrabajos
        frm.ShowDialog()
        'BrwTrabajos.Show()
    End Sub

    Private Sub TecnicosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TecnicosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWTECNICOS.Show()
    End Sub

    Private Sub TiposDeServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TiposDeServicioToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 1
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ServiciosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwServicios.Show()
    End Sub

    Private Sub CableModemsYAparatosDigitalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CableModemsYAparatosDigitalesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCablemodems.Show()
    End Sub

    Private Sub ColoniasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ColoniasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwColonias.Show()
    End Sub

    Private Sub TiposDeColoniasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TiposDeColoniasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwTiposColonia.Show()
    End Sub

    Private Sub CallesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CallesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCalles.Show()
    End Sub

    Private Sub CiudadesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CiudadesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCiudades.Show()
    End Sub

    Private Sub PromocionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPromociones.Show()
    End Sub

    Private Sub SucursalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SucursalesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwSucursales.Show()
    End Sub

    Private Sub BancosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BancosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwBancos.Show()
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UsuariosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwUsuarios.Show()
    End Sub

    Private Sub CajasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CajasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCajas.Show()
    End Sub

    Private Sub MotivosDeCancelaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosDeCancelaciónToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWMotivoCancelacion.Show()
    End Sub

    Private Sub FrmMiMenu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        bitsist(GloUsuario, 0, LocGloSistema, "Salida Del Sistema", "", "Salida Del Sistema", GloSucursal, LocClv_Ciudad)
        End
    End Sub

    Private Sub DameEspecifF()

        '@ColorButton int output
        '@ColorLetraButton int output
        '@ColorMenu int output
        '@ColorLetraMenu int output
        '@ColorBrowse int output
        '@ColorLetraBwr int output
        '@ColorFondoGrid int output
        '@ColorForm int output
        '@ColorLabel int output
        '@ColorLetraLabel int output
        '@ColorLetraForm int output

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameEspecif", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim parametro As New SqlParameter("@ColorButton", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)
        Dim parametro1 As New SqlParameter("@ColorLetraButton", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)
        Dim parametro2 As New SqlParameter("@ColorMenu", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)
        Dim parametro3 As New SqlParameter("@ColorLetraMenu", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)
        Dim parametro4 As New SqlParameter("@ColorBrowse", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)
        Dim parametro5 As New SqlParameter("@ColorLetraBwr", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)
        Dim parametro6 As New SqlParameter("@ColorFondoGrid", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)
        Dim parametro7 As New SqlParameter("@ColorForm", SqlDbType.Int)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)
        Dim parametro8 As New SqlParameter("@ColorLabel", SqlDbType.Int)
        parametro8.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro8)
        Dim parametro9 As New SqlParameter("@ColorLetraLabel", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)
        Dim parametro10 As New SqlParameter("@ColorLetraForm", SqlDbType.Int)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)
        'ColorBut, ColorLetraBut, ColorMenu, ColorMenuLetra, ColorBwr, ColorBwrLetra, ColorGrid, ColorForm, ColorLabel, ColorLetraLabel, ColorLetraForm

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            ColorBut = CLng(parametro.Value.ToString)
            ColorLetraBut = CLng(parametro1.Value.ToString)
            ColorMenu = CLng(parametro2.Value.ToString)
            ColorMenuLetra = CLng(parametro3.Value.ToString)
            ColorBwr = CLng(parametro4.Value.ToString)
            ColorBwrLetra = CLng(parametro5.Value.ToString)
            ColorGrid = CLng(parametro6.Value.ToString)
            ColorForm = CLng(parametro7.Value.ToString)
            ColorLabel = CLng(parametro8.Value.ToString)
            ColorLetraLabel = CLng(parametro9.Value.ToString)
            ColorLetraForm = CLng(parametro10.Value.ToString)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub DameTipoUsusarioF(ByVal oclv_usuario As String)

        '@ColorButton int output
        '@ColorLetraButton int output
        '@ColorMenu int output
        '@ColorLetraMenu int output
        '@ColorBrowse int output
        '@ColorLetraBwr int output
        '@ColorFondoGrid int output
        '@ColorForm int output
        '@ColorLabel int output
        '@ColorLetraLabel int output
        '@ColorLetraForm int output

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameTipoUsusario", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim parametro As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 20)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oclv_usuario
        comando.Parameters.Add(parametro)
        Dim parametro1 As New SqlParameter("@tipo", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()

            GloTipoUsuario = CLng(parametro1.Value.ToString)


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ALTASformsF(ByVal oNombre As String, ByVal oclv_sistema As Integer, ByVal oclv_menu As Integer, ByVal oname As String)

        '@NOMBRE AS VARCHAR(50),@clv_sistema as int,@clv_menu as int,@name as varchar(50)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ALTASforms", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@NOMBRE", SqlDbType.VarChar, 50)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oNombre
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@clv_sistema", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = oNombre
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@clv_menu", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = oclv_menu
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@name", SqlDbType.VarChar, 50)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = oname
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Public Sub MuestraImagenF()
        Dim con As New SqlConnection(MiConexion)
        Try

            Dim obyte As Byte()
            Dim ms As IO.MemoryStream
            Dim img As Image

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("MUESTRAIMAGEN", con)
            cmd.CommandType = CommandType.StoredProcedure
            con.Open()
            reader = cmd.ExecuteReader()
            Using reader
                While reader.Read
                    obyte = CType(reader.GetValue(0), Byte())
                    ms = New IO.MemoryStream(obyte)
                    img = Image.FromStream(ms)
                    PictureBox2.Image = img
                End While
            End Using


        Catch ex As Exception

            MsgBox(ex.Message)

        Finally
            con.Close()
        End Try
    End Sub

    Private Sub FrmMiMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataSetLidia.MUESTRAIMAGEN' Puede moverla o quitarla según sea necesario.
        'Me.MUESTRAIMAGENTableAdapter.Fill(Me.DataSetLidia.MUESTRAIMAGEN)

        SP_SERVICIOS_HABILITADOS()

        SP_Dame_General_sistema_II()

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Inicio guardar menus para perfiles
        Dim NOMBREMENU As ToolStripMenuItem
        Dim NOMBRESUBMENU As ToolStripMenuItem

        Dim I, J, K As Integer

        If GloUsuario = "SISTE" Then
            For I = 0 To MenuStrip1.Items.Count - 1
                UspGuardaMenu(Me.MenuStrip1.Items(I).Name, Me.MenuStrip1.Items(I).Text, "", 1)
                NOMBREMENU = Me.MenuStrip1.Items(I)
                For J = 0 To NOMBREMENU.DropDownItems.Count - 1
                    UspGuardaMenu(NOMBREMENU.DropDownItems(J).Name, NOMBREMENU.DropDownItems(J).Text, NOMBREMENU.Name, 2)
                    NOMBRESUBMENU = NOMBREMENU.DropDownItems(J)
                    For K = 0 To NOMBRESUBMENU.DropDownItems.Count - 1
                        UspGuardaMenu(NOMBRESUBMENU.DropDownItems(K).Name, NOMBRESUBMENU.DropDownItems(K).Text, NOMBRESUBMENU.Name, 3)
                    Next
                Next
            Next
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''fin guardar menus para perfiles

        'arreglar
        DameEspecifF()
        'Me.DameEspecifTableAdapter.Connection = CON
        'Me.DameEspecifTableAdapter.Fill(Me.DataSetLidia.DameEspecif, ColorBut, ColorLetraBut, ColorMenu, ColorMenuLetra, ColorBwr, ColorBwrLetra, ColorGrid, ColorForm, ColorLabel, ColorLetraLabel, ColorLetraForm)
        CON.Close()
        bitsist(GloUsuario, 0, LocGloSistema, "Entrada Sistema", "", "Entrada Al Sistema", GloSucursal, LocClv_Ciudad)

        'Dim asm As Reflection.Assembly = Reflection.Assembly.LoadFrom(Application.StartupPath + "\sofTV.exe")
        'For Each frm As Form In     'sofTV.My.Application.OpenForms
        '    colorea(Me, Me.Name)
        'Next
        'Dim name As String = My.Forms.BrwrSectores.Name
        'Dim form As Form
        'form = New Form
        'form = CType(name, Form)
        'form.Show()
        CMBLabelSistema.Text = GloEmpresa
        Me.CMBLabel1.Text = GloCiudad
        If IdSistema = "TO" Then
            Me.GeneralesDeBancosToolStripMenuItem.Visible = False
            Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Visible = False
            'Me.GeneralesDeInterfacesInternetToolStripMenuItem.Visible = False
            Me.GeneralesDeOXXOToolStripMenuItem.Visible = False
            'Me.EncargadosDelSistemaToolStripMenuItem.Visible = False
        End If
        If IdSistema <> "TO" Then
            'Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = False
            Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = False
            Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Visible = False
        End If
        If IdSistema = "SA" Then
            Me.ProcesosDeServicioPremiumToolStripMenuItem.Visible = False
            Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = True
            Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Visible = True
            Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = True
            '  Me.ProcesoDeCierreDeMesToolStripMenuItem.Visible = True
            Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = True
            Me.SegundoPeriodoToolStripMenuItem1.Visible = False
            Me.GeneralesDeOXXOToolStripMenuItem.Visible = False
            Me.DepuraciónDeÓrdenesToolStripMenuItem.Visible = True
            Me.MesajesInstantaneosToolStripMenuItem.Visible = True
            Me.DesconexionToolStripMenuItem.Visible = True
        End If
        If IdSistema = "VA" Then
            Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = True
            Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Visible = True
            Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = True
            '.Visible = True
            'Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = True
            'Me.CargosEspecialesToolStripMenuItem.Visible = True
            Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = True
            Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Visible = True
            'Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Visible = False
            Me.MesajesInstantaneosToolStripMenuItem.Visible = True
            Me.ActivaciónPaqueteDePruebaToolStripMenuItem.Visible = True
            'Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Visible = False
        End If
        '
        'Validación de Menus de la barra principal
        CatálogosToolStripMenuItem.Enabled = TipoAccesoMenusPerfiles(GloTipoUsuario, "CatálogosToolStripMenuItem")
        ProcesosToolStripMenuItem.Enabled = TipoAccesoMenusPerfiles(GloTipoUsuario, "ProcesosToolStripMenuItem")
        ReportesToolStripMenuItem.Enabled = TipoAccesoMenusPerfiles(GloTipoUsuario, "ReportesToolStripMenuItem")
        GeneralesToolStripMenuItem.Enabled = TipoAccesoMenusPerfiles(GloTipoUsuario, "GeneralesToolStripMenuItem")

        If SubCiudad <> "TV" And SubCiudad <> "VA" Then
            Me.PolizaToolStripMenuItem.Visible = False
        End If

        MuestraImagenF()
        'PictureBox2.Image = 
        'CON.Open()
        'arreglar
        'Me.MUESTRAIMAGENTableAdapter.Connection = CON
        'Me.MUESTRAIMAGENTableAdapter.Fill(Me.DataSetLidia.MUESTRAIMAGEN)
        'LLENA LA TABLA DE MENUS AUTOMATICAMENTE
        'arreglar
        DameTipoUsusarioF(GloUsuario)
        'Me.DameTipoUsusarioTableAdapter.Connection = CON
        'Me.DameTipoUsusarioTableAdapter.Fill(Me.DataSetLidia.DameTipoUsusario, GloUsuario, GloTipoUsuario)
        'Me.DamePermisosTableAdapter.Connection = CON
        'Me.DamePermisosTableAdapter.Fill(Me.DataSetLidia.DamePermisos, GloTipoUsuario, Me.Text, 1, glolec, gloescr, gloctr)
        'CON.Close()
        Me.RecorrerEstructuraMenu(Me.MenuStrip1)
        ' Me.RecorrerFormularios()

        If IdSistema = "TO" Then
            Me.TecnicosToolStripMenuItem.Visible = False
            Me.CatálogoDeVentasToolStripMenuItem.Visible = False
            Me.VentasToolStripMenuItem.Visible = False
            'Me.ReportesDelCanalToolStripMenuItem.Visible = False
            Me.CableModemsYAparatosDigitalesToolStripMenuItem.Visible = False
            'Me.InterfasCablemodemsToolStripMenuItem.Visible = False
            Me.InterfasDecodificadoresToolStripMenuItem.Visible = True
            'Me.ContratoMaestroToolStripMenuItem.Visible = False
        End If
        If IdSistema = "AG" Then
            If GloTipoUsuario <> 40 Then
                'InterfasCablemodemsToolStripMenuItem.Enabled = False
                InterfasDecodificadoresToolStripMenuItem.Enabled = True
            End If
            'Me.TelefoníaToolStripMenuItem.Visible = False
            Me.PolizaToolStripMenuItem.Visible = False
            'Me.GeneralesProsaBancomerToolStripMenuItem.Visible = True
        End If
        If IdSistema = "LO" Or IdSistema = "YU" Then
            Me.EstadoDeCuentaToolStripMenuItem.Visible = False
            'Me.EstadoDeCuentaTelefoníaToolStripMenuItem.Visible = True
            'Me.TelefoníaToolStripMenuItem.Visible = True
            ' Me.TelefoníaToolStripMenuItem1.Visible = True
            'Me.CargosEspecialesToolStripMenuItem.Visible = True
            'Procesos
            'Me.CambioDeClienteAClienteNormalToolStripMenuItem.Visible = False
            'Me.CambioDeClienteASoloInternetToolStripMenuItem.Visible = False
            Me.CortesToolStripMenuItem.Visible = True
            'Me.ReprocesamientoPorClienteToolStripMenuItem.Visible = True
            'Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Visible = False
            'Reportes
            Me.InterfazCablemodemsToolStripMenuItem.Visible = False
            Me.InterfazDigitalesToolStripMenuItem.Visible = True
            Me.CarteraEjecutivaToolStripMenuItem.Visible = False
            Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Visible = False
            Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Visible = False
            'Me.PuntosDeAntigüedadToolStripMenuItem.Visible = False
            ''Reportes Ventas
            'Me.PelículasToolStripMenuItem.Visible = False
            'Me.PPVToolStripMenuItem.Visible = False
            'Generales
            'Me.InterfasCablemodemsToolStripMenuItem.Visible = False
            Me.InterfasDecodificadoresToolStripMenuItem.Visible = True
            Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Visible = False
            'Me.GeneralesDeInterfacesInternetToolStripMenuItem.Visible = False
            Me.GeneralesDeOXXOToolStripMenuItem.Visible = False
            'Me.ReferenciasBancariasToolStripMenuItem1.Visible = True
            'Me.ProcesamientoDeLlamadasToolStripMenuItem.Visible = True
            'Me.ProcesamientoDeCDRToolStripMenuItem.Visible = True
        End If
        colorea(Me, Me.Name)

        'PARA TUXTLA
        Me.BajaDeServiciosToolStripMenuItem.Visible = False
        Me.ÓrdenesClientesToolStripMenuItem.Visible = False

        'If IdSistema <> "LO" And IdSistema <> "YU" Then
        'TelefoníaToolStripMenuItem.Visible = False
        'End If
        'ElseIf IdSistema <> "AG" Then
        '    Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Visible = False
        'End If
        Me.ClasificaciónTécnicaToolStripMenuItem.Visible = False
        Me.ReseteoMasivoDeAparatosToolStripMenuItem.Visible = False
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Visible = False
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Visible = False

        VALIDAAlertaProrroga(GloUsuario)



        MensajesPrefijosToolStripMenuItem.Visible = False
        'RentaACajasToolStripMenuItem.Visible = False
        ActivaciónPaqueteDePruebaToolStripMenuItem.Visible = True
        'CambioDeServicioToolStripMenuItem.Visible = False
        MesajesInstantaneosToolStripMenuItem.Visible = True
        MsjsPersonalizadosToolStripMenuItem.Visible = False
        CorreoToolStripMenuItem.Visible = False
        BitácoraDePruebasToolStripMenuItem.Visible = True
        BitácoraDeCorreosToolStripMenuItem.Visible = False
        InterfazDigitalesToolStripMenuItem.Visible = True
        RecontratacionesToolStripMenuItem.Visible = False
        DecodificadoresToolStripMenuItem1.Visible = False
        InterfasDecodificadoresToolStripMenuItem.Visible = True
        GeneralesDeBancosToolStripMenuItem.Visible = False
        'GeneralesDeInterfacesDigitalesToolStripMenuItem.Visible = False
        GeneralesDeOXXOToolStripMenuItem.Visible = False
        ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem.Visible = False
        ProcesosDeServicioPremiumToolStripMenuItem.Visible = False
        DesconexionToolStripMenuItem.Visible = False
        'PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = False
        'RegresaAparatosAlAlmacenToolStripMenuItem.Visible = False
        'GenerarÓrdenesDeDesconexiónToolStripMenuItem.Visible = False
        PrórrogasToolStripMenuItem.Visible = False
        PromesasDePagoToolStripMenuItem.Visible = False
        EstablecerComisionesCobroADomicilioToolStripMenuItem.Visible = False
        CobrosADomicilioToolStripMenuItem.Visible = False
        Me.CambioDeClienteAClienteNormalToolStripMenuItem.Visible = True
        Me.CambioDeClienteASoloInternetToolStripMenuItem.Visible = True

        If GloTipoUsuario = 40 Then
            Me.EstadosDeCarteraToolStripMenuItem.Visible = True
            Me.ReportesVariosToolStripMenuItem.Visible = True
            Me.PruebaDeInternetToolStripMenuItem.Visible = True
        Else
            Me.EstadosDeCarteraToolStripMenuItem.Visible = False
            Me.ReportesVariosToolStripMenuItem.Visible = False
            Me.PruebaDeInternetToolStripMenuItem.Visible = False
        End If

        If GloTipoUsuario = 21 Then
            VentasToolStripMenuItem.Visible = False
        End If

        eOpPPE = 5
        FrmImprimirPPE.Show()

        ToolStripMenuItem2.Visible = False

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''inicio Para armar el menu segun el tipo de ususario
        If GloUsuario <> "SISTE" Then
            UspMostrarMenusCliente()
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''final Para armar el menu segun el tipo de ususario
        ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = False
        SP_DamedatosUsuario(GloUsuario)
    End Sub

    Private Sub PromotoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PromotoresToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwVendedores.Show()
    End Sub

    Private Sub SeriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeriesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCatalogoSeries.Show()
    End Sub

    Private Sub OrdenesDeServicioToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdenesDeServicioToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloRepAreaTec = "O"
        Frmareatecnica.Show()
    End Sub

    Private Sub QuejasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuejasToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWQUEJAS.Show()
    End Sub

    Private Sub EstadosDeCarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadosDeCarteraToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCarteras.Show()
    End Sub

    Private Sub QuejasToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuejasToolStripMenuItem2.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloRepAreaTec = "Q"
        FrmQuejas.Show()
    End Sub

    Private Sub ÁreasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ÁreasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwTap.Show()
    End Sub

    Private Sub TiposDePromotoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub SectoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SectoresToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwSectore.Show()
    End Sub

    Private Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
        Dim CON As New SqlConnection(MiConexion)
        Dim menu As ToolStripMenuItem

        For Each oOpcionMenu In oMenu.Items
            CON.Open()
            'Me.DamePermisosTableAdapter.Connection = CON
            'Me.DamePermisosTableAdapter.Fill(Me.DataSetLidia.DamePermisos, GloTipoUsuario, oOpcionMenu.Text, oOpcionMenu.Name, 1, glolec, gloescr, gloctr)
            menu = New ToolStripMenuItem
            menu = oOpcionMenu
            If gloctr = 1 Then
                menu.Enabled = False
            End If
            menu = Nothing
            j = j + 1
            'Me.ALTASMENUSTableAdapter.Connection = CON
            'Me.ALTASMENUSTableAdapter.Fill(Me.DataSetLidia.ALTASMENUS, oOpcionMenu.Text, 1, oOpcionMenu.Name, 10)
            CON.Close()
            If oOpcionMenu.DropDownItems.Count > 0 Then
                Me.RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
            End If
        Next
    End Sub

    Private Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim submenu As ToolStripItem
        For Each oSubitem As ToolStripItem In oSubmenuItems
            If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
                CON.Open()
                'Me.DamePermisosTableAdapter.Connection = CON
                'Me.DamePermisosTableAdapter.Fill(Me.DataSetLidia.DamePermisos, GloTipoUsuario, oOpcionMenu.Text + " " + oSubitem.Text, oSubitem.Name, 1, glolec, gloescr, gloctr)
                submenu = New ToolStripMenuItem
                submenu = oSubitem
                If gloctr = 1 Then
                    submenu.Enabled = False
                End If
                submenu = Nothing
                'Me.ALTASMENUSTableAdapter.Connection = CON
                'Me.ALTASMENUSTableAdapter.Fill(Me.DataSetLidia.ALTASMENUS, (oOpcionMenu.Text + " " + oSubitem.Text), 1, oSubitem.Name, j)
                CON.Close()
                Menu = Nothing
                If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
                    Me.RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
                End If
            End If
        Next
    End Sub

    Public Sub RecorrerFormularios()
        Dim CON As New SqlConnection(MiConexion)
        'DESDE UN PROYECTO DE FUERA 
        'Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.LoadFile("c:\prog.exe")
        ' Obtengo el ensamblado 
        Reflection.Assembly.LoadFile(Application.StartupPath + "Softv.exe")
        Dim asm As Reflection.Assembly = Reflection.Assembly.GetCallingAssembly
        Dim t As System.Type
        ' Obtengo todos los tipos definidos en el ensamblado
        ' Obtengo el tipo System.Windows.Forms.Form 
        ' Recorro los tipos buscando aquellos que 
        ' hereden de System.Windows.Forms.Form
        'For Each CT As Control In asm.GetObjectData

        For Each t In asm.GetTypes()
            Dim base As Type = t.BaseType
            If base Is GetType(System.Windows.Forms.Form) Then
                base = t.BaseType
            End If
            If base Is GetType(System.Windows.Forms.Form) Then
                'arreglar
                'CON.Open()
                'Me.ALTASformsTableAdapter.Connection = CON
                'Me.ALTASformsTableAdapter.Fill(Me.DataSetLidia.ALTASforms, t.Name, 1, 0, t.Name)
                ALTASformsF(t.Name, 1, 0, t.Name)
                'CON.Close()
            End If

        Next
    End Sub
    Private Sub LlamadasTelefónicasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LlamadasTelefónicasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloRepAreaTec = "L"
        FrmLlamadasTelefonicasReporte.Show()
    End Sub

    Private Sub GeneralesDeInterfacesDigitalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDeInterfacesDigitalesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 7
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub InterfasDecodificadoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfasDecodificadoresToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 4
        'BRWCNRDIG.Show()
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloClv_tipser2 = InputBox("Dame el Clave de Servicio", "Prueba")
        FrmSelServRep.Show()

    End Sub

    Private Sub GeneralesDeOXXOToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDeOXXOToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 8
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub ReportesDinamicosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ConfiguracionDelSistemaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguracionDelSistemaToolStripMenuItem.Click
        With Proceso
            .StartInfo.FileName = RutaReportes + "\Softkinssoftv.exe"
            .StartInfo.WindowStyle = ProcessWindowStyle.Normal
            .StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
            .Start()
        End With
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Acceso a Softv Skin", "", "", LocClv_Ciudad)
    End Sub

    Private Sub ReporteDinámicosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With proceso2
            .StartInfo.FileName = Application.StartupPath + "\Reportes\" + "\WizardReport.exe"
            .StartInfo.WindowStyle = ProcessWindowStyle.Normal
            .StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
            .Start()
        End With
    End Sub

    Private Sub GeneralesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub GeneralesDeBancosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDeBancosToolStripMenuItem.Click

    End Sub

    Private Sub ProcesosDeCobranzaBancariaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 5
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub BancosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BancosToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 6
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub MotivosDeCancelaciónFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosDeCancelaciónFacturasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eReImpresion = 0
        BrwMotivosCancelacionFactura.Show()
    End Sub

    Private Sub MotivosDeReImpresiónFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosDeReImpresiónFacturasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eReImpresion = 1
        BrwMotivosCancelacionFactura.Show()
    End Sub

    Private Sub GeneralesDeInterfacesInternetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 5
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub RangosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RangosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwRangos.Show()
    End Sub

    Private Sub PrecioDeComisionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrecioDeComisionesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'BrwRangosComisiones.Show()
        FrmRangosComisiones.Show()
    End Sub

    Private Sub CalcularComisionesPorVendedorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub VentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VentasToolStripMenuItem.Click

    End Sub

    Private Sub CalcularComisionesPorVendedorToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularComisionesPorVendedorToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'FrmComision.Show()
        FrmRepGralVentas.Show()
    End Sub


    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub



    Private Sub SoftvToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With Proceso
            .StartInfo.FileName = RutaReportes + "\Rutas_Reportes.exe"
            .StartInfo.WindowStyle = ProcessWindowStyle.Normal
            .StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
            .Start()
        End With
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Acceso a Ruta Reportes Softv", "", "", LocClv_Ciudad)
    End Sub

    Private Sub FacsoftvToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With Proceso
            .StartInfo.FileName = RutaReportes + "\Ruta_rep_facsoftv.exe"
            .StartInfo.WindowStyle = ProcessWindowStyle.Normal
            .StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
            .Start()
        End With
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Acceso a Ruta Reportes FacSoftv", "", "", LocClv_Ciudad)
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelColonias_Rep.Show()

    End Sub

    Private Sub EncargadosDelSistemaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwEncargadosEmails.Show()
    End Sub



    Private Sub PrimerPeriodoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrimerPeriodoToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If IdSistema = "SA" Then
            opcFrm = 16
        Else
            opcFrm = 10
        End If
        Acceso_TipoServicios.Show()

    End Sub

    'Public Sub desconexion()
    '    Dim CON As New SqlConnection(MiConexion)
    '    Try
    '        Dim Respuesta As MsgBoxResult
    '        Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
    '        If Respuesta = MsgBoxResult.Ok Then
    '            bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Primer Periodo", "", "Generó Proceso de Desconexión Primer Periodo", GloSucursal, LocClv_Ciudad)
    '            CON.Open()
    '            Dim comando As SqlClient.SqlCommand
    '            comando = New SqlClient.SqlCommand
    '            With comando
    '                .Connection = CON
    '                .CommandText = "EXEC Proceso_Cierre_Mes_PRIMER_Periodo 1"
    '                .CommandType = CommandType.Text
    '                .CommandTimeout = 0
    '                .ExecuteReader()
    '            End With
    '            CON.Close()
    '            MsgBox(" Proceso Finalizado con Éxito ")
    '        End If
    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Public Sub desconexion()
        Me.BackgroundWorker1.RunWorkerAsync()
    End Sub

    'Public Sub desconexion2()
    '    Dim CON As New SqlConnection(MiConexion)
    '    Try
    '        Dim Respuesta As MsgBoxResult
    '        Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Segundo Periodo > ?", MsgBoxStyle.OkCancel)
    '        If Respuesta = MsgBoxResult.Ok Then
    '            bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Segundo Periodo", "", "Generó Proceso de Desconexión Segundo Periodo", GloSucursal, LocClv_Ciudad)
    '            CON.Open()

    '            Dim comando As SqlClient.SqlCommand
    '            comando = New SqlClient.SqlCommand
    '            With comando
    '                .Connection = CON
    '                .CommandText = "EXEC Proceso_Cierre_Mes_Primer_Periodo 2"
    '                .CommandType = CommandType.Text
    '                .CommandTimeout = 0
    '                .ExecuteReader()
    '            End With
    '            CON.Close()
    '            MsgBox(" Proceso Finalizado con Éxito ")
    '        End If
    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub


    Public Sub desconexion2()
        Me.BackgroundWorker2.RunWorkerAsync()
    End Sub

    Private Sub SegundoPeriodoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SegundoPeriodoToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 11
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub AvisosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BwrAvisos.Show()
    End Sub




    Private Sub Button2_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelPeriodo2.Show()
    End Sub

    Private Sub ClavesTécnicasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub AnálisisDePenetraciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnálisisDePenetraciónToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepPenetracion.Show()
    End Sub

    Private Sub ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 13
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ContratoMaestroToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 15
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ActivaciónPaqueteDePruebaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActivaciónPaqueteDePruebaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmLoginPremium.Show()
    End Sub

    Private Sub PaquetesPremiumToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 1
        FrmSelFechasPPE.Show()
    End Sub


    Private Sub PreciosDeArticulosDeInstalaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreciosDeArticulosDeInstalaciónToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCapArtiAcom.Show()
    End Sub

    Private Sub ClientesConAdeudoDeMaterialToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesConAdeudoDeMaterialToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Locbndrep = 1
        FrmImprimirContrato.Show()
    End Sub

    Private Sub BitácoraDePruebasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BitácoraDePruebasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 2
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ProcesoDeReactivaciónDeContratoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmReactivarContrato.Show()
    End Sub

    Private Sub DepuraciónDeÓrdenesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DepuraciónDeÓrdenesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmDepOrd.Show()
    End Sub

    Private Sub CambioDeServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioDeServicioToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'FrmCamServCte.Show()
        BrwCamServCte.Show()
    End Sub


    Private Sub ReporteDePaquetesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If (GloTipoUsuario = 40 And IdSistema = "AG") Or (IdSistema <> "AG") Then
            reporte_Paquetes()
        ElseIf GloTipoUsuario <> 40 Then
            LReporte2 = True
            FrmAccesoReportes.Show()
        End If
    End Sub
    Public Sub reporte_Paquetes()
        Dim CON As New SqlConnection(MiConexion)
        Dim CON1 As New SqlConnection(MiConexion)
        LocOp = 30
        CON.Open()
        Me.Valida_periodo_reportesTableAdapter.Connection = CON
        Me.Valida_periodo_reportesTableAdapter.Fill(Me.DataSetarnoldo.Valida_periodo_reportes, LocValidaHab)
        CON.Close()
        If LocClv_session > 0 Then
            CON.Open()
            Me.Borra_Separacion_ClientesTableAdapter.Connection = CON
            Me.Borra_Separacion_ClientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Separacion_Clientes, LocClv_session)
            Me.Borrar_Tablas_Reporte_nuevoTableAdapter.Connection = CON
            Me.Borrar_Tablas_Reporte_nuevoTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borrar_Tablas_Reporte_nuevo, LocClv_session)
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        ElseIf LocClv_session = 0 Then
            CON1.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON1
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON1.Close()
        End If
        FrmTipoClientes.Show()
    End Sub
    Public Sub reporte_Importe_mensualidades_adelantados()
        Dim CON As New SqlConnection(MiConexion)
        Dim CON1 As New SqlConnection(MiConexion)
        LocOp = 80
        CON.Open()
        Me.Valida_periodo_reportesTableAdapter.Connection = CON
        Me.Valida_periodo_reportesTableAdapter.Fill(Me.DataSetarnoldo.Valida_periodo_reportes, LocValidaHab)
        CON.Close()
        If LocClv_session > 0 Then
            Borra_Tablas_Reporte_mensualidades(LocClv_session)
            CON.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        ElseIf LocClv_session = 0 Then
            CON1.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON1
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON1.Close()
        End If
        FrmSelTipServRep.Show()
    End Sub
    Private Sub PrimerPeriodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 17
        Acceso_TipoServicios.Show()
    End Sub

    Public Sub desconexionSA()

        Try
            Using CON2 As New SqlConnection(MiConexion)
                'Dim CON2 As New SqlConnection(MiConexion)
                Dim Respuesta As MsgBoxResult
                Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
                If Respuesta = MsgBoxResult.Ok Then
                    bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Primer Periodo", "", "Generó Proceso de Desconexión Primer Periodo", GloSucursal, LocClv_Ciudad)
                    CON2.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON2
                        .CommandText = "EXEC SAHUAYO_PROCESO_DESCONEXION_TODOS_SERVICIOS 1,1"
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .ExecuteReader()
                    End With
                    CON2.Close()
                    MsgBox(" Proceso Finalizado con Éxito ")
                End If
            End Using
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message & " " & ex.ToString)
        End Try
    End Sub

    Public Sub desconexionSAInternet()

        Try
            Using CON2 As New SqlConnection(MiConexion)
                'Dim CON2 As New SqlConnection(MiConexion)
                Dim Respuesta As MsgBoxResult
                Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
                If Respuesta = MsgBoxResult.Ok Then
                    bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Primer Periodo", "", "Generó Proceso de Desconexión Primer Periodo", GloSucursal, LocClv_Ciudad)
                    CON2.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON2
                        .CommandText = "EXEC SAHUAYO_PROCESO_DESCONEXION_TODOS_SERVICIOSINTERNET 1,1"
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .ExecuteReader()
                    End With
                    CON2.Close()
                    MsgBox(" Proceso Finalizado con Éxito ")
                End If
            End Using
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message & " " & ex.ToString)
        End Try
    End Sub



    Public Sub CierredeMesSA()
        Dim CON2 As New SqlConnection(MiConexion)
        Try
            Dim Respuesta As MsgBoxResult
            Respuesta = MsgBox("¿ Deseas Correr el Proceso de Cierre de Mes del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
            If Respuesta = MsgBoxResult.Ok Then
                bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Cierre de Mes Primer Periodo", "", "Generó Proceso de Cierre de Mes Primer Periodo", GloSucursal, LocClv_Ciudad)
                CON2.Open()
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON2
                    .CommandText = "EXEC SAHUAYO_PROCESO_CIERREDEMES_TODOS_SERVICIOS 1,1"
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .ExecuteReader()
                End With
                CON2.Close()
                MsgBox(" Proceso Finalizado con Éxito ")
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ListadoDeActividadesDelTécnicoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeActividadesDelTécnicoToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Dim CON As New SqlConnection(MiConexion)
        If LocClv_session > 0 Then
            CON.Close()
            Me.Borra_temporales_trabajosTableAdapter.Connection = CON
            Me.Borra_temporales_trabajosTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales_trabajos, LocClv_session)
            LocClv_session = 0
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        ElseIf LocClv_session = 0 Then
            CON.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        End If
        FrmSelTecnico_Rep.Show()
    End Sub

    Private Sub ResumenDeVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenDeVentasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 3
        FrmSelFechasPPE.Show()
    End Sub


    Private Sub CorreoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CorreoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Using CON As New SqlConnection(MiConexion)
            CON.Open()
            LocOp = 23
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            GloClv_tipser2 = 0
            CON.Close()
        End Using
        FrmTipoClientes.Show()
    End Sub

    Private Sub MenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub

    Private Sub ResetearAparatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResetearAparatosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmReset.Show()
    End Sub

    Private Sub PelículasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 1
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ResumenVendedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenVendedoresToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 4
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub NotasDeCréditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwNotasdeCredito.Show()
    End Sub

    'Private Sub ConsolidadoVentasYOficinasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsolidadoVentasYOficinasToolStripMenuItem.Click
    '    eOpVentas = 11
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub ConsilidadoOficinasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsilidadoOficinasToolStripMenuItem.Click
    '    eOpVentas = 12
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub ConsolidadoVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsolidadoVentasToolStripMenuItem.Click
    '    eOpVentas = 13
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub OficinaEnParticularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OficinaEnParticularToolStripMenuItem.Click
    '    eOpVentas = 14
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub VendedorEnParticularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VendedorEnParticularToolStripMenuItem.Click
    '    eOpVentas = 15
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub NúmeroDeVentasVendedotesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NúmeroDeVentasVendedotesToolStripMenuItem.Click
    '    eOpVentas = 16
    '    eBndGraf = True
    '    eBndVen = True
    '    FrmSelFechasPPE.Show()
    'End Sub
    'Private Sub PorPaquetesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PorPaquetesToolStripMenuItem.Click
    '    eOpVentas = 17
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    '    'FrmSelTipServE.Show()
    'End Sub

    Private Sub ClientesVariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesVariosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Using CON As New SqlConnection(MiConexion)
            CON.Open()
            LocOp = 22
            eBndMenIns = True
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            GloClv_tipser2 = 3
            CON.Close()
        End Using
        FrmTipoClientes.Show()
    End Sub

    Private Sub ClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClienteToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMsjPorCliente.Show()
    End Sub

    Private Sub VisitasDelClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwVisitas.Show()
    End Sub

    Private Sub MotivosDeLlamadaAtenciónAClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwMotAtenTel.Show()
    End Sub

    Private Sub PruebaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PruebaToolStripMenuItem.Click
        BrwProgramaciones.Show()
        'Using CON As New SqlConnection(MiConexion)
        '    CON.Open()
        '    LocOp = 22
        '    eBndMenIns = True
        '    Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
        '    Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        '    GloClv_tipser2 = 3
        'End Using
        'Programacion = 27
        'FrmTipoClientes.Show()
        'FrmProgramacion_msjs.Show()
    End Sub

    Private Sub AtenciónTelefónicaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwAtenTel.Show()
    End Sub

    Private Sub EricToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmGraficas.Show()
    End Sub
    Private Sub GráficasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GráficasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmGraficas.Show()
    End Sub

    Private Sub CambioDeClienteASoloInternetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioDeClienteASoloInternetToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        LocProceso = 1
        FrmCambioClienteSoloInternet.Show()
    End Sub

    Private Sub CambioDeClienteAClienteNormalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioDeClienteAClienteNormalToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        LocProceso = 0
        FrmCambioClienteSoloInternet.Show()
    End Sub

    Private Sub ProcesosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesosToolStripMenuItem.Click

    End Sub
    Private Sub AgendaDeActividadesDelTécnicoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgendaDeActividadesDelTécnicoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelRepAgendaTecnico.Show()
    End Sub

    Private Sub PolizaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PolizaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmPolizaDiario.Show()
    End Sub

    Private Sub ProgramacionesDeMesnajesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgramacionesDeMesnajesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwProgramaciones.Show()
    End Sub

    Private Sub GeneraPolizaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPolizas.Show()
    End Sub

    Private Sub MetasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetas.Show()
    End Sub

    Private Sub GeneralesToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesToolStripMenuItem.Click

    End Sub

    Private Sub MetasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetasRep.Show()
    End Sub

    Private Sub MedidoresDeIngresosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetasIngresos.Show()
    End Sub

    Private Sub ReportesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportesToolStripMenuItem.Click

    End Sub

    Private Sub DesgloceDeMensualidadesAdelantadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BndDesPagAde = True
        FrmSelCd_Cartera.Show()
    End Sub

    Private Sub ContratoForzosoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoForzosoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'eBndContratoF = True
        If (GloTipoUsuario = 40 And IdSistema = "AG") Or (IdSistema <> "AG") Then
            eOpVentas = 40
            'FrmSelTipServE.Show()
            FrmContratoF.Show()
        Else
            FrmAccesoReportes.Show()
        End If
    End Sub

    Private Sub RToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If (GloTipoUsuario = 40 And IdSistema = "AG") Or (IdSistema <> "AG") Then
            FrmSelContratoRango.Show()
        ElseIf (GloTipoUsuario <> 40 And IdSistema = "AG") Then
            LReporte1 = True
            FrmAccesoReportes.Show()
        End If
    End Sub

    Private Sub MedidoresDeCarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetasCarteras.Show()
    End Sub

    Private Sub ServiciosDeVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosDeVentasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetas.Show()
    End Sub

    Private Sub IngresosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetasIngresos.Show()
    End Sub

    Private Sub CarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CarteraToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetasCarteras.Show()
    End Sub

    Private Sub MedidoresToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MedidoresToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetasRep.Show()
    End Sub


    Private Sub GrupoDeVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GrupoDeVentasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmGrupoVentas.Show()
    End Sub

    Private Sub BitacoraDelSistemaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BitacoraDelSistemaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 19
        Acceso_TipoServicios.Show()
    End Sub



    Private Sub AgendaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgendaToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwAgenda.Show()
    End Sub

    Private Sub IndividualesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IndividualesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetasInd.Show()
    End Sub

    Private Sub CargosEspecialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'FrmCargosEspeciales.Show()
        My.Forms.BrwCargosEspeciales.Show()
    End Sub

    Private Sub CargosEspecialesToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCargoBonif.Show()

    End Sub

    Private Sub Medidores2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmMetasRep2.Show()
    End Sub

    Private Sub DescuentosComboToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescuentosComboToolStripMenuItem.Click
        'MsgBox("Temporalmente en fuera de servicio.")
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwDescuentoCombo.Show()
    End Sub


    Private Sub PaisesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPaises.Show()
    End Sub

    Private Sub TipoPaquetesAdicionalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwTipoPaquetesAdicTel.Show()
    End Sub

    Private Sub NumérosDeTeléfonoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCatalogoTelefonos.Show()
    End Sub

    Private Sub PaquetesAdicionalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPaqueteAdicional.Show()
    End Sub

    Private Sub ServiciosDigitalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwServiciosDig.Show()
    End Sub

    Private Sub ImporteDeMensualidadesAdelantadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImporteDeMensualidadesAdelantadasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        reporte_Importe_mensualidades_adelantados()
    End Sub


    Private Sub PPVToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepPPV.Show()
    End Sub

    Private Sub EstadoDeCuentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadoDeCuentaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        LEdo_Cuenta = True
        LEdo_Cuenta2 = False
        FrmEstado_Cuenta.Comienzo_Reporte()
        FrmTipoClientes.Show()
    End Sub
    Public Sub reporte_combo()
        Dim CON As New SqlConnection(MiConexion)
        Dim CON1 As New SqlConnection(MiConexion)
        LocOp = 90
        CON.Open()
        Me.Valida_periodo_reportesTableAdapter.Connection = CON
        Me.Valida_periodo_reportesTableAdapter.Fill(Me.DataSetarnoldo.Valida_periodo_reportes, LocValidaHab)
        CON.Close()
        If LocClv_session > 0 Then
            CON.Open()
            FrmImprimirContrato.borra_tablas_Reporte_clientes_combo(LocClv_session)
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        ElseIf LocClv_session = 0 Then
            CON1.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON1
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON1.Close()
        End If
        FrmTipoClientes.Show()
    End Sub

    Private Sub ReporteClientesConComboToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteClientesConComboToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If (GloTipoUsuario = 40 And IdSistema = "AG") Or (IdSistema <> "AG") Then
            reporte_combo()
        ElseIf GloTipoUsuario <> 40 Then
            LReportecombo = True
            FrmAccesoReportes.Show()
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim CON As New SqlConnection(MiConexion)
        Try
            Dim Respuesta As MsgBoxResult
            Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
            If Respuesta = MsgBoxResult.Ok Then
                bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Primer Periodo", "", "Generó Proceso de Desconexión Primer Periodo", GloSucursal, LocClv_Ciudad)
                CON.Open()
                Me.BackgroundWorker1.ReportProgress(50)
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "EXEC Proceso_Cierre_Mes_PRIMER_Periodo 1"
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .ExecuteReader()
                    e.Result = comando
                End With
                CON.Close()
                Me.BackgroundWorker1.ReportProgress(100)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        'Me.ProgressBar1.Value = e.ProgressPercentage
        'If v.Status <> StatusInfo.Playing Then
        '    v.Repeat = True
        '    v.Size = Me.Panel1.Size
        '    v.Location = Me.Panel1.Location
        '    v.Play()
        'End If
        If Formtrabajando.Visible = False Then
            Formtrabajando.Show()
        End If
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'MsgBox(" Proceso Finalizado con Éxito ")
        'v.StopPlay()
        'v.Dispose()
        Formtrabajando.Close()
    End Sub

    Private Sub BackgroundWorker2_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
        Dim CON As New SqlConnection(MiConexion)
        Try
            Dim Respuesta As MsgBoxResult
            Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Segundo Periodo > ?", MsgBoxStyle.OkCancel)
            If Respuesta = MsgBoxResult.Ok Then
                bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Segundo Periodo", "", "Generó Proceso de Desconexión Segundo Periodo", GloSucursal, LocClv_Ciudad)
                CON.Open()
                Me.BackgroundWorker2.ReportProgress(50)
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "EXEC Proceso_Cierre_Mes_Primer_Periodo 2"
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .ExecuteReader()
                    e.Result = comando
                End With
                CON.Close()
                Me.BackgroundWorker2.ReportProgress(100)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorker2_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker2.ProgressChanged
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If Formtrabajando.Visible = False Then
            Formtrabajando.Show()
        End If
    End Sub

    Private Sub BackgroundWorker2_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker2.RunWorkerCompleted

        Formtrabajando.Close()
    End Sub

    Private Sub CarteraEjecutivaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CarteraEjecutivaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelFechaCarteraEjec.Show()
    End Sub

    Private Sub ServiciosContratadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 55
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ListadoDeClientesPorStausConAdeudoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        LEdo_Cuenta2 = True
        LEdo_Cuenta = False
        FrmEstado_Cuenta.Comienzo_Reporte()
        FrmTipoClientes.Show()
    End Sub

    Private Sub CancelacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 56
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub MarcacionesEspecialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BwrMarcacion.Show()
    End Sub

    Private Sub EquiposALaVentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BwrEquipoVenta.Show()
    End Sub

    Private Sub BitácoraDeCorreosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BitácoraDeCorreosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 57
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub CódigosMéxicoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BwrCodigosMexico.Show()
    End Sub

    Private Sub VentasTotalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 61
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ServicioBasicoYCanalesPremiumToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServicioBasicoYCanalesPremiumToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If IdSistema = "SA" Then
            opcFrm = 16
        End If
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub InternetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InternetToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If IdSistema = "SA" Then
            opcFrm = 26
        End If
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub PuntosDeAntigüedadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 100
        FrmSelFechasPPE.Show()

    End Sub

    Private Sub InterfazCablemodemsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfazCablemodemsToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 62
        eOpCNRCNRDIG = 0
        eServicio = "de Cablemodems"
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub InterfazDigitalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfazDigitalesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 62
        eOpCNRCNRDIG = 1
        eServicio = "Digital"
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ResumenDeClientesPorServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpVentas = 63
        FrmImprimirComision.Show()
    End Sub

    Private Sub TarifasEspecialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmTarifasEspeciales.Show()
    End Sub

    Private Sub ReferenciasBancariasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwRefBancarias.Show()
    End Sub

    Private Sub RecuperaciónDeCarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwClientesPorRecuperar.Show()
    End Sub

    Private Sub CortesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCortes.Show()
    End Sub

    Private Sub AuxiliarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpVentas = 64
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub GeneralesProsaBancomerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesProsaBancomerToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 20
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub EstadoDeCuentaTelefoníaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        ImpresionEstadosDeCuenta.Show()
        'Dim ProcessId As Integer = Nothing
        'ProcessId = Shell("Http://" & GloServerName & "/llamadasweb/consultas.aspx")
        'ProcessId = Shell("http://localhost:3448/LlamadasWeb/consultas.aspx")
        'http://localhost:3448/LlamadasWeb/imprime.aspx
    End Sub

    Private Sub ReprocesamientoPorClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmReprocesamiento.Show()
    End Sub

    Private Sub ProcesamientoDeLlamadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmProcesamiento_Llamadas.Show()
    End Sub

    Private Sub ProcesamientoDeCDRToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmProcesamiento_CDR.Show()
    End Sub

    Private Sub ImpresiónDeEstadosDeCuentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Dim ImpresionEstadosDeCuenta_DEV As New ImpresionEdoCuenta_DEV
        ImpresionEstadosDeCuenta_DEV.Show()
        'ImpresionEstadosDeCuenta.Show()
    End Sub

    Private Sub AuxToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpVentas = 64
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ReferenciasBancariasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwRefBancarias.Show()
    End Sub

    Private Sub ImpresiónDeEstadosDeCuentaPorPáginaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        ImpresionEstadosDeCuenta2.Show()
    End Sub

    Private Sub ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        ImpresionEstadosDeCuenta2.Show()
    End Sub

    Private Sub HotelesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HotelesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpVentas = 65
        FrmImprimirComision.Show()
    End Sub

    Private Sub OrdenesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdenesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelFechasRep.Show()
    End Sub

    Private Sub GerencialToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpVentas = 67
        FrmImprimirComision.Show()
    End Sub

    Private Sub CancelacionDeVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 68
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ReporteDePermanenciaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteDePermanenciaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 69
        FrmRepPermanencia.Show()
    End Sub

    Private Sub ResumenVentasPorStatusToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 70
        FrmRepPermanencia.Show()
    End Sub

    Private Sub ReseteoMasivoDeAparatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReseteoMasivoDeAparatosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Dim nuevo As ProcesoReseteoMasivo = New ProcesoReseteoMasivo
        nuevo.Show()
    End Sub

    Private Sub EnviarSMSMasivoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Using CON As New SqlConnection(MiConexion)
            CON.Open()
            LocOp = 23
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            GloClv_tipser2 = 0
            CON.Close()
        End Using
        sms = True
        FrmTipoClientes.Show()
    End Sub

    Private Sub ValorDeCarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelFecha.Show()
    End Sub

    Private Sub MaterialesUtilizadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Using CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        End Using
        rvalida = True
        FrmSelTecnico_Rep.Show()
    End Sub

    Private Sub ReporteDeAntigüedadDeCancelacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 72
        FrmRepPermanencia.Show()
    End Sub

    Private Sub CancelacionesPorEjecutivoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 73
        FrmSelTipSerFechas.Show()
    End Sub

    Private Sub PlazoForzosoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlazoForzosoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmPlazoForzoso.Show()
    End Sub

    Private Sub HistorialDesconexionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        rdesconexiones = True
        FrmSelTipSerFechas.Show()
    End Sub

    Private Sub QuitarServiciosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        rquitaServ = True
        FrmSelTipServE.Show()
    End Sub

    Private Sub MensajesPrefijosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MensajesPrefijosToolStripMenuItem.Click

    End Sub

    Private Sub ServiciosPPEToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmServiciosPPE.Show()
    End Sub

    Private Sub BajaDeServiciosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BajaDeServiciosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmProcesoDeBaja.Show()
    End Sub

    Private Sub InterfazPPEToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCnrPPE.Show()
    End Sub

    Private Sub ProspectosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwClientesProspectos.Show()
    End Sub

    Private Sub PreguntasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPreguntas.Show()
    End Sub

    Private Sub CatalogoDeMensajesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CatalogoDeMensajesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwMensajesPrefijos.Show()
    End Sub

    Private Sub TipoDeMensajesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoDeMensajesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmTipoMensajes.Show()
    End Sub

    Private Sub PrefijosDeClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrefijosDeClientesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmPrefijos.Show()
    End Sub

    'Private Sub MensajesInstantáneosPersonalizadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MensajesInstantáneosPersonalizadosToolStripMenuItem.Click

    'End Sub

    'Private Sub NuevoMensajeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoMensajeToolStripMenuItem.Click
    '    Module1.clvSessionMensajes()
    '    FrmSelCiudad.Show()
    'End Sub


    Private Sub CuestionarioTMKToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwQuiz.Show()
    End Sub


    Private Sub NuevaProgramaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevaProgramaciónToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Module1.clvSessionMensajes()
        FrmSelCiudad.Show()
    End Sub

    Private Sub CtgMsjPerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CtgMsjPerToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwProgramacionPersonalizados.Show()
    End Sub

    Private Sub EncuestasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwEncuestas.Show()
    End Sub

    Private Sub ProspectosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'FrmReporteProspectos.Show() //Actualizado el 19 de Septiembre de 2011
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepProspectos
        frm.ShowDialog()
    End Sub



    Private Sub RecontrataciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecontrataciónToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRecontratacion.Show()
    End Sub

    Private Sub RecontratacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecontratacionesToolStripMenuItem.Click
        'eOpPPE = 0
        'eOpVentas = 77
        'FrmSelFechasPPE.Show()
    End Sub

    Private Sub PagosDiferidosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepPagosDif.Show()
    End Sub

    Private Sub DecodificadoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmDecodificadoresDig.Show()
    End Sub

    Private Sub PrecioADecodificadoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmDetDecodificadores.Show()
    End Sub

    Private Sub DecodificadoresToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DecodificadoresToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eBndDeco = True
        DameSession()
        FrmSelCiudad.Show()


    End Sub

    Private Sub DameSession()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Dame_clv_session_Reportes", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Output
        com.Parameters.Add(par)

        Try
            con.Open()
            com.ExecuteNonQuery()
            LocClv_session = par.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub CostoAparatosPagaréToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwTarifasPagare.Show()
    End Sub

    Private Sub SeriesSATToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwSucursalesCFD.Show()
    End Sub

    Private Sub RetiroDeAparatoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepOrdSer.Show()
    End Sub

    Private Sub PromociónPrimerMensualidadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmPromocionPrimerMensualidad.Show()
    End Sub

    Private Sub PrioridadDeCajasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        Dim frm As New SoftvMod.FrmPrioridadServicioCaja
        frm.ShowDialog()
    End Sub

    Private Sub RentaACajasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RentaACajasToolStripMenuItem.Click
        'SoftvMod.VariablesGlobales.MiConexion = MiConexion
        'Dim frm As New SoftvMod.FrmRentaCajas
        'frm.ShowDialog()

        BrwTarifasPagare.Show()
    End Sub

    Private Sub PromociónAntigüedadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        Dim frm As New SoftvMod.FrmPromocion
        frm.ShowDialog()
    End Sub

    Private Sub AplicarPromociónDeAntigüedadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim LocNomImpresora_Tarjetas As String
        Dim LocNomImpresora_Contratos As String
        Dim CON As New SqlConnection(MiConexion)

        CON.Open()
        Me.Selecciona_Impresora_SucursalTableAdapter.Connection = CON
        Me.Selecciona_Impresora_SucursalTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora_Sucursal, GloClv_Sucursal, LocNomImpresora_Tarjetas, LocNomImpresora_Contratos)
        CON.Close()
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.GloServerName = GloServerName
        SoftvMod.VariablesGlobales.GloDatabaseName = GloDatabaseName
        SoftvMod.VariablesGlobales.GloUserID = GloUserID
        SoftvMod.VariablesGlobales.GloPassword = GloPassword
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        SoftvMod.VariablesGlobales.LocNomImpresora_Contratos = LocNomImpresora_Contratos
        SoftvMod.VariablesGlobales.Clv_Usuario = GloUsuario
        Dim frm As New SoftvMod.FrmAsignarPromocion
        frm.ShowDialog()
    End Sub

    Private Sub PromociónDeAntigüedadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepPromocion
        frm.ShowDialog()
    End Sub

    Private Sub PromocionDeRecuperacionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        Dim frm As New SoftvMod.BrwPromocionRecuperacion
        frm.Show()
    End Sub

    Private Sub AplicarPromocionDescuentoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim LocNomImpresora_Tarjetas As String
        Dim LocNomImpresora_Contratos As String
        Dim CON As New SqlConnection(MiConexion)

        CON.Open()
        Me.Selecciona_Impresora_SucursalTableAdapter.Connection = CON
        Me.Selecciona_Impresora_SucursalTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora_Sucursal, GloClv_Sucursal, LocNomImpresora_Tarjetas, LocNomImpresora_Contratos)
        CON.Close()
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.GloServerName = GloServerName
        SoftvMod.VariablesGlobales.GloDatabaseName = GloDatabaseName
        SoftvMod.VariablesGlobales.GloUserID = GloUserID
        SoftvMod.VariablesGlobales.GloPassword = GloPassword
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        SoftvMod.VariablesGlobales.LocNomImpresora_Contratos = LocNomImpresora_Contratos
        SoftvMod.VariablesGlobales.Clv_Usuario = GloUsuario
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        Dim frm As New SoftvMod.FrmAsignarPromocionRecuperacion
        frm.Show()
    End Sub

    Private Sub ReportePromocionDeRecuperacionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepPromocionRecuperacion
        frm.ShowDialog()
    End Sub

    Private Sub ListadoBuroDeCreditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As FrmListadoDeBuroDeCredito = New FrmListadoDeBuroDeCredito()
        frm.ShowDialog()
    End Sub

    Private Sub RetencionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        Dim frm As New SoftvMod.BrwRetenciones
        frm.ShowDialog()
    End Sub

    Private Sub RetencionesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepRetenciones
        frm.ShowDialog()
    End Sub

    Private Sub ListaDeCumpleañosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListaDeCumpleañosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepCumpleanosDeLosClientes
        frm.ShowDialog()
    End Sub

    Private Sub GráficaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eBndRepEncuesta = True
        eOpVentas = 76
        FrmSelEncuesta.Show()
    End Sub

    Private Sub DetalladoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepEncuesta
        frm.ShowDialog()
    End Sub

    Private Sub DetalladoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpPPE = 0
        eOpVentas = 77
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ListadoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepRecontratacion
        frm.ShowDialog()
    End Sub

    Private Sub GráficoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eBndRepEncuesta = True
        eOpVentas = 76
        FrmSelEncuesta.Show()
    End Sub

    Private Sub DetalladoToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepEncuesta
        frm.ShowDialog()
    End Sub

    Private Sub EncuestasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'eBndRepEncuesta = True
        'eOpVentas = 76
        'FrmSelEncuesta.Show()
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.FrmRepEncuesta
        frm.ShowDialog()
    End Sub

    Private Sub NivelesDeRiesgoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        SoftvMod.VariablesGlobales.OpReporte = 9
        SoftvMod.VariablesGlobales.IdSession = SoftvMod.VariablesGlobales.DameSession()
        Dim frm As New SoftvMod.FrmStatusTmp
        frm.ShowDialog()
    End Sub

    Private Sub NivelesDeRiesgoToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        Dim frm As New SoftvMod.BrwNivelesDeRiesgo
        frm.ShowDialog()
    End Sub

#Region "Catálogos"

    '                               Inicio de Catálogos de Telefonía
    '*********************************************************************************



    '                               Fin de Catálogos de Telefonía
    '*********************************************************************************

#End Region

    Private Sub ClasificaciónDeTarifasDeLlamadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwClasificacionLlamadas.Show()
    End Sub

    Private Sub ÓrdenesClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ÓrdenesClientesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelFechasRep.Show()
    End Sub

    Private Sub PostesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PostesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCatalogoPostes.Show()
    End Sub

    Private Sub ClasificaciónProblemasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClasificaciónProblemasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwClasificacionProblemas.Show()
    End Sub

    Private Sub RegresaAparatosAlAlmacenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresaAparatosAlAlmacenToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRegresaAparatos.Show()
    End Sub

    Private Sub ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguraciónDeCambiosDeTipoDeServicioToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCambioTipoServicio.Show()

    End Sub

    Private Sub PrórrogasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrórrogasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmProrrogas.Show()
    End Sub

    Private Sub VALIDAAlertaProrroga(ByVal Clv_Usuario As String)
      
        Dim Alerta As Boolean = False
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, Clv_Usuario)
        BaseII.CreateMyParameter("@Alerta", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("VALIDAAlertaProrroga")
        Alerta = Boolean.Parse(BaseII.dicoPar("@Alerta").ToString())

        If Alerta = True Then
            OpProrroga = 2
            eFechaIni = DateTime.Today
            eFechaFin = DateTime.Today
            eOpVentas = 99
            FrmImprimirComision.Show()
        End If

    End Sub

    Private Sub PromesasDePagoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PromesasDePagoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepProrroga.Show()
    End Sub

    Private Sub EstablecerComisionesCobroADomicilioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstablecerComisionesCobroADomicilioToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmComisionesCobroADomicilio.Show()

    End Sub

    Private Sub CobrosADomicilioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobrosADomicilioToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepCobrosADomicilio.Show()
    End Sub

    Private Sub ClaveTécnicaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepClientesClaveTecnica.Show()
    End Sub

    Private Sub PruebaDeInternetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PruebaDeInternetToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmPruebaInternet.Show()
    End Sub

    Private Sub PruebasDeInternetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PruebasDeInternetToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepPruebaInternet.Show()
    End Sub

    Private Sub InterfazCablemodemsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfazCablemodemsToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 3
        'BRWBUSCACNR.Show()
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ClaveTécnicaToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClaveTécnicaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepClientesClaveTecnica.Show()
    End Sub

    Private Sub DevoluciónDeAparatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DevoluciónDeAparatosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRegresarAparatosAlmacen.Show()
    End Sub

    Private Sub DevoluciónDeAparatosAlAlmacénToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DevoluciónDeAparatosAlAlmacénToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        Dim frm As New SoftvMod.MTY.FrmRepDevolucionAparatosAlmacen
        frm.Show()
    End Sub

    Private Sub GenerarÓrdenesDeDesconexiónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GenerarÓrdenesDeDesconexiónToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmDesconexionSeleccionCiudades.Show()
    End Sub

    Private Sub FoliosFaltantesYCanceladosDeLosVendedoresToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        'Try
        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
        '    BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt,eClv_Session)

        '    Dim listNombreTablas As New List(Of String)
        '    listNombreTablas.Add("USPReporteFoliosVentas")

        '    Dim DS As DataSet = BaseII.ConsultaDS("USPReporteFoliosVentas", listNombreTablas)

        '    Dim diccioFormulasReporte As New Dictionary(Of String, String)
        '    diccioFormulasReporte.Add("Empresa", LocGloNomEmpresa)
        '    diccioFormulasReporte.Add("Titulo", "Reporte de Folios Faltantes y Cancelados")
        '    diccioFormulasReporte.Add("Sucursal", GloCiudadEmpresa)

        '    BaseII.llamarReporteCentralizado(RutaReportes + "\rptFoliosVentas", DS, diccioFormulasReporte)
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

        eOpVentas = 2310


        'JUAN PABLO REPORTES
        'Dim selDiaMetas As New FrmSelDiaMetas()
        Dim selVendedor As New FrmSelVendedor()
        Dim imprimirCentralizada As New FrmImprimirCentralizada()
        Dim reporte As New ReportDocument()


        If (selVendedor.ShowDialog() = DialogResult.OK) Then
            Try
                'ruta = RutaReportes & "\ReporteServicioNivel2.rpt"
                ''ruta = "D:\ReporteServicioNivel2.rpt"

                'reporte.Load(ruta)
                'reporte.SetDataSource(_DataSet)

                'imprimirCentralizada.rd = reporte
                'imprimirCentralizada.ShowDialog()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, eClv_Session)

                Dim listNombreTablas As New List(Of String)
                listNombreTablas.Add("USPReporteFoliosVentas")

                Dim DS As DataSet = BaseII.ConsultaDS("USPReporteFoliosVentas", listNombreTablas)

                Dim diccioFormulasReporte As New Dictionary(Of String, String)
                diccioFormulasReporte.Add("Empresa", LocGloNomEmpresa)
                diccioFormulasReporte.Add("Titulo", "Reporte de Folios Faltantes y Cancelados")
                diccioFormulasReporte.Add("Sucursal", GloCiudadEmpresa)

                BaseII.llamarReporteCentralizado(RutaReportes + "\rptFoliosVentas", DS, diccioFormulasReporte)
                'Else
                'MsgBox("Seleccione el Vendedor(es)", MsgBoxStyle.Information)
                'Exit Sub
                'End If

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information)
            End Try

        End If
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCambioDeTipoDeServicio.Show()
    End Sub

    Private Sub ToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem2.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmTarifasFijas.Show()
    End Sub

    '''''''''''''''''PARA PERFILES LLENA TABLA MENU...............INICIO
    Private Sub UspMostrarMenusCliente()
        Try
            Dim NOMBREMENU As ToolStripMenuItem
            Dim NOMBRESUBMENU As ToolStripMenuItem

            Dim I, J, K As Integer
            For I = 0 To MenuStrip1.Items.Count - 1
                Me.MenuStrip1.Items(I).Visible = UspValidaMenu(Me.MenuStrip1.Items(I).Name)
                NOMBREMENU = Me.MenuStrip1.Items(I)
                For J = 0 To NOMBREMENU.DropDownItems.Count - 1
                    NOMBREMENU.DropDownItems(J).Visible = UspValidaMenu(NOMBREMENU.DropDownItems(J).Name)
                    NOMBRESUBMENU = NOMBREMENU.DropDownItems(J)
                    For K = 0 To NOMBRESUBMENU.DropDownItems.Count - 1
                        NOMBRESUBMENU.DropDownItems(K).Visible = UspValidaMenu(NOMBRESUBMENU.DropDownItems(K).Name)
                    Next
                Next
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '''''''''''''''''PARA PERFILES LLENA TABLA MENU...............FIN

    Private Sub PerfilesSISTEToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerfilesSISTEToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmPerfilesSiste.Show()

    End Sub

    Private Sub PerfilesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerfilesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmPerfilesUsuarios.Show()

    End Sub

    Private Sub RecordatoriosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecordatoriosToolStripMenuItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        
        Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
        Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)

        bnd1 = True
        LocOp = 95
        FrmSelPeriodo.Show()

        'Recordatorio = 1
        'GloOpEtiqueta = "1"
        'LocOp = 7
        'FrmTipoClientes.Show()
    End Sub

    Private Sub TipoDeGastosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TipoDeGastosToolStripMenuItem.Click
        BrwCatalogoGastos.Show()
    End Sub

    Private Sub MensualidadesTotalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MensualidadesTotalesToolStripMenuItem.Click
        eOpVentas = 101
        FrmImprimirComision.Show()


    End Sub

    Private Sub CostoPorAparatoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CostoPorAparatoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwTarifasPagare.Show()
    End Sub

    Private Sub TipoUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoUsuarioToolStripMenuItem.Click
        BrwTipoUsuario.Show()
    End Sub

    Private Sub ProcesosDeServicioPremiumToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesosDeServicioPremiumToolStripMenuItem.Click

    End Sub

    Private Sub TvsAdicionalesPorCiudadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TvsAdicionalesPorCiudadToolStripMenuItem.Click
        FrmCiudadTv.Show()
    End Sub

    Private Sub IndicadoresPagosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IndicadoresPagosToolStripMenuItem.Click
        TarifasEco.Show()
    End Sub

    Private Sub RoboDeSeñalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RoboDeSeñalToolStripMenuItem.Click


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        LocClv_session = BaseII.dicoPar("@Clv_Session").ToString
        LocOp = 100
        FrmSelCiudad.Show()

    End Sub

    Private Sub ReporteSTBToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteSTBToolStripMenuItem.Click
        FrmTipoStb.Show()
    End Sub

    Private Sub CambioDePeriodoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioDePeriodoToolStripMenuItem1.Click
        FrmCambiosPeriodo.Show()
    End Sub

    Private Sub HistorialDeCambiosDePeriodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HistorialDeCambiosDePeriodoToolStripMenuItem.Click
        EsqoPer = "P"
        BrwCambioEsquema.Show()
    End Sub

    Private Sub DdToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DdToolStripMenuItem.Click
        BrwCambiaClientePlaca.Show()
    End Sub

    Private Sub InterfazDigitalesXcriptToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfazDigitalesXcriptToolStripMenuItem.Click
        opcFrm = 21
        'BRWCNRDIGx.Show()
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ResumenEjecutivoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenEjecutivoToolStripMenuItem.Click
        LocGloOpRep = 28
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ProcesoMasivoCambioDeAparatoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesoMasivoCambioDeAparatoToolStripMenuItem.Click
        gloClvTipSerReportes = 0
        If frmFiltroTipoServicio.ShowDialog = DialogResult.OK Then
            gloClvTipSerReportes = frmFiltroTipoServicio.cmbTipoServicio.SelectedValue
            LocClv_session = DAMESclv_Sessionporfavor()
            LocOp = 2000
            LocMasivaCapar = True
            FrmSelCiudad.Show()
        End If
    End Sub

    Private Sub HUBToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles HUBToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwHub.Show()
    End Sub

    Private Sub OLTToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles OLTToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCatalogoOlt.Show()
    End Sub


    Private Sub NAPToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NAPToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwNap.Show()
    End Sub

    Private Sub VelocidadesDeInternetFTTHToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles VelocidadesDeInternetFTTHToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwVelInternet.Show()
    End Sub

    Private Sub InterfazFtthToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InterfazFtthToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        opcFrm = 21
        BRWBUSCACNRONU.Show()
    End Sub

    Private Sub ResumenOrdenesQuejasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResumenOrdenesQuejasToolStripMenuItem.Click
        Dim CON1 As New SqlConnection(MiConexion)
        CON1.Open()
        Me.DameClv_Session_ServiciosTableAdapter.Connection = CON1
        Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        CON1.Close()
        'varfrmselcompania = "ResumenOrdenQueja"
        rOrdenQueja = True
        FrmFiltroReporteResumen.Show()
    End Sub

    Private Sub ProcesosComcastToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProcesosComcastToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmProceComcast.Show()
    End Sub

    Private Sub DireccionesIPToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DireccionesIPToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenu(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwCatalogoIP.Show()
    End Sub
End Class
