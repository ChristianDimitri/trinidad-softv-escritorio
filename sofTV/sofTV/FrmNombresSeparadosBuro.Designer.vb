﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmNombresSeparadosBuro
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CMBLabel52 As System.Windows.Forms.Label
        Dim CMBLabel51 As System.Windows.Forms.Label
        Dim CMBLabel50 As System.Windows.Forms.Label
        Dim CMBLabel2 As System.Windows.Forms.Label
        Dim CMBLabel1 As System.Windows.Forms.Label
        Dim CMBLabel3 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtSegundoNombre = New System.Windows.Forms.TextBox()
        Me.txtApellidoMaterno = New System.Windows.Forms.TextBox()
        Me.txtApellidoPaterno = New System.Windows.Forms.TextBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.dtpFechaNacimiento = New System.Windows.Forms.DateTimePicker()
        Me.txtRFC = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.TxtCI = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CMBLabel52 = New System.Windows.Forms.Label()
        CMBLabel51 = New System.Windows.Forms.Label()
        CMBLabel50 = New System.Windows.Forms.Label()
        CMBLabel2 = New System.Windows.Forms.Label()
        CMBLabel1 = New System.Windows.Forms.Label()
        CMBLabel3 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CMBLabel52
        '
        CMBLabel52.AutoSize = True
        CMBLabel52.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel52.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel52.Location = New System.Drawing.Point(12, 9)
        CMBLabel52.Name = "CMBLabel52"
        CMBLabel52.Size = New System.Drawing.Size(105, 15)
        CMBLabel52.TabIndex = 0
        CMBLabel52.Text = "Primer Nombre"
        '
        'CMBLabel51
        '
        CMBLabel51.AutoSize = True
        CMBLabel51.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel51.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel51.Location = New System.Drawing.Point(268, 54)
        CMBLabel51.Name = "CMBLabel51"
        CMBLabel51.Size = New System.Drawing.Size(116, 15)
        CMBLabel51.TabIndex = 0
        CMBLabel51.Text = "Apellido Materno"
        '
        'CMBLabel50
        '
        CMBLabel50.AutoSize = True
        CMBLabel50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel50.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel50.Location = New System.Drawing.Point(12, 54)
        CMBLabel50.Name = "CMBLabel50"
        CMBLabel50.Size = New System.Drawing.Size(113, 15)
        CMBLabel50.TabIndex = 0
        CMBLabel50.Text = "Apellido Paterno"
        '
        'CMBLabel2
        '
        CMBLabel2.AutoSize = True
        CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel2.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel2.Location = New System.Drawing.Point(265, 9)
        CMBLabel2.Name = "CMBLabel2"
        CMBLabel2.Size = New System.Drawing.Size(173, 15)
        CMBLabel2.TabIndex = 0
        CMBLabel2.Text = "Segundo o mas  Nombres"
        '
        'CMBLabel1
        '
        CMBLabel1.AutoSize = True
        CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel1.Location = New System.Drawing.Point(12, 96)
        CMBLabel1.Name = "CMBLabel1"
        CMBLabel1.Size = New System.Drawing.Size(123, 15)
        CMBLabel1.TabIndex = 7
        CMBLabel1.Text = "Fecha Nacimiento"
        '
        'CMBLabel3
        '
        CMBLabel3.AutoSize = True
        CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel3.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel3.Location = New System.Drawing.Point(268, 96)
        CMBLabel3.Name = "CMBLabel3"
        CMBLabel3.Size = New System.Drawing.Size(29, 15)
        CMBLabel3.TabIndex = 9
        CMBLabel3.Text = "NIT"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(12, 142)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(28, 15)
        Label1.TabIndex = 11
        Label1.Text = "C.I."
        '
        'txtNombre
        '
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(12, 30)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(250, 21)
        Me.txtNombre.TabIndex = 1
        '
        'txtSegundoNombre
        '
        Me.txtSegundoNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSegundoNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSegundoNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSegundoNombre.Location = New System.Drawing.Point(268, 30)
        Me.txtSegundoNombre.Name = "txtSegundoNombre"
        Me.txtSegundoNombre.Size = New System.Drawing.Size(250, 21)
        Me.txtSegundoNombre.TabIndex = 2
        '
        'txtApellidoMaterno
        '
        Me.txtApellidoMaterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtApellidoMaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApellidoMaterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApellidoMaterno.Location = New System.Drawing.Point(268, 72)
        Me.txtApellidoMaterno.Name = "txtApellidoMaterno"
        Me.txtApellidoMaterno.Size = New System.Drawing.Size(250, 21)
        Me.txtApellidoMaterno.TabIndex = 4
        '
        'txtApellidoPaterno
        '
        Me.txtApellidoPaterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtApellidoPaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApellidoPaterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApellidoPaterno.Location = New System.Drawing.Point(12, 72)
        Me.txtApellidoPaterno.Name = "txtApellidoPaterno"
        Me.txtApellidoPaterno.Size = New System.Drawing.Size(250, 21)
        Me.txtApellidoPaterno.TabIndex = 3
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(406, 160)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(124, 33)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(271, 160)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(129, 33)
        Me.btnGuardar.TabIndex = 5
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'dtpFechaNacimiento
        '
        Me.dtpFechaNacimiento.CustomFormat = "dd/MM/yyyy"
        Me.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaNacimiento.Location = New System.Drawing.Point(15, 115)
        Me.dtpFechaNacimiento.Name = "dtpFechaNacimiento"
        Me.dtpFechaNacimiento.Size = New System.Drawing.Size(247, 20)
        Me.dtpFechaNacimiento.TabIndex = 8
        '
        'txtRFC
        '
        Me.txtRFC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRFC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRFC.Location = New System.Drawing.Point(268, 114)
        Me.txtRFC.Name = "txtRFC"
        Me.txtRFC.Size = New System.Drawing.Size(250, 21)
        Me.txtRFC.TabIndex = 10
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'TxtCI
        '
        Me.TxtCI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtCI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtCI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCI.Location = New System.Drawing.Point(12, 160)
        Me.TxtCI.Name = "TxtCI"
        Me.TxtCI.Size = New System.Drawing.Size(250, 21)
        Me.TxtCI.TabIndex = 12
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmNombresSeparadosBuro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(543, 205)
        Me.Controls.Add(Me.TxtCI)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.txtRFC)
        Me.Controls.Add(CMBLabel3)
        Me.Controls.Add(Me.dtpFechaNacimiento)
        Me.Controls.Add(CMBLabel1)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(CMBLabel2)
        Me.Controls.Add(Me.txtApellidoMaterno)
        Me.Controls.Add(Me.txtApellidoPaterno)
        Me.Controls.Add(Me.txtSegundoNombre)
        Me.Controls.Add(CMBLabel52)
        Me.Controls.Add(CMBLabel51)
        Me.Controls.Add(CMBLabel50)
        Me.Controls.Add(Me.txtNombre)
        Me.Name = "FrmNombresSeparadosBuro"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nombre Separado"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Public WithEvents txtNombre As System.Windows.Forms.TextBox
    Public WithEvents txtSegundoNombre As System.Windows.Forms.TextBox
    Public WithEvents txtApellidoMaterno As System.Windows.Forms.TextBox
    Public WithEvents txtApellidoPaterno As System.Windows.Forms.TextBox
    Public WithEvents dtpFechaNacimiento As System.Windows.Forms.DateTimePicker
    Public WithEvents txtRFC As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Public WithEvents TxtCI As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
