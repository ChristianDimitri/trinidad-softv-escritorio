<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCatalogoSector
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim SectorLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCatalogoSector))
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.CONCatalogoSectorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.CALLEComboBox = New System.Windows.Forms.ComboBox()
        Me.MUESTRACALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DescripTextBox = New System.Windows.Forms.TextBox()
        Me.COLONIAComboBox = New System.Windows.Forms.ComboBox()
        Me.DAMECOLONIA_CALLEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CIUDADComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraCVECOLCIUBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SectorTextBox = New System.Windows.Forms.TextBox()
        Me.SectorComboBox = New System.Windows.Forms.ComboBox()
        Me.MUESTRASECTORESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SalidaTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.MUESTRACALLESTableAdapter = New sofTV.DataSetLidiaTableAdapters.MUESTRACALLESTableAdapter()
        Me.BUSCASectoresBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BUSCASectoresTableAdapter = New sofTV.DataSetLidiaTableAdapters.BUSCASectoresTableAdapter()
        Me.DAMECOLONIA_CALLETableAdapter = New sofTV.DataSetLidiaTableAdapters.DAMECOLONIA_CALLETableAdapter()
        Me.MuestraCVECOLCIUTableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraCVECOLCIUTableAdapter()
        Me.MUESTRACOLONIASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACOLONIASTableAdapter = New sofTV.DataSetLidiaTableAdapters.MUESTRACOLONIASTableAdapter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.CONCatalogoSectorBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONCatalogoSectorBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CONCatalogoSectorTableAdapter = New sofTV.DataSetLidiaTableAdapters.CONCatalogoSectorTableAdapter()
        Me.MUESTRASECTORESTableAdapter = New sofTV.DataSetLidiaTableAdapters.MUESTRASECTORESTableAdapter()
        CONTRATOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        CIUDADLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        SectorLabel = New System.Windows.Forms.Label()
        CType(Me.CONCatalogoSectorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECOLONIA_CALLEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.MUESTRASECTORESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCASectoresBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACOLONIASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.CONCatalogoSectorBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCatalogoSectorBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CONTRATOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CONTRATOLabel.Location = New System.Drawing.Point(108, 53)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(78, 15)
        CONTRATOLabel.TabIndex = 75
        CONTRATOLabel.Text = "Clave Tap :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel.Location = New System.Drawing.Point(141, 82)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(56, 15)
        NOMBRELabel.TabIndex = 77
        NOMBRELabel.Text = "Sector :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLELabel.ForeColor = System.Drawing.Color.LightSlateGray
        CALLELabel.Location = New System.Drawing.Point(149, 112)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(48, 15)
        CALLELabel.TabIndex = 79
        CALLELabel.Text = "Calle :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NUMEROLabel.Location = New System.Drawing.Point(135, 141)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(62, 15)
        NUMEROLabel.TabIndex = 80
        NUMEROLabel.Text = "Numero:"
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COLONIALabel.ForeColor = System.Drawing.Color.LightSlateGray
        COLONIALabel.Location = New System.Drawing.Point(143, 170)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(54, 15)
        COLONIALabel.TabIndex = 83
        COLONIALabel.Text = "Barrio :"
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CIUDADLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CIUDADLabel.Location = New System.Drawing.Point(16, 200)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(181, 15)
        CIUDADLabel.TabIndex = 85
        CIUDADLabel.Text = "Ciudad/Region/Municipio  :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(141, 229)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(56, 15)
        Label1.TabIndex = 87
        Label1.Text = "Salida :"
        '
        'SectorLabel
        '
        SectorLabel.AutoSize = True
        SectorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SectorLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SectorLabel.Location = New System.Drawing.Point(280, 54)
        SectorLabel.Name = "SectorLabel"
        SectorLabel.Size = New System.Drawing.Size(147, 15)
        SectorLabel.TabIndex = 88
        SectorLabel.Text = "Descripción del Tap  :"
        SectorLabel.Visible = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoSectorBindingSource, "Clv_Sector", True))
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.ForeColor = System.Drawing.Color.Red
        Me.ClaveTextBox.Location = New System.Drawing.Point(203, 49)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ClaveTextBox.Size = New System.Drawing.Size(75, 24)
        Me.ClaveTextBox.TabIndex = 76
        Me.ClaveTextBox.TabStop = False
        '
        'CONCatalogoSectorBindingSource
        '
        Me.CONCatalogoSectorBindingSource.DataMember = "CONCatalogoSector"
        Me.CONCatalogoSectorBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CALLEComboBox
        '
        Me.CALLEComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCatalogoSectorBindingSource, "Clv_Calle", True))
        Me.CALLEComboBox.DataSource = Me.MUESTRACALLESBindingSource
        Me.CALLEComboBox.DisplayMember = "NOMBRE"
        Me.CALLEComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CALLEComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLEComboBox.FormattingEnabled = True
        Me.CALLEComboBox.Location = New System.Drawing.Point(203, 109)
        Me.CALLEComboBox.Name = "CALLEComboBox"
        Me.CALLEComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CALLEComboBox.Size = New System.Drawing.Size(364, 24)
        Me.CALLEComboBox.TabIndex = 2
        Me.CALLEComboBox.ValueMember = "Clv_Calle"
        '
        'MUESTRACALLESBindingSource
        '
        Me.MUESTRACALLESBindingSource.DataMember = "MUESTRACALLES"
        Me.MUESTRACALLESBindingSource.DataSource = Me.DataSetLidia
        '
        'DescripTextBox
        '
        Me.DescripTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoSectorBindingSource, "Numero", True))
        Me.DescripTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripTextBox.Location = New System.Drawing.Point(203, 139)
        Me.DescripTextBox.Name = "DescripTextBox"
        Me.DescripTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DescripTextBox.Size = New System.Drawing.Size(75, 22)
        Me.DescripTextBox.TabIndex = 3
        '
        'COLONIAComboBox
        '
        Me.COLONIAComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCatalogoSectorBindingSource, "Clv_Colonia", True))
        Me.COLONIAComboBox.DataSource = Me.DAMECOLONIA_CALLEBindingSource
        Me.COLONIAComboBox.DisplayMember = "COLONIA"
        Me.COLONIAComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.COLONIAComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIAComboBox.FormattingEnabled = True
        Me.COLONIAComboBox.Location = New System.Drawing.Point(203, 167)
        Me.COLONIAComboBox.Name = "COLONIAComboBox"
        Me.COLONIAComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.COLONIAComboBox.Size = New System.Drawing.Size(364, 24)
        Me.COLONIAComboBox.TabIndex = 4
        Me.COLONIAComboBox.ValueMember = "CLV_COLONIA"
        '
        'DAMECOLONIA_CALLEBindingSource
        '
        Me.DAMECOLONIA_CALLEBindingSource.DataMember = "DAMECOLONIA_CALLE"
        Me.DAMECOLONIA_CALLEBindingSource.DataSource = Me.DataSetLidia
        '
        'CIUDADComboBox
        '
        Me.CIUDADComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCatalogoSectorBindingSource, "Clv_Ciudad", True))
        Me.CIUDADComboBox.DataSource = Me.MuestraCVECOLCIUBindingSource
        Me.CIUDADComboBox.DisplayMember = "Nombre"
        Me.CIUDADComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CIUDADComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADComboBox.FormattingEnabled = True
        Me.CIUDADComboBox.Location = New System.Drawing.Point(203, 197)
        Me.CIUDADComboBox.Name = "CIUDADComboBox"
        Me.CIUDADComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CIUDADComboBox.Size = New System.Drawing.Size(242, 24)
        Me.CIUDADComboBox.TabIndex = 5
        Me.CIUDADComboBox.ValueMember = "Clv_Ciudad"
        '
        'MuestraCVECOLCIUBindingSource
        '
        Me.MuestraCVECOLCIUBindingSource.DataMember = "MuestraCVECOLCIU"
        Me.MuestraCVECOLCIUBindingSource.DataSource = Me.DataSetLidia
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(SectorLabel)
        Me.Panel1.Controls.Add(Me.SectorTextBox)
        Me.Panel1.Controls.Add(Me.SectorComboBox)
        Me.Panel1.Controls.Add(Me.SalidaTextBox)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Controls.Add(CONTRATOLabel)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(Me.CIUDADComboBox)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(CIUDADLabel)
        Me.Panel1.Controls.Add(CALLELabel)
        Me.Panel1.Controls.Add(Me.COLONIAComboBox)
        Me.Panel1.Controls.Add(Me.CALLEComboBox)
        Me.Panel1.Controls.Add(COLONIALabel)
        Me.Panel1.Controls.Add(NUMEROLabel)
        Me.Panel1.Controls.Add(Me.DescripTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 37)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel1.Size = New System.Drawing.Size(631, 326)
        Me.Panel1.TabIndex = 87
        '
        'SectorTextBox
        '
        Me.SectorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SectorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoSectorBindingSource, "Sector", True))
        Me.SectorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectorTextBox.Location = New System.Drawing.Point(433, 52)
        Me.SectorTextBox.MaxLength = 30
        Me.SectorTextBox.Name = "SectorTextBox"
        Me.SectorTextBox.Size = New System.Drawing.Size(171, 21)
        Me.SectorTextBox.TabIndex = 0
        Me.SectorTextBox.TabStop = False
        Me.SectorTextBox.Visible = False
        '
        'SectorComboBox
        '
        Me.SectorComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCatalogoSectorBindingSource, "Clv_SectorMa", True))
        Me.SectorComboBox.DataSource = Me.MUESTRASECTORESBindingSource
        Me.SectorComboBox.DisplayMember = "Descripcion_Sector"
        Me.SectorComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SectorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectorComboBox.FormattingEnabled = True
        Me.SectorComboBox.Location = New System.Drawing.Point(203, 79)
        Me.SectorComboBox.Name = "SectorComboBox"
        Me.SectorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SectorComboBox.Size = New System.Drawing.Size(364, 24)
        Me.SectorComboBox.TabIndex = 1
        Me.SectorComboBox.ValueMember = "Clv_SectorMa"
        '
        'MUESTRASECTORESBindingSource
        '
        Me.MUESTRASECTORESBindingSource.DataMember = "MUESTRASECTORES"
        Me.MUESTRASECTORESBindingSource.DataSource = Me.DataSetLidia
        '
        'SalidaTextBox
        '
        Me.SalidaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SalidaTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SalidaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoSectorBindingSource, "Salida", True))
        Me.SalidaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SalidaTextBox.Location = New System.Drawing.Point(203, 227)
        Me.SalidaTextBox.MaxLength = 5
        Me.SalidaTextBox.Name = "SalidaTextBox"
        Me.SalidaTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SalidaTextBox.Size = New System.Drawing.Size(75, 22)
        Me.SalidaTextBox.TabIndex = 6
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(507, 369)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'MUESTRACALLESTableAdapter
        '
        Me.MUESTRACALLESTableAdapter.ClearBeforeFill = True
        '
        'BUSCASectoresBindingSource
        '
        Me.BUSCASectoresBindingSource.DataMember = "BUSCASectores"
        Me.BUSCASectoresBindingSource.DataSource = Me.DataSetLidia
        '
        'BUSCASectoresTableAdapter
        '
        Me.BUSCASectoresTableAdapter.ClearBeforeFill = True
        '
        'DAMECOLONIA_CALLETableAdapter
        '
        Me.DAMECOLONIA_CALLETableAdapter.ClearBeforeFill = True
        '
        'MuestraCVECOLCIUTableAdapter
        '
        Me.MuestraCVECOLCIUTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACOLONIASBindingSource
        '
        Me.MUESTRACOLONIASBindingSource.DataMember = "MUESTRACOLONIAS"
        Me.MUESTRACOLONIASBindingSource.DataSource = Me.DataSetLidia
        '
        'MUESTRACOLONIASTableAdapter
        '
        Me.MUESTRACOLONIASTableAdapter.ClearBeforeFill = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.CONCatalogoSectorBindingNavigator)
        Me.Panel2.Location = New System.Drawing.Point(-2, 6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel2.Size = New System.Drawing.Size(665, 25)
        Me.Panel2.TabIndex = 88
        '
        'CONCatalogoSectorBindingNavigator
        '
        Me.CONCatalogoSectorBindingNavigator.AddNewItem = Nothing
        Me.CONCatalogoSectorBindingNavigator.BindingSource = Me.CONCatalogoSectorBindingSource
        Me.CONCatalogoSectorBindingNavigator.CountItem = Nothing
        Me.CONCatalogoSectorBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCatalogoSectorBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONCatalogoSectorBindingNavigatorSaveItem})
        Me.CONCatalogoSectorBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCatalogoSectorBindingNavigator.MoveFirstItem = Nothing
        Me.CONCatalogoSectorBindingNavigator.MoveLastItem = Nothing
        Me.CONCatalogoSectorBindingNavigator.MoveNextItem = Nothing
        Me.CONCatalogoSectorBindingNavigator.MovePreviousItem = Nothing
        Me.CONCatalogoSectorBindingNavigator.Name = "CONCatalogoSectorBindingNavigator"
        Me.CONCatalogoSectorBindingNavigator.PositionItem = Nothing
        Me.CONCatalogoSectorBindingNavigator.Size = New System.Drawing.Size(665, 25)
        Me.CONCatalogoSectorBindingNavigator.TabIndex = 7
        Me.CONCatalogoSectorBindingNavigator.TabStop = True
        Me.CONCatalogoSectorBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(81, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONCatalogoSectorBindingNavigatorSaveItem
        '
        Me.CONCatalogoSectorBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONCatalogoSectorBindingNavigatorSaveItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCatalogoSectorBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCatalogoSectorBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCatalogoSectorBindingNavigatorSaveItem.Name = "CONCatalogoSectorBindingNavigatorSaveItem"
        Me.CONCatalogoSectorBindingNavigatorSaveItem.Size = New System.Drawing.Size(118, 22)
        Me.CONCatalogoSectorBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'CONCatalogoSectorTableAdapter
        '
        Me.CONCatalogoSectorTableAdapter.ClearBeforeFill = True
        '
        'MUESTRASECTORESTableAdapter
        '
        Me.MUESTRASECTORESTableAdapter.ClearBeforeFill = True
        '
        'FrmCatalogoSector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(667, 425)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmCatalogoSector"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nuevo Tap"
        CType(Me.CONCatalogoSectorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECOLONIA_CALLEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.MUESTRASECTORESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCASectoresBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACOLONIASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.CONCatalogoSectorBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCatalogoSectorBindingNavigator.ResumeLayout(False)
        Me.CONCatalogoSectorBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DescripTextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIAComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CIUDADComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents MUESTRACALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACALLESTableAdapter As sofTV.DataSetLidiaTableAdapters.MUESTRACALLESTableAdapter
    Friend WithEvents BUSCASectoresBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCASectoresTableAdapter As sofTV.DataSetLidiaTableAdapters.BUSCASectoresTableAdapter
    Friend WithEvents DAMECOLONIA_CALLEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECOLONIA_CALLETableAdapter As sofTV.DataSetLidiaTableAdapters.DAMECOLONIA_CALLETableAdapter
    Friend WithEvents MuestraCVECOLCIUBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraCVECOLCIUTableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraCVECOLCIUTableAdapter
    Friend WithEvents MUESTRACOLONIASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACOLONIASTableAdapter As sofTV.DataSetLidiaTableAdapters.MUESTRACOLONIASTableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents CONCatalogoSectorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCatalogoSectorTableAdapter As sofTV.DataSetLidiaTableAdapters.CONCatalogoSectorTableAdapter
    Friend WithEvents CONCatalogoSectorBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCatalogoSectorBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents SectorComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SalidaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRASECTORESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRASECTORESTableAdapter As sofTV.DataSetLidiaTableAdapters.MUESTRASECTORESTableAdapter

   
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

    End Sub

    Private Sub FrmCatalogoSector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    Friend WithEvents SectorTextBox As System.Windows.Forms.TextBox
End Class
